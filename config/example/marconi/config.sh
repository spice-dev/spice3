#!/bin/bash

# Sets up the correct environment for compilation

# Should load these modules
# 1) profile/base
# 2) intel/pe-xe-2017--binary
# 3) lapack/3.6.1--intel--pe-xe-2017--binary
# 4) blas/3.6.0--intel--pe-xe-2017--binary
# 5) intelmpi/2017--binary

module purge
module load intel/pe-xe-2017--binary
module load lapack/3.6.1--intel--pe-xe-2017--binary
module load blas/3.6.0--intel--pe-xe-2017--binary
module load intelmpi/2017--binary

# your installation path
# use your real path, this is just a placeholder
export SPICE3_INSTALL_DIR=/marconi/home/userexternal/user/installation/directory

# compilers
# should be compatible with BLAS/LAPACK libraries you are going to use
export SPICE3_CC=icc
export SPICE3_FC=ifort
export SPICE3_MPIF=mpiifort
export SPICE3_CPP=icpc

# external libraries
# these variables are used in files like makefile.inc etc.
# blas and lapack are needed for umfpack which is needed for solver
export SPICE3_F2C="$SPICE3_INSTALL_DIR/lib/f2c/f2c/libf2c.a"
export SPICE3_BLAS="-L$BLAS_LIB -lblas $SPICE3_F2C"
export SPICE3_LAPACK="-L$LAPACK_LIB -llapack"

# external libraries for solver Python compilation scripts
# used in solver library
export PY_SPICE3_BLAS_DIR=$BLAS_LIB
export PY_SPICE3_BLAS_LIBS="blas ifcore"

echo "SPICE3 paths loaded"
