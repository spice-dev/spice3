#!/bin/bash

source ../../config/config.sh

cd matio-1.3.4
make clean

export FC=$SPICE3_FC
export F77=$SPICE3_FC

# configure matio library with the fortran interface

./configure --enable-fortran
make

