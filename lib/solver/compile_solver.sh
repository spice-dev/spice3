#!/bin/bash

source ../../config/config.sh

mkdir -p lib
python build.py

set -o verbose
CPP=$SPICE3_CPP
UFDIR=$SPICE3_INSTALL_DIR/libs/UF

rm wrapper/*.o

$CPP -C -g -O3 -xSSE4.2 -qopenmp -g -Wall -I./include -I$UFDIR/UFconfig -I$UFDIR/AMD/Include -I$UFDIR/UMFPACK/Include wrapper/wrapper.c -c -o wrapper/wrapper.o 
$CPP -C -g -O3 -xSSE4.2 -qopenmp -g -Wall -I./include -I$UFDIR/UFconfig -I$UFDIR/AMD/Include -I$UFDIR/UMFPACK/Include wrapper/cusewrapper.c -c -o wrapper/cusewrapper.o

