#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import subprocess

PathUFLibs = os.environ['SPICE3_INSTALL_DIR'] + '/lib/UF'
PathBlas = os.environ['PY_SPICE3_BLAS_DIR']

PathPrefixOutput     = '.'
PathBuild            = PathPrefixOutput + '/build/'
PathLib              = PathPrefixOutput + '/lib/'
PathBin              = PathPrefixOutput + '/bin/'
PathPrefixSrc        = './src'
PathSrcLib           = PathPrefixSrc + '/lib/'
PathSrcBin           = PathPrefixSrc + '/examples/'
PathInclude          = './include/'
NameLib              = 'poissolver'
NameLibFull          = 'lib' + NameLib + '.a'

CXX                  = 'icpc'
AR                   = 'ar'
RANLIB               = 'ranlib'
#CXXFLAGS             = ['-C', '-g', '-fopenmp', '-O3', '-axCORE-AVX2', '-xSSE4.2', '-qopt-report1', '-qopt-report-phase=vec', '-openmp']#['-fast', '-openmp']
#CXXFLAGS             = ['-fopenmp', '-O3', '-openmp']#['-fast', '-openmp']
CXXFLAGS             = ['-C', '-g', '-fopenmp', '-O3', '-xSSE4.2', '-qopenmp']#['-fast', '-openmp']
INCLUDEPATHS         = [PathInclude, PathUFLibs + '/AMD/Include/', PathUFLibs + '/UMFPACK/Include/', PathUFLibs + '/UFconfig']
LIBPATHS             = [PathLib, PathUFLibs + '/AMD/Lib', PathUFLibs + '/UMFPACK/Lib', PathBlas]#, '/usr/lib']
#20101122LIBS                 = [NameLib, 'm', 'umfpack', 'amd', 'goto', 'pthread' ]
LIBS                 = [NameLib, 'm', 'umfpack', 'amd', 'pthread' ]
LIBS.extend(os.environ['PY_SPICE3_BLAS_LIBS'].split(' '))

#TODO - test availability of CXX and all the libraries


#build the library - first object files
listLibFiles = []
listAllFiles = os.listdir(PathSrcLib)
listAllFiles.sort()
for filename in listAllFiles:
	if filename[-4:] == ".cpp":
		#print filename
		listLibFiles.append(filename)

listObjFiles = []
for srcfile in listLibFiles:
	objfile = srcfile[:-4] + ".o"
	listObjFiles.append(objfile)
	#print srcfile, objfile
	cmd = CXX + " -c -o " + PathBuild + objfile + " "
	for cxxflag in CXXFLAGS:
		cmd += " " + cxxflag
	cmd += " " + PathSrcLib + srcfile
	for includepath in INCLUDEPATHS:
		cmd += " -I" + includepath
	print cmd
	subprocess.call([cmd], shell=True)
	
	#/usr/bin/ar rc default/src/lib/libpoissolver.a default/src/lib/poissolver_1.o default/src/lib/poissolver_utils_1.o default/src/lib/poissolver_umfpack_1.o default/src/lib/poissolver_multigrid_1.o && /usr/bin/ranlib  default/src/lib/libpoissolver.a
#link the library
cmd = AR + " rc " + PathLib + NameLibFull
for objfile in listObjFiles:
	cmd += " " + PathBuild + objfile
print cmd
subprocess.call([cmd], shell=True)

cmd = RANLIB + " " + PathLib + NameLibFull
print cmd
subprocess.call([cmd], shell=True)

#build the examples - first the object files
listExampleFiles = []
listAllFiles = os.listdir(PathSrcBin)
listAllFiles.sort()
for filename in listAllFiles:
	if filename[-4:] == ".cpp":
		#print filename
		listExampleFiles.append(filename)
		
#link each example and create a binary
#/usr/bin/g++ default/src/examples/sphere3d_14.o -o default/src/examples/sphere3d -Ldefault/src/lib -L/home/AE/lib/debug/ -Wl,-Bstatic -lpoissolver -Wl,-Bdynamic -lm -lumfpack -lamd -lgoto -lpthread
for file in listExampleFiles:
	binfile = file[:-4]
	cmd = CXX + " " + PathSrcBin + file + " -o " + PathBin + binfile
	for cxxflag in CXXFLAGS:
		cmd += " " + cxxflag
	for includepath in INCLUDEPATHS:
		cmd += " -I" + includepath
	for libpath in LIBPATHS:
		cmd += " -L" + libpath
	#cmd += " -Wl,-Bstatic "
	for lib in LIBS:
		cmd += " -l" + lib
		
	print cmd
	subprocess.call([cmd], shell=True)
