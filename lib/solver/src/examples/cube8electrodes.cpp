/***************************************************************************
*   Copyright (C) 2005 by Zdenek Pekarek                                  *
*   zdenek.pekarek@mff.cuni.cz                                            *
***************************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "poissolver_umfpack.h"
#include "poissolver_multigrid.h"

int main( int argc, char *argv[] )
{
  //define grid dimensions
  int nx = 64 +1;//208-104-52-26
  int ny = nx/2+1;
  int nz = nx;///2 +1;
  //allocate memory for density and potential, create the alias pointers
  std::vector<double> RHS(nx*ny*nz, 0.0);
  std::vector<double> LHS1(nx*ny*nz, 0.0);
//  std::vector<double> LHS2(nx*ny*nz, 0.0);
//  std::vector<double> DIFF(nx*ny*nz, 0.0);
  double *pRHS = &(RHS[0]);
  double *pLHS1 = &(LHS1[0]);
//  double *pLHS2 = &(LHS2[0]);
  //double *pDIFF = &(DIFF[0]);


  //THIS SECTION NEEDS TO BE DONE ONLY ONCE
  //define the type of the solver used
//  PoisSolverUMFPACK SolverD;
//  SolverD.SetDimensions(nx, ny, nz);
//  SolverD.SetPeriodic(true, true, false);
//  SolverD.SetVerbosityLevel(PoisSolver::VERBOSITY_DETAILS_EACH_STEP);

  PoisSolverMultigrid SolverMG;
  //set the grid dimensions
  SolverMG.SetDimensions(nx, ny, nz);
  SolverMG.SetPeriodic(true, true, false);

  SolverMG.SetNumLevels(3);
  SolverMG.SetResidualLimit(1e-10);

  SolverMG.SetGeometry(0.0, 0.0, 0.0, 1.0/(nx-1));

  double PosCloser = 0.1;
  double PosFurther = 0.1;

  bool OnlyUpdateElectrodes = false;

  //SolverMG.AddSphericalElectrode3DSmooth(0.0, 0.0, 0.0, 1.0, true, false);


  int NSteps = 5;
  //double RndAmplitude = 0.01;

  for (int iStep = 0; iStep < NSteps; iStep++)
  {
    double Pot1 = -3.0*cos(6.28*iStep/double(NSteps));
    double Pot2 = 8.0*cos(6.28*iStep/double(NSteps));

    SolverMG.AddSphericalElectrode3DSmooth(-0.2, -PosCloser, -0.3, 0.1, 1.2*Pot1, true, OnlyUpdateElectrodes);
    SolverMG.AddSphericalElectrode3DSmooth(-0.1, +0.7*PosCloser, -0.3, 0.1, 1.5*Pot2, true, OnlyUpdateElectrodes);
    SolverMG.AddSphericalElectrode3DSmooth(+0.2, -1.2*PosCloser, -0.3, 0.1, 0.8*Pot1, true, OnlyUpdateElectrodes);
    SolverMG.AddSphericalElectrode3DSmooth(+0.3, +PosCloser, -0.3, 0.1, 1.1*Pot2, true, OnlyUpdateElectrodes);
    SolverMG.AddSphericalElectrode3DSmooth(-0.1, -PosCloser, +0.3, 0.1, 1.1*Pot1, true, OnlyUpdateElectrodes);
    SolverMG.AddSphericalElectrode3DSmooth(-0.3, +0.5*PosCloser, +0.3, 0.1, 0.5*Pot2, true, OnlyUpdateElectrodes);
    SolverMG.AddSphericalElectrode3DSmooth(+0.35, -PosCloser, +0.3, 0.1, 0.3*Pot1, true, OnlyUpdateElectrodes);
    SolverMG.AddSphericalElectrode3DSmooth(+0.2, +PosCloser, +0.3, 0.1, 0.9*Pot2, true, OnlyUpdateElectrodes);

    OnlyUpdateElectrodes = true;

    SolverMG.Solve(pLHS1, pRHS);
    PoisSolverUtils::DumpVectorToFile(pLHS1, "Potential8", nx, ny, nz, iStep,-1,-1,10);
  }

  return EXIT_SUCCESS;
}


