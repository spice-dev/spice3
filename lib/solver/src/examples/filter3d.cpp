/***************************************************************************
*   Copyright (C) 2005 by Zdenek Pekarek                                  *
*   zdenek.pekarek@mff.cuni.cz                                            *
***************************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "poissolver_umfpack.h"
#include "poissolver_multigrid.h"

//! Creates a block electrode defined by its extents (both belong to the created block!) with a bias Potential.
//! If OnlyUpdatePotential is true, no new electrode nodes are set, only the existing are set with new bias.
void BlockElectrodeWithHole3D(PoisSolver &Solver, int nx, int ny, int z1in, int z2in, double Radius, double Potential, bool OnlyUpdatePotential = false)
{
  for (int z = z1in; z <= z2in; z++)
    Solver.AddCircularElectrode2DSmooth(0.0, 0.0, Radius, Potential, false, OnlyUpdatePotential, z);
}

int main( int argc, char *argv[] )
{
  //define grid dimensions
  int nx = 104 +1;//208-104-52-26
  int ny = nx;
  int nz = nx;///2 +1;
  //allocate memory for density and potential, create the alias pointers
  std::vector<double> RHS(nx*ny*nz, 0.0);
  std::vector<double> LHS(nx*ny*nz, 0.0);
  double *pRHS = &(RHS[0]);
  double *pLHS = &(LHS[0]);


  //THIS SECTION NEEDS TO BE DONE ONLY ONCE
  //define the type of the solver used
  PoisSolverMultigrid Solver;
  //PoisSolverUMFPACK Solver;
  //set the grid dimensions
  Solver.SetDimensions(nx, ny, nz);
  Solver.SetPeriodic(true, true, false);
  Solver.SetNumLevels(3);
  //Solver.SetPostSmooths(3);
  //print a one line of info for each solve
  //Solver.SetVerbosityLevel(PoisSolver::VERBOSITY_BRIEF_INIT);
  Solver.SetVerbosityLevel(PoisSolver::VERBOSITY_MUTE);
  //Solver.SetFileDumpLevel(PoisSolver::FILEDUMP_DETAILED);
  Solver.SetFileDumpLevel(PoisSolver::FILEDUMP_INIT_ONLY);

  double Pot = 10.0;
  double Radius = 0.3;

  Solver.SetGeometry(0.0, 0.0, 0.0, 1.0/(nx-1));
  BlockElectrodeWithHole3D(Solver, nx, ny, nx/2-5, nx/2-5, Radius, 0.0, false);
  BlockElectrodeWithHole3D(Solver, nx, ny, nx/2-4, nx/2+4, Radius, Pot, false);
  BlockElectrodeWithHole3D(Solver, nx, ny, nx/2+5, nx/2+5, Radius, 0.0, false);

  int NSteps = 1;
  double RndAmplitude = 0.0;

  for (int iStep = 0; iStep < NSteps; iStep++)
  {
    PoisSolverUtils::SetRandom(pRHS, nx, ny, nz, RndAmplitude);
    Solver.Solve(pLHS, pRHS);
    //PoisSolverUtils::DumpVectorToFile(pRHS, "DensityPIC", nx, ny, nz, iStep);
    PoisSolverUtils::DumpVectorToFile(pLHS, "PotentialPIC", nx, ny, nz, iStep);
  }

  return EXIT_SUCCESS;
}


