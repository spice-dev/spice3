/***************************************************************************
*   Copyright (C) 2005 by Zdenek Pekarek                                  *
*   zdenek.pekarek@mff.cuni.cz                                            *
***************************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "poissolver_umfpack.h"
#include "poissolver_multigrid.h"

//! Creates a block electrode defined by its extents (both belong to the created block!) with a bias Potential.
//! If OnlyUpdatePotential is true, no new electrode nodes are set, only the existing are set with new bias.
void BlockElectrode3D(PoisSolver &Solver, int x1, int x2, int y1, int y2, int z1, int z2, double Potential, bool OnlyUpdatePotential = false)
{
  printf("Adding a block electrode, x %5d - %5d, y %5d - %5d, z %5d - %5d\n", x1, x2, y1, y2, z1, z2);
  for (int z = z1; z <= z2; z++)
    for (int y = y1; y <= y2; y++)
      for (int x = x1; x <= x2; x++)
        Solver.SetElectrode(Potential, x, y, z, OnlyUpdatePotential);
}


int main( int argc, char *argv[] )
{
  //define grid dimensions
  int nx = 16 +1;//208-104-52-26
  int ny = nx;
  int nz = nx;///2 +1;
  //allocate memory for density and potential, create the alias pointers
  std::vector<double> RHS(nx*ny*nz, 0.0);
  std::vector<double> LHS(nx*ny*nz, 0.0);
  double *pRHS = &(RHS[0]);
  double *pLHS = &(LHS[0]);


  //THIS SECTION NEEDS TO BE DONE ONLY ONCE
  //define the type of the solver used
  //PoisSolverMultigrid Solver;
  PoisSolverUMFPACK Solver1;
  PoisSolverUMFPACK Solver2;
  //set the grid dimensions
  Solver1.SetDimensions(nx, ny, nz);
  Solver1.SetPeriodic(true, true, false);
  //Solver.SetNumLevels(3);

  Solver2.SetDimensions(nx, ny, nz);
  Solver2.SetPeriodic(true, true, false);

  //print a one line of info for each solve
  Solver1.SetVerbosityLevel(PoisSolver::VERBOSITY_DETAILS_EACH_STEP);
  Solver2.SetVerbosityLevel(PoisSolver::VERBOSITY_DETAILS_EACH_STEP);
  double Pot = -3.0;

  //set the "floor"
  BlockElectrode3D(Solver1, 0, nx-1, 0, ny-1, 0, 3, Pot, false);
  BlockElectrode3D(Solver2, 0, nx-1, 0, ny-1, 0, 3, Pot, false);

  PoisSolverUtils::DumpVectorToFile(pRHS, "DensityBasal", nx, ny, nz);

  PoisSolverUtils::FillDensity3D(pRHS, nx, ny, nz, 1.0, 1.0);
  Solver1.Solve(pLHS, pRHS);
  PoisSolverUtils::DumpVectorToFile(pRHS, "DensityTreated", nx, ny, nz);
  PoisSolverUtils::DumpVectorToFile(pLHS, "PotentialTreated", nx, ny, nz);

  PoisSolverUtils::FillDensity3D(pRHS, nx, ny, nz, 1.0, 1.0);
  Solver2.SetSummationPeriodicDensity(false);
  Solver2.Solve(pLHS, pRHS);
  PoisSolverUtils::DumpVectorToFile(pRHS, "DensityUntreated", nx, ny, nz);
  PoisSolverUtils::DumpVectorToFile(pLHS, "PotentialUntreated", nx, ny, nz);

  return EXIT_SUCCESS;
}


