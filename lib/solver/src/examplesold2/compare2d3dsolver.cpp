/***************************************************************************
*   Copyright (C) 2005 by Zdenek Pekarek                                  *
*   zdenek.pekarek@mff.cuni.cz                                            *
***************************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "poissolver_umfpack.h"
#include "poissolver_multigrid.h"

//! Creates a block electrode defined by its extents (both belong to the created block!) with a bias Potential.
//! If OnlyUpdatePotential is true, no new electrode nodes are set, only the existing are set with new bias.
void BlockElectrode3D(PoisSolver &Solver, int x1, int x2, int y1, int y2, int z1, int z2, double Potential, bool OnlyUpdatePotential = false)
{
  printf("Adding a block electrode, x %5d - %5d, y %5d - %5d, z %5d - %5d\n", x1, x2, y1, y2, z1, z2);
  for (int z = z1; z <= z2; z++)
    for (int y = y1; y <= y2; y++)
      for (int x = x1; x <= x2; x++)
        Solver.SetElectrode(Potential, x, y, z, OnlyUpdatePotential);
}


int main( int argc, char *argv[] )
{
  //define grid dimensions
  int nx = 20 +1;//208-104-52-26
  int ny = nx;
  int nz = nx;///2 +1;
  //allocate memory for density and potential, create the alias pointers
  std::vector<double> RHS2D(nx*ny, 0.0);
  std::vector<double> RHS3D(nx*ny*nz, 0.0);
  std::vector<double> LHS2D(nx*ny, 0.0);
  std::vector<double> LHS2DProjected(nx*ny, 0.0);
  std::vector<double> LHS2DDiff(nx*ny, 0.0);
  std::vector<double> LHS3D(nx*ny*nz, 0.0);
  double *pRHS2D = &(RHS2D[0]);
  double *pRHS3D = &(RHS3D[0]);
  double *pLHS2D = &(LHS2D[0]);
  double *pLHS3D = &(LHS3D[0]);


  //THIS SECTION NEEDS TO BE DONE ONLY ONCE
  //define the type of the solver used
  PoisSolverMultigrid Solver3D;
  PoisSolverUMFPACK Solver2D;
  //PoisSolverUMFPACK Solver3D;
  //set the grid dimensions
  Solver2D.SetDimensions(nx, ny, 1);
  Solver3D.SetDimensions(nx, ny, nz);
  Solver2D.SetGeometry(0.0, 0.0, 0.0, 1.0/(nx-1));
  Solver3D.SetGeometry(0.0, 0.0, 0.0, 1.0/(nx-1));

  Solver2D.SetPeriodic(true, false, false);
  Solver3D.SetPeriodic(true, true, false);
  //Solver2D.SetPeriodic(false, false, false);
  //Solver3D.SetPeriodic(true, false, false);

  Solver2D.SetSummationPeriodicDensity(false);
  Solver3D.SetSummationPeriodicDensity(false);

  Solver3D.SetNumLevels(2);
  Solver3D.SetPostSmooths(1);
  Solver3D.SetResidualLimit(1e-10);
  //print a one line of info for each solve
  //Solver.SetVerbosityLevel(PoisSolver::VERBOSITY_DETAILS_EACH_STEP);
  //Solver.SetFileDumpLevel(PoisSolver::FILEDUMP_DETAILED);

  //double Pot = 10.0;
  double Pot = 10.0;

  //set the "floor"
  Solver2D.AddRectangElectrodeDiscrete2D(0, nx-1, ny-1, ny-1, Pot);
  BlockElectrode3D(Solver3D, 0, nx-1, 0, ny-1, nz-1, nz-1, Pot);

  Solver2D.AddRectangElectrodeDiscrete2D(0, nx/3, 0, ny/2, 0.0);
  Solver2D.AddRectangElectrodeDiscrete2D(2*nx/3, nx-1, 0, ny/2, 0.0);

  BlockElectrode3D(Solver3D, 0, nx-1, 0, ny/3, 0, nz/2, 0.0);
  BlockElectrode3D(Solver3D, 0, nx-1, 2*ny/3, ny-1, 0, nz/2, 0.0);

  double RndAmplitude = 0.00001;
  //PoisSolverUtils::SetRandom(pRHS3D, nx, ny, nz, RndAmplitude);
//  PoisSolverUtils::Average3DTo2DOverX(RHS2D, pRHS3D, nx, ny, nz);
  //Average3DTo2DOverX(std::vector<double> &Arr2D, const double *Arr3D, int nx, int ny, int nz)
  //PoisSolverUtils::Average3DTo2DOverX(RHS2D, pRHS3D, nx, ny, nz);
  //PoisSolverUtils::Dummy();

  PoisSolverUtils::SetRandom(pRHS2D, nx, ny, 1, RndAmplitude);
  PoisSolverUtils::Distribute2DTo3DOverX(RHS3D, pRHS2D, nx, ny, nz);

  PoisSolverUtils::DumpVectorToFile(pRHS2D, "DensityCompare2D", nx, ny, 1, 0);
  PoisSolverUtils::DumpVectorToFile(pRHS3D, "DensityCompare3D", nx, ny, nz, 0);

  Solver2D.Solve(pLHS2D,pRHS2D);
  Solver3D.Solve(pLHS3D,pRHS3D);

  PoisSolverUtils::DumpVectorToFile(pLHS2D, "PotentialCompare2DIntrinsic", nx, ny, 1, 0);
  PoisSolverUtils::DumpVectorToFile(pLHS3D, "PotentialCompare3D", nx, ny, nz, 0);
//  for (int i= 0; i< nx*ny; i++)
//    LHS2D[i] = 0.0;

  PoisSolverUtils::Average3DTo2DOverX(LHS2DProjected,pLHS3D, nx, ny, nz);
  PoisSolverUtils::DumpVectorToFile(&(LHS2DProjected[0]), "PotentialCompare2DConverted", nx, ny, 1, 0);

  for (int i= 0; i< nx*ny; i++)
    LHS2DDiff[i] = LHS2D[i] - LHS2DProjected[i];

  PoisSolverUtils::DumpVectorToFile(&(LHS2DDiff[0]), "PotentialCompare2DDiff", nx, ny, 1, 0);

/*
  int NSteps = 1;
  double RndAmplitude = 0.0;

  for (int iStep = 0; iStep < NSteps; iStep++)
  {
    PoisSolverUtils::SetRandom(pRHS, nx, ny, nz, RndAmplitude);
    Solver.Solve(pLHS, pRHS);
    //PoisSolverUtils::DumpVectorToFile(pRHS, "DensityPIC", nx, ny, nz, iStep);
    PoisSolverUtils::DumpVectorToFile(pLHS, "PotentialPIC", nx, ny, nz, iStep);
  }
*/
  return EXIT_SUCCESS;
}


