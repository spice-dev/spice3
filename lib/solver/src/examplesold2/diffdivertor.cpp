/***************************************************************************
*   Copyright (C) 2005 by Zdenek Pekarek                                  *
*   zdenek.pekarek@mff.cuni.cz                                            *
***************************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "poissolver_umfpack.h"
#include "poissolver_multigrid.h"

void SetSolver(PoisSolver *pSolver, int nx, int ny)
{
  pSolver->SetDimensions(nx, ny, 1);
  pSolver->SetPeriodic(true, false, false);

  pSolver->AddRectangElectrodeDiscrete2D(0, nx/3, 0, ny/3, 1.0);
  pSolver->AddRectangElectrodeDiscrete2D(2*nx/3, nx, 0, ny/3, 1.0);
  pSolver->AddRectangElectrodeDiscrete2D(0, nx, 0, 0, 1.0);
}


int main( int argc, char *argv[] )
{
  //define grid dimensions
  int nx = 800 +1;
  int ny = nx/2+1;
  //allocate memory for density and potential, create the alias pointers
  std::vector<double> RHS(nx*ny, 0.0);
  std::vector<double> LHS1(nx*ny, 0.0);
  std::vector<double> LHS2(nx*ny, 0.0);
  std::vector<double> DIFF(nx*ny, 0.0);
  std::vector<double> INT1(nx*ny, 0.0);
  std::vector<double> INT2(nx*ny, 0.0);
  double *pRHS = &(RHS[0]);
  double *pLHS1 = &(LHS1[0]);
  double *pLHS2 = &(LHS2[0]);
  double *pDIFF = &(DIFF[0]);
  double *pINT1 = &(INT1[0]);
  double *pINT2 = &(INT2[0]);

  PoisSolverMultigrid MGSolver;
  SetSolver(&MGSolver, nx, ny);
  MGSolver.Solve(pLHS1, pRHS);

  PoisSolverUMFPACK DirectSolver;
  //DirectSolver.SetFileDumpLevel(PoisSolver::FILEDUMP_DETAILED);
  SetSolver(&DirectSolver, nx, ny);
  DirectSolver.Solve(pLHS2, pRHS);

  //store the potentials and their diff
  PoisSolverUtils::DumpVectorToFile(pLHS1, "PotentialDiffDivertorMG", nx, ny, 1);
  PoisSolverUtils::DumpVectorToFile(pLHS2, "PotentialDiffDivertorDirect", nx, ny, 1);

  for (int i = 0; i< nx*ny; i++)
    DIFF[i] = LHS1[i]-LHS2[i];
  PoisSolverUtils::DumpVectorToFile(pDIFF, "PotentialDiffDivertor", nx, ny, 1);

  //compute and store the X dir intensities and their diff
  PoisSolverUtils::ComputeIntensity(pINT1, pLHS1, 0, nx, ny, 1);
  PoisSolverUtils::ComputeIntensity(pINT2, pLHS2, 0, nx, ny, 1);
  PoisSolverUtils::DumpVectorToFile(pINT1, "IntensityXDiffDivertorMG", nx, ny, 1);
  PoisSolverUtils::DumpVectorToFile(pINT2, "IntensityXDiffDivertorDirect", nx, ny, 1);

  for (int i = 0; i< nx*ny; i++)
    DIFF[i] = INT1[i]-INT2[i];
  PoisSolverUtils::DumpVectorToFile(pDIFF, "IntensityXDiffDivertor", nx, ny, 1);

  //compute and store the Y dir intensities and their diff
  PoisSolverUtils::ComputeIntensity(pINT1, pLHS1, 1, nx, ny, 1);
  PoisSolverUtils::ComputeIntensity(pINT2, pLHS2, 1, nx, ny, 1);
  PoisSolverUtils::DumpVectorToFile(pINT1, "IntensityYDiffDivertorMG", nx, ny, 1);
  PoisSolverUtils::DumpVectorToFile(pINT2, "IntensityYDiffDivertorDirect", nx, ny, 1);

  for (int i = 0; i< nx*ny; i++)
    DIFF[i] = INT1[i]-INT2[i];
  PoisSolverUtils::DumpVectorToFile(pDIFF, "IntensityYDiffDivertor", nx, ny, 1);

  return EXIT_SUCCESS;
}


