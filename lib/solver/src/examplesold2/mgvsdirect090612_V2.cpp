/***************************************************************************
*   Copyright (C) 2005 by Zdenek Pekarek                                  *
*   zdenek.pekarek@mff.cuni.cz                                            *
***************************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "poissolver_umfpack.h"
#include "poissolver_multigrid.h"

#include <cmath>

//! Creates a block electrode defined by its extents (both belong to the created block!) with a bias Potential.
//! If OnlyUpdatePotential is true, no new electrode nodes are set, only the existing are set with new bias.
void BlockElectrode3D(PoisSolver &Solver, int x1, int x2, int y1, int y2, int z1, int z2, double Potential, bool OnlyUpdatePotential = false)
{
  printf("Adding a block electrode, x %5d - %5d, y %5d - %5d, z %5d - %5d\n", x1, x2, y1, y2, z1, z2);
  for (int z = z1; z <= z2; z++)
    for (int y = y1; y <= y2; y++)
      for (int x = x1; x <= x2; x++)
        Solver.SetElectrode(Potential, x, y, z, OnlyUpdatePotential);
}


int main( int argc, char *argv[] )
{
  //define grid dimensions
  int nx = 16 +1;//208-104-52-26
  int ny = 64+1;
  int nz = 64+1;///2 +1;
  //allocate memory for density and potential, create the alias pointers
  std::vector<double> RHS(nx*ny*nz, 0.0);
  std::vector<double> LHS1(nx*ny*nz, 0.0);
  std::vector<double> LHS2(nx*ny*nz, 0.0);
  std::vector<double> DIFF(nx*ny*nz, 0.0);
  std::vector<double> LHSDotcom(nx*ny*nz, 0.0);
  double *pRHS = &(RHS[0]);
  double *pLHS1 = &(LHS1[0]);
  double *pLHS2 = &(LHS2[0]);
  double *pDIFF = &(DIFF[0]);

  int MagnitudeResidual = 13;
  double ResidualLimit = std::pow(10.0,-MagnitudeResidual);

  int limity1 = 22;
  int limity2 = 42;
  int limitz = 20;

  double Pot = -3.0;
  int iStep = 0;

  size_t nxOUT, nyOUT, nzOUT;
  //!!!called again later
  std::string Filename("../Density-DirectDotcom090615.mtx");
  PoisSolverUtils::FillFromFile(RHS, nxOUT, nyOUT, nzOUT, Filename);
  std::string Filename2("../Potential-DirectDotcom090615.mtx");
  PoisSolverUtils::FillFromFile(LHSDotcom, nxOUT, nyOUT, nzOUT, Filename2);

  PoisSolverUtils::DumpVectorToFile(pRHS, "DensityPIC", nx, ny, nz, iStep,-1,-1,MagnitudeResidual+2);

  //THIS SECTION NEEDS TO BE DONE ONLY ONCE
  //define the type of the solver used
  {
    PoisSolverUMFPACK SolverD;
    SolverD.SetDimensions(nx, ny, nz);
    SolverD.SetPeriodic(true, true, false);
    //SolverD.SetVerbosityLevel(PoisSolver::VERBOSITY_DETAILS_EACH_STEP);
    //SolverD.SetFileDumpLevel(PoisSolver::FILEDUMP_DETAILED);
    SolverD.SetSummationPeriodicDensity(false);

    BlockElectrode3D(SolverD, 0, nx-1, 0, limity1, 0, limitz, Pot, false);
    BlockElectrode3D(SolverD, 0, nx-1, limity2, ny-1, 0, limitz, Pot, false);
    //set the "floor"
    BlockElectrode3D(SolverD, 0, nx-1, 0, ny-1, 0, 0, Pot, false);

    SolverD.Solve(pLHS2, pRHS);
  }

  RHS.clear();
  RHS.resize(nx*ny*nz, 0.0);
  pRHS = &(RHS[0]);
  //called already before, needed to get clean result
  PoisSolverUtils::FillFromFile(RHS, nxOUT, nyOUT, nzOUT, Filename);

  {
    PoisSolverMultigrid SolverMG;
    //PoisSolverUMFPACK SolverMG;
    printf("MG residual limit %e\n", ResidualLimit);
    SolverMG.SetPostSmooths(20);
    SolverMG.SetResidualLimit(ResidualLimit);
    SolverMG.SetSummationPeriodicDensity(false);

    //set the grid dimensions
    SolverMG.SetDimensions(nx, ny, nz);
    SolverMG.SetPeriodic(true, true, false);

    //print a one line of info for each solve
  //  SolverMG.SetVerbosityLevel(PoisSolver::VERBOSITY_DETAILS_EACH_STEP);
  //  SolverMG.SetVerbosityLevel(PoisSolver::VERBOSITY_BRIEF_INIT);
    SolverMG.SetFileDumpLevel(PoisSolver::FILEDUMP_DETAILED);

    //SolverMG.SetNumLevels(3);

    BlockElectrode3D(SolverMG, 0, nx-1, 0, limity1, 0, limitz, Pot, false);
    BlockElectrode3D(SolverMG, 0, nx-1, limity2, ny-1, 0, limitz, Pot, false);
    //set the "floor"
    BlockElectrode3D(SolverMG, 0, nx-1, 0, ny-1, 0, 0, Pot, false);

    SolverMG.Solve(pLHS1, pRHS);
  }

    PoisSolverUtils::DumpVectorToFile(pLHS1, "PotentialPIC-MG", nx, ny, nz, iStep,-1,-1,MagnitudeResidual+2);


    PoisSolverUtils::DumpVectorToFile(pLHS2, "PotentialPIC-Direct", nx, ny, nz, iStep,-1,-1,MagnitudeResidual+2);
    //PoisSolverUtils::DumpVectorToFile(pRHS, "DensityPIC", nx, ny, nz, iStep);


    double ABSDIFF = 0.0;
    for (int i = 0; i < nx*ny*nz; i++)
    {
      pDIFF[i] = pLHS1[i] - pLHS2[i];
      if (pDIFF[i] > ABSDIFF)
        ABSDIFF = pDIFF[i];
    }
    PoisSolverUtils::DumpVectorToFile(pDIFF, "PotentialDiffMG-Direct", nx, ny, nz, iStep,-1,-1,MagnitudeResidual+2, true);
    printf("MG-Direct maxdiff %e\n", ABSDIFF);

    ABSDIFF = 0.0;
    for (int i = 0; i < nx*ny*nz; i++)
    {
      pDIFF[i] = LHSDotcom[i] - pLHS1[i];
      if (pDIFF[i] > ABSDIFF)
        ABSDIFF = pDIFF[i];
    }
    PoisSolverUtils::DumpVectorToFile(pDIFF, "PotentialDiffDotcom-MG", nx, ny, nz, iStep,-1,-1,MagnitudeResidual+2, true);
    printf("Dotcom-MG maxdiff %e\n", ABSDIFF);

    ABSDIFF = 0.0;
    for (int i = 0; i < nx*ny*nz; i++)
    {
      pDIFF[i] = LHSDotcom[i] - pLHS2[i];
      if (pDIFF[i] > ABSDIFF)
        ABSDIFF = pDIFF[i];
    }
    PoisSolverUtils::DumpVectorToFile(pDIFF, "PotentialDiffDotcom-Direct", nx, ny, nz, iStep,-1,-1,MagnitudeResidual+2, true);
    printf("Dotcom-Direct maxdiff %e\n", ABSDIFF);



  return EXIT_SUCCESS;
}


