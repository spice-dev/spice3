/***************************************************************************
*   Copyright (C) 2005 by Zdenek Pekarek                                  *
*   zdenek.pekarek@mff.cuni.cz                                            *
***************************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "poissolver_umfpack.h"
#include "poissolver_multigrid.h"

void AddHyperElectrode(PoisSolver *Solver, double CoefX, double CoefY, double Const, int nx, int ny, double Potential)
{
  double stepx = 1.0/(nx-1);
  double stepy = 1.0/(ny-1);
  for(int j = 0; j < ny; j++)
  {
    double posy = -0.5+ j*stepy;
    for(int i = 0; i < nx; i++)
    {
      double posx = -0.5+ i*stepx;
      if ((CoefX*posx*posx + CoefY*posy*posy) >= Const)
        Solver->SetElectrode(Potential, i, j, 0, false);
    }
  }
}


int main( int argc, char *argv[] )
{
  //define grid dimensions
  int nx = 800 +1;
  int ny = nx;
  //allocate memory for density and potential, create the alias pointers
  std::vector<double> RHS(nx*ny, 0.0);
  std::vector<double> LHS(nx*ny, 0.0);
  std::vector<double> LHS2(nx*ny, 0.0);
  double *pRHS = &(RHS[0]);
  double *pLHS = &(LHS[0]);
  double *pLHS2 = &(LHS2[0]);

  //THIS SECTION NEEDS TO BE DONE ONLY ONCE
  //define the type of the solver used
  PoisSolverMultigrid Solver;
  //PoisSolverUMFPACK Solver;
  //set the grid dimensions
  Solver.SetDimensions(nx, ny, 1);
  //set the spatial coordinates used for electrode definition
  Solver.SetGeometry(0.0, 0.0, 0.0, 1.0/(nx-1));
  //print a one line of info for each solve
  //Solver.SetFileDumpLevel(PoisSolver::FILEDUMP_DETAILED);
  Solver.SetVerbosityLevel(PoisSolver::VERBOSITY_ONELINER_EACH_SOLVE);
  //define the electrodes
  double Hyperoffset = 0.03;
  AddHyperElectrode(&Solver, 1.0, -1.0, Hyperoffset, nx, ny, 10.0);
  AddHyperElectrode(&Solver, -1.0, 1.0, Hyperoffset, nx, ny, -10.0);
  //THIS IS REPEATED EVERY PIC STEP
  Solver.Solve(pLHS, pRHS);
  //store the resulting potential
  PoisSolverUtils::DumpVectorToFile(pLHS, "PotentialHyper", nx, ny, 1);


  PoisSolverMultigrid Solver4Circles;
  //PoisSolverUMFPACK Solver;
  //set the grid dimensions
  Solver4Circles.SetDimensions(nx, ny, 1);
  //set the spatial coordinates used for electrode definition
  Solver4Circles.SetGeometry(0.0, 0.0, 0.0, 1.0/(nx-1));

  double Radius = 0.5;
  double Offset = 0.2 + Hyperoffset + Radius;
  Solver4Circles.AddCircularElectrode2D( Offset, 0.0, Radius, 10.0, true, false);
  Solver4Circles.AddCircularElectrode2D(-Offset, 0.0, Radius, 10.0, true, false);
  Solver4Circles.AddCircularElectrode2D(0.0,  Offset, Radius,-10.0, true, false);
  Solver4Circles.AddCircularElectrode2D(0.0, -Offset, Radius,-10.0, true, false);
  Solver4Circles.Solve(pLHS2, pRHS);
  PoisSolverUtils::DumpVectorToFile(pLHS2, "PotentialHyper4Circles", nx, ny, 1);



  return EXIT_SUCCESS;
}


