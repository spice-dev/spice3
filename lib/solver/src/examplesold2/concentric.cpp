/***************************************************************************
*   Copyright (C) 2008 by Zdenek Pekarek                                   *
*   zdenek.pekarek@gmail.com                                               *
***************************************************************************/

#include "poissolver_umfpack.h"
#include "poissolver_multigrid.h"

int main()
{
  int nx = 400 +1;
  int ny = nx;
  int ntotal = nx*ny;

  double *LeftSide  = new double[ntotal];
  double *RightSide = new double[ntotal];

  for (int i = 0; i< ntotal; i++)
    LeftSide[i] = 0.0;
  for (int i = 0; i< ntotal; i++)
    RightSide[i] = 0.0;

  PoisSolverMultigrid Solver;

  Solver.SetDimensions(nx, ny, 1);
  Solver.SetNumLevels(3);
  Solver.SetGeometry(0.0, 0.0, 0.0, 1.0/(nx-1));
  Solver.AddCircularElectrode2D(0.0, 0.0, 0.2, 10.0, true, false);
  Solver.AddCircularElectrode2D(0.0, 0.0, 0.5, 0.0, false, false);

  Solver.SetVerbosityLevel(PoisSolver::VERBOSITY_ONELINER_EACH_SOLVE);
  Solver.SetVerbosityLevel(PoisSolver::VERBOSITY_DETAILS_EACH_STEP);
  Solver.Solve(LeftSide, RightSide);
  PoisSolverUtils::DumpVectorToFile(LeftSide, "Concentric", nx, ny, 1);



  delete[] LeftSide;
  delete[] RightSide;
  return 0;
}
