/***************************************************************************
*   Copyright (C) 2005 by Zdenek Pekarek                                  *
*   zdenek.pekarek@mff.cuni.cz                                            *
***************************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "poissolver_umfpack.h"
#include "poissolver_multigrid.h"

int main( int argc, char *argv[] )
{
  //define grid dimensions
  int nx = 320 +1;
  int ny = nx;
  //allocate memory for density and potential, create the alias pointers
  std::vector<double> RHS(nx*ny, 0.0);
  std::vector<double> LHS(nx*ny, 0.0);
  double *pRHS = &(RHS[0]);
  double *pLHS = &(LHS[0]);


  //THIS SECTION NEEDS TO BE DONE ONLY ONCE
  //define the type of the solver used
  PoisSolverMultigrid MGSolver;
  //set the grid dimensions
  MGSolver.SetDimensions(nx, ny, 1);
  MGSolver.SetNumLevels(4);
  MGSolver.SetPostSmooths(1);
  //set the spatial coordinates used for electrode definition
  MGSolver.SetGeometry(0.0, 0.0, 0.0, 1.0/(nx-1));
  //print a one line of info for each solve
  MGSolver.SetVerbosityLevel(PoisSolver::VERBOSITY_DETAILS_EACH_STEP);
  //MGSolver.SetFileDumpLevel(PoisSolver::FILEDUMP_DETAILED);
  //define the electrodes
  //one small circular in the centre
  MGSolver.AddCircularElectrode2D(0.0, 0.0, 0.03, 10.0, true, false);

  //THIS IS REPEATED EVERY PIC STEP
  MGSolver.Solve(pLHS, pRHS);
  //store the resulting potential
  PoisSolverUtils::DumpVectorToFile(pLHS, "PotentialSimplest", nx, ny, 1);

  return EXIT_SUCCESS;
}


