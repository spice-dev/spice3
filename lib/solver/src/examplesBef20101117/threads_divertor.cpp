/***************************************************************************
*   Copyright (C) 2005 by Zdenek Pekarek                                  *
*   zdenek.pekarek@mff.cuni.cz                                            *
***************************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "poissolver_umfpack.h"
#include "poissolver_multigrid.h"

//! Creates a block electrode defined by its extents (both belong to the created block!) with a bias Potential.
//! If OnlyUpdatePotential is true, no new electrode nodes are set, only the existing are set with new bias.
void BlockElectrode3D(PoisSolver &Solver, int x1, int x2, int y1, int y2, int z1, int z2, double Potential, bool OnlyUpdatePotential = false)
{
  printf("Adding a block electrode, x %5d - %5d, y %5d - %5d, z %5d - %5d\n", x1, x2, y1, y2, z1, z2);
  for (int z = z1; z <= z2; z++)
    for (int y = y1; y <= y2; y++)
      for (int x = x1; x <= x2; x++)
        Solver.SetElectrode(Potential, x, y, z, OnlyUpdatePotential);
}


int main( int argc, char *argv[] )
{
  //define grid dimensions
  const int nx = 64 +1;
  const int ny = nx;
  const int nz = nx/2 +1;
  //allocate memory for density and potential, create the alias pointers
  std::vector<double> RHS(nx*ny*nz, 0.0);
  std::vector<double> LHS1(nx*ny*nz, 0.0);
  //std::vector<double> LHS2(nx*ny*nz, 0.0);
  double *pRHS = &(RHS[0]);
  double *pLHS1 = &(LHS1[0]);
  //double *pLHS2 = &(LHS2[0]);


  //THIS SECTION NEEDS TO BE DONE ONLY ONCE
  //define the type of the solver used
  PoisSolverMultigrid Solver1;
  //PoisSolverMultigrid Solver2;
  //set the grid dimensions
  Solver1.SetDimensions(nx, ny, nz);
  Solver1.SetPeriodic(true, true, false);
  //Solver2.SetPeriodic(true, true, false);
  Solver1.SetNumLevels(3);
  //Solver2.SetNumLevels(2);

  //set the floor and ceiling potentials for both solvers

  int limitx1 = nx*2/5 -1;
  int limitx2 = nx+1 - limitx1;
  int limity1 = ny*2/5 -1;
  int limity2 = ny+1 - limity1;
  int limitz = nz/3;

  double Pot = 10.0;

  //BlockElectrode3D(Solver, 0, nx-1, 0, ny-1, 0, 3, Pot, false);

  BlockElectrode3D(Solver1, 0, limitx1, 0, limity1, 0, limitz, Pot, false);
  BlockElectrode3D(Solver1, limitx2, nx-1, 0, limity1, 0, limitz, Pot, false);
  BlockElectrode3D(Solver1, 0, limitx1, limity2, ny-1, 0, limitz, Pot, false);
  BlockElectrode3D(Solver1, limitx2, nx-1, limity2, ny-1, 0, limitz, Pot, false);

  //set the "floor"
  BlockElectrode3D(Solver1, 0, nx-1, 0, ny-1, 0, 0, Pot, false);

  Solver1.FinishEditing();
  Solver1.SetNumThreads(2);

  for (int iLoop=0; iLoop < 10; iLoop++)
  {
    //fill the density with random values (properly tapered off at edge cells of the plasma)
    PoisSolverUtils::SetRandom(pRHS, nx, ny, nz, 1.0);

    //THIS IS REPEATED EVERY PIC STEP
    //solve with multigrids

    Solver1.Solve(pLHS1, pRHS);
  }
  PoisSolverUtils::DumpVectorToFile(pLHS1, "PerXY3D-Solve1", nx, ny, nz);

  printf("abort!\n");
  abort();
  /*
  //store the density, resulting potentials and their diff
  PoisSolverUtils::DumpVectorToFile(pRHS, "PerXY3D-Density", nx, ny, nz);

  Solver2.FinishEditing();
  Solver2.Solve(pLHS2, pRHS);
  PoisSolverUtils::DumpVectorToFile(pLHS2, "PerXY3D-Solve2", nx, ny, nz);
  //recycle the RHS vector to compute the difference
  for (int i = 0; i<nx*ny*nz; i++)
    pRHS[i] = pLHS1[i] - pLHS2[i];

  PoisSolverUtils::DumpVectorToFile(pRHS, "PerXY3D-Diff", nx, ny, nz);

  //actually what would matter is the difference in field intensities, which should be much lower
*/
  return EXIT_SUCCESS;
}


