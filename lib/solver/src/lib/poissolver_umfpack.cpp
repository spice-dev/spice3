#include <iostream>
#include <stdlib.h>
#include "umfpack.h"
#include "poissolver_umfpack.h"

PoisSolverUMFPACK::PoisSolverUMFPACK()
{
  umfpack_numeric_ = NULL;
  umfpack_wi_ = NULL;
  umfpack_w_ = NULL;
  umfpack_control_ = NULL;
  umfpack_info_ = NULL;
};

PoisSolverUMFPACK::~PoisSolverUMFPACK()
{
  DestroyDynalloc();
};

void PoisSolverUMFPACK::DestroyDynalloc()
{
  umfpack_dl_free_numeric(&umfpack_numeric_);
  delete[] umfpack_wi_;
  delete[] umfpack_w_;
  delete[] umfpack_control_;
  delete[] umfpack_info_;

  umfpack_numeric_ = NULL;
  umfpack_wi_ = NULL;
  umfpack_w_ = NULL;
  umfpack_control_ = NULL;
  umfpack_info_ = NULL;
};

bool PoisSolverUMFPACK::ConstructDecomposition()
{
  bool Answ = true;
  try
  {
    void *Symbolic = NULL;
    //DestroyInternals();
    //test if already constructed
    PoisSolverUtils::GridPack &GP = grid_pack_[0];

    int ntotal = GP.GetNTotal();
    std::cout << "Preparing LU data structures...\n";
    umfpack_control_ = new double[UMFPACK_CONTROL];
    umfpack_info_ = new double[UMFPACK_INFO];
    umfpack_dl_defaults(umfpack_control_);//set the default controls for UMFPACK
        //changle only those which are of interest
    umfpack_control_[UMFPACK_PRL] = 3.0;//printout detail level
    umfpack_control_[UMFPACK_IRSTEP] = 0.0;//max. #iters to improve convergence
    if (level_verbosity_ >= VERBOSITY_VERBOSE_INIT)
      std::cout << "Running umfpack symbolic...\n";
    //umfpack_dl_symbolic (ntotal, ntotal, &(xa[0]), &(asub[0]), &(a[0]), &Symbolic, umfpack_control_, umfpack_info_) ;
    umfpack_dl_symbolic (ntotal, ntotal, xa_, asub_, a_, &Symbolic, umfpack_control_, umfpack_info_) ;
    //umfpack_dl_symbolic (5, 5, &(xa[0]), &(asub[0]), &(a[0]), &Symbolic, Control, Info) ;
    umfpack_dl_report_info (umfpack_control_, umfpack_info_) ;
    if (level_verbosity_ >= VERBOSITY_VERBOSE_INIT)
      std::cout << "Running umfpack numeric...\n";
    int status_numeric = umfpack_dl_numeric (xa_, asub_, a_, Symbolic, &umfpack_numeric_, umfpack_control_, umfpack_info_) ;
    if (level_verbosity_ >= VERBOSITY_VERBOSE_INIT)
      umfpack_dl_report_info (umfpack_control_, umfpack_info_) ;

    if (level_verbosity_ >= VERBOSITY_VERBOSE_INIT)
      std::cout << "Status of numeric...\n";
    if (level_verbosity_ >= VERBOSITY_VERBOSE_INIT)
      umfpack_dl_report_status( umfpack_control_, status_numeric );

    if (level_verbosity_ >= VERBOSITY_VERBOSE_INIT)
      std::cout << "Freeing symbolic object\n";
    umfpack_dl_free_symbolic(&Symbolic);

    //allocate additional memory to be reused in solver
    umfpack_wi_ = new long[ntotal];
    umfpack_w_ = new double[5*ntotal];

    if (level_verbosity_ >= VERBOSITY_VERBOSE_INIT)
      std::cout << "End of UMFPACK init\n";
  }
  catch (...)
  {
    Answ = false;
    //DecompositionUp2Date = false;
  }
    //!!!base return on success
  return Answ;
}

bool PoisSolverUMFPACK::FinishEditing()
{
  bool Answ = true;
  if (status_ != STATUS_READY_FOR_SOLVING)
  {
    DestroyDynalloc();
    Answ = PoisSolverDirect::FinishEditing();
    if (Answ)
      Answ = ConstructDecomposition();

    if (Answ)
      status_ = STATUS_READY_FOR_SOLVING;
    else
      status_ = STATUS_ERROR;

    //!!!doesnt get carried from MG solver to the coarsest one if (filedump_flag_ >= FILEDUMP_DETAILED)
//    if (level_filedump_ >= FILEDUMP_DETAILED)
//    {
//      PoisSolverUtils::DumpVectorToFile(grid_pack_[0].GetEquipotential(), "Equipotential_UMFPACK", grid_pack_[0]);
//      PoisSolverUtils::DumpVectorToFile(grid_pack_[0].GetNextToElectrode(), "NextToElectrode_UMFPACK", grid_pack_[0]);
//      PoisSolverUtils::DumpVectorToFile(grid_pack_[0].GetBitmap(), "Bitmap_UMFPACK", grid_pack_[0]);
//    }

  }
  return Answ;
}

void PoisSolverUMFPACK::Solve(PoisSolverUtils::outerfloat_t *_Potential, PoisSolverUtils::outerfloat_t *_Density)
{
  //!!!test if already constructed
  PoisSolverUtils::GridPack &GP = grid_pack_[0];
/*
  if (!FullyReadyForSolve)
  {
    ConstructSparseMatrix();
    ConstructDecomposition();
    FullyReadyForSolve = true;
  }
*/
  if (status_ != STATUS_READY_FOR_SOLVING)
    FinishEditing();

  PreprocessDensity(_Density, SummationPeriodicDensity);

  double time_start = PoisSolverUtils::GetWallclockTime();

  GP.OverwriteWithElectrodes(_Density);
  umfpack_dl_wsolve(UMFPACK_At ,xa_, asub_, a_, _Potential, _Density, umfpack_numeric_, umfpack_control_, umfpack_info_, umfpack_wi_, umfpack_w_);
  double time_stop = PoisSolverUtils::GetWallclockTime();

  if (level_verbosity_ >= VERBOSITY_ONELINER_EACH_SOLVE)
    printf("Solve took %f seconds\n", time_stop - time_start);
}

void PoisSolverUMFPACK::Reset()
{
  grid_pack_.clear();
  num_levels_ = 0;

  umfpack_di_free_numeric(&umfpack_numeric_);

  delete[] umfpack_wi_;
  delete[] umfpack_w_;
  delete[] umfpack_control_;
  delete[] umfpack_info_;
}
