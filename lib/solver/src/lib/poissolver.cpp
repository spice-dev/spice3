#include <map>
#include "poissolver.h"
#include "umfpack.h"

//temp
#include <iostream>

PoisSolver::PoisSolver()
{
  num_levels_ = 0;
  posx_[0] = posx_[1] = 0.0;
  posy_[0] = posy_[1] = 0.0;
  posz_[0] = posz_[1] = 0.0;

  status_ = STATUS_CONSTRUCTED;
  level_filedump_ = FILEDUMP_NONE;
  //level_verbosity_ = VERBOSITY_VERBOSE_INIT;
  level_verbosity_ = VERBOSITY_MUTE;

  SummationPeriodicDensity = true;
  //IsUsedAsCoarsestSolver = false;
}

PoisSolver::~PoisSolver()
{
//  if (!IsUsedAsCoarsestSolver)
//    for(size_t i=0; i< grid_pack_.size(); i++)
//      delete grid_pack_[i];
}

void PoisSolver::SetSummationPeriodicDensity(bool Summation)
{
  SummationPeriodicDensity = Summation;
}

//overload for multigrids
bool PoisSolver::SetDimensions(int nx, int ny, int nz)
{
  bool Answ = true;

  if (nx <= 1|| ny <= 1)
    Answ = false;

  if (((nx %2) != 1) || ((ny %2) != 1) || ((nz %2) != 1))
    Answ = false;

  if (status_ != STATUS_CONSTRUCTED)
    Answ = false;

  if (Answ)
  {
    if (nz > 1)
      is_3d_ = true;
    else
      is_3d_ = false;

    status_ = STATUS_DIMENSIONS_SET;
    posx_[0] = -1.0*(nx/2);
    posx_[1] =  1.0*(nx/2);
    posy_[0] = -1.0*(ny/2);
    posy_[1] =  1.0*(ny/2);
    posz_[0] = -1.0*(nz/2);
    posz_[1] =  1.0*(nz/2);

    grid_pack_.clear();//better clear - dealloc!!!
    //PoisSolverUtils::GridPack *GP = new PoisSolverUtils::GridPack;
    //grid_pack_.push_back(GP);
    PoisSolverUtils::GridPack NewGP(level_verbosity_);
    grid_pack_.push_back(NewGP);
    PoisSolverUtils::GridPack &GP = grid_pack_.back();
    Answ = GP.AllocateAndInit(nx, ny, nz);

    GP.UnsetPeriodicInDirection(0, 0.0, 0.0);
    GP.UnsetPeriodicInDirection(1, 0.0, 0.0);
    GP.UnsetPeriodicInDirection(2, 0.0, 0.0);
  }

  if (!Answ)
    status_ = STATUS_ERROR;

  return Answ;
}

bool PoisSolver::SetGeometry(double centerx, double centery, double centerz, double cellstep)
{
  bool Answ = true;

  if (status_ != STATUS_DIMENSIONS_SET)
    Answ = false;

  if (Answ)
  {
    int nx = grid_pack_[0].GetNX();
    int ny = grid_pack_[0].GetNY();
    int nz = grid_pack_[0].GetNZ();

    posx_[0] = centerx - (nx/2)*cellstep;
    posx_[1] = centerx + (nx/2)*cellstep;
    posy_[0] = centery - (ny/2)*cellstep;
    posy_[1] = centery + (ny/2)*cellstep;
    posz_[0] = centerz - (nz/2)*cellstep;
    posz_[1] = centerz + (nz/2)*cellstep;
  }

  if (!Answ)
    status_ = STATUS_ERROR;

  return Answ;
}

bool PoisSolver::SetElectrode(double Potential, int ix, int iy, int iz, bool OnlyUpdatePotential)
{
  bool Answ = false;
  if (status_ >= STATUS_DIMENSIONS_SET)
  {
    PoisSolverUtils::GridPack &GP = grid_pack_[0];
    int nx = GP.GetNX();
    int ny = GP.GetNY();
    int nz = GP.GetNZ();
    if (ix >= 0 && ix < nx && iy >= 0 && iy < ny && iz >= 0 && iz < nz)
    {
      if (OnlyUpdatePotential)
      {
        GP.SetEquipotentialAtNode(Potential, ix, iy, iz);
      }
      else
      {
        GP.SetElectrodeNode(Potential, ix, iy, iz);
        status_ = STATUS_DIMENSIONS_SET;
      }
      Answ = true;
    }

  }
  return Answ;
}

bool PoisSolver::UnsetElectrode(int ix, int iy, int iz)
{
  bool Answ = false;
  if (status_ >= STATUS_DIMENSIONS_SET)
  {
    PoisSolverUtils::GridPack &GP = grid_pack_[0];
    int nx = GP.GetNX();
    int ny = GP.GetNY();
    int nz = GP.GetNZ();
    if (ix >= 0 && ix < nx && iy >= 0 && iy < ny && iz >= 0 && iz < nz)
    {
      GP.UnsetElectrodeNode(ix, iy, iz);
      Answ = true;
    }
    status_ = STATUS_DIMENSIONS_SET;
  }
  return Answ;
}

bool PoisSolver::SetNextToElectrode(int ix, int iy, int iz, const PoisSolverUtils::Stencil& stencil)
{
  bool Answ = true;
  if (status_ >= STATUS_DIMENSIONS_SET)
  {
    PoisSolverUtils::GridPack &GP = grid_pack_[0];
    int nx = GP.GetNX();
    int ny = GP.GetNY();
    int nz = GP.GetNZ();
    if (ix >= 0 && ix < nx && iy >= 0 && iy < ny && iz >= 0 && iz < nz)
    {
      //bool Is3D = (nz > 1) ? true : false;
      const int baseidx = ix + iy*nx + iz*nx*ny;
      GP.SetNextToElectrode(baseidx, stencil);
      Answ = true;
    }
    status_ = STATUS_DIMENSIONS_SET;
  }
  return Answ;
}

bool PoisSolver::AddRectangElectrodeDiscrete2D(int IdxXLow, int IdxXHigh, int IdxYLow, int IdxYHigh, double Potential)
{
  bool Answ = false;
  if (status_ >= STATUS_DIMENSIONS_SET)
  {
    PoisSolverUtils::GridPack &GP = grid_pack_[0];
    int nx = GP.GetNX();
    int ny = GP.GetNY();
    for (int i = 0; i< nx; i++)
    {
      for (int j = 0; j< ny; j++)
      {
        if ((i >= IdxXLow) && (i <= IdxXHigh) && (j >= IdxYLow) && (j <= IdxYHigh))
        {
          //the node is inside the circle
          GP.SetElectrodeNode(Potential, i, j);
          Answ = true;
        }
      }
    }

//    if (level_filedump_ >= FILEDUMP_INIT_ONLY)
//    {
//      PoisSolverUtils::DumpVectorToFile(GP.GetBitmap(), "AfterRectangular-Bitmap", GP);
//      PoisSolverUtils::DumpVectorToFile(GP.GetEquipotential(), "AfterRectangular-Potential", GP);
//    }

    status_ = STATUS_DIMENSIONS_SET;
  }
  return Answ;
}

bool PoisSolver::AddCircularElectrode2D(double centerx, double centery, double radius, double potential, bool ElectrodeIsInside, bool OnlyUpdatePotential)
{
  bool Answ = false;
  if (status_ >= STATUS_DIMENSIONS_SET)
  {
    PoisSolverUtils::GridPack &GP = grid_pack_[0];
    int nx = GP.GetNX();
    int ny = GP.GetNY();
    double stepx = (posx_[1] - posx_[0])/(double(nx)-1.0);
    double stepy = (posy_[1] - posy_[0])/(double(ny)-1.0);
    for (int i = 0; i< nx; i++)
    {
      double nodeposx = posx_[0] + stepx*i;
      for (int j = 0; j< ny; j++)
      {
        bool IsNodeInsideTheShape = false;
        double nodeposy = posy_[0] + stepy*j;
        if ((PoisSolverUtils::SQR(nodeposx - centerx)
            + PoisSolverUtils::SQR(nodeposy - centery))
            <= PoisSolverUtils::SQR(radius))
        {
          IsNodeInsideTheShape = true;
        }

        if (IsNodeInsideTheShape == ElectrodeIsInside)
        {
          //the node is inside the circle
          if (OnlyUpdatePotential)
            GP.SetEquipotentialAtNode(potential, i, j);
          else
            GP.SetElectrodeNode(potential, i, j);

          Answ = true;
        }
      }
    }

//    if (level_filedump_ >= FILEDUMP_INIT_ONLY)
//    {
//      PoisSolverUtils::DumpVectorToFile(GP.GetBitmap(), "AfterCircular-Bitmap", GP);
//      PoisSolverUtils::DumpVectorToFile(GP.GetEquipotential(), "AfterCircular-Potential", GP);
//    }
    if (Answ && !OnlyUpdatePotential)
      status_ = STATUS_DIMENSIONS_SET;
  }
  return Answ;
}

bool PoisSolver::AddCircularElectrode2DSmooth(double centerx, double centery, double radius, double potential, bool ElectrodeIsInside, bool OnlyUpdatePotential, int OffsetZFor3D)
{
  bool Answ = false;
  if (status_ >= STATUS_DIMENSIONS_SET)
  {
    PoisSolverUtils::GridPack &GP = grid_pack_[0];
    int nx = GP.GetNX();
    int ny = GP.GetNY();
    double stepx = (posx_[1] - posx_[0])/(double(nx)-1.0);
    double stepy = (posy_[1] - posy_[0])/(double(ny)-1.0);
    double radiusouter = radius + std::max(stepx, stepy);
    double radiusinner = radius - std::max(stepx, stepy);
    for (int j = 0; j< ny; j++)
    {
      double nodeposy = posy_[0] + stepy*j;

      for (int i = 0; i< nx; i++)
      {
        bool IsNodeInsideTheShape = false;
        bool IsNextToElectrode = false;
        double nodeposx = posx_[0] + stepx*i;
        double DistFromCenter = PoisSolverUtils::SQR(nodeposx - centerx) + PoisSolverUtils::SQR(nodeposy - centery);
        if (DistFromCenter <= PoisSolverUtils::SQR(radius))
        {
          IsNodeInsideTheShape = true;
        }
        else
        {
          if (ElectrodeIsInside)
          {
            if (DistFromCenter <= PoisSolverUtils::SQR(radiusouter))
              IsNextToElectrode = true;
          }
          else
          {
            if (DistFromCenter >= PoisSolverUtils::SQR(radiusinner))
              IsNextToElectrode = true;
          }
        }

        if (IsNodeInsideTheShape == ElectrodeIsInside)
        {
          //the node is inside the circle
          if (OnlyUpdatePotential)
            GP.SetEquipotentialAtNode(potential, i, j, OffsetZFor3D);
          else
            GP.SetElectrodeNode(potential, i, j, OffsetZFor3D);

          Answ = true;
        }

        if (IsNextToElectrode)
        {
          const int baseidx = i + j*nx + OffsetZFor3D*nx*ny;
          PoisSolverUtils::innerfloat_t coefs[6];
          for (int i6 = 0; i6<6; i6++)
            coefs[i6] = 1.0;

          //sqrtx is defined by y and vice versa
          double sqrtx = sqrt(PoisSolverUtils::SQR(radius) - PoisSolverUtils::SQR(nodeposy-centery));
          double sqrty = sqrt(PoisSolverUtils::SQR(radius) - PoisSolverUtils::SQR(nodeposx-centerx));
          double intersectionxleft = centerx - sqrtx;
          double intersectionxright = centerx + sqrtx;
          double intersectionyleft = centery - sqrty;
          double intersectionyright = centery + sqrty;

          if (ElectrodeIsInside)
          {
          	//!!!works only for internal electrodes
            coefs[0] = nodeposx - intersectionxright;
            coefs[1] = intersectionxleft - nodeposx;//center == 0
            coefs[2] = nodeposy - intersectionyright;
            coefs[3] = intersectionyleft - nodeposy;
          }
          else
          {
          	//!!!works only for external electrodes
            coefs[0] = nodeposx - intersectionxleft;
            coefs[1] = intersectionxright - nodeposx;
            coefs[2] = nodeposy - intersectionyleft;
            coefs[3] = intersectionyright - nodeposy;
          }

          //!!!assumes stepx == stepy
          for (int i6 = 0; i6<6; i6++)
          {
            if (coefs[i6] <= 0.0 || coefs[i6] > stepx)
              coefs[i6] = 1.0;
            else
              coefs[i6] /= stepx;

            //take care of NaN (if there's no intersection)
            if(coefs[i6] != coefs[i6])
              coefs[i6] = 1.0;
          }


          //printf("baseidx %5d: stepx %e\t%e\t%e\t%e\t%e\n", baseidx, stepx, coefs[0], coefs[1], coefs[2], coefs[3]);

          bool Is3D = (OffsetZFor3D > 0) ? true : false;
          PoisSolverUtils::Stencil stencil(Is3D);
          stencil.Set(Is3D, coefs);
          //printf("electrode idx %5d ", baseidx);
          //stencil.PrintStencil(Is3D);
          //printf("baseidx %5d: stencil lrbtc %e\t%e\t%e\t%e\t%e\n", baseidx, stencil.GetXLeft(), stencil.GetXRight(), stencil.GetYLeft(), stencil.GetYRight(), stencil.GetCenter());
//          GP.SetNextToElectrode(baseidx, stencil);
          SetNextToElectrode(i,j,0, stencil);
        }
      }
    }

    if (Answ && !OnlyUpdatePotential)
      status_ = STATUS_DIMENSIONS_SET;
  }
  return Answ;
}

bool PoisSolver::AddTubularElectrode3DXZSmooth(double centerx, double centerz, double radius, double potential, bool OnlyUpdatePotential, int LayerYFor3D)
{
  bool ElectrodeIsInside = true;//at the moment it doesn't make sense to consider a hollow cylinder with its axis parallel with the y-axis

  bool Answ = false;
   if (status_ >= STATUS_DIMENSIONS_SET)
   {
     PoisSolverUtils::GridPack &GP = grid_pack_[0];
     int nx = GP.GetNX();
     int ny = GP.GetNY();
     int nz = GP.GetNZ();

     double stepx = (posx_[1] - posx_[0])/(double(nx)-1.0);
     double stepy = (posy_[1] - posy_[0])/(double(ny)-1.0);
     double stepz = (posz_[1] - posz_[0])/(double(nz)-1.0);
     double radiusouter = radius + std::max(std::max(stepx, stepy), stepz);
     double radiusinner = radius - std::max(std::max(stepx, stepy), stepz);

     for (int k = 0; k< nz; k++)
     {
       //double nodeposy = posy_[0] + stepy*j;
       double nodeposz = posz_[0] + stepz*k;

       for (int i = 0; i< nx; i++)
       {
         bool IsNodeInsideTheShape = false;
         bool IsNextToElectrode = false;
         double nodeposx = posx_[0] + stepx*i;
         //double DistFromCenter = PoisSolverUtils::SQR(nodeposx - centerx) + PoisSolverUtils::SQR(nodeposy - centery);
         double DistFromCenter = PoisSolverUtils::SQR(nodeposx - centerx) + PoisSolverUtils::SQR(nodeposz - centerz);
         if (DistFromCenter <= PoisSolverUtils::SQR(radius))
         {
           IsNodeInsideTheShape = true;
         }
         else
         {
           if (ElectrodeIsInside)
           {
             if (DistFromCenter <= PoisSolverUtils::SQR(radiusouter))
               IsNextToElectrode = true;
           }
           else
           {
             if (DistFromCenter >= PoisSolverUtils::SQR(radiusinner))
               IsNextToElectrode = true;
           }
         }

         if (IsNodeInsideTheShape == ElectrodeIsInside)
         {
           //the node is inside the circle
           if (OnlyUpdatePotential)
             GP.SetEquipotentialAtNode(potential, i, LayerYFor3D, k);
           else
             GP.SetElectrodeNode(potential, i, LayerYFor3D, k);

           Answ = true;
         }

         if (IsNextToElectrode)
         {
           //const int baseidx = i + j*nx + OffsetZFor3D*nx*ny;
           const int baseidx = i + LayerYFor3D*nx + k*nx*ny;
           PoisSolverUtils::innerfloat_t coefs[6];
           for (int i6 = 0; i6<6; i6++)
             coefs[i6] = 1.0;

           //sqrtx is defined by y and vice versa
           //double sqrtx = sqrt(PoisSolverUtils::SQR(radius) - PoisSolverUtils::SQR(nodeposy-centery));
           //double sqrty = sqrt(PoisSolverUtils::SQR(radius) - PoisSolverUtils::SQR(nodeposx-centerx));
           double sqrtx = sqrt(PoisSolverUtils::SQR(radius) - PoisSolverUtils::SQR(nodeposz-centerz));
           double sqrtz = sqrt(PoisSolverUtils::SQR(radius) - PoisSolverUtils::SQR(nodeposx-centerx));
//           double intersectionxleft = centerx - sqrtx;
//           double intersectionxright = centerx + sqrtx;
//           double intersectionyleft = centery - sqrty;
//           double intersectionyright = centery + sqrty;
           double intersectionxleft = centerx - sqrtx;
           double intersectionxright = centerx + sqrtx;
           double intersectionzleft = centerz - sqrtz;
           double intersectionzright = centerz + sqrtz;

           if (ElectrodeIsInside)
           {
             //!!!works only for internal electrodes
             coefs[0] = nodeposx - intersectionxright;
             coefs[1] = intersectionxleft - nodeposx;//center == 0
             coefs[4] = nodeposz - intersectionzright;
             coefs[5] = intersectionzleft - nodeposz;
           }
           else
           {
             //!!!works only for external electrodes
             coefs[0] = nodeposx - intersectionxleft;
             coefs[1] = intersectionxright - nodeposx;
             coefs[4] = nodeposz - intersectionzleft;
             coefs[5] = intersectionzright - nodeposz;
           }

           //!!!assumes stepx == stepy
           for (int i6 = 0; i6<6; i6++)
           {
             if (coefs[i6] <= 0.0 || coefs[i6] > stepx)
               coefs[i6] = 1.0;
             else
               coefs[i6] /= stepx;

             //take care of NaN (if there's no intersection)
             if(coefs[i6] != coefs[i6])
               coefs[i6] = 1.0;
           }


           //printf("baseidx %5d: stepx %e\t%e\t%e\t%e\t%e\n", baseidx, stepx, coefs[0], coefs[1], coefs[2], coefs[3]);

           bool Is3D = true;
           PoisSolverUtils::Stencil stencil(Is3D);
           stencil.Set(Is3D, coefs);
           //printf("electrode idx %5d ", baseidx);
           //stencil.PrintStencil(Is3D);
           //printf("baseidx %5d: stencil lrbtc %e\t%e\t%e\t%e\t%e\n", baseidx, stencil.GetXLeft(), stencil.GetXRight(), stencil.GetYLeft(), stencil.GetYRight(), stencil.GetCenter());
           GP.SetNextToElectrode(baseidx, stencil);
         }
       }
     }

     if (Answ && !OnlyUpdatePotential)
       status_ = STATUS_DIMENSIONS_SET;
   }
   return Answ;




}



bool PoisSolver::AddSphericalElectrode3D(double centerx, double centery, double centerz, double radius, double potential, bool ElectrodeIsInside, bool OnlyUpdatePotential)
{
  bool Answ = false;
  if (status_ >= STATUS_DIMENSIONS_SET)
  {
    PoisSolverUtils::GridPack &GP = grid_pack_[0];
    int nx = GP.GetNX();
    int ny = GP.GetNY();
    int nz = GP.GetNZ();

    if (nz <= 1)
      return false;

    double stepx = (posx_[1] - posx_[0])/(nx-1);
    double stepy = (posy_[1] - posy_[0])/(ny-1);
    double stepz = (posz_[1] - posz_[0])/(nz-1);


    radius *= 1.001;

    for (int k = 0; k< nz; k++)
    {
      double nodeposz = posz_[0] + stepz*k;
      for (int j = 0; j< ny; j++)
      {
        double nodeposy = posy_[0] + stepy*j;
        for (int i = 0; i< nx; i++)
        {
          bool IsNodeInsideTheShape = false;
          double nodeposx = posx_[0] + stepx*i;
          if ((PoisSolverUtils::SQR(nodeposx - centerx)
              + PoisSolverUtils::SQR(nodeposy - centery)
              + PoisSolverUtils::SQR(nodeposz - centerz))
             <= PoisSolverUtils::SQR(radius))
          {
            IsNodeInsideTheShape = true;
          }

          if (IsNodeInsideTheShape == ElectrodeIsInside)
          {
            //the node is inside the circle
            if (OnlyUpdatePotential)
              GP.SetEquipotentialAtNode(potential, i, j, k);
            else
              GP.SetElectrodeNode(potential, i, j, k);

            Answ = true;
          }
        }
      }
    }

//    if (level_filedump_ >= FILEDUMP_INIT_ONLY)
//    {
//      PoisSolverUtils::DumpVectorToFile(GP.GetBitmap(), "AfterCircular-Bitmap", GP);
//      PoisSolverUtils::DumpVectorToFile(GP.GetEquipotential(), "AfterCircular-Potential", GP);
//    }
    if (Answ && !OnlyUpdatePotential)
      status_ = STATUS_DIMENSIONS_SET;
  }
  return Answ;
}

bool PoisSolver::AddSphericalElectrode3DSmooth(double centerx, double centery, double centerz, double radius, double potential, bool ElectrodeIsInside, bool OnlyUpdatePotential)
{
  //AddSphericalElectrode3D(centerx, centery, centerz, radius, potential, ElectrodeIsInside, OnlyUpdatePotential);
  bool Answ = false;
  if (status_ >= STATUS_DIMENSIONS_SET)
  {
    PoisSolverUtils::GridPack &GP = grid_pack_[0];
    int nx = GP.GetNX();
    int ny = GP.GetNY();
    int nz = GP.GetNZ();

    if (nz <= 1)
      return false;

    radius *= 1.001;

    double stepx = (posx_[1] - posx_[0])/(nx-1);
    double stepy = (posy_[1] - posy_[0])/(ny-1);
    double stepz = (posz_[1] - posz_[0])/(nz-1);

    double radiusouter = radius + std::max(std::max(stepx, stepy), stepz);

    for (int k = 0; k< nz; k++)
    {
      double nodeposz = posz_[0] + stepz*k;
      for (int j = 0; j< ny; j++)
      {
        double nodeposy = posy_[0] + stepy*j;
        for (int i = 0; i< nx; i++)
        {
          double nodeposx = posx_[0] + stepx*i;

          bool IsNodeInsideTheShape = false;
          bool IsNextToElectrode = false;

          double DistFromCenter = PoisSolverUtils::SQR(nodeposx - centerx)
                                + PoisSolverUtils::SQR(nodeposy - centery)
                                + PoisSolverUtils::SQR(nodeposz - centerz);
          if (DistFromCenter <= PoisSolverUtils::SQR(radius))
          {
            IsNodeInsideTheShape = true;
          }
          else
            if (DistFromCenter <= PoisSolverUtils::SQR(radiusouter))
              IsNextToElectrode = true;

          if (IsNodeInsideTheShape == ElectrodeIsInside)
          {
            //the node is inside the circle
            if (OnlyUpdatePotential)
              GP.SetEquipotentialAtNode(potential, i, j, k);
            else
              GP.SetElectrodeNode(potential, i, j, k);

            Answ = true;
          }

          if (IsNextToElectrode)
          {
            const int baseidx = i + j*nx + k*nx*ny;
            PoisSolverUtils::innerfloat_t coefs[6];
            for (int i6 = 0; i6<6; i6++)
              coefs[i6] = 1.0;

            //sqrtx is defined by y and vice versa
            //!!!z
             double sqrtx = sqrt(PoisSolverUtils::SQR(radius) - PoisSolverUtils::SQR(nodeposy-centery) - PoisSolverUtils::SQR(nodeposz-centerz));
             double sqrty = sqrt(PoisSolverUtils::SQR(radius) - PoisSolverUtils::SQR(nodeposx-centerx) - PoisSolverUtils::SQR(nodeposz-centerz));
             double sqrtz = sqrt(PoisSolverUtils::SQR(radius) - PoisSolverUtils::SQR(nodeposx-centerx) - PoisSolverUtils::SQR(nodeposy-centery));
             //might need to subtract nodeposx for x etc
             double intersectionxleft = centerx - sqrtx;
             double intersectionxright = centerx + sqrtx;
             double intersectionyleft = centery - sqrty;
             double intersectionyright = centery + sqrty;
             double intersectionzleft = centerz - sqrtz;
             double intersectionzright = centerz + sqrtz;

             //!!!works only for internal electrodes
             coefs[0] = nodeposx - intersectionxright;
             coefs[1] = intersectionxleft - nodeposx;
             coefs[2] = nodeposy - intersectionyright;
             coefs[3] = intersectionyleft - nodeposy;
             coefs[4] = nodeposz - intersectionzright;
             coefs[5] = intersectionzleft - nodeposz;

             //!!!assumes stepx == stepy == stepz
             for (int i6 = 0; i6<6; i6++)
             {
               if (coefs[i6] <= 0.0 || coefs[i6] > stepx)
                 coefs[i6] = 1.0;
               else
                 coefs[i6] /= stepx;

               //take care of NaN (if there's no intersection)
               if(coefs[i6] != coefs[i6])
                 coefs[i6] = 1.0;
              }


              //printf("baseidx %5d: stepx %e\t%e\t%e\t%e\t%e\n", baseidx, stepx, coefs[0], coefs[1], coefs[2], coefs[3]);
              PoisSolverUtils::Stencil stencil(true);
              stencil.Set(true, coefs);
              printf("electrode idx %5d ", baseidx);
              stencil.PrintStencil(true);
              //printf("baseidx %5d: stencil lrbtc %e\t%e\t%e\t%e\t%e\n", baseidx, stencil.GetXLeft(), stencil.GetXRight(), stencil.GetYLeft(), stencil.GetYRight(), stencil.GetCenter());
              GP.SetNextToElectrode(baseidx, stencil);
            }
          }
       }
    }

    if (Answ && !OnlyUpdatePotential)
      status_ = STATUS_DIMENSIONS_SET;
  }
  return Answ;
}


bool PoisSolver::AddRectangElectrodeDiscrete3D(int IdxXLow, int IdxXHigh, int IdxYLow, int IdxYHigh, int IdxZLow, int IdxZHigh, double Potential)
{
  bool Answ = false;
  if (status_ >= STATUS_DIMENSIONS_SET)
  {
    PoisSolverUtils::GridPack &GP = grid_pack_[0];
    int nx = GP.GetNX();
    int ny = GP.GetNY();
    int nz = GP.GetNZ();
    for (int k = 0; k< nz; k++)
    {
      for (int j = 0; j< ny; j++)
      {
        for (int i = 0; i< nx; i++)
        {
          if ((i >= IdxXLow) && (i <= IdxXHigh) && (j >= IdxYLow) && (j <= IdxYHigh) && (k >= IdxZLow) && (k <= IdxZHigh))
          {
            //the node is inside the circle
            GP.SetElectrodeNode(Potential, i, j, k);
            Answ = true;
          }
        }
      }
    }

    if (level_verbosity_ >= VERBOSITY_VERBOSE_INIT)
    {
      printf("RectangElectrode3D: (%5d-%5d)x(%5d-%5d)x(%5d-%5d)\n", IdxXLow, IdxXHigh, IdxYLow, IdxYHigh, IdxZLow, IdxZHigh);
    }

//    if (level_filedump_ >= FILEDUMP_INIT_ONLY)
//    {
//      PoisSolverUtils::DumpVectorToFile(GP.GetBitmap(), "AfterRectangular-Bitmap", GP);
//      PoisSolverUtils::DumpVectorToFile(GP.GetEquipotential(), "AfterRectangular-Potential", GP);
//    }
    status_ = STATUS_DIMENSIONS_SET;
  }
  return Answ;

}

void PoisSolver::SetPeriodic(bool DirX, bool DirY, bool DirZ)
{
  if (DirX)
    grid_pack_[0].SetPeriodicInDirection(0);
  else
    grid_pack_[0].UnsetPeriodicInDirection(0, 0.0, 0.0);

  if (DirY)
      grid_pack_[0].SetPeriodicInDirection(1);
    else
      grid_pack_[0].UnsetPeriodicInDirection(1, 0.0, 0.0);

//  if (DirX && DirY)
//	grid_pack_[0].SetPeriodicCornersXY();

  if (DirZ)
      grid_pack_[0].SetPeriodicInDirection(2);
    else
      grid_pack_[0].UnsetPeriodicInDirection(2, 0.0, 0.0);
}

bool PoisSolver::AcceptGridPack(PoisSolverUtils::GridPack &GP)
{
  grid_pack_.clear();

  PoisSolverUtils::GridPack NewGP(level_verbosity_);
  grid_pack_.push_back(NewGP);
  grid_pack_[0].Copy(GP);

  if (GP.GetNZ() > 1)
    is_3d_ = true;
  else
    is_3d_ = false;
  FinishEditing();
  return true;
}; ///!!!move to protected

bool PoisSolver::AddTriangularElectrode2D(double Vrtxs[6], double Potential, bool OnlyUpdatePotential)
{
    bool Answ = false;
    if (status_ >= STATUS_DIMENSIONS_SET)
    {
      PoisSolverUtils::GridPack &GP = grid_pack_[0];
      int nx = GP.GetNX();
      int ny = GP.GetNY();
      double stepx = (posx_[1] - posx_[0])/(double(nx)-1.0);
      double stepy = (posy_[1] - posy_[0])/(double(ny)-1.0);

      for (int i = 0; i< nx; i++)
      {
        double nodeposx = posx_[0] + stepx*i;
        for (int j = 0; j< ny; j++)
        {
          double nodeposy = posy_[0] + stepy*j;

          double d;
          bool IsInside = true;

          for(int iFirst=0; iFirst<6; iFirst+=2)
          {
            int iSecond = iFirst+2;
            if (iSecond>=6)
              iSecond = 0;
            d = (Vrtxs[iSecond] - Vrtxs[iFirst])*(nodeposy - Vrtxs[iFirst+1]) -
                (Vrtxs[iSecond+1] - Vrtxs[iFirst+1])*(nodeposx - Vrtxs[iFirst]);

            if (d < 0.0)
            {
              IsInside = false;
              break;
            }
          }

          if (IsInside)
          {
            //the node is inside the circle
            if (OnlyUpdatePotential)
              GP.SetEquipotentialAtNode(Potential, i, j);
            else
              GP.SetElectrodeNode(Potential, i, j);

            Answ = true;
          }
        }
      }

//      if (level_filedump_ >= FILEDUMP_INIT_ONLY)
//      {
//        PoisSolverUtils::DumpVectorToFile(GP.GetBitmap(), "AfterTriangular-Bitmap", GP);
//        PoisSolverUtils::DumpVectorToFile(GP.GetEquipotential(), "AfterTriangular-Potential", GP);
//      }
      if (Answ && !OnlyUpdatePotential)
        status_ = STATUS_DIMENSIONS_SET;
    }
    return Answ;

}

bool PoisSolver::AddPolygonElectrode2D(double CenterX, double CenterY, double Radius, const std::vector<double> &Potentials, double Insulator, bool OnlyUpdatePotential)
{
  bool Answ = true;

  int NSegments = (int) Potentials.size();
  if (NSegments < 3)
    return false;

  double InsulatorAngle = asin(Insulator/(2.0*Radius));
  double SegmentAngle = 4*asin(1.0)/NSegments - InsulatorAngle;
  double Vrtxs[6];

  for (int iSeg=0; iSeg < NSegments; iSeg++)
  {
    double Angle1 = InsulatorAngle/2.0 + iSeg*(InsulatorAngle + SegmentAngle);
    double Angle2 = Angle1 + SegmentAngle;

    Vrtxs[0] = CenterX + 0.5*Insulator*cos(0.5*(Angle1+Angle2));
    Vrtxs[1] = CenterY + 0.5*Insulator*sin(0.5*(Angle1+Angle2));
    Vrtxs[2] = CenterX + Radius*cos(Angle1);
    Vrtxs[3] = CenterY + Radius*sin(Angle1);
    Vrtxs[4] = CenterX + Radius*cos(Angle2);
    Vrtxs[5] = CenterY + Radius*sin(Angle2);

    bool Succ = AddTriangularElectrode2D(Vrtxs, Potentials[iSeg], OnlyUpdatePotential);
    if (!Succ)
      Answ = false;
  }

  return Answ;
}

void PoisSolver::PreprocessDensity(PoisSolverUtils::outerfloat_t *Density, bool SumPeriodicDensity) const
{
  const int nx = grid_pack_[0].GetNX();
  const int ny = grid_pack_[0].GetNY();
  const int nz = grid_pack_[0].GetNZ();

  if (nz <= 1)
  {
    //2D case
    //periodic in x
    for(int j = 0; j < ny; j++)
    {
      const int baseidx = j*nx;
      if (SumPeriodicDensity)
        Density[baseidx] += Density[baseidx + nx-1];

      Density[baseidx + nx-1] = 0.0;
    }
    //periodic in y
    for(int i = 0; i < nx; i++)
    {
      const int baseidx = i;
      if (SumPeriodicDensity)
        Density[baseidx] += Density[baseidx + nx*(ny-1)];

      Density[baseidx + nx*(ny-1)] = 0.0;
    }
  }
  else
  {
    const int planez = nx*ny;
    //3D case
    //periodic in x
    for(int k = 0; k < nz; k++)
     for(int j = 0; j < ny; j++)
      {
        const int baseidx = j*nx + k*planez;
        if (SumPeriodicDensity)
          Density[baseidx] += Density[baseidx + nx-1];

        Density[baseidx + nx-1] = 0.0;
      }
    //periodic in y
    for(int k = 0; k < nz; k++)
      for(int i = 0; i < nx; i++)
      {
        const int baseidx = i + k*planez;
        if (SumPeriodicDensity)
          Density[baseidx] += Density[baseidx + nx*(ny-1)];

        Density[baseidx + nx*(ny-1)] = 0.0;
      }

    //!!!periodic in z isn't implemented!
  }
}

/////////////////////////////////// PoisSolverDirect ////////////////////////////////////////

PoisSolverDirect::PoisSolverDirect()
{
  a_ = NULL;
  asub_ = NULL;
  xa_ = NULL;
}
PoisSolverDirect::~PoisSolverDirect()
{
  delete[] a_;
  delete[] asub_;
  delete[] xa_;
}

bool PoisSolverDirect::FinishEditing()
{
  bool Answ = false;
  if (status_ != STATUS_READY_FOR_SOLVING)
  {
    Answ = true;
    if (status_ == STATUS_DIMENSIONS_SET)
    {
      Answ = ConstructSparseMatrix();
    }
  }
  return Answ;
}

bool PoisSolverDirect::ConstructSparseMatrix()
{
  bool Answ = false;
    Answ = true;
    PoisSolverUtils::GridPack &GP = grid_pack_[0];
    if (a_)
      delete[] a_;
    if (xa_)
      delete[] xa_;
    if (asub_)
      delete[] asub_;

    std::vector<double> a;
    std::vector<PoisSolverUtils::idx_t> asub;
    std::vector<PoisSolverUtils::idx_t> xa;

    std::map<int, double> NonZeroValues;
    int num_nonzeros_curr_line = 0;

    size_t nx = GP.GetNX();
    size_t ny = GP.GetNY();
    size_t nz = GP.GetNZ();
    size_t TopLimitOnNNonZeroValues = 7*nx*ny*nz;
    size_t NLines = nx*ny*nz;

    try
    {
      a.reserve(TopLimitOnNNonZeroValues);
      asub.reserve(TopLimitOnNNonZeroValues);
      xa.reserve(NLines+1);

      //first row has no offset
      PoisSolverUtils::idx_t xaoffset = 0;
      xa.push_back(xaoffset);

      if (nz <= 1)
      {
        //2D case
        PoisSolverUtils::Stencil stencil(false);
        for (size_t j = 0; j < ny; j++)
        {
          for (size_t i = 0; i < nx; i++)
          {
            const size_t baseidx = i + j*nx;
            NonZeroValues.clear();
            //!!!test that both periodic nodes are not electrodes!!!

            if (GP.IsInsideElectrode(baseidx))
            {
              //potential fixed via internal electrode or Dirichlet BC
              NonZeroValues[baseidx] = 1.0;
            }
            else
            {
              //first test for periodic boundaries
              bool NotProcessedYet = true;

              GP.GetNextToElectrode(baseidx, false, stencil);

              if (i == 0 && j == 0 && NotProcessedYet)
              {
                //periodic BC, corner in case both x and y are periodic
                  NotProcessedYet = false;
                  NonZeroValues[0        ] = stencil.GetCenter();
                  NonZeroValues[1        ] = stencil.GetXRight();
                  NonZeroValues[nx -2    ] = stencil.GetXLeft();
                  NonZeroValues[nx       ] = stencil.GetYRight();
                  NonZeroValues[nx*(ny-2)] = stencil.GetYLeft();
              }

              if (NotProcessedYet && ((i ==0 && j == (ny-1)) || (i == (nx-1) && j == 0) || (i ==(nx-1) && j == (ny-1))))
              {
                NotProcessedYet = false;
                NonZeroValues[baseidx      ] = 1.0;
                NonZeroValues[0            ] =-1.0;
              }


              if ((i == 0) && NotProcessedYet)
              {
                //periodic BC, left edge of the modeled region
                  NotProcessedYet = false;
                  NonZeroValues[baseidx   -nx] = stencil.GetYLeft();
                  NonZeroValues[baseidx      ] = stencil.GetCenter();
                  NonZeroValues[baseidx    +1] = stencil.GetXRight();
                  NonZeroValues[baseidx +nx-2] = stencil.GetXLeft();
                  NonZeroValues[baseidx   +nx] = stencil.GetYRight();
              }

              if ((i == (nx-1)) && NotProcessedYet)
              {
                //periodic BC, right edge of the modeled region

                  NotProcessedYet = false;
                  NonZeroValues[j*nx      ] = 1.0;
                  NonZeroValues[j*nx +nx-1] =-1.0;
              }


              if((j == 0) && NotProcessedYet)
              {
                //periodic BC, bottom edge of the modeled region
                  NotProcessedYet = false;
                  NonZeroValues[baseidx         -1] = stencil.GetXLeft();
                  NonZeroValues[baseidx           ] = stencil.GetCenter();
                  NonZeroValues[baseidx         +1] = stencil.GetYLeft();
                  NonZeroValues[baseidx        +nx] = stencil.GetYRight();
                  NonZeroValues[baseidx +(ny-2)*nx] = stencil.GetYLeft();
              }

              if((j == (ny-1)) && NotProcessedYet)
              {
                //periodic BC, top edge of the modeled region
                  NotProcessedYet = false;
                  NonZeroValues[i      ] = 1.0;//!!!3D
                  NonZeroValues[baseidx] =-1.0;
              }

              if (NotProcessedYet)
              {
                //normal free plasma, inside the modelled region
                NonZeroValues[baseidx -nx] = stencil.GetYLeft();
                NonZeroValues[baseidx  -1] = stencil.GetXLeft();
                NonZeroValues[baseidx    ] = stencil.GetCenter();
                NonZeroValues[baseidx  +1] = stencil.GetXRight();
                NonZeroValues[baseidx +nx] = stencil.GetYRight();
              }
            }
            num_nonzeros_curr_line = int(NonZeroValues.size());

            xaoffset += num_nonzeros_curr_line;
            xa.push_back(xaoffset);

            std::map<int, double>:: const_iterator itNonZero;
            for(itNonZero = NonZeroValues.begin(); itNonZero != NonZeroValues.end(); ++itNonZero)
            {
              asub.push_back(itNonZero->first);
              a.push_back(itNonZero->second);
            }
          }
        }
      }
      else
      {
        //3D case
        PoisSolverUtils::Stencil stencil(true);
        const size_t offsetz = nx*ny;
        for (size_t k = 0; k < nz; k++)
        {
          for (size_t j = 0; j < ny; j++)
          {
            for (size_t i = 0; i < nx; i++)
            {
              const size_t baseidx = i + j*nx + k*offsetz;
              NonZeroValues.clear();
              //!!!test that both periodic nodes are not electrodes!!!

              if (GP.IsInsideElectrode(baseidx))
              {
                //potential fixed via internal electrode or Dirichlet BC
                NonZeroValues[baseidx] = 1.0;
              }
              else
              {
                //first test for periodic boundaries
                bool NotProcessedYet = true;

                GP.GetNextToElectrode(baseidx, true, stencil);

                if (i == 0 && j == 0 && NotProcessedYet)
                {
                  //periodic BC, corner of the plane
                    NotProcessedYet = false;
                    NonZeroValues[baseidx           ] = stencil.GetCenter();
                    NonZeroValues[baseidx +1        ] = stencil.GetXRight();
                    NonZeroValues[baseidx +nx -2    ] = stencil.GetXLeft();
                    NonZeroValues[baseidx +nx       ] = stencil.GetYRight();
                    NonZeroValues[baseidx +nx*(ny-2)] = stencil.GetYLeft();
                    NonZeroValues[baseidx -offsetz  ] = stencil.GetZLeft();
                    NonZeroValues[baseidx +offsetz  ] = stencil.GetZRight();
                }

                if (NotProcessedYet && ((i ==0 && j == (ny-1)) || (i == (nx-1) && j == 0) || (i ==(nx-1) && j == (ny-1))))
                {
                  //remaining corners of the plane
                  NotProcessedYet = false;
                  NonZeroValues[baseidx      ] = 1.0;
                  NonZeroValues[k*offsetz    ] =-1.0;
                }


                if ((i == 0) && NotProcessedYet)
                {
                  //periodic BC, left edge of the plane
                    NotProcessedYet = false;
                    NonZeroValues[baseidx   -nx] = stencil.GetYLeft();
                    NonZeroValues[baseidx      ] = stencil.GetCenter();
                    NonZeroValues[baseidx    +1] = stencil.GetXRight();
                    NonZeroValues[baseidx +nx-2] = stencil.GetXLeft();
                    NonZeroValues[baseidx   +nx] = stencil.GetYRight();
                    NonZeroValues[baseidx -offsetz  ] = stencil.GetZLeft();
                    NonZeroValues[baseidx +offsetz  ] = stencil.GetZRight();
                }

                if ((i == (nx-1)) && NotProcessedYet)
                {
                  //periodic BC, right edge of the plane
                    NotProcessedYet = false;
                    NonZeroValues[baseidx        ] = 1.0;
                    NonZeroValues[baseidx -(nx-1)] =-1.0;
                }


                if((j == 0) && NotProcessedYet)
                {
                  //periodic BC, bottom edge of the plane
                    NotProcessedYet = false;
                    NonZeroValues[baseidx         -1] = stencil.GetXLeft();
                    NonZeroValues[baseidx           ] = stencil.GetCenter();
                    NonZeroValues[baseidx         +1] = stencil.GetXRight();
                    NonZeroValues[baseidx        +nx] = stencil.GetYRight();
                    NonZeroValues[baseidx +(ny-2)*nx] = stencil.GetYLeft();
                    NonZeroValues[baseidx -offsetz  ] = stencil.GetZLeft();
                    NonZeroValues[baseidx +offsetz  ] = stencil.GetZRight();
                }

                if((j == (ny-1)) && NotProcessedYet)
                {
                  //periodic BC, top edge of the plane
                    NotProcessedYet = false;
                    NonZeroValues[baseidx      ] = 1.0;
                    NonZeroValues[k*offsetz + i] =-1.0;
                }

                if (NotProcessedYet)
                {
                  //normal free plasma, inside the modelled region
                  NonZeroValues[baseidx -nx] = stencil.GetYLeft();
                  NonZeroValues[baseidx  -1] = stencil.GetXLeft();
                  NonZeroValues[baseidx    ] = stencil.GetCenter();
                  NonZeroValues[baseidx  +1] = stencil.GetXRight();
                  NonZeroValues[baseidx +nx] = stencil.GetYRight();
                  NonZeroValues[baseidx -offsetz  ] = stencil.GetZLeft();
                  NonZeroValues[baseidx +offsetz  ] = stencil.GetZRight();
                }
              }

              num_nonzeros_curr_line = int(NonZeroValues.size());

              xaoffset += num_nonzeros_curr_line;
              xa.push_back(xaoffset);

              std::map<int, double>:: const_iterator itNonZero;
              for(itNonZero = NonZeroValues.begin(); itNonZero != NonZeroValues.end(); ++itNonZero)
              {
                asub.push_back(itNonZero->first);
                a.push_back(itNonZero->second);
              }
            }
          }
        }
      }


      size_t num_nonzeros_total = xa.back();
      nonzeros_ = num_nonzeros_total;
      nrows_ = xa.size() - 1;


      a_ = new double[num_nonzeros_total];
      asub_ = new PoisSolverUtils::idx_t[num_nonzeros_total];
      xa_ = new PoisSolverUtils::idx_t[xa.size()];

      for(size_t i = 0; i < xa.size(); i++)
        xa_[i] = xa[i];

      for(size_t i = 0; i < num_nonzeros_total; i++)
        a_[i] = a[i];

      for(size_t i = 0; i < num_nonzeros_total; i++)
        asub_[i] = asub[i];

    }
    catch (...)
    {
      Answ = false;
    }
  return Answ;
}

//EOF
