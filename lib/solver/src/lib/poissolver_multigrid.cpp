#include "poissolver_utils.h"
#include "poissolver_multigrid.h"

double GetCoef(int iLevel)
{
  return 2.0;
}


PoisSolverMultigrid::PoisSolverMultigrid()
{
  num_levels_ = 0;

  num_pre_smooths_ = 0;
  num_post_smooths_ = 3; //!!!find the optimal value
  limit_residual_ = 1.0e-7;

  num_steps_last_solve_ = 0;
  time_last_solve_ = 0.0;
  level_verbosity_ = VERBOSITY_ONELINER_EACH_SOLVE;

#ifdef _OPENMP
  num_threads_ = omp_get_num_threads();
#else
  num_threads_ = 1;
#endif
}

PoisSolverMultigrid::~PoisSolverMultigrid()
{
}

bool PoisSolverMultigrid::SetNumThreads(int nThreads)
{
  if (nThreads > 0)
  {
    num_threads_ = nThreads;
    return true;
  }else
    return false;
}

void PoisSolverMultigrid::SetVerbosityLevel(verbosity_enum Level)
{
  for(size_t i_level = 0; i_level < grid_pack_.size(); i_level++)
  {
    grid_pack_[ i_level ].SetVerbosityLevel(level_verbosity_);
  }


}

int PoisSolverMultigrid::GetMaxNumLevels(int nx, int ny, int nz)
{
  int n_levels = 1;
  if (nx <= 1 || ny <= 1)
    n_levels = 0;
  //if nz wasn't specified properly, we have a 2D case
  if (nz <= 1)
    nz = nx;

  if (n_levels)
  {
    bool b_continue = true;

    while (b_continue)
    {
      if (nx < 3 || ny < 3 || nz < 3)
      {
        b_continue = false;
        break;
      }

      nx = (nx-1)/2 +1;
      ny = (ny-1)/2 +1;
      nz = (nz-1)/2 +1;

      if ( (nx %2) == 0 || (ny %2) == 0 || (nz %2) == 0 )
        b_continue = false;

      n_levels++;
    }

    n_levels--;
  }

  return n_levels;
}

bool PoisSolverMultigrid::SetDimensions(int nx, int ny, int nz)
{
  bool Answ = PoisSolver::SetDimensions(nx, ny, nz);
  if (Answ)
  {
    vector_pack_.resize(1);
    Answ = vector_pack_[0].AllocateAndInit(nx,ny, nz);
  }
  if (level_verbosity_ >= VERBOSITY_VERBOSE_INIT)
    printf("Setting up a grid level  %d, nx %5d ny %5d, nz %5d\n", (int)grid_pack_.size() -1, nx, ny, nz);
  if (Answ)
  {
    int max_num_levels = GetMaxNumLevels(nx, ny, nz);
    max_num_levels--;//one already set as basic
    for (int i_additional_level = 0; i_additional_level < max_num_levels; i_additional_level++)
    {
      nx = (nx-1)/2+1;
      ny = (ny-1)/2+1;
      if (nz > 1)
        nz = (nz-1)/2+1;
      PoisSolverUtils::GridPack NewGP(level_verbosity_);
      grid_pack_.push_back(NewGP);
      PoisSolverUtils::GridPack &GP = grid_pack_.back();
      Answ = GP.AllocateAndInit(nx, ny, nz);
      if (Answ)
      {
        GP.UnsetPeriodicInDirection(0, 0.0, 0.0);
        GP.UnsetPeriodicInDirection(1, 0.0, 0.0);
        GP.UnsetPeriodicInDirection(2, 0.0, 0.0);
        vector_pack_.resize(vector_pack_.size() +1);
        Answ = vector_pack_[i_additional_level +1].AllocateAndInit(nx,ny, nz);
      }
      //if (level_verbosity_ >= VERBOSITY_VERBOSE_INIT)
        printf("Setting up a grid level  %d, nx %5d ny %5d, nz %5d\n", (int)grid_pack_.size() -1, nx, ny, nz);
      if (!Answ)
        break;
    }

    num_levels_ = max_num_levels;
  }
  return Answ;
}

bool PoisSolverMultigrid::FinishEditing()
{
  grid_pack_[0].FillImplicitNextToElectrodes();
  for(size_t i_level = 0; i_level < grid_pack_.size(); i_level++)
  {
    if (i_level > 0)
      grid_pack_[ i_level ].RestrictElectrodesFromFiner( grid_pack_[ i_level -1 ] );
    if (level_filedump_ >= FILEDUMP_INIT_ONLY)
      PoisSolverUtils::DumpVectorToFile(grid_pack_[i_level].GetBitmap(), "Bitmap", grid_pack_[i_level], i_level);
  }

  for(size_t i_level = 0; i_level < grid_pack_.size(); i_level++)
  {
    grid_pack_[ i_level ].FinishEditing(i_level);
    if (level_filedump_ >= FILEDUMP_INIT_ONLY)
    {
      printf("Total NextToElectrode on level %zu: %zu\n", i_level, grid_pack_[ i_level ].GetNumNextToElectrode());
      int GPntotal = grid_pack_[ i_level ].GetNTotal();
      //just create a temp. array for storing to output
      std::vector<int> NextToElectrode(GPntotal, 0);
      for(int i = 0; i < GPntotal; i++)
        if (grid_pack_[ i_level ].IsNextToElectrode(i))
        {
          NextToElectrode[i] = 1;
          //printf("Idx %d has a NextToElectrode for Diskwriting\n", i);
        }
      PoisSolverUtils::DumpVectorToFile(&(NextToElectrode[0]), "NextToElectrode", grid_pack_[i_level], i_level);
    }
  }

  if (level_verbosity_ >= VERBOSITY_VERBOSE_INIT)
    printf("\nSetting up the coarsest level solver on grid level %d (%5d x %5d x %5d):\n",
        num_levels_-1, grid_pack_[num_levels_-1].GetNX(), grid_pack_[num_levels_-1].GetNY(), grid_pack_[num_levels_-1].GetNZ());

  if (level_verbosity_ >= VERBOSITY_VERBOSE_INIT)
    coarsest_solver_.SetVerbosityLevel(VERBOSITY_BRIEF_INIT);
  if (level_verbosity_ >= VERBOSITY_DETAILS_EACH_STEP)
    coarsest_solver_.SetVerbosityLevel(VERBOSITY_ONELINER_EACH_SOLVE);

  coarsest_solver_.SetFileDumpLevel(level_filedump_);
  coarsest_solver_.SetDimensions(grid_pack_[num_levels_-1].GetNX(), grid_pack_[num_levels_-1].GetNY(), grid_pack_[num_levels_-1].GetNZ());
  coarsest_solver_.AcceptGridPack(grid_pack_[num_levels_-1]);
  coarsest_solver_.FinishEditing();

  if (level_verbosity_ >= VERBOSITY_VERBOSE_INIT)
    printf("\nEnd of the coarsest level solver init output\n");

  if (level_verbosity_ >= VERBOSITY_VERBOSE_INIT)
  {
    if (sizeof(PoisSolverUtils::innerfloat_t) >=8)
      printf("Multigrid internals configured with double precision\n");
    else
      printf("Multigrid internals configured with single precision\n");
  }

  status_ = STATUS_READY_FOR_SOLVING;
  return true;
}

template <class T, class U> void ComputeResiduum2D(U *Result, T *LHS, const T *RHS, int iLevel, const PoisSolverUtils::GridPack &CurrGP)
{
    const T h = T(1 << iLevel);
    const T h2i = 1/(h*h);

    const int nx = CurrGP.GetNX();
    const int ny = CurrGP.GetNY();
    const int ntotal = CurrGP.GetNTotal();

    CurrGP.OverwriteWithElectrodes(LHS);

    for (int j = 1; j< (ny-1); j++)
      for (int i = 1; i< (nx-1); i++)//!!!not vectorized
      {
         const int baseidx = i + j*nx;//!!!move to the previous loop
         //was at first arrResiduum[baseidx] = arrPot[baseidx -1] + arrPot[baseidx +1] + arrPot[baseidx -nx] + arrPot[baseidx +nx] - 4*arrPot[baseidx] - arrDens[baseidx];
         Result[baseidx] = U(-h2i*(LHS[baseidx -1] + LHS[baseidx +1] + LHS[baseidx -nx] + LHS[baseidx +nx] - 4*LHS[baseidx]) + RHS[baseidx]);
      }

    const std::vector<PoisSolverUtils::IndexedStencil> &Storage = CurrGP.GetStencilsInside().NextToElectrodeBoth;
    for(size_t iNeigh = 0; iNeigh < Storage.size(); iNeigh++)
    {
      const int baseidx = Storage[iNeigh].Idx;
      Result[baseidx] = U(-h2i*(Storage[iNeigh].GetXLeft()*LHS[baseidx -1] + Storage[iNeigh].GetXRight()*LHS[baseidx +1] + Storage[iNeigh].GetYLeft()*LHS[baseidx -nx] + Storage[iNeigh].GetYRight()*LHS[baseidx +nx] + Storage[iNeigh].GetCenter()*LHS[baseidx]) + RHS[baseidx]);
    }


    if (CurrGP.IsPeriodicX())
    {
      for (int j = 1; j < (ny-1); j++)//!!!not vectorized
      {
        const size_t baseidx = j*nx;
        //this grid node is the leftmost in a given row
        Result[baseidx] = U(-h2i*(LHS[baseidx +nx -2] + LHS[baseidx +1] + LHS[baseidx -nx] + LHS[baseidx +nx] - 4*LHS[baseidx]) + RHS[baseidx]);
        //this grid node is the rightmost in a given row
        Result[baseidx + nx -1] = 0.0;
      }

      const std::vector<PoisSolverUtils::IndexedStencil> &Storage = CurrGP.GetStencilsPerX().NextToElectrodeBoth;
      for(size_t iNeigh = 0; iNeigh < Storage.size(); iNeigh++)
      {
        const int baseidx = Storage[iNeigh].Idx;
        Result[baseidx] = U(-h2i*(Storage[iNeigh].GetXLeft()*LHS[baseidx +nx-2] + Storage[iNeigh].GetXRight()*LHS[baseidx +1] + Storage[iNeigh].GetYLeft()*LHS[baseidx -nx] + Storage[iNeigh].GetYRight()*LHS[baseidx +nx] + Storage[iNeigh].GetCenter()*LHS[baseidx]) + RHS[baseidx]);
      }

    }

    if (CurrGP.IsPeriodicY())
    {
      const int offsetup1 = (ny-2)*nx;
      const int offsetup2 = (ny-1)*nx;
      for (int i = 1; i < (nx-1); i++)//!!!not vectorized
      {
        const int &baseidx = i;
        //this grid node is the leftmost in a given row
        Result[baseidx] = U(-h2i*(LHS[baseidx -1] + LHS[baseidx +1] + LHS[baseidx +offsetup1] + LHS[baseidx +nx] - 4*LHS[baseidx]) + RHS[baseidx]);
        //Result[baseidx] += U(RHS[baseidx + offsetup2]);
        //this grid node is the rightmost in a given row
        Result[baseidx + offsetup2] = 0.0;
      }

      const std::vector<PoisSolverUtils::IndexedStencil> &Storage = CurrGP.GetStencilsPerY().NextToElectrodeBoth;
      for(size_t iNeigh = 0; iNeigh < Storage.size(); iNeigh++)
      {
        const int baseidx = Storage[iNeigh].Idx;
        Result[baseidx] = U(-h2i*(Storage[iNeigh].GetXLeft()*LHS[baseidx -1] + Storage[iNeigh].GetXRight()*LHS[baseidx +1] + Storage[iNeigh].GetYLeft()*LHS[baseidx +offsetup1] + Storage[iNeigh].GetYRight()*LHS[baseidx +nx] + Storage[iNeigh].GetCenter()*LHS[baseidx]) + RHS[baseidx]);
      }
    }

    if (CurrGP.IsPeriodicX() && CurrGP.IsPeriodicY())
    {
      Result[0] = U(-h2i*(LHS[nx-2] + LHS[1] + LHS[(ny-2)*nx] + LHS[nx] - 4*LHS[0]) + RHS[0]);
      //this grid node is the rightmost in a given row
      Result[nx-1] = 0.0;
      Result[nx*(ny-1)] = 0.0;
      Result[nx*ny-1] = 0.0;

      const std::vector<PoisSolverUtils::IndexedStencil> &Storage = CurrGP.GetStencilsPerCornerXY().NextToElectrodeBoth;
      for(size_t iNeigh = 0; iNeigh < Storage.size(); iNeigh++)
      {
        const int baseidx = Storage[iNeigh].Idx;
        Result[baseidx] = U(-h2i*(Storage[iNeigh].GetXLeft()*LHS[baseidx +nx-2] + Storage[iNeigh].GetXRight()*LHS[baseidx +1] + Storage[iNeigh].GetYLeft()*LHS[baseidx +(ny-2)*nx] + Storage[iNeigh].GetYRight()*LHS[baseidx +nx] + Storage[iNeigh].GetCenter()*LHS[baseidx]) + RHS[baseidx]);
      }
    }

    for(int baseidx = 0; baseidx < ntotal; baseidx++)//!!!not vectorized
    {
      if (CurrGP.IsInsideElectrode(baseidx))
        Result[baseidx] = 0.0;
    }
}

template <class T, class U> void ComputeResiduum3D(U *Result, T *LHS, T *RHS, int iLevel, const PoisSolverUtils::GridPack &CurrGP)
{
    const T h = T(1 << iLevel);
    const T h2i = 1/(h*h);

    const int nx = CurrGP.GetNX();
    const int ny = CurrGP.GetNY();
    const int nz = CurrGP.GetNZ();
    const int ntotal = CurrGP.GetNTotal();
    const int offsetz = nx*ny;

    CurrGP.OverwriteWithElectrodes(LHS);
//#pragma omp for default(none) shared(vec) schedule(static, 1024)
//schedule x z-layers
//    #pragma omp for default(none) shared(Result) schedule(static, 5)
#pragma omp parallel for schedule(static, 5)
    for (int k = 1; k< (nz-1); k++)
      for (int j = 1; j< (ny-1); j++)
        for (int i = 1; i< (nx-1); i++)//!!!not vectorized
        {
           const int baseidx = i + j*nx + k*offsetz;//!!!move to the previous loop
           Result[baseidx] = U(-h2i*(LHS[baseidx -1] + LHS[baseidx +1] + LHS[baseidx -nx] + LHS[baseidx +nx] + LHS[baseidx -offsetz] + LHS[baseidx +offsetz] - 6*LHS[baseidx]) + RHS[baseidx]);
        }

    const std::vector<PoisSolverUtils::IndexedStencil> &Storage = CurrGP.GetStencilsInside().NextToElectrodeBoth;
    for(size_t iNeigh = 0; iNeigh < Storage.size(); iNeigh++)
    {
      const int baseidx = Storage[iNeigh].Idx;
      //!!!depends on the position of constants - double vs float!
      Result[baseidx] = U(-h2i*(Storage[iNeigh].GetXLeft()*LHS[baseidx -1] + Storage[iNeigh].GetXRight()*LHS[baseidx +1] + Storage[iNeigh].GetYLeft()*LHS[baseidx -nx] + Storage[iNeigh].GetYRight()*LHS[baseidx +nx] + Storage[iNeigh].GetZLeft()*LHS[baseidx -offsetz] + Storage[iNeigh].GetZRight()*LHS[baseidx +offsetz] + Storage[iNeigh].GetCenter()*LHS[baseidx]) + RHS[baseidx]);
    }

    if (CurrGP.IsPeriodicX())
    {
      for (int k = 1; k < (nz-1); k++)
        for (int j = 1; j < (ny-1); j++)//!!!not vectorized
        {
          const size_t baseidx = j*nx + k*offsetz;
          //this grid node is the leftmost in a given row
          Result[baseidx] = U(-h2i*(LHS[baseidx +nx -2] + LHS[baseidx +1] + LHS[baseidx -nx] + LHS[baseidx +nx] + LHS[baseidx -offsetz] + LHS[baseidx +offsetz] - 6*LHS[baseidx]) + RHS[baseidx]);

          //this grid node is the rightmost in a given row and plane
          Result[baseidx + nx -1] = 0.0;
        }

      const std::vector<PoisSolverUtils::IndexedStencil> &Storage = CurrGP.GetStencilsPerX().NextToElectrodeBoth;
      for(size_t iNeigh = 0; iNeigh < Storage.size(); iNeigh++)
      {
        const int baseidx = Storage[iNeigh].Idx;
        //!!!depends on the position of constants - double vs float!
        Result[baseidx] = U(-h2i*(Storage[iNeigh].GetXLeft()*LHS[baseidx +nx -2] + Storage[iNeigh].GetXRight()*LHS[baseidx +1] + Storage[iNeigh].GetYLeft()*LHS[baseidx -nx] + Storage[iNeigh].GetYRight()*LHS[baseidx +nx] + Storage[iNeigh].GetZLeft()*LHS[baseidx -offsetz] + Storage[iNeigh].GetZRight()*LHS[baseidx +offsetz] + Storage[iNeigh].GetCenter()*LHS[baseidx]) + RHS[baseidx]);
      }
    }

    if (CurrGP.IsPeriodicY())
    {
      const int offsetup1 = (ny-2)*nx;
      const int offsetup2 = offsetup1 + nx;
      for (int k = 1; k < (nz-1); k++)
        for (int i = 1; i < (nx-1); i++)//!!!not vectorized
        {
          const int baseidx = i + k*offsetz;
          //this grid node is the leftmost in a given row
          Result[baseidx] = U(-h2i*(LHS[baseidx -1] + LHS[baseidx +1] + LHS[baseidx +offsetup1] + LHS[baseidx +nx] + LHS[baseidx -offsetz] + LHS[baseidx +offsetz] - 6*LHS[baseidx]) + RHS[baseidx]);
         //this grid node is the rightmost in a given row
          Result[baseidx + offsetup2] = 0.0;
        }

      const std::vector<PoisSolverUtils::IndexedStencil> &Storage = CurrGP.GetStencilsPerY().NextToElectrodeBoth;
      for(size_t iNeigh = 0; iNeigh < Storage.size(); iNeigh++)
      {
        const int baseidx = Storage[iNeigh].Idx;
        //!!!depends on the position of constants - double vs float!
        Result[baseidx] = U(-h2i*(Storage[iNeigh].GetXLeft()*LHS[baseidx -1] + Storage[iNeigh].GetXRight()*LHS[baseidx +1] + Storage[iNeigh].GetYLeft()*LHS[baseidx +offsetup1] + Storage[iNeigh].GetYRight()*LHS[baseidx +nx] + Storage[iNeigh].GetZLeft()*LHS[baseidx -offsetz] + Storage[iNeigh].GetZRight()*LHS[baseidx +offsetz] + Storage[iNeigh].GetCenter()*LHS[baseidx]) + RHS[baseidx]);
      }
    }

    if (CurrGP.IsPeriodicX() && CurrGP.IsPeriodicY())
    {
      for (int k = 1; k < (nz-1); k++)//!!!not vectorized
      {
        const int baseidx = k*offsetz;
        Result[baseidx] = U(-h2i*(LHS[baseidx + nx-2] + LHS[baseidx + 1] + LHS[baseidx + (ny-2)*nx] + LHS[ baseidx + nx] + LHS[baseidx-offsetz] + LHS[baseidx+offsetz] - 6*LHS[baseidx]) + RHS[baseidx]);
        //this grid node is the rightmost in a given row
        Result[baseidx + nx-1] = 0.0;
        Result[baseidx + nx*(ny-1)] = 0.0;
        Result[baseidx + nx*ny-1] = 0.0;
      }

      const std::vector<PoisSolverUtils::IndexedStencil> &Storage = CurrGP.GetStencilsPerCornerXY().NextToElectrodeBoth;
      for(size_t iNeigh = 0; iNeigh < Storage.size(); iNeigh++)
      {
        const int baseidx = Storage[iNeigh].Idx;
         //!!!depends on the position of constants - double vs float!
        Result[baseidx] = U(-h2i*(Storage[iNeigh].GetXLeft()*LHS[baseidx +nx-2] + Storage[iNeigh].GetXRight()*LHS[baseidx +1] + Storage[iNeigh].GetYLeft()*LHS[baseidx + (ny-2)*nx] + Storage[iNeigh].GetYRight()*LHS[baseidx +nx] + Storage[iNeigh].GetZLeft()*LHS[baseidx -offsetz] + Storage[iNeigh].GetZRight()*LHS[baseidx +offsetz] + Storage[iNeigh].GetCenter()*LHS[baseidx]) + RHS[baseidx]);
      }
    }

    for(int baseidx = 0; baseidx < ntotal; baseidx++)//!!!not vectorized
    {
      if (CurrGP.IsInsideElectrode(baseidx))
        Result[baseidx] = 0.0;
    }
}


double PoisSolverMultigrid::ComputeMaxResiduum(const PoisSolverUtils::innerfloat_t *Resid,
                                const PoisSolverUtils::GridPack &GP,
                                bool IsResiduumComputed, int *IdxMax)
{
  double Answ = 0.0;
  double AbsMax = 0.0;
  int idx_max_residuum = -1;

  size_t num_total = GP.GetNTotal();
  for(size_t baseidx = 0; baseidx < num_total; baseidx++)
  {
    //if (!GP.IsNextToElectrode(baseidx))//!!!might be unnecessary
    {
      if (fabs(Resid[baseidx]) > AbsMax)
      {
        Answ = Resid[baseidx];
        AbsMax = fabs(Resid[baseidx]);
        idx_max_residuum = baseidx;
      }
    }
  }

  if (IdxMax != NULL)
    *IdxMax = idx_max_residuum;
  return Answ;
}

void PoisSolverMultigrid::Restrict2D(PoisSolverUtils::innerfloat_t *Coarse,
                      const PoisSolverUtils::innerfloat_t *Fine,
                      const PoisSolverUtils::GridPack &GPCoarse) const
{
  const int CX = GPCoarse.GetNX();
  const int CY = GPCoarse.GetNY();
  const int FX = 2*(CX-1)+1;
  const int FY = 2*(CY-1)+1;

  // destroys stale content of the output array
  for (int i = 0; i<CX*CY; i++)
      Coarse[i] = 0.0;

  const int offsetx = 1;
  const int offsety = FX;

  //hlavni cyklus pres vnitrek matice
  for(int y = 1; y < (CY -1); y++)
  {
    const int offsetC =  CX*y;
    const int offsetF =  FX*2*y; //double stride

//#pragma ivdep
    for(int x = 1; x < (CX -1); x++)//!!!not vectorized
    {
        const int bidxC = offsetC + x;
        const int bidxF = offsetF + 2*x;

        Coarse[bidxC] = (((Fine[bidxF-offsetx-offsety] + Fine[bidxF+offsetx-offsety]
                + Fine[bidxF-offsetx+offsety] + Fine[bidxF+offsetx+offsety])/2
                + Fine[bidxF-offsetx] + Fine[bidxF+offsetx] + Fine[bidxF-offsety] + Fine[bidxF+offsety])/2
                + Fine[bidxF])/4;
    }
    //posledni dva body fine matice na konci radku
  }//konec hlavniho dvojcyklu pres vnitrek matice

  if (GPCoarse.IsPeriodicX())
  {
//#pragma ivdep
    for(int y = 1; y < (CY -1); y++)
    {
        const int bidxC1 = CX*y;
        const int bidxC2 = bidxC1 + CX-1;
        const int bidxF1 = FX*2*y;
        const int bidxF2 = bidxF1 + FX-1;

        Coarse[bidxC1] = Fine[bidxF1]/4;
        Coarse[bidxC1] +=(Fine[bidxF2-offsetx] + Fine[bidxF1+offsetx] +
                          Fine[bidxF1-offsety] + Fine[bidxF1+offsety])/8;
        Coarse[bidxC1] += (Fine[bidxF2-offsetx-offsety] + Fine[bidxF2-offsetx+offsety] +
                          Fine[bidxF1+offsetx-offsety] + Fine[bidxF1+offsetx+offsety])/16;

        Coarse[bidxC2] = 0.0;
    }
  }

  if (GPCoarse.IsPeriodicY())
  {
//#pragma ivdep
    for(int x = 1; x < (CX -1); x++)
    {
        const int bidxC1 = x;
        const int bidxC2 = bidxC1 + CX*(CY-1);
        const int bidxF1 = 2*x;
        const int bidxF2 = bidxF1 + FX*(FY-1);

        Coarse[bidxC1] = Fine[bidxF1]/4;
        Coarse[bidxC1] +=(Fine[bidxF1-offsetx] + Fine[bidxF1+offsetx] +
                          Fine[bidxF2-offsety] + Fine[bidxF1+offsety])/8;
        Coarse[bidxC1] += (Fine[bidxF2-offsetx-offsety] + Fine[bidxF2+offsetx-offsety] +
                          Fine[bidxF1-offsetx+offsety] + Fine[bidxF1+offsetx+offsety])/16;

        Coarse[bidxC2] = 0.0;
    }
  }

  if (GPCoarse.IsPeriodicX() && GPCoarse.IsPeriodicY())
  {
    Coarse[0] = Fine[0+offsetx+offsety]/16 + (Fine[0+offsetx] + Fine[0+offsety])/8 + Fine[0]/4;

    Coarse[0] += (Fine[FX -2] + Fine[FX*(FY-1)])/8;
    Coarse[0] += (Fine[2*FX-2] + Fine[FX*(FY-1) +1] + Fine[FX*FY - FX - 1])/16;
    Coarse[CX-1] = 0.0;
    Coarse[CX*(CY-1)] = 0.0;
    Coarse[CX*CY-1] = 0.0;
  }
}//end of Restrict

void PoisSolverMultigrid::Restrict3D(PoisSolverUtils::innerfloat_t *Coarse,
                      const PoisSolverUtils::innerfloat_t *Fine,
                      const PoisSolverUtils::GridPack &GPCoarse) const
{
  const int CX = GPCoarse.GetNX();
  const int CY = GPCoarse.GetNY();
  const int CZ = GPCoarse.GetNZ();
  const int FX = 2*(CX-1)+1;
  const int FY = 2*(CY-1)+1;

  // destroys stale content of the output array
  for (int i = 0; i<CX*CY*CZ; i++)
      Coarse[i] = 0.0;

  const int offsetx = 1;
  const int offsety = FX;
  const int offsetzF = FX*FY;
  const int offsetzC = CX*CY;

  for(int z = 1; z < (CZ -1); z++)
  {
    for(int y = 1; y < (CY -1); y++)
    {
      const int offsetC =  CX*y + z*CX*CY;
      const int offsetF =  FX*2*y + 2*z*FX*FY; //double stride

      for(int x = 1; x < (CX -1); x++)
      {
        const int bidxC = offsetC + x;
        int bidxF = offsetF + 2*x;

        //first the lower plane
        bidxF -= offsetzF;
        Coarse[bidxC] = (((Fine[bidxF-offsetx-offsety] + Fine[bidxF+offsetx-offsety]
                + Fine[bidxF-offsetx+offsety] + Fine[bidxF+offsetx+offsety])/2
                + Fine[bidxF-offsetx] + Fine[bidxF+offsetx] + Fine[bidxF-offsety] + Fine[bidxF+offsety])/2
                + Fine[bidxF])/16;

        //now the middle plane - adding to already partially filled Coarse[bidxC]
        bidxF += offsetzF;
        Coarse[bidxC] += (((Fine[bidxF-offsetx-offsety] + Fine[bidxF+offsetx-offsety]
                + Fine[bidxF-offsetx+offsety] + Fine[bidxF+offsetx+offsety])/2
                + Fine[bidxF-offsetx] + Fine[bidxF+offsetx] + Fine[bidxF-offsety] + Fine[bidxF+offsety])/2
                + Fine[bidxF])/8;
        //then the top plane - adding to already partially filled Coarse[bidxC]
        bidxF += offsetzF;
        Coarse[bidxC] += (((Fine[bidxF-offsetx-offsety] + Fine[bidxF+offsetx-offsety]
                + Fine[bidxF-offsetx+offsety] + Fine[bidxF+offsetx+offsety])/2
                + Fine[bidxF-offsetx] + Fine[bidxF+offsetx] + Fine[bidxF-offsety] + Fine[bidxF+offsety])/2
                + Fine[bidxF])/16;
      }
    }
  }

  if (GPCoarse.IsPeriodicX())
  {
    for(int z = 1; z < (CZ -1); z++)
      for(int y = 1; y < (CY -1); y++)
      {
        const int bidxC1 = CX*y + z*offsetzC;
        const int bidxC2 = bidxC1 + CX-1;
        int bidxF1 = FX*2*y + 2*z*offsetzF;//!!!??? x2
        int bidxF2 = bidxF1 + FX-1;

        bidxF1 -= offsetzF;
	    bidxF2 -= offsetzF;
        Coarse[bidxC1] = (((Fine[bidxF2-offsetx-offsety] + Fine[bidxF1+offsetx-offsety]
                + Fine[bidxF2-offsetx+offsety] + Fine[bidxF1+offsetx+offsety])/2
                + Fine[bidxF2-offsetx] + Fine[bidxF1+offsetx]
                + Fine[bidxF1-offsety] + Fine[bidxF1+offsety])/2
                + Fine[bidxF1])/16;

        //now the middle plane - adding to already partially filled Coarse[bidxC]
        bidxF1 += offsetzF;
        bidxF2 += offsetzF;
        Coarse[bidxC1] += (((Fine[bidxF2-offsetx-offsety] + Fine[bidxF1+offsetx-offsety]
                + Fine[bidxF2-offsetx+offsety] + Fine[bidxF1+offsetx+offsety])/2
                + Fine[bidxF2-offsetx] + Fine[bidxF1+offsetx]
                + Fine[bidxF1-offsety] + Fine[bidxF1+offsety])/2
                + Fine[bidxF1])/8;

        //then the top plane - adding to already partially filled Coarse[bidxC]
        bidxF1 += offsetzF;
        bidxF2 += offsetzF;
        Coarse[bidxC1] += (((Fine[bidxF2-offsetx-offsety] + Fine[bidxF1+offsetx-offsety]
                + Fine[bidxF2-offsetx+offsety] + Fine[bidxF1+offsetx+offsety])/2
                + Fine[bidxF2-offsetx] + Fine[bidxF1+offsetx]
                + Fine[bidxF1-offsety] + Fine[bidxF1+offsety])/2
                + Fine[bidxF1])/16;

        //!!!Coarse[bidxC2] = Coarse[bidxC1];
        Coarse[bidxC2] = 0.0;
      }
  }

  if (GPCoarse.IsPeriodicY())
  {
    for (int z = 1; z < (CZ -1); z++)
      for(int x = 1; x < (CX -1); x++)
      {
        const int bidxC1 = x + z*offsetzC;
        const int bidxC2 = bidxC1 + CX*(CY-1);//!!!??? (y-1)
        int bidxF1 = 2*x + 2*z*offsetzF;//!!!??? x2
        int bidxF2 = bidxF1 + FX*(FY-1);//!!!??? (y-1)

        bidxF1 -= offsetzF;
        bidxF2 -= offsetzF;
        Coarse[bidxC1] = (((Fine[bidxF2-offsetx-offsety] + Fine[bidxF2+offsetx-offsety]
                + Fine[bidxF1-offsetx+offsety] + Fine[bidxF1+offsetx+offsety])/2
                + Fine[bidxF1-offsetx] + Fine[bidxF1+offsetx]
                + Fine[bidxF2-offsety] + Fine[bidxF1+offsety])/2
                + Fine[bidxF1])/16;

        //now the middle plane - adding to already partially filled Coarse[bidxC]
        bidxF1 += offsetzF;
        bidxF2 += offsetzF;
        Coarse[bidxC1] += (((Fine[bidxF2-offsetx-offsety] + Fine[bidxF2+offsetx-offsety]
            + Fine[bidxF1-offsetx+offsety] + Fine[bidxF1+offsetx+offsety])/2
            + Fine[bidxF1-offsetx] + Fine[bidxF1+offsetx]
            + Fine[bidxF2-offsety] + Fine[bidxF1+offsety])/2
            + Fine[bidxF1])/8;
        //then the top plane - adding to already partially filled Coarse[bidxC]
        bidxF1 += offsetzF;
        bidxF2 += offsetzF;
        Coarse[bidxC1] += (((Fine[bidxF2-offsetx-offsety] + Fine[bidxF2+offsetx-offsety]
                + Fine[bidxF1-offsetx+offsety] + Fine[bidxF1+offsetx+offsety])/2
                + Fine[bidxF1-offsetx] + Fine[bidxF1+offsetx]
                + Fine[bidxF2-offsety] + Fine[bidxF1+offsety])/2
                + Fine[bidxF1])/16;
        //!!!Coarse[bidxC2] = Coarse[bidxC1];
        Coarse[bidxC2] = 0.0;
    }
  }

  if (GPCoarse.IsPeriodicX() && GPCoarse.IsPeriodicY())
  {
    for (int z = 1; z < (CZ -1); z++)
    {
      const int baseidxC = z*offsetzC;
      const int baseidxFbase = 2*z*offsetzF;

      ///!!!
      Coarse[baseidxC] = 0.0;
      //bottom plane
      //bottom left corner
      int baseidxF = baseidxFbase - offsetzF;
      Coarse[baseidxC] += ((Fine[baseidxF + offsetx + offsety]/2 + Fine[baseidxF + offsetx] + Fine[baseidxF + offsety])/2 +Fine[baseidxF])/16;
      //bottom right corner
      baseidxF = baseidxFbase - offsetzF + FX-1;
      Coarse[baseidxC] += ((Fine[baseidxF - offsetx + offsety]/2 + Fine[baseidxF - offsetx])/2 +Fine[baseidxF])/16;
      //top left corner
      baseidxF = baseidxFbase - offsetzF + FX*(FY-1);
      Coarse[baseidxC] += (Fine[baseidxF + offsetx - offsety]/2 + Fine[baseidxF - offsety])/32;
      //top right corner
      baseidxF = baseidxFbase - offsetzF + FX*FY -1;
      Coarse[baseidxC] +=Fine[baseidxF - offsetx - offsety]/64;

      //middle plane
      //bottom left corner
      baseidxF = baseidxFbase;
      Coarse[baseidxC] += ((Fine[baseidxF + offsetx + offsety]/2 + Fine[baseidxF + offsetx] + Fine[baseidxF + offsety])/2 +Fine[baseidxF])/8;
      //bottom right corner
      baseidxF = baseidxFbase + FX-1;
      Coarse[baseidxC] += ((Fine[baseidxF - offsetx + offsety]/2 + Fine[baseidxF - offsetx])/2 +Fine[baseidxF])/8;
      //top left corner
      baseidxF = baseidxFbase + FX*(FY-1);
      Coarse[baseidxC] += (Fine[baseidxF + offsetx - offsety]/2 + Fine[baseidxF - offsety])/16;
      //top right corner
      baseidxF = baseidxFbase + FX*FY -1;
      Coarse[baseidxC] +=Fine[baseidxF - offsetx - offsety]/32;

      //bottom plane
      //bottom left corner
      baseidxF = baseidxFbase + offsetzF;
      Coarse[baseidxC] += ((Fine[baseidxF + offsetx + offsety]/2 + Fine[baseidxF + offsetx] + Fine[baseidxF + offsety])/2 +Fine[baseidxF])/16;
      //bottom right corner
      baseidxF = baseidxFbase + offsetzF + FX-1;
      Coarse[baseidxC] += ((Fine[baseidxF - offsetx + offsety]/2 + Fine[baseidxF - offsetx])/2 +Fine[baseidxF])/16;
      //top left corner
      baseidxF = baseidxFbase + offsetzF + FX*(FY-1);
      Coarse[baseidxC] += (Fine[baseidxF + offsetx - offsety]/2 + Fine[baseidxF - offsety])/32;
      //top right corner
      baseidxF = baseidxFbase + offsetzF + FX*FY -1;
      Coarse[baseidxC] +=Fine[baseidxF - offsetx - offsety]/64;
      Coarse[baseidxC + CX-1] = 0.0;
      Coarse[baseidxC + CX*(CY-1)] = 0.0;
      Coarse[baseidxC + CX*CY-1] = 0.0;
    }
  }

}//end of Restrict

void PoisSolverMultigrid::Prolongate2D(PoisSolverUtils::innerfloat_t *Fine,
                          const PoisSolverUtils::innerfloat_t *Coarse,
                          const PoisSolverUtils::GridPack &GPFine) const
{
    //podle hackbusche - w = arrFine, v = arrCoarse; ("u" patri az do addinterpolation)
  const int FX = GPFine.GetNX();
  const int FY = GPFine.GetNY();
  const int CX = FX/2 +1;
  const int CY = FY/2 +1;

    // destroys stale content of the output array
    for (int i = 0; i<FX*FY; i++)
      Fine[i] = 0.0;

    //first directly; takes care also of coarser nodes on boundaries and in corners!
    for(int y = 0; y < CY; y++)
      for(int x = 0; x < CX; x++)
        Fine[2*x + FX*2*y] = Coarse[x + CX*y];//!!!2x -> "<<"

    //fine nodes residing on the same line/between coarse nodes, working only with arrFine
    for(int y = 0; y < FY; y+=2)
      for(int x = 1; x < FX; x+=2)
      {
        const int baseidx = x + FX*y;
        Fine[baseidx] = 0.5*(Fine[baseidx-1] + Fine[baseidx +1]);
      }

    //remaining nodes on odd lines, working only with arrFine
    for(int y = 1; y < FY; y+=2)
      for(int x = 0; x < FX; x++)
      {
        const int baseidx = x + FX*y;
        Fine[baseidx] = 0.5*(Fine[baseidx - FX] + Fine[baseidx + FX]);
      }
}//end of Prolongate

void PoisSolverMultigrid::Prolongate3D(PoisSolverUtils::innerfloat_t *Fine,
                          const PoisSolverUtils::innerfloat_t *Coarse,
                          const PoisSolverUtils::GridPack &GPFine) const
{
    //podle hackbusche - w = arrFine, v = arrCoarse; ("u" patri az do addinterpolation)
    //!!!pozor, neda se jednoduse odstranit posledni sloupec, pak by nefungoval hlavni cyklus!
  const size_t FX = GPFine.GetNX();
  const size_t FY = GPFine.GetNY();
  const size_t FZ = GPFine.GetNZ();
  const size_t CX = FX/2 +1;
  const size_t CY = FY/2 +1;
  const size_t CZ = FZ/2 +1;

    // destroys stale content of the output array
    for (size_t i = 0; i<FX*FY*FZ; i++)
        Fine[i] = 0.0;

    size_t baseidx;

    //first directly on the coarse grid
    for(size_t z = 0; z < CZ; z++)
      for(size_t y = 0; y < CY; y++)
        for(size_t x = 0; x < CX; x++)
          Fine[2*x + FX*2*y + FX*FY*2*z] = Coarse[x + CX*y+CX*CY*z];//!!!2x -> "<<"

    //fine nodes residing on the same line/between coarse nodes, working only with arrFine
    for(size_t z = 0; z < FZ; z+=2)
      for(size_t y = 0; y < FY; y+=2)
        for(size_t x = 1; x < FX; x+=2)
        {
          baseidx = x + FX*y + FX*FY*z;
          Fine[baseidx] = 0.5*(Fine[baseidx-1] + Fine[baseidx +1]);
        }

    //remaining nodes on odd lines, working only with arrFine
    for(size_t z = 0; z < FZ; z+=2)
      for(size_t y = 1; y < FY; y+=2)
        for(size_t x = 0; x < FX; x++)
        {
          baseidx = x + FX*y + FX*FY*z;
          Fine[baseidx] = 0.5*(Fine[baseidx - FX] + Fine[baseidx + FX]);
        }

    //now finally odd planes
    for(size_t z = 1; z < FZ; z+=2)
      for(size_t y = 0; y < FY; y++)
        for(size_t x = 0; x < FX; x++)
        {
          baseidx = x + FX*y + FX*FY*z;
          Fine[baseidx] = 0.5*(Fine[baseidx - FX*FY] + Fine[baseidx + FX*FY]);
        }
    //!!!optimize


}//end of Prolongate
//void HalfStep2D(double* arrPot, const double* arrDens, double h2, size_t offset, const PoisSolverUtils::GridPack &GP)
//template<class T> void HalfStep2D(T* arrPot, const T* arrDens, double h2, size_t offset, const PoisSolverUtils::GridPack &GP)
template<class T> void HalfStep2D(T* arrPotMutable, const T* arrPot, const T* arrDens, double h2, const size_t offsetorig, const PoisSolverUtils::GridPack &GP)
{
    //process each line, every other node on the line, with beginning alternating between idx=1 or 2
    //initial idx on line 1 is determined by the "offset" argument, which should be only 0 or 1

  const int &nx = GP.GetNX();
  const int &ny = GP.GetNY();

  //periodic in x, i == 0
  if (GP.IsPeriodicX())
  {
    for (int j = (2 - offsetorig); j < (ny-1); j+= 2)//!!!not vectorized
    {
      const size_t baseidx = j*nx;
      arrPotMutable[baseidx] = 0.25*(arrPot[baseidx +nx-2] + arrPot[baseidx +1] + arrPot[baseidx -nx] + arrPot[baseidx +nx] - h2*arrDens[baseidx]);
      //arrPotMutable[baseidx + nx -1] = arrPot[baseidx];
    }

    const std::vector<PoisSolverUtils::IndexedStencil> &Storage = (offsetorig%2 == 0) ? GP.GetStencilsPerX().NextToElectrodeEven : GP.GetStencilsPerX().NextToElectrodeOdd;
    for(size_t iNeigh = 0; iNeigh < Storage.size(); iNeigh++)
    {
      const int baseidx = Storage[iNeigh].Idx;
      arrPotMutable[baseidx] = (-1.0/Storage[iNeigh].GetCenter())*(Storage[iNeigh].GetXLeft()*arrPot[baseidx +nx-2] + Storage[iNeigh].GetXRight()*arrPot[baseidx +1] + Storage[iNeigh].GetYLeft()*arrPot[baseidx -nx] + Storage[iNeigh].GetYRight()*arrPot[baseidx +nx] - h2*arrDens[baseidx]);
    }

    for (int j = (2 - offsetorig); j < (ny-1); j+= 2)//!!!not vectorized
    {
      const size_t baseidx = j*nx;
      arrPotMutable[baseidx + nx -1] = arrPotMutable[baseidx];//!!!refers in fact to the same memory window, cannot be order-optimized!
    }
  }

  //periodic in y, j == 0
  if (GP.IsPeriodicY())
  {
    const int offsetup1 = (ny-2)*nx;
    const int offsetup2 = (ny-1)*nx;
    for (int i = (2 - offsetorig); i < (nx-1); i+= 2)//!!!not vectorized
    {
      const size_t &baseidx = i;
      arrPotMutable[baseidx] = 0.25*(arrPot[baseidx -1] + arrPot[baseidx +1] + arrPot[baseidx +offsetup1] + arrPot[baseidx +nx] - h2*arrDens[baseidx]);
    }

    const std::vector<PoisSolverUtils::IndexedStencil> &Storage = (offsetorig%2 == 0) ? GP.GetStencilsPerY().NextToElectrodeEven : GP.GetStencilsPerY().NextToElectrodeOdd;
    for(size_t iNeigh = 0; iNeigh < Storage.size(); iNeigh++)
    {
      const int baseidx = Storage[iNeigh].Idx;
      arrPotMutable[baseidx] = (-1.0/Storage[iNeigh].GetCenter())*(Storage[iNeigh].GetXLeft()*arrPot[baseidx -1] + Storage[iNeigh].GetXRight()*arrPot[baseidx +1] + Storage[iNeigh].GetYLeft()*arrPot[baseidx +offsetup1] + Storage[iNeigh].GetYRight()*arrPot[baseidx +nx] - h2*arrDens[baseidx]);
    }

    for (int i = (2 - offsetorig); i < (nx-1); i+= 2)//!!!not vectorized
    {
      const size_t baseidx = i;
      arrPotMutable[baseidx + offsetup2] = arrPotMutable[baseidx];//!!!refers in fact to the same memory window, cannot be order-optimized!
    }
  }

  if (GP.IsPeriodicX() && GP.IsPeriodicY() && (offsetorig == 0))
  {
    arrPotMutable[0] = 0.25*(arrPot[nx-2] + arrPot[1] + arrPot[(ny-2)*nx] + arrPot[nx] - h2*arrDens[0]);

    const std::vector<PoisSolverUtils::IndexedStencil> &Storage = (offsetorig%2 == 0) ? GP.GetStencilsPerCornerXY().NextToElectrodeEven : GP.GetStencilsPerCornerXY().NextToElectrodeOdd;
    for(size_t iNeigh = 0; iNeigh < Storage.size(); iNeigh++)
    {
      const int baseidx = Storage[iNeigh].Idx;
      arrPotMutable[baseidx] = (-1.0/Storage[iNeigh].GetCenter())*(Storage[iNeigh].GetXLeft()*arrPot[nx-2] + Storage[iNeigh].GetXRight()*arrPot[1] + Storage[iNeigh].GetYLeft()*arrPot[(ny-2)*nx] + Storage[iNeigh].GetYRight()*arrPot[nx] - h2*arrDens[0]);
    }

    arrPotMutable[nx-1] = arrPotMutable[0];//!!!refers in fact to the same memory window, cannot be order-optimized!
    arrPotMutable[nx*(ny-1)] = arrPotMutable[0];
    arrPotMutable[nx*ny-1] = arrPotMutable[0];
  }

  int offset = offsetorig;
  for(int j = 1; j< (ny-1); j++)
  {
      for (int i = (offset + 1); i< (nx-1); i+= 2)//!!!not vectorized
      {
          size_t baseidx = i + j*nx;
          //G-S (other might work better):
          arrPotMutable[baseidx] = 0.25*(arrPot[baseidx -1] + arrPot[baseidx +1] + arrPot[baseidx -nx] + arrPot[baseidx +nx] - h2*arrDens[baseidx]);
      }
      //offset changes value, i.e. periodic BC should be handled first
      offset = 1 - offset;
  }

  const std::vector<PoisSolverUtils::IndexedStencil> &Storage = (offsetorig%2 == 0) ? GP.GetStencilsInside().NextToElectrodeEven : GP.GetStencilsInside().NextToElectrodeOdd;
  for(size_t iNeigh = 0; iNeigh < Storage.size(); iNeigh++)
  {
    const int baseidx = Storage[iNeigh].Idx;
    arrPotMutable[baseidx] = (-1.0/Storage[iNeigh].GetCenter())*(Storage[iNeigh].GetXLeft()*arrPot[baseidx -1] + Storage[iNeigh].GetXRight()*arrPot[baseidx +1] + Storage[iNeigh].GetYLeft()*arrPot[baseidx -nx] + Storage[iNeigh].GetYRight()*arrPot[baseidx +nx] - h2*arrDens[baseidx]);
  }

}

template<class T> void HalfStep3D(T* arrPotMutable, const T* arrPot, const T* arrDens, double h2, const size_t offsetorig, const PoisSolverUtils::GridPack &GP)
{
    //process each line, every other node on the line, with beginning alternating between idx=1 or 2
    //initial idx on line 1 is determined by the "offset" argument, which should be only 0 or 1

  const int &nx = GP.GetNX();
  const int &ny = GP.GetNY();
  const int &nz = GP.GetNZ();

  const int offsetz = nx*ny;
  const double onesixth = 1.0/6.0;

  //periodic in x, i == 0
  if (GP.IsPeriodicX())
  {
    int offsetperx = offsetorig;
    for(int k = 1; k< (nz-1); k++)
    {
      for (int j = (1 + offsetperx); j < (ny-1); j+= 2)//!!!not vectorized
      {
        const size_t baseidx = j*nx + k*offsetz;
        arrPotMutable[baseidx] = onesixth*(arrPot[baseidx +nx-2] + arrPot[baseidx +1] + arrPot[baseidx -nx] + arrPot[baseidx +nx] + arrPot[baseidx -offsetz] + arrPot[baseidx +offsetz] - h2*arrDens[baseidx]);
      }
      offsetperx = 1- offsetperx;
    }

    const std::vector<PoisSolverUtils::IndexedStencil> &Storage = (offsetorig%2 == 0) ? GP.GetStencilsPerX().NextToElectrodeEven : GP.GetStencilsPerX().NextToElectrodeOdd;
    for(size_t iNeigh = 0; iNeigh < Storage.size(); iNeigh++)
    {
      const int baseidx = Storage[iNeigh].Idx;
      arrPotMutable[baseidx] = (-1.0/Storage[iNeigh].GetCenter())*(Storage[iNeigh].GetXLeft()*arrPot[baseidx +nx-2] + Storage[iNeigh].GetXRight()*arrPot[baseidx +1] + Storage[iNeigh].GetYLeft()*arrPot[baseidx -nx] + Storage[iNeigh].GetYRight()*arrPot[baseidx +nx] + Storage[iNeigh].GetZLeft()*arrPot[baseidx -offsetz] + Storage[iNeigh].GetZRight()*arrPot[baseidx +offsetz] - h2*arrDens[baseidx]);
    }

    int offsetperx2 = offsetorig;
    for(int k = 1; k< (nz-1); k++)
    {
      for (int j = (1 + offsetperx2); j < (ny-1); j+= 2)//!!!not vectorized
      {
        const size_t baseidx = j*nx + k*offsetz;
        arrPotMutable[baseidx + nx -1] = arrPotMutable[baseidx];//!!!refers in fact to the same memory window, cannot be order-optimized!
      }
      offsetperx2 = 1- offsetperx2;
    }
  }

  //periodic in y, j == 0
  if (GP.IsPeriodicY())
  {
    const int offsetup1 = (ny-2)*nx;
    const int offsetup2 = (ny-1)*nx;
    int offsetpery = offsetorig;
    for(int k = 1; k< (nz-1); k++)
    {
      for (int i = (1+ offsetpery); i < (nx-1); i+= 2)//!!!not vectorized
      {
        const size_t baseidx = i + k*offsetz;
        arrPotMutable[baseidx] = onesixth*(arrPot[baseidx -1] + arrPot[baseidx +1] + arrPot[baseidx +offsetup1] + arrPot[baseidx +nx] + arrPot[baseidx -offsetz] + arrPot[baseidx +offsetz] - h2*arrDens[baseidx]);
      }
      offsetpery = 1 - offsetpery;
    }

    const std::vector<PoisSolverUtils::IndexedStencil> &Storage = (offsetorig%2 == 0) ? GP.GetStencilsPerY().NextToElectrodeEven : GP.GetStencilsPerY().NextToElectrodeOdd;
    for(size_t iNeigh = 0; iNeigh < Storage.size(); iNeigh++)
    {
      const int baseidx = Storage[iNeigh].Idx;
      arrPotMutable[baseidx] = (-1.0/Storage[iNeigh].GetCenter())*(Storage[iNeigh].GetXLeft()*arrPot[baseidx -1] + Storage[iNeigh].GetXRight()*arrPot[baseidx +1] + Storage[iNeigh].GetYLeft()*arrPot[baseidx +offsetup1] + Storage[iNeigh].GetYRight()*arrPot[baseidx +nx] + Storage[iNeigh].GetZLeft()*arrPot[baseidx -offsetz] + Storage[iNeigh].GetZRight()*arrPot[baseidx +offsetz] - h2*arrDens[baseidx]);
    }

    int offsetpery2 = offsetorig;
    for(int k = 1; k< (nz-1); k++)
    {
      for (int i = (1 + offsetpery2); i < (nx-1); i+= 2)//!!!not vectorized
      {
        const size_t baseidx = i + k*offsetz;
        arrPotMutable[baseidx + offsetup2] = arrPotMutable[baseidx];//!!!refers in fact to the same memory window, cannot be order-optimized!
      }
      offsetpery2 = 1 - offsetpery2;
    }
  }

  if (GP.IsPeriodicX() && GP.IsPeriodicY())
  {
    for(int k = (2-offsetorig); k<(nz-1); k+= 2)
    {
      const int baseidx = k*offsetz;
      arrPotMutable[baseidx] = onesixth*(arrPot[baseidx + nx-2] + arrPot[baseidx +1] + arrPot[baseidx + (ny-2)*nx] + arrPot[baseidx + nx] + arrPot[baseidx -offsetz]  + arrPot[baseidx +offsetz] - h2*arrDens[baseidx]);
    }

    const std::vector<PoisSolverUtils::IndexedStencil> &Storage = (offsetorig%2 == 0) ? GP.GetStencilsPerCornerXY().NextToElectrodeEven : GP.GetStencilsPerCornerXY().NextToElectrodeOdd;
    for(size_t iNeigh = 0; iNeigh < Storage.size(); iNeigh++)
    {
      const int baseidx = Storage[iNeigh].Idx;
      arrPotMutable[baseidx] = (-1.0/Storage[iNeigh].GetCenter())*(Storage[iNeigh].GetXLeft()*arrPot[baseidx + nx-2] + Storage[iNeigh].GetXRight()*arrPot[baseidx + 1] + Storage[iNeigh].GetYLeft()*arrPot[baseidx + (ny-2)*nx] + Storage[iNeigh].GetYRight()*arrPot[baseidx + nx] + Storage[iNeigh].GetZLeft()*arrPot[baseidx -offsetz] + Storage[iNeigh].GetZRight()*arrPot[baseidx +offsetz] - h2*arrDens[baseidx]);
    }

    for(int k = (2-offsetorig); k<(nz-1); k+= 2)
    {
      const int baseidx = k*offsetz;
      arrPotMutable[baseidx + nx-1] = arrPotMutable[baseidx];//!!!refers in fact to the same memory window, cannot be order-optimized!
      arrPotMutable[baseidx + nx*(ny-1)] = arrPotMutable[baseidx];
      arrPotMutable[baseidx + nx*ny-1] = arrPotMutable[baseidx];
    }
  }
  int offset = offsetorig;
  for(int k = 1; k< (nz-1); k++)
  {
    for(int j = 1; j< (ny-1); j++)
    {
      for (int i = (2-offset); i< (nx-1); i+= 2)//!!!not vectorized
      {
        size_t baseidx = i + j*nx + k*offsetz;
        //G-S (other might work better):
        arrPotMutable[baseidx] = onesixth*(arrPot[baseidx -1] + arrPot[baseidx +1] + arrPot[baseidx -nx] + arrPot[baseidx +nx] + arrPot[baseidx -offsetz] + arrPot[baseidx +offsetz] - h2*arrDens[baseidx]);
      }
      //offset changes value, i.e. periodic BC should be handled first
      offset = 1 - offset;
    }
  }
  const std::vector<PoisSolverUtils::IndexedStencil> &Storage = (offsetorig%2 == 0) ? GP.GetStencilsInside().NextToElectrodeEven : GP.GetStencilsInside().NextToElectrodeOdd;
  size_t NNeighs = Storage.size();
  for(size_t iNeigh = 0; iNeigh < NNeighs; iNeigh++)
  {
    const PoisSolverUtils::IndexedStencil &CurrStencil = Storage[iNeigh];
    const int baseidx = CurrStencil.Idx;
    arrPotMutable[baseidx] = (-1.0/CurrStencil.GetCenter())*(CurrStencil.GetXLeft()*arrPot[baseidx -1] + CurrStencil.GetXRight()*arrPot[baseidx +1] + CurrStencil.GetYLeft()*arrPot[baseidx -nx] + CurrStencil.GetYRight()*arrPot[baseidx +nx] + CurrStencil.GetZLeft()*arrPot[baseidx -offsetz] + CurrStencil.GetZRight()*arrPot[baseidx +offsetz] - h2*arrDens[baseidx]);
  }

}


template<class T> void Smoothen(T *U, const T *RHS, int iLevel, int nRepeats, const PoisSolverUtils::GridPack &GP)
{
    T h = double(1 << iLevel);
    T h2 = h*h;

    //grid_pack_[iLevel]
    GP.OverwriteWithElectrodes(U);
/*
    if(nRepeats < 0)
    {
      printf("Using only one halfstep of the multigrid smoothing!\n");
      if (GP.GetNZ() <= 1)
        HalfStep2D(U, U, RHS, h2, 0, GP);
      else
        HalfStep3D(U, U, RHS, h2, 0, GP);
    }
*/

    for(int iRep = 0; iRep < nRepeats; iRep++)
    {
      if (GP.GetNZ() <= 1)
        HalfStep2D(U, U, RHS, h2, 0, GP);
      else
        HalfStep3D(U, U, RHS, h2, 0, GP);

      GP.OverwriteWithElectrodes(U);

      if (GP.GetNZ() <= 1)
        HalfStep2D(U, U, RHS, h2, 1, GP);
      else
        HalfStep3D(U, U, RHS, h2, 1, GP);

      GP.OverwriteWithElectrodes(U);

      //abort();
    }
}

void PoisSolverMultigrid::SolveCoarsest(PoisSolverUtils::innerfloat_t *U,
                                        PoisSolverUtils::innerfloat_t *RHS,
                                        int iLevel)
{
    const size_t &ntotal = grid_pack_[iLevel].GetNTotal();

    double h = double(1 << iLevel);
    double h2=h*h;

    for (size_t i =0; i< ntotal; i++)
      RHS[i] *= h2;

    if (sizeof(PoisSolverUtils::outerfloat_t) == sizeof(PoisSolverUtils::innerfloat_t))
    {
      coarsest_solver_.Solve((PoisSolverUtils::outerfloat_t*)U, (PoisSolverUtils::outerfloat_t*)RHS);
    }
    else
    {
      PoisSolverUtils::outerfloat_t *RHSdouble = new PoisSolverUtils::outerfloat_t[ntotal];
      PoisSolverUtils::outerfloat_t *LHSdouble = new PoisSolverUtils::outerfloat_t[ntotal];

      for (size_t i =0; i< ntotal; i++)
        RHSdouble[i] = (PoisSolverUtils::outerfloat_t)RHS[i];
      for (size_t i =0; i< ntotal; i++)
        LHSdouble[i] = (PoisSolverUtils::outerfloat_t)U[i];

      coarsest_solver_.Solve(LHSdouble, RHSdouble);

      for (size_t i =0; i< ntotal; i++)
        U[i] = (PoisSolverUtils::innerfloat_t)LHSdouble[i];
      delete[] LHSdouble;
      delete[] RHSdouble;
    }
}


void PoisSolverMultigrid::Solve(PoisSolverUtils::outerfloat_t *_Potential, PoisSolverUtils::outerfloat_t *_Density)
{

  if (status_ != STATUS_READY_FOR_SOLVING)
    FinishEditing();

  PreprocessDensity(_Density, SummationPeriodicDensity);

  double num_limit_steps = 1000;
  double current_max_residuum = 2*limit_residual_;
  double previous_max_resid = current_max_residuum;
  int idx_step = 0;
  double time_start = PoisSolverUtils::GetWallclockTime();

  bool Is3D = (grid_pack_[0].GetNZ() <= 1) ? false : true;

  if (num_threads_ <= 0)
    num_threads_ = omp_get_num_threads();

  omp_set_num_threads(num_threads_);

#pragma omp parallel
{
  #pragma omp master
  while (fabs(current_max_residuum) > limit_residual_ && idx_step < num_limit_steps)
  {
    int IdxMaxResid = -1;

    //these aliases will change their contents during computation
    PoisSolverUtils::innerfloat_t * Res = NULL;
    PoisSolverUtils::innerfloat_t * V = NULL;
    PoisSolverUtils::innerfloat_t * Temp = NULL;

    //MOVING TO THE COARSEST LEVEL
    for(int iLevel = 0; iLevel < (num_levels_ -1); iLevel++)
    {
      if (level_verbosity_ >= VERBOSITY_DETAILS_EACH_STEP)
        printf("Level %d\n", iLevel);
      //???!!! where to put the smoothing?

      Temp = vector_pack_[iLevel].VectorTemp();
      if (iLevel == 0)
      {
        if (Is3D)
          ComputeResiduum3D(Temp, _Potential, _Density, iLevel, grid_pack_[iLevel]);
        else
          ComputeResiduum2D(Temp, _Potential, _Density, iLevel, grid_pack_[iLevel]);
      }
      else
      {
        PoisSolverUtils::innerfloat_t * U = vector_pack_[iLevel].VectorUpdate();
        PoisSolverUtils::innerfloat_t * RHS = vector_pack_[iLevel].VectorResid();

        if (Is3D)
          ComputeResiduum3D(Temp, U, RHS, iLevel, grid_pack_[iLevel]);
        else
          ComputeResiduum2D(Temp, U, RHS, iLevel, grid_pack_[iLevel]);
      }

      if (level_filedump_ >= FILEDUMP_DETAILED)
        PoisSolverUtils::DumpVectorToFile(Temp, "Residuum", grid_pack_[iLevel], iLevel, idx_step);

      if (iLevel == 0)
        current_max_residuum = ComputeMaxResiduum(Temp, grid_pack_[iLevel], true, &IdxMaxResid);

      if (idx_step < 2)
        current_max_residuum = previous_max_resid;


//      for (int iSmooth = 0; iSmooth < num_pre_smooths_; iSmooth++)
//        Smoothen(Temp, ???, iLevel);

      Res = vector_pack_[iLevel +1].VectorResid();
      if (Is3D)
        Restrict3D(Res, Temp, grid_pack_[iLevel +1]);
      else
        Restrict2D(Res, Temp, grid_pack_[iLevel +1]);

      vector_pack_[iLevel +1].NullUpdate();
    }


    //COARSEST LEVEL

    if (level_filedump_ >= FILEDUMP_DETAILED)
      PoisSolverUtils::DumpVectorToFile(Res, "CoarsestRHS", grid_pack_[num_levels_ -1], idx_step);
    if (level_verbosity_ >= VERBOSITY_DETAILS_EACH_STEP)
      printf("Coarsest level\n");
    V = vector_pack_[num_levels_ -1].VectorUpdate();

    SolveCoarsest(V, Res, num_levels_ -1);
    grid_pack_[num_levels_ -1].NullifyWithElectrodes(V);//!!!

    if (level_filedump_ >= FILEDUMP_DETAILED)
      PoisSolverUtils::DumpVectorToFile(V, "CoarsestSolution", grid_pack_[num_levels_ -1], num_levels_ -1, idx_step);

    //MOVE BACK TO THE FINEST LEVEL
    for(int iLevel = (num_levels_ -2); iLevel >= 0; iLevel--)
    {
      if (level_verbosity_ >= VERBOSITY_DETAILS_EACH_STEP)
        printf("Level %d\n", iLevel);
      Temp = vector_pack_[iLevel].VectorTemp();
      if (Is3D)
        Prolongate3D(Temp, V, grid_pack_[iLevel]);
      else
        Prolongate2D(Temp, V, grid_pack_[iLevel]);

      size_t ntotal = grid_pack_[iLevel].GetNTotal();

      if (iLevel == 0)
      {
        for(size_t i = 0; i < ntotal; i++)
          _Potential[i] += Temp[i];

        if (level_filedump_ >= FILEDUMP_DETAILED)
          PoisSolverUtils::DumpVectorToFile(_Potential, "PotEstimateBefSmoothing", grid_pack_[0], 0, idx_step);

        Smoothen(_Potential, _Density, iLevel, num_post_smooths_, grid_pack_[iLevel]);
      }
      else
      {
        PoisSolverUtils::innerfloat_t *U = vector_pack_[iLevel].VectorUpdate();
        PoisSolverUtils::innerfloat_t *RHS = vector_pack_[iLevel].VectorResid();


        for(size_t i = 0; i < ntotal; i++)
          U[i] += Temp[i];

        Smoothen(U, RHS, iLevel, num_post_smooths_, grid_pack_[iLevel]);

        V = U;

        if (level_filedump_ >= FILEDUMP_DETAILED)
          PoisSolverUtils::DumpVectorToFile(Temp, "UpdateSmoothened", grid_pack_[iLevel], iLevel, idx_step);
      }
    }
    if (level_filedump_ >= FILEDUMP_DETAILED)
      PoisSolverUtils::DumpVectorToFile(_Potential, "PotEstimate", grid_pack_[0], idx_step);

    if (level_verbosity_ >= VERBOSITY_DETAILS_EACH_STEP)
    {
      if (grid_pack_[0].GetNZ() <= 1)
        printf("Step %d completed, max. residuum %e (%.2f times smaller) at %d = %d x %d (is nexttoelectrode: %zu)\n", idx_step, current_max_residuum, previous_max_resid/current_max_residuum, IdxMaxResid, IdxMaxResid % grid_pack_[0].GetNX(), IdxMaxResid / grid_pack_[0].GetNX(), grid_pack_[0].IsNextToElectrode(IdxMaxResid));
      else
      {
        int ix = IdxMaxResid % grid_pack_[0].GetNX();
        int temp = IdxMaxResid % (grid_pack_[0].GetNX()*grid_pack_[0].GetNY());
        int iy = temp / grid_pack_[0].GetNX();
        int iz = IdxMaxResid / (grid_pack_[0].GetNX()*grid_pack_[0].GetNY());
        printf("Step %d completed, max. residuum %e (%.2f times smaller) at %d = %d x %d x %d (is nexttoelectrode: %zu)\n", idx_step, current_max_residuum, previous_max_resid/current_max_residuum, IdxMaxResid, ix, iy, iz, grid_pack_[0].IsNextToElectrode(IdxMaxResid));
      }

      previous_max_resid = current_max_residuum;
    }

    idx_step++;
  }
}
  double time_stop = PoisSolverUtils::GetWallclockTime();

  num_steps_last_solve_ = idx_step;
  time_last_solve_ = time_stop - time_start;

  //if (level_verbosity_ >= VERBOSITY_ONELINER_EACH_SOLVE)
    //printf("Solve took %f seconds with %d steps\n", time_last_solve_, idx_step);
}

int PoisSolverMultigrid::SetNumLevels(int NumLevels)
{
  std::cout << "Levels should be set " << NumLevels << "\n";
  if ((grid_pack_.size() > 0) && NumLevels > 1)
  {
    int MaxNumLevels = GetMaxNumLevels(grid_pack_[0].GetNX(), grid_pack_[0].GetNY());
    if (NumLevels <= MaxNumLevels) { //!!!nz
      std::cout << "Setting levels to " << NumLevels << "\n";
      num_levels_ = NumLevels;
    }
    else {
      std::cout << "Maximal number of levels reached, setting to maximum (" << NumLevels << ")\n";
      num_levels_ = MaxNumLevels;
    }
  } else {
      std::cout << "Could not set number of levels. " << grid_pack_.size() << " " << NumLevels << "\n";

  }

  return num_levels_;
}

//!might be removed later
void PoisSolverMultigrid::Reset()
{
  coarsest_solver_.Reset();
  grid_pack_.clear();
  vector_pack_.clear();
  num_levels_ = 0;
}


//EOF
