#include <iostream>
#include <cstdlib>
#include <climits>
#include <cmath>

#include "poissolver_utils.h"
#include "poissolver.h"

PoisSolverUtils::GridPack::GridPack(int input_verbosity_)
{
  nx_ = ny_ = nz_ = 0;
  offsety_ = offsetz_ = 0;
  periodicx_ = periodicy_ = periodicz_ = true;
  status_ = CONSTRUCTED;

  idx_strategy_ = 0;
  verbosity_ = input_verbosity_;
  printf("verbosity constructed %d\n", verbosity_);
}

PoisSolverUtils::GridPack::~GridPack()
{

}

bool PoisSolverUtils::GridPack::AllocateAndInit(size_t _nx, size_t _ny, size_t _nz)
{
  bool Answ = true;
  nx_ = _nx;
  ny_ = _ny;
  nz_ = _nz;

  offsety_ = nx_;
  offsetz_ = nx_*ny_;
  ntotal_ = nx_*ny_*nz_;

  try
  {
    Bitmap_.resize(ntotal_, 0);
    //NextToElectrode_.resize(ntotal_, 0);
    Equipotential_.resize(ntotal_, 0.0);

    status_ = UP2DATE;
  }
  catch (...)
  {
    Answ = false;
    status_ = ERROR;
  }

  return Answ;
}

void PoisSolverUtils::GridPack::SetElectrodeNode(innerfloat_t _Potential, size_t _ix, size_t _iy, size_t _iz)
{
  Bitmap_[_ix + _iy*offsety_ + _iz*offsetz_] = 1;
  Equipotential_[_ix + _iy*offsety_ + _iz*offsetz_] = _Potential;
  status_ = STALE;
};

void PoisSolverUtils::GridPack::SetEquipotentialAtNode(innerfloat_t _Potential, size_t _ix, size_t _iy, size_t _iz)
{
  Equipotential_[_ix + _iy*offsety_ + _iz*offsetz_] = _Potential;
}

void PoisSolverUtils::GridPack::UnsetElectrodeNode(size_t _ix, size_t _iy, size_t _iz)
{
  Bitmap_[_ix + _iy*offsety_ + _iz*offsetz_] = 0;
  status_ = STALE;
};

bool PoisSolverUtils::GridPack::SetPeriodicInDirection(int _iDir)
{
  bool Answ = false;
  if (GetNZ() <= 1)
  {
    //2D case
    if (_iDir == 0)
    {
      for(size_t j = 1; j < (ny_-1); j++)
      {
        UnsetElectrodeNode(0, j);
        UnsetElectrodeNode(nx_-1, j);
        periodicx_ = true;
      }
      //PoisSolverUtils::Dump2DVectorToFile(Bitmap_, GetNX(), GetNY(), "Bitmap-Periodic");//!!!
      Answ = true;
    }
    else
      if (_iDir == 1)
      {
        for(size_t i = 1; i < (nx_-1); i++)
        {
          UnsetElectrodeNode(i, 0);
          UnsetElectrodeNode(i, ny_-1);
          periodicy_ = true;
        }
        //PoisSolverUtils::Dump2DVectorToFile(Bitmap_, GetNX(), GetNY(), "Bitmap-Periodic");//!!!
        Answ = true;
      }

    //take care of corners
    if(periodicx_ && periodicy_)//for set negate both!!!
    {
      UnsetElectrodeNode(0   , 0   );
      UnsetElectrodeNode(nx_-1, 0   );
      UnsetElectrodeNode(0   , ny_-1);
      UnsetElectrodeNode(nx_-1, ny_-1);
    }
  }
  else
  {
    //3D case
    if (_iDir == 0)
    {
      for(size_t k = 1; k < (nz_-1); k++)
        for(size_t j = 1; j < (ny_-1); j++)
        {
          UnsetElectrodeNode(0, j, k);
          UnsetElectrodeNode(nx_-1, j, k);
          periodicx_ = true;
        }
      Answ = true;
    }
    else
      if (_iDir == 1)
      {
        for(size_t k = 1; k < (nz_-1); k++)
          for(size_t i = 1; i < (nx_-1); i++)
          {
            UnsetElectrodeNode(i, 0, k);
            UnsetElectrodeNode(i, ny_-1, k);
            periodicy_ = true;
          }
        Answ = true;
      }

    //take care of corners
    if(periodicx_ && periodicy_)//for set negate both!!!
    {
      for(size_t k = 1; k < (nz_-1); k++)
      {
        UnsetElectrodeNode(0    , 0    , k);
        UnsetElectrodeNode(nx_-1, 0    , k);
        UnsetElectrodeNode(0    , ny_-1, k);
        UnsetElectrodeNode(nx_-1, ny_-1, k);
      }
    }
  }
  return Answ;
}

bool PoisSolverUtils::GridPack::UnsetPeriodicInDirection(int _iDir, innerfloat_t _Potential1, innerfloat_t _Potential2)
{
  bool Answ = false;
  if (GetNZ() <= 1)
  {
    //2D case
    if (_iDir == 0)
    {
      for(size_t j = 1; j < (ny_-1); j++)
      {
        SetElectrodeNode(_Potential1, 0, j);
        SetElectrodeNode(_Potential2, nx_-1, j);
        periodicx_ = false;
      }
      Answ = true;
    }
    else
      if (_iDir == 1)
      {
        for(size_t i = 1; i < (nx_-1); i++)
        {
          SetElectrodeNode(_Potential1, i, 0);
          SetElectrodeNode(_Potential2, i, ny_-1);
          periodicy_ = false;
        }
        Answ = true;
      }

    //take care of corners
    if(!periodicx_ && !periodicy_)//for set negate both!!!
    {
      if (_iDir == 0)
      {
        SetElectrodeNode(_Potential1, 0   , 0   );
        SetElectrodeNode(_Potential1, nx_-1, 0   );
        SetElectrodeNode(_Potential2, 0   , ny_-1);
        SetElectrodeNode(_Potential2, nx_-1, ny_-1);
      }
      if (_iDir == 1)
      {
        SetElectrodeNode(_Potential1, 0   , 0   );
        SetElectrodeNode(_Potential2, nx_-1, 0   );
        SetElectrodeNode(_Potential1, 0   , ny_-1);
        SetElectrodeNode(_Potential2, nx_-1, ny_-1);
      }
    }
  }
  else
  {
    //3D case
    if (_iDir == 0)
    {
      for(size_t k = 0; k < nz_; k++)
        for(size_t j = 0; j < ny_; j++)
        {
          SetElectrodeNode(_Potential1, 0, j, k);
          SetElectrodeNode(_Potential2, nx_-1, j, k);
          periodicx_ = false;
        }
      Answ = true;
    }
    else
      if (_iDir == 1)
      {
        for(size_t k = 0; k < nz_; k++)
          for(size_t i = 0; i < nx_; i++)
          {
            SetElectrodeNode(_Potential1, i, 0, k);
            SetElectrodeNode(_Potential2, i, ny_-1, k);
            periodicy_ = false;
          }
        Answ = true;
      }
      else
        if (_iDir == 2)
        {
          for(size_t j = 0; j < ny_; j++)
            for(size_t i = 0; i < nx_; i++)
            {
              SetElectrodeNode(_Potential1, i, j, 0);
              SetElectrodeNode(_Potential2, i, j, nz_ -1);
              periodicz_ = false;
            }
          Answ = true;
        }
  }
  return Answ;
}

bool PoisSolverUtils::GridPack::RestrictElectrodesFromFiner(GridPack const &FinerGridPack)
{
  bool Answ = false;

  if(FinerGridPack.IsPeriodicX())
    periodicx_ = true;
  else
    periodicx_ = false;

  if(FinerGridPack.IsPeriodicY())
    periodicy_ = true;
  else
    periodicy_ = false;

  if(FinerGridPack.IsPeriodicZ())
    periodicz_ = true;
  else
    periodicz_ = false;

  //FillImplicitNextToElectrodes();

  if (GetNZ() <= 1)
  {
    //2D case
    for (int j = 0; j < GetNY(); j++)
    {
      int j_fine = 2*j;
      for (int i = 0; i < GetNX(); i++)
      {
        int i_fine = 2*i;
        int baseidx_coarse = i + j*GetNX();
        int baseidx_fine = i_fine + j_fine*FinerGridPack.GetNX();
        bitmap_t is_inside_electrode_coarse = IsInsideElectrode(baseidx_coarse);
        bitmap_t is_inside_electrode_fine = FinerGridPack.IsInsideElectrode(baseidx_fine);
        if (is_inside_electrode_coarse != is_inside_electrode_fine)
        {
          if (is_inside_electrode_fine)
            SetElectrodeNode(0.0, i, j);
          else
            UnsetElectrodeNode(i,j);
          Answ = true;
        }


        bool IsNextToElectrode = false;
        PoisSolverUtils::Stencil LocalStencil(false);
        PoisSolverUtils::Stencil NeighStencil(false);
        PoisSolverUtils::Stencil NewStencil(false);
        PoisSolverUtils::innerfloat_t coords[6] = {1.0, 1.0, 1.0, 1.0, 0.0, 0.0};//!2D
        FinerGridPack.GetNextToElectrode(baseidx_fine, false, LocalStencil);

        for (int i4 = 0; i4<4; i4++)
        {
          coords[i4] = LocalStencil.GetLength(i4);
          if (coords[i4] < 1.0)
          {
            coords[i4] *= 0.5;
            IsNextToElectrode = true;
          }
          else
          {
            int iNeighbour = -1;
            //xleft
            if (i4 == 0)
            {
              if (i == 0)
                iNeighbour = baseidx_fine + FinerGridPack.GetNX()-2;
              else
                iNeighbour = baseidx_fine -1;
            }

            //xright
            if (i4 == 1)
            {
              if (i == (GetNX() -1))
                iNeighbour = baseidx_fine - (FinerGridPack.GetNX()-2);
              else
                iNeighbour = baseidx_fine +1;
            }

            //yleft (down)
            if (i4 == 2)
            {
              if (j == 0)
                iNeighbour = baseidx_fine + FinerGridPack.GetNX()*(FinerGridPack.GetNY()-2);
              else
                iNeighbour = baseidx_fine - FinerGridPack.GetNX();
            }

            //yleft (down)
            if (i4 == 3)
            {
              if (j == (GetNY() -1))
                iNeighbour = baseidx_fine - FinerGridPack.GetNX()*(FinerGridPack.GetNY()-2);
              else
                iNeighbour = baseidx_fine + FinerGridPack.GetNX();
            }

            FinerGridPack.GetNextToElectrode(iNeighbour, false, NeighStencil);

            bool IsNextInThisDir = false;
            coords[i4] = NeighStencil.GetLength(i4);
            if (coords[i4] < 1.0)
            {
              coords[i4] *= 0.5;
              coords[i4] += 0.5;
              IsNextToElectrode = true;
              IsNextInThisDir = true;
            }

            if (!IsNextInThisDir)
            {
              if (FinerGridPack.IsInsideElectrode(iNeighbour))
              {
                coords[i4] = 0.5;
                IsNextToElectrode = true;
              }
            }
          }
        }

        if (IsInsideElectrode(baseidx_coarse))
          IsNextToElectrode = false;

        if (IsNextToElectrode)
        {
          NewStencil.Set(false, coords);
          //printf("idx %5d ", baseidx_coarse);
          //NewStencil.PrintStencil();
          SetNextToElectrode(baseidx_coarse, NewStencil);
        }
      }
    }
  }
  else
  {
    //3D case
    int fineoffsetz = FinerGridPack.GetNX()*FinerGridPack.GetNY();
    for (int k = 0; k < GetNZ(); k++)
    {
      int k_fine = 2*k;
      for (int j = 0; j < GetNY(); j++)
      {
        int j_fine = 2*j;
        for (int i = 0; i < GetNX(); i++)
        {
          int i_fine = 2*i;
          int baseidx_coarse = i + j*offsety_ + k*offsetz_;
          int baseidx_fine = i_fine + j_fine*FinerGridPack.GetNX() + k_fine*fineoffsetz;
          bitmap_t is_inside_electrode_coarse = IsInsideElectrode(baseidx_coarse);
          bitmap_t is_inside_electrode_fine = FinerGridPack.IsInsideElectrode(baseidx_fine);
          if (is_inside_electrode_coarse != is_inside_electrode_fine)
          {
            if (is_inside_electrode_fine)
              SetElectrodeNode(0.0, i, j, k);
            else
              UnsetElectrodeNode(i,j, k);
            Answ = true;
          }


          bool IsNextToElectrode = false;
          PoisSolverUtils::Stencil LocalStencil(true);
          PoisSolverUtils::Stencil NeighStencil(true);
          PoisSolverUtils::Stencil NewStencil(true);
          PoisSolverUtils::innerfloat_t coords[6] = {1.0, 1.0, 1.0, 1.0, 1.0, 1.0};//!3D
          FinerGridPack.GetNextToElectrode(baseidx_fine, true, LocalStencil);

          for (int i6 = 0; i6<6; i6++)
          {
            coords[i6] = LocalStencil.GetLength(i6);
            if (coords[i6] < 1.0)
            {
              coords[i6] *= 0.5;
              IsNextToElectrode = true;
            }
            else
            {
              int iNeighbour = -1;
              //xleft
              if (i6 == 0)
              {
                if (i == 0)
                  iNeighbour = baseidx_fine + FinerGridPack.GetNX()-2;
                else
                  iNeighbour = baseidx_fine -1;
              }

              //xright
              if (i6 == 1)
              {
                if (i == (GetNX() -1))
                  iNeighbour = baseidx_fine - (FinerGridPack.GetNX()-2);
                else
                  iNeighbour = baseidx_fine +1;
              }

              //yleft
              if (i6 == 2)
              {
                if (j == 0)
                  iNeighbour = baseidx_fine + FinerGridPack.GetNX()*(FinerGridPack.GetNY()-2);
                else
                  iNeighbour = baseidx_fine - FinerGridPack.GetNX();
              }

              //yright
              if (i6 == 3)
              {
                if (j == (GetNY() -1))
                  iNeighbour = baseidx_fine - FinerGridPack.GetNX()*(FinerGridPack.GetNY()-2);
                else
                  iNeighbour = baseidx_fine + FinerGridPack.GetNX();
              }

              //zleft (no periodicity considered)
              if (i6 == 4)
              {
                if (k > 0)
                  iNeighbour = baseidx_fine - fineoffsetz;
                else
                  continue;
              }

              //zright (no periodicity considered)
              if (i6 == 5)
              {
                if (k < (GetNZ() -1))
                  iNeighbour = baseidx_fine + fineoffsetz;
                else
                  continue;
              }

              FinerGridPack.GetNextToElectrode(iNeighbour, true, NeighStencil);

              bool IsNextInThisDir = false;
              coords[i6] = NeighStencil.GetLength(i6);
              if (coords[i6] < 1.0)
              {
                coords[i6] *= 0.5;
                coords[i6] += 0.5;
                IsNextToElectrode = true;
                IsNextInThisDir = true;
              }

              if (!IsNextInThisDir)
              {
                if (FinerGridPack.IsInsideElectrode(iNeighbour))
                {
                  coords[i6] = 0.5;
                  IsNextToElectrode = true;
                }
              }
            }
          }

          if (IsInsideElectrode(baseidx_coarse))
            IsNextToElectrode = false;

          if (IsNextToElectrode)
          {
            NewStencil.Set(true, coords);
            //printf("idx %5d ", baseidx_coarse);
            //NewStencil.PrintStencil();
            SetNextToElectrode(baseidx_coarse, NewStencil);
          }

        }
      }
    }
  }

  return Answ;
}

void PoisSolverUtils::GridPack::FillImplicitNextToElectrodes()
{
  if (GetNZ() <= 1)
  {
    //2D case
    Stencil simplestencil(false);

    for (size_t j = 1; j < (ny_-1); j++)
      for (size_t i = 1; i < (nx_-1); i++)
      {
        const size_t baseidx = i + j*nx_;
        bitmap_t status_principal_node = IsInsideElectrode(baseidx);
        if (!IsInsideElectrode(baseidx) && !IsNextToElectrode(baseidx))
        {
          bool IsNext = false;
          if (IsInsideElectrode(baseidx -1) != status_principal_node)
            IsNext = true;
          if (IsInsideElectrode(baseidx +1) != status_principal_node)
            IsNext = true;
          if (IsInsideElectrode(baseidx -offsety_) != status_principal_node)
            IsNext = true;
          if (IsInsideElectrode(baseidx +offsety_) != status_principal_node)
            IsNext = true;

          if(IsNext && (MapStencils_.count(baseidx) == 0))
            MapStencils_.insert(std::pair<int, Stencil>(baseidx, simplestencil));
        }
      }

    //periodic in x
    for (size_t j = 1; j < (ny_-1); j++)
    {
      const size_t baseidx = j*nx_;
      bitmap_t status_principal_node = IsInsideElectrode(baseidx);
      if (!IsInsideElectrode(baseidx) && !IsNextToElectrode(baseidx))
      {
        bool IsNext = false;
        if (IsInsideElectrode(baseidx +nx_-2) != status_principal_node)
          IsNext = true;
        if (IsInsideElectrode(baseidx +1) != status_principal_node)
          IsNext = true;
        if (IsInsideElectrode(baseidx -offsety_) != status_principal_node)
          IsNext = true;
        if (IsInsideElectrode(baseidx +offsety_) != status_principal_node)
          IsNext = true;

        if(IsNext && (MapStencils_.count(baseidx) == 0))
          MapStencils_.insert(std::pair<int, Stencil>(baseidx, simplestencil));
      }
    }

    //periodic in y
    for (size_t i = 1; i < (nx_-1); i++)
    {
      size_t &baseidx = i;
      bitmap_t status_principal_node = IsInsideElectrode(baseidx);
      if (!IsInsideElectrode(baseidx)  && !IsNextToElectrode(baseidx))
      {
        bool IsNext = false;
        if (IsInsideElectrode(baseidx -1) != status_principal_node)
          IsNext = true;
        if (IsInsideElectrode(baseidx +1) != status_principal_node)
          IsNext = true;
        if (IsInsideElectrode(baseidx +nx_*(ny_-2)) != status_principal_node)
          IsNext = true;
        if (IsInsideElectrode(baseidx +offsety_) != status_principal_node)
          IsNext = true;

        if(IsNext && (MapStencils_.count(baseidx) == 0))
          MapStencils_.insert(std::pair<int, Stencil>(baseidx, simplestencil));
       }
    }

  }
  else
  {
    //3D case
    Stencil simplestencil(true);

    for (size_t k = 1; k < (nz_-1); k++)
      for (size_t j = 1; j < (ny_-1); j++)
        for (size_t i = 1; i < (nx_-1); i++)
        {
          size_t baseidx = i + j*nx_ + k*offsetz_;
          bitmap_t status_principal_node = IsInsideElectrode(baseidx);
          if (!IsInsideElectrode(baseidx))
          {
            bool IsNext = false;
            if (IsInsideElectrode(baseidx -1) != status_principal_node)
              IsNext = true;
            if (IsInsideElectrode(baseidx +1) != status_principal_node)
              IsNext = true;
            if (IsInsideElectrode(baseidx -offsety_) != status_principal_node)
              IsNext = true;
            if (IsInsideElectrode(baseidx +offsety_) != status_principal_node)
              IsNext = true;
            if (IsInsideElectrode(baseidx -offsetz_) != status_principal_node)
              IsNext = true;
            if (IsInsideElectrode(baseidx +offsetz_) != status_principal_node)
              IsNext = true;

            if(IsNext && (MapStencils_.count(baseidx) == 0))
              MapStencils_.insert(std::pair<int, Stencil>(baseidx, simplestencil));
          }
        }

    //periodic in x
    for (size_t k = 1; k < (nz_-1); k++)
      for (size_t j = 1; j < (ny_-1); j++)
      {
        const size_t baseidx = j*nx_ + k*offsetz_;
        bitmap_t status_principal_node = IsInsideElectrode(baseidx);
        if (!IsInsideElectrode(baseidx))
        {
          bool IsNext = false;
          if (IsInsideElectrode(baseidx +nx_-2) != status_principal_node)
            IsNext = true;
          if (IsInsideElectrode(baseidx +1) != status_principal_node)
            IsNext = true;
          if (IsInsideElectrode(baseidx -offsety_) != status_principal_node)
            IsNext = true;
          if (IsInsideElectrode(baseidx +offsety_) != status_principal_node)
            IsNext = true;
          if (IsInsideElectrode(baseidx -offsetz_) != status_principal_node)
            IsNext = true;
          if (IsInsideElectrode(baseidx +offsetz_) != status_principal_node)
            IsNext = true;

          if(IsNext && (MapStencils_.count(baseidx) == 0))
            MapStencils_.insert(std::pair<int, Stencil>(baseidx, simplestencil));
        }
      }

    //periodic in y
    for (size_t k = 1; k < (nz_-1); k++)
      for (size_t i = 1; i < (nx_-1); i++)
      {
        const size_t baseidx = i + k*offsetz_;
        bitmap_t status_principal_node = IsInsideElectrode(baseidx);
        if (!IsInsideElectrode(baseidx))
        {
          bool IsNext = false;
          if (IsInsideElectrode(baseidx -1) != status_principal_node)
            IsNext = true;
          if (IsInsideElectrode(baseidx +1) != status_principal_node)
            IsNext = true;
          if (IsInsideElectrode(baseidx +nx_*(ny_-2)) != status_principal_node)
            IsNext = true;
          if (IsInsideElectrode(baseidx +offsety_) != status_principal_node)
            IsNext = true;
          if (IsInsideElectrode(baseidx -offsetz_) != status_principal_node)
            IsNext = true;
          if (IsInsideElectrode(baseidx +offsetz_) != status_principal_node)
            IsNext = true;

          if(IsNext && (MapStencils_.count(baseidx) == 0))
            MapStencils_.insert(std::pair<int, Stencil>(baseidx, simplestencil));
          }
      }

    //periodic in both x and y
    for (size_t k = 1; k < (nz_-1); k++)
    {
      const size_t baseidx = k*offsetz_;
      bitmap_t status_principal_node = IsInsideElectrode(baseidx);
      if (!IsInsideElectrode(baseidx))
      {
        bool IsNext = false;
        if (IsInsideElectrode(baseidx +nx_ -2) != status_principal_node)
          IsNext = true;
        if (IsInsideElectrode(baseidx +1) != status_principal_node)
          IsNext = true;
        if (IsInsideElectrode(baseidx +nx_*(ny_-2)) != status_principal_node)
          IsNext = true;
        if (IsInsideElectrode(baseidx +offsety_) != status_principal_node)
          IsNext = true;
        if (IsInsideElectrode(baseidx -offsetz_) != status_principal_node)
          IsNext = true;
        if (IsInsideElectrode(baseidx +offsetz_) != status_principal_node)
          IsNext = true;

        if(IsNext && (MapStencils_.count(baseidx) == 0))
          MapStencils_.insert(std::pair<int, Stencil>(baseidx, simplestencil));
      }
    }
  }

  //this->PrintMapStencils();
}

void PoisSolverUtils::GridPack::ConstructAdditionalNextToElectrodes()
{
  StencilsInside.Clear();
  StencilsPerX.Clear();
  StencilsPerY.Clear();
  StencilsPerCornerXY.Clear();

  PoisSolverUtils::StencilPack *Storage = NULL;
  bool Is3D = (GetNZ() > 1)? true : false;
  IndexedStencil stencil(Is3D);
  for (int k = 0; k < GetNZ(); k++)
    for (int j = 0; j < GetNY(); j++)
      for (int i = 0; i < GetNX(); i++)
      {
        const int baseidx = i + j*GetNX() + k*GetNX()*GetNY();
        stencil.Idx = baseidx;
        if (IsNextToElectrode(baseidx))
        {

          GetNextToElectrode(baseidx, Is3D, stencil);
          if(i == 0 && j == 0)
            Storage = &StencilsPerCornerXY;
          else
            if (i==0)
              Storage = &StencilsPerX;
            else
              if (j==0)
                Storage = &StencilsPerY;
              else
                Storage = &StencilsInside;

          if (Storage != NULL)
          {
            Storage->NextToElectrodeBoth.push_back(stencil);
            if (baseidx%2 == 0)
              Storage->NextToElectrodeEven.push_back(stencil);
            else
              Storage->NextToElectrodeOdd.push_back(stencil);
          }
        }
      }

}

void PoisSolverUtils::GridPack::CleanImplicitNextToElectrodes()
{
  bool Is3D = (GetNZ() > 1) ? true : false;
  std::map<int, Stencil> MapStencilsCopy;
  MapStencilsCopy.insert(MapStencils_.begin(),MapStencils_.end());
  MapStencils_.clear();
  //for (std::map<int, Stencil>::iterator itMap = MapStencils_.begin(); itMap != MapStencils_.end(); ++itMap)
  for (std::map<int, Stencil>::const_iterator itMap = MapStencilsCopy.begin(); itMap != MapStencilsCopy.end(); ++itMap)
  {
    if ((itMap->second).IsImplicit(Is3D))
    {
      //printf("Erasing stencil with idx %5d\n", itMap->first);
      //MapStencils_.erase(itMap);
    }
    else
    {
      //printf("Keeping stencil with idx %5d\n", itMap->first);
      //MapStencils_.insert(std::pair<int, Stencil>(baseidx, simplestencil));
      MapStencils_.insert(std::pair<int, Stencil>(itMap->first, itMap->second));
    }
  }
}

void PoisSolverUtils::GridPack::FinishEditing(int iLevel)
{
//  printf("Printing stencils before removing implicit\n");
//  PrintMapStencils();

  CleanImplicitNextToElectrodes();
  SortedIdxsNextToElectrode_.clear();
  SortedIdxsNextToElectrode_.reserve(MapStencils_.size());
  for (std::map<int, Stencil>::const_iterator itMap = MapStencils_.begin(); itMap != MapStencils_.end(); ++itMap)
    SortedIdxsNextToElectrode_.push_back(itMap->first);

  ConstructAdditionalNextToElectrodes();

    if(verbosity_ >= PoisSolver::VERBOSITY_VERBOSE_INIT)
  {

    printf("----------------------------------------------------------------------------\n");
    printf("Verbosity %d\n", verbosity_);
    printf("Level %d, number of NextToElectrode %d\n", iLevel, (int)MapStencils_.size());
    printf("Stencils inside the region (level %d)\n", iLevel);
    StencilsInside.PrintStencils(this);
    printf("Stencils used in periodic X (level %d)\n", iLevel);
    StencilsPerX.PrintStencils(this);
    printf("Stencils used in periodic Y (level %d)\n", iLevel);
    StencilsPerY.PrintStencils(this);
    printf("Stencils used in corners with periodic X and Y (level %d)\n", iLevel);
    StencilsPerCornerXY.PrintStencils(this);
    printf("----------------------------------------------------------------------------\n");
  }
}

void PoisSolverUtils::GridPack::Copy(PoisSolverUtils::GridPack &GP)
{
  status_ = GP.status_;

  nx_ = GP.nx_;
  ny_ = GP.ny_;
  nz_ = GP.nz_;
  ntotal_ = GP.ntotal_;
  offsety_ = GP.offsety_;
  offsetz_ = GP.offsetz_;

  periodicx_ = GP.periodicx_;
  periodicy_ = GP.periodicy_;
  periodicz_ = GP.periodicz_;

  Bitmap_.clear();
  Bitmap_.insert(Bitmap_.begin(), GP.Bitmap_.begin(), GP.Bitmap_.end());
  Equipotential_.clear();
  Equipotential_.insert(Equipotential_.begin(), GP.Equipotential_.begin(), GP.Equipotential_.end());
  MapStencils_.clear();
  MapStencils_.insert(GP.MapStencils_.begin(), GP.MapStencils_.end());

  SortedIdxsNextToElectrode_.clear();
  SortedIdxsNextToElectrode_.insert(SortedIdxsNextToElectrode_.begin(), GP.SortedIdxsNextToElectrode_.begin(), GP.SortedIdxsNextToElectrode_.end());

  StencilsInside = GP.GetStencilsInside();
  StencilsPerX = GP.GetStencilsPerX();
  StencilsPerY = GP.GetStencilsPerY();
  StencilsPerCornerXY = GP.GetStencilsPerCornerXY();
}


void PoisSolverUtils::GridPack::OverwriteWithElectrodes(float *vec_input) const
{
  PoisSolverUtils::OverwriteWithElectrodes(vec_input, Equipotential_, Bitmap_, idx_strategy_);
}

void PoisSolverUtils::GridPack::OverwriteWithElectrodes(double *vec_input) const
{
  PoisSolverUtils::OverwriteWithElectrodes(vec_input, Equipotential_, Bitmap_, idx_strategy_);
}


void PoisSolverUtils::GridPack::NullifyWithElectrodes(innerfloat_t  *vec_input) const
{
  for(int baseidx = 0; baseidx< GetNTotal(); baseidx++)
  {
    if (IsInsideElectrode(baseidx))
      vec_input[baseidx] = 0.0;
  }
}

void PoisSolverUtils::GridPack::NullifyNextToElectrodes(innerfloat_t  *vec_input) const
{
  const int NNextToElectrode = int(SortedIdxsNextToElectrode_.size());
  for(int i = 0; i< NNextToElectrode; i++)
    vec_input[SortedIdxsNextToElectrode_[i]] = 0.0;
}

void PoisSolverUtils::GridPack::GetNextToElectrode(size_t baseidx, bool Is3D, Stencil &stencil) const
{
  std::map<int, Stencil>::const_iterator it = MapStencils_.find(baseidx);
  if (it != MapStencils_.end())
    stencil = it->second;
  else
    stencil.Reset(Is3D);
}


////////////////////////////////// PoisSolverUtils::VectorPack //////////////////////////
bool PoisSolverUtils::VectorPack::AllocateAndInit(size_t nx, size_t ny, size_t nz)
{
  bool Answ = true;
  size_t n_total = nx*ny*nz;
  try
  {
    vec_temp_.resize(n_total, 0.0);
    vec_update_.resize(n_total, 0.0);
    vec_resid_.resize(n_total, 0.0);
  }
  catch (...)
  {
    Answ = false;
  }

  return Answ;
}


////////////////////////////////// PoisSolverUtils general ////////////////////////////////////

PoisSolverUtils::Stencil::Stencil(bool Is3D)
{
  Reset(Is3D);
}

PoisSolverUtils::Stencil::~Stencil()
{}

void PoisSolverUtils::Stencil::Set(bool Is3D, innerfloat_t coefs[6])
{
//  for (int i6 = 0; i6 < 6; i6++)
//    coefs_[i6] = coefs[i6];
  length0 = coefs[0];
  length1 = coefs[1];
  length2 = coefs[2];
  length3 = coefs[3];

  if (Is3D)
  {
    length4 = coefs[4];
    length5 = coefs[5];
  }
  else
  {
    length4 = 0.0;
    length5 = 0.0;
  }
  SetX(Is3D, coefs[0], coefs[1]);
  SetY(Is3D, coefs[2], coefs[3]);
  SetZ(Is3D, coefs[4], coefs[5]);
}

void PoisSolverUtils::Stencil::Reset(bool Is3D)
{
  xl_ = xh_ = yl_ = yh_ = 1.0;
  if (Is3D)
  {
    zl_ = zh_ = 1.0;
    length4 = length5 = 1.0;
    center_ = -6.0;
  }
  else
  {
    zl_ = zh_ = 0.0;
    length4 = length5 = 0.0;
    center_ = -4.0;
  }
  length0 = 1.0;
  length1 = 1.0;
  length2 = 1.0;
  length3 = 1.0;
}

void PoisSolverUtils::StencilPack::PrintStencils (PoisSolverUtils::GridPack* const GP)
{
  printf("Even\n");
  bool Is3D = (GP->GetNZ() > 1) ? true : false;
  for (size_t i = 0; i < NextToElectrodeEven.size(); i++)
  {
    const int baseidx = NextToElectrodeEven[i].Idx;
    printf("%6d = %3d x %3d x %5d\t", baseidx, GP->GetXfromBaseidx(baseidx), GP->GetYfromBaseidx(baseidx), GP->GetZfromBaseidx(baseidx));
    NextToElectrodeEven[i].PrintStencil(Is3D);
  }
  printf("Odd\n");
  for (size_t i = 0; i < NextToElectrodeOdd.size(); i++)
  {
    const int baseidx = NextToElectrodeOdd[i].Idx;
    printf("%6d = %3d x %3d x %5d\t", baseidx, GP->GetXfromBaseidx(baseidx), GP->GetYfromBaseidx(baseidx), GP->GetZfromBaseidx(baseidx));
    NextToElectrodeOdd[i].PrintStencil(Is3D);
  }
  printf("Both\n");
  for (size_t i = 0; i < NextToElectrodeBoth.size(); i++)
  {
    const int baseidx = NextToElectrodeBoth[i].Idx;
    printf("%6d = %3d x %3d x %5d\t", baseidx, GP->GetXfromBaseidx(baseidx), GP->GetYfromBaseidx(baseidx), GP->GetZfromBaseidx(baseidx));
    NextToElectrodeBoth[i].PrintStencil(Is3D);
  }
}

//EOF
