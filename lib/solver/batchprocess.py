#!/usr/bin/python
import os
#import pylab
import subprocess
#from matplotlib.ticker import MultipleLocator



listWorkFiles = []
listAllFiles = os.listdir(".")
listAllFiles.sort()
for filename in listAllFiles:
	if filename[-4:] == ".rmt":
		#print filename
		listWorkFiles.append(filename)

counter = 1

for filenameInput in listWorkFiles:
	print "[" + str(counter) + "/" + str(len(listWorkFiles)) + "]: " + filenameInput
	counter += 1
	subprocess.call(["./processsinglemtx.py " + filenameInput], shell=True)
