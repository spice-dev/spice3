#!/bin/sh

rm -f *.o

set -o verbose

CC=icpc

UFDIR=/home/podolnik/Coding/others/uf


$CC -C -g -O3 -vec -vec-report1 -xSSE4.2 -qopenmp -g -Wall  -I../include -I$UFDIR/UFconfig -I$UFDIR/AMD/Include -I$UFDIR/UMFPACK/Include wrapper.c -c -o wrapper.o 
$CC -C -g -O3 -vec -vec-report1 -xSSE4.2 -qopenmp -g -Wall  -I../include -I$UFDIR/UFconfig -I$UFDIR/AMD/Include -I$UFDIR/UMFPACK/Include cusewrapper.c -c -o cusewrapper.o

#$CC cusewrapper.o wrapper.o ../lib/libpoissolver.a -o cusewrapper -L/home/komm/pic/ -Wl,-Bdynamic -lm -lumfpack -lamd -lgoto -lpthread 