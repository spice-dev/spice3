#!/bin/bash

source ../../config/config.sh

# compile the umfpack library

cd UMFPACK
make clean

# make the library and fortran interface
make all
make fortran
make fortran64
