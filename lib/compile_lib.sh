#!/bin/bash
# Compile all necessary libraries

source ../config/config.sh
set -o verbose

LIB_NAME=clock
cd $LIB_NAME
./compile_$LIB_NAME.sh
cd $SPICE3_INSTALL_DIR/lib

LIB_NAME=f2c
cd $LIB_NAME
./compile_$LIB_NAME.sh
cd $SPICE3_INSTALL_DIR/lib

LIB_NAME=UF
cd $LIB_NAME
./compile_$LIB_NAME.sh
cd $SPICE3_INSTALL_DIR/lib

LIB_NAME=solver
cd $LIB_NAME
./compile_$LIB_NAME.sh
cd $SPICE3_INSTALL_DIR/lib

LIB_NAME=matio
cd $LIB_NAME
./compile_$LIB_NAME.sh
cd $SPICE3_INSTALL_DIR/lib

