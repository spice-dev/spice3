#!/bin/bash

source ../config/config.sh

BIN_DIR=$SPICE3_INSTALL_DIR/bin
TEST_DIR=$SPICE3_INSTALL_DIR/test
DATA_DIR=$TEST_DIR/data

mkdir -p $DATA_DIR

INPUT_FILE=$TEST_DIR/test.inp

# we have to have unlimited stack size (or rewrite the code completely...)
ulimit -s unlimited

ldd $BIN_DIR/spice3-0.5.bin

# runs the simulation at 16 cpus
# -i location of the input file (there is a character limit inside the code!)
# -v verbose execution (use -d for a lot of more messages)
# -s limits the execution for x time steps (do not use this option if you want the simulation to complete)
# -t, -o path to output files

cd $BIN_DIR
CMD="mpirun --outfile-pattern=$DATA_DIR/test-%r.log -np 16 $BIN_DIR/spice3-0.5.bin -i $INPUT_FILE -v -s 21 -t $DATA_DIR/test-t -o $DATA_DIR/test-o"
echo $CMD
eval $CMD

# stitches together some output files
# variable Pot contains the potential
CMD="$BIN_DIR/stitcher.bin -i $INPUT_FILE -v -d -t $DATA_DIR/test-t"
echo $CMD
eval $CMD





