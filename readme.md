# SPICE3

This is a SPICE3 code repository.

SPICE3 is a particle-in-cell plasma model, more can be found in various publications. [citation-needed]

## Dependencies

SPICE 3 (./src) requires:

- Clock library (./lib/clock) for timekeepiing.
- F2C library (./lib/f2c) for Fortran/C interface.
- Matio (./lib/matio) library for MATLAB file I/O.
- Solver (./lib/solver) library containing the Poisson equation solver. This library requires:
    - UMFPACK sparse matrix solver (./lib/UF/UMFPACK). UMFPACK requires:
        - AMD matrix sorting (./lib/UF/AMD)
        - BLAS (system installation)
        - LAPACK (system installation)

Some other libraries are required as well, see compiler options in ./compile.sh (-lz, -lstdc++ etc.).

## Input files

SPICE3 is controlled via an input file, example of a such file is in ./test/test.inp.

Otherwise, distribution functions are needed (.dat files in ./bin and ./bin/DF directories).

## Source files

Main source file of the model is pic-0.5.f90.

## Installation

### Configuration

- Clone this repository.
- Look inside the config/examples directory for an example config.sh file.
- Modify SPICE3_INSTALL_DIR to the path you have cloned this repository to.
    - In case you want to use different compilers, modify those as well, including appropriate module references.
- Create a file named config.sh in ./config directory using the example file. For installation at Marconi, only the installation directory is to be changed.

All appropriate configuration files in ./lib directory and subdirectories use environment variables defined in ./config/config.sh. If a modification is needed, keep in mind that nothing in ./lib directory should be hard-coded -- you should introduce a new environment variable defined in the ./config/config.sh and use it on appropriate place. If you do so, you should either modify the example config.sh or create a new one.

### Building the code

- Run ./lib/compile_lib.sh
    - This compiles all necessary libraries for further use with SPICE3.
- Run ./compile.sh
    - Compiles the model itself and the MATLAB output stitching utility.
- Run ./test/run-test.sh
    - Runs a sample simulation on a small simulation box and stitches the output to one file using the stitching utility.
    - Simulation is set to run for a limited number of steps (20)

### Output

Matlab files are created in ./test/data directory. To review the potential calculated by the solver, one should open the file test-t.mat (this is a product of the stitching utility) and search for the variable Pot.

Example Matlab code for reviewing the potential profile at half of the box:

```
load('test-t.mat', 'Pot', 'Ny')
imagesc(squeeze(Pot(:,Ny/2,:)))
```

## Poisson equation solver

Current solver is:

- Multigrid
- Serial, running at process 0
- Memory limiting
- Provided as a library in ./lib/solver

Disregarding particular features of the current solver, the variables (3D matrices) needed to solve the Poisson equation are:

- poiss_flag(Nx, Ny, Nz): electrodes map for constructing the matrix
- equipot(Nx, Ny, Nz): potential for grid points at fixed potential
- rho_tot_total(Nx, Ny, Nz): density at grid points at calculated potential
- Pot_total(Nx, Ny, Nz): resulting potential used by the model in further calculations

Variables equipot and rho_tot_total should be merged into one that reflects the right hand side of the Poisson equation using the matrix poiss_flag. Poisson equation matrix is constructed from poiss_flag (1 == electrode, 0 == plasma). In the current implementation, only rho_tot_total is provided at each step, since the solver is pre-configured with equipot.

All code parts employing the Poisson equation solver are marked by `! POISSON SOLVER UTILITY` comment.

