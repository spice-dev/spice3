


subroutine grid_particles(x,y,z,dx,dy,dz,no_tot,A1,A2,A3,A4,A5,A6,A7,A8,ix,iy,iz,model)
    implicit none
    include 'struct.h'
    integer:: no_tot
    real:: dx,dy,dz,rx,ry,rz,V
    real, dimension(no_tot):: x,y,z,A1,A2,A3,A4,A5,A6,A7,A8
    integer, dimension(no_tot):: ix,iy,iz
    integer:: i
    type(model_t):: model


    if (dx.eq.1.0.and.dy.eq.1.0.and.dz.eq.1.0) then
        ! fast gridding for nice cells
        !1 fully vectorized run
        ! need to use floor to qvoid problems with particles of negative z
        ix = floor(x)
        iy = floor(y)
        iz = floor(z)
        !now the weights
        ! here we do looping
        do i=1,no_tot
            ! only grid particles
            rx = x(i) - ix(i)
            ry = y(i) - iy(i)
            rz = z(i) - iz(i)
            A1(i) = (dx - rx)*(dy - ry)*(dz - rz)
            A2(i) = (dx - rx)*ry*(dz - rz)
            A3(i) = rx*ry*(dz - rz)
            A4(i) = rx*(dy - ry)*(dz - rz)
            A5(i) = (dx - rx)*(dy - ry)*rz
            A6(i) = (dx - rx)*ry*rz
            A7(i) = rx*ry*rz
            A8(i) = rx*(dy - ry)*rz
        end do
        ! add 1 to ix coords
        ix = ix +1
        iy = iy +1
        iz = iz +1

    else
        ! slow gridding for not-quite-as-nice cells
        !1 fully vectorized run
        ix = floor(x/dx)
        iy = floor(y/dy)
        iz = floor(z/dz)
        ! here we do looping
        V = dx*dy*dz
        do i=1,no_tot
            rx = x(i)/dx - ix(i)
            ry = y(i)/dy - iy(i)
            rz = z(i)/dz - iz(i)

            A1(i) = (dx - rx)*(dy - ry)*(dz - rz)/V
            A2(i) = (dx - rx)*ry*(dz - rz)/V
            A3(i) = rx*ry*(dz - rz)/V
            A4(i) = rx*(dy - ry)*(dz - rz)/V
            A5(i) = (dx - rx)*(dy - ry)*rz/V
            A6(i) = (dx - rx)*ry*rz/V
            A7(i) = rx*ry*rz/V
            A8(i) = rx*(dy - ry)*rz/V
        end do
        ! add 1 to ix coords
        ix = ix +1
        iy = iy +1
        iz = iz +1



    end if

end subroutine


subroutine weight_particles(x,y,z,ux,uy,uz,stype,rho,no_tot,Nx,Ny,Nz,no_species,q,objects,model,&
        border_matrix,dx,dy,dz,vx,vy,vz,vx2,vy2,vz2,vx3,vy3,vz3,take_weight_diag,proc_no,proc_max,slices,use_moments)
    include 'struct.h'
    integer:: no_tot,i,Nx,Ny,Nz,no_species,sp,j,proc_no,proc_max
    real, dimension(no_tot):: x,y,z,ux,uy,uz
    real:: A1,A2,A3,A4,A5,A6,A7,A8,dx,dy,dz,rx,ry,rz
    integer, dimension(no_tot):: stype
    integer, dimension(Nx,Ny,Nz):: objects
    real, dimension(Nx,Ny,Nz):: border_matrix
    integer:: ix1,iy1,iz1,ix,iy,iz

    real, dimension(no_species,Nx,Ny,Nz):: rho,vx,vy,vz,vx2,vy2,vz2,vx3,vy3,vz3

    real, dimension(no_species):: q
    real:: lower_boundary
    logical::take_weight_diag,use_moments
    type(model_t):: model
    type(slice_info),dimension(proc_max+1):: slices
    ! construct the charge density
    ! first, zero it out
    rho = 0.0

    ! zero out diagnostic matrices if necessary
    if (take_weight_diag) then
        vx = 0.0
        vy = 0.0
        vz = 0.0
        if (use_moments) then
            vx2 = 0.0
            vy2 = 0.0
            vz2 = 0.0
            vx3 = 0.0
            vy3 = 0.0
            vz3 = 0.0
        end if
    end if
    ! if (model%debug) then
    !   write(*,*) 'Received boundaries',Nx,Ny,Nz
    !   write(*,*) 'Testing z',max(0.0,(model%Lz_grid - model%Lz_start))
    ! end if
    ! write(*,*) 'Grid Zlimit',max(0.0,(model%Lz_grid - model%Lz_start))
    do i=1,no_tot
        sp = stype(i)
        ! if (sp.lt.1.or.sp.gt.no_species) then
        !   write(*,*) 'Error: stype out of range'
        !   write (*,*) 'Pos',x(i),y(i),z(i)
        !   write (*,*) 'Vel',ux(i),uy(i),uz(i)
        !   write	(*,*) 'Stype',sp
        !   stop
        !
        ! end if

        ! calculate coefficients
        if (z(i).lt.max(0.0,(model%Lz_grid - model%Lz_start)).and.z(i).gt.0.0) then
            ix = int(x(i)/dx)
            iy = int(y(i)/dy)
            iz = int(z(i)/dz)
            rx = x(i)/dx - real(ix)
            ry = y(i)/dy - real(iy)
            rz = z(i)/dz - real(iz)
            A1 = (1.0 - rx)*(1.0 - ry)*(1.0 - rz)
            A2 = (1.0 - rx)*ry*(1.0 - rz)
            A3 = rx*ry*(1.0 - rz)
            A4 = rx*(1.0 - ry)*(1.0 - rz)
            A5 = (1.0 - rx)*(1.0 - ry)*rz
            A6 = (1.0 - rx)*ry*rz
            A7 = rx*ry*rz
            A8 = rx*(1.0 - ry)*rz
            ix = ix + 1
            iy = iy + 1
            iz = iz + 1
            if (model%debug)  then
                if (ix.lt.1.or.iy.lt.1.or.iz.lt.1.or.ix.gt.Nx.or.iy.gt.Ny.or.iz.gt.Nz) then

                    write(*,*) 'wrong particle',ix,iy,iz
                    write(*,*) 'Pos',x(i),y(i),z(i)
                    write(*,*) 'Vel',ux(i),uy(i),uz(i)
                    write(*,*) 'Pos',stype(i)
                end if
            end if

            rho(sp,ix,iy,iz) =rho(sp,ix,iy,iz) + q(sp)*A1
            rho(sp,ix,iy +1,iz) =rho(sp,ix,iy+1,iz) + q(sp)*A2
            rho(sp,ix+1,iy +1,iz) =rho(sp,ix+1,iy+1,iz) + q(sp)*A3
            rho(sp,ix+1,iy ,iz) =rho(sp,ix+1,iy,iz) + q(sp)*A4
            rho(sp,ix,iy,iz+1) =rho(sp,ix,iy,iz+1) + q(sp)*A5
            rho(sp,ix,iy +1,iz+1) =rho(sp,ix,iy+1,iz+1) + q(sp)*A6
            rho(sp,ix+1,iy +1,iz+1) =rho(sp,ix+1,iy+1,iz+1) + q(sp)*A7
            rho(sp,ix+1,iy ,iz+1) =rho(sp,ix+1,iy,iz+1) + q(sp)*A8
            if (take_weight_diag) then
                vx(sp,ix,iy,iz) =vx(sp,ix,iy,iz) + ux(i)*A1
                vx(sp,ix,iy +1,iz) =vx(sp,ix,iy+1,iz) + ux(i)*A2
                vx(sp,ix+1,iy +1,iz) =vx(sp,ix+1,iy+1,iz) + ux(i)*A3
                vx(sp,ix+1,iy ,iz) =vx(sp,ix+1,iy,iz) + ux(i)*A4
                vx(sp,ix,iy,iz+1) =vx(sp,ix,iy,iz+1) + ux(i)*A5
                vx(sp,ix,iy +1,iz+1) =vx(sp,ix,iy+1,iz+1) + ux(i)*A6
                vx(sp,ix+1,iy +1,iz+1) =vx(sp,ix+1,iy+1,iz+1) + ux(i)*A7
                vx(sp,ix+1,iy ,iz+1) =vx(sp,ix+1,iy,iz+1) + ux(i)*A8

                vy(sp,ix,iy,iz) =vy(sp,ix,iy,iz) + uy(i)*A1
                vy(sp,ix,iy +1,iz) =vy(sp,ix,iy+1,iz) + uy(i)*A2
                vy(sp,ix+1,iy +1,iz) =vy(sp,ix+1,iy+1,iz) + uy(i)*A3
                vy(sp,ix+1,iy ,iz) =vy(sp,ix+1,iy,iz) + uy(i)*A4
                vy(sp,ix,iy,iz+1) =vy(sp,ix,iy,iz+1) + uy(i)*A5
                vy(sp,ix,iy +1,iz+1) =vy(sp,ix,iy+1,iz+1) + uy(i)*A6
                vy(sp,ix+1,iy +1,iz+1) =vy(sp,ix+1,iy+1,iz+1) + uy(i)*A7
                vy(sp,ix+1,iy ,iz+1) =vy(sp,ix+1,iy,iz+1) + uy(i)*A8

                vz(sp,ix,iy,iz) =vz(sp,ix,iy,iz) + uz(i)*A1
                vz(sp,ix,iy +1,iz) =vz(sp,ix,iy+1,iz) + uz(i)*A2
                vz(sp,ix+1,iy +1,iz) =vz(sp,ix+1,iy+1,iz) + uz(i)*A3
                vz(sp,ix+1,iy ,iz) =vz(sp,ix+1,iy,iz) + uz(i)*A4
                vz(sp,ix,iy,iz+1) =vz(sp,ix,iy,iz+1) + uz(i)*A5
                vz(sp,ix,iy +1,iz+1) =vz(sp,ix,iy+1,iz+1) + uz(i)*A6
                vz(sp,ix+1,iy +1,iz+1) =vz(sp,ix+1,iy+1,iz+1) + uz(i)*A7
                vz(sp,ix+1,iy ,iz+1) =vz(sp,ix+1,iy,iz+1) + uz(i)*A8
                ! second order
                if (use_moments) then
                    vx2(sp,ix,iy,iz) =vx2(sp,ix,iy,iz) + ux(i)*ux(i)*A1
                    vx2(sp,ix,iy +1,iz) =vx2(sp,ix,iy+1,iz) + ux(i)*ux(i)*A2
                    vx2(sp,ix+1,iy +1,iz) =vx2(sp,ix+1,iy+1,iz) + ux(i)*ux(i)*A3
                    vx2(sp,ix+1,iy ,iz) =vx2(sp,ix+1,iy,iz) + ux(i)*ux(i)*A4
                    vx2(sp,ix,iy,iz+1) =vx2(sp,ix,iy,iz+1) + ux(i)*ux(i)*A5
                    vx2(sp,ix,iy +1,iz+1) =vx2(sp,ix,iy+1,iz+1) + ux(i)*ux(i)*A6
                    vx2(sp,ix+1,iy +1,iz+1) =vx2(sp,ix+1,iy+1,iz+1) + ux(i)*ux(i)*A7
                    vx2(sp,ix+1,iy ,iz+1) =vx2(sp,ix+1,iy,iz+1) + ux(i)*ux(i)*A8

                    vy2(sp,ix,iy,iz) =vy2(sp,ix,iy,iz) + uy(i)*uy(i)*A1
                    vy2(sp,ix,iy +1,iz) =vy2(sp,ix,iy+1,iz) + uy(i)*uy(i)*A2
                    vy2(sp,ix+1,iy +1,iz) =vy2(sp,ix+1,iy+1,iz) + uy(i)*uy(i)*A3
                    vy2(sp,ix+1,iy ,iz) =vy2(sp,ix+1,iy,iz) + uy(i)*uy(i)*A4
                    vy2(sp,ix,iy,iz+1) =vy2(sp,ix,iy,iz+1) + uy(i)*uy(i)*A5
                    vy2(sp,ix,iy +1,iz+1) =vy2(sp,ix,iy+1,iz+1) + uy(i)*uy(i)*A6
                    vy2(sp,ix+1,iy +1,iz+1) =vy2(sp,ix+1,iy+1,iz+1) + uy(i)*uy(i)*A7
                    vy2(sp,ix+1,iy ,iz+1) =vy2(sp,ix+1,iy,iz+1) + uy(i)*uy(i)*A8

                    vz2(sp,ix,iy,iz) =vz2(sp,ix,iy,iz) + uz(i)*uz(i)*A1
                    vz2(sp,ix,iy +1,iz) =vz2(sp,ix,iy+1,iz) + uz(i)*uz(i)*A2
                    vz2(sp,ix+1,iy +1,iz) =vz2(sp,ix+1,iy+1,iz) + uz(i)*uz(i)*A3
                    vz2(sp,ix+1,iy ,iz) =vz2(sp,ix+1,iy,iz) + uz(i)*uz(i)*A4
                    vz2(sp,ix,iy,iz+1) =vz2(sp,ix,iy,iz+1) + uz(i)*uz(i)*A5
                    vz2(sp,ix,iy +1,iz+1) =vz2(sp,ix,iy+1,iz+1) + uz(i)*uz(i)*A6
                    vz2(sp,ix+1,iy +1,iz+1) =vz2(sp,ix+1,iy+1,iz+1) + uz(i)*uz(i)*A7
                    vz2(sp,ix+1,iy ,iz+1) =vz2(sp,ix+1,iy,iz+1) + uz(i)*uz(i)*A8
                    ! third order

                    vx3(sp,ix,iy,iz) =vx3(sp,ix,iy,iz) + ux(i)*ux(i)*A1
                    vx3(sp,ix,iy +1,iz) =vx3(sp,ix,iy+1,iz) + ux(i)*ux(i)*ux(i)*A2
                    vx3(sp,ix+1,iy +1,iz) =vx3(sp,ix+1,iy+1,iz) + ux(i)*ux(i)*ux(i)*A3
                    vx3(sp,ix+1,iy ,iz) =vx3(sp,ix+1,iy,iz) + ux(i)*ux(i)*ux(i)*A4
                    vx3(sp,ix,iy,iz+1) =vx3(sp,ix,iy,iz+1) + ux(i)*ux(i)*ux(i)*A5
                    vx3(sp,ix,iy +1,iz+1) =vx3(sp,ix,iy+1,iz+1) + ux(i)*ux(i)*ux(i)*A6
                    vx3(sp,ix+1,iy +1,iz+1) =vx3(sp,ix+1,iy+1,iz+1) + ux(i)*ux(i)*ux(i)*A7
                    vx3(sp,ix+1,iy ,iz+1) =vx3(sp,ix+1,iy,iz+1) + ux(i)*ux(i)*ux(i)*A8

                    vy3(sp,ix,iy,iz) =vy3(sp,ix,iy,iz) + uy(i)*uy(i)*uy(i)*A1
                    vy3(sp,ix,iy +1,iz) =vy3(sp,ix,iy+1,iz) + uy(i)*uy(i)*uy(i)*A2
                    vy3(sp,ix+1,iy +1,iz) =vy3(sp,ix+1,iy+1,iz) + uy(i)*uy(i)*uy(i)*A3
                    vy3(sp,ix+1,iy ,iz) =vy3(sp,ix+1,iy,iz) + uy(i)*uy(i)*uy(i)*A4
                    vy3(sp,ix,iy,iz+1) =vy3(sp,ix,iy,iz+1) + uy(i)*uy(i)*uy(i)*A5
                    vy3(sp,ix,iy +1,iz+1) =vy3(sp,ix,iy+1,iz+1) + uy(i)*uy(i)*uy(i)*A6
                    vy3(sp,ix+1,iy +1,iz+1) =vy3(sp,ix+1,iy+1,iz+1) + uy(i)*uy(i)*uy(i)*A7
                    vy3(sp,ix+1,iy ,iz+1) =vy3(sp,ix+1,iy,iz+1) + uy(i)*uy(i)*uy(i)*A8

                    vz3(sp,ix,iy,iz) =vz3(sp,ix,iy,iz) + uz(i)*uz(i)*uz(i)*A1
                    vz3(sp,ix,iy +1,iz) =vz3(sp,ix,iy+1,iz) + uz(i)*uz(i)*uz(i)*A2
                    vz3(sp,ix+1,iy +1,iz) =vz3(sp,ix+1,iy+1,iz) + uz(i)*uz(i)*uz(i)*A3
                    vz3(sp,ix+1,iy ,iz) =vz3(sp,ix+1,iy,iz) + uz(i)*uz(i)*uz(i)*A4
                    vz3(sp,ix,iy,iz+1) =vz3(sp,ix,iy,iz+1) + uz(i)*uz(i)*uz(i)*A5
                    vz3(sp,ix,iy +1,iz+1) =vz3(sp,ix,iy+1,iz+1) + uz(i)*uz(i)*uz(i)*A6
                    vz3(sp,ix+1,iy +1,iz+1) =vz3(sp,ix+1,iy+1,iz+1) + uz(i)*uz(i)*uz(i)*A7
                    vz3(sp,ix+1,iy ,iz+1) =vz3(sp,ix+1,iy,iz+1) + uz(i)*uz(i)*uz(i)*A8
                end if


            end if
        else
            !    iz = iz + 1
            !    iy = iy + 1
            !    iz = iz + 1
            ! write(*,*)'Weird partcle',x(i),y(i),z(i)
            ! stop
        end if


    end do
    if (model%debug) then
        write(*,*) 'Done loop in weight'
    end if
    ! fix periodic boundaries
    !needs to be supressed for now
    call fix_periodic_r(slices,proc_no,proc_max,Nx,Ny,Nz,rho,no_species,model)



    !         rho(:,1:Nx,1,1:Nz)=(rho(:,1:Nx,1,1:Nz)+rho(:,1:Nx,Ny,1:Nz))   !PC
    !         rho(:,1,1:Ny,1:Nz)=(rho(:,1,1:Ny,1:Nz)+rho(:,Nx,1:Ny,1:Nz))   !PC
    !        rho(:,1:Nx,Ny,1:Nz)=(rho(:,1:Nx,1,1:Nz))   !PC
    !         rho(:,Nx,1:Ny,1:Nz)=(rho(:,1,1:Ny,1:Nz))   !PC
    ! ! top boundary --assume to be free
    !         rho(:,1:Nx,1:Ny,Nz)=rho(:,1:Nx,1:Ny,Nz)*2.0

    !      do j=1,Nz
    !         rho(:,:,1,j)=(rho(:,:,1,j)+rho(:,:,Ny,j))   !PC
    !         rho(:,1,j,:)=(rho(:,1,j,:)+rho(:,Nx,j,:))/2.0   !PC
    !         rho(:,:,j,Nz)=rho(:,:,j,1)
    !         rho(:,:,Ny,j)=rho(:,:,1,j)
    !      enddo
    !fix edges
    do sp=1,no_species
        ! write(*,*) 'Fixing borders rho',sum(rho(sp,:,:,:))
        call    fix_borders(Nx,Ny,Nz,rho(sp,:,:,:),border_matrix)

    end do


    if (take_weight_diag) then
        call fix_periodic_r(slices,proc_no,proc_max,Nx,Ny,Nz,vx,no_species,model)
        call fix_periodic_r(slices,proc_no,proc_max,Nx,Ny,Nz,vy,no_species,model)
        call fix_periodic_r(slices,proc_no,proc_max,Nx,Ny,Nz,vz,no_species,model)

        if (use_moments) then
            call fix_periodic_r(slices,proc_no,proc_max,Nx,Ny,Nz,vx2,no_species,model)
            call fix_periodic_r(slices,proc_no,proc_max,Nx,Ny,Nz,vy2,no_species,model)
            call fix_periodic_r(slices,proc_no,proc_max,Nx,Ny,Nz,vz2,no_species,model)


            call fix_periodic_r(slices,proc_no,proc_max,Nx,Ny,Nz,vx3,no_species,model)
            call fix_periodic_r(slices,proc_no,proc_max,Nx,Ny,Nz,vy3,no_species,model)
            call fix_periodic_r(slices,proc_no,proc_max,Nx,Ny,Nz,vz3,no_species,model)
        end if

        !      do j=1,Nz
        !         rho(:,:,1,j)=(rho(:,:,1,j)+rho(:,:,Ny,j))   !PC
        !         rho(:,1,j,:)=(rho(:,1,j,:)+rho(:,Nx,j,:))/2.0   !PC
        !         rho(:,:,j,Nz)=rho(:,:,j,1)
        !         rho(:,:,Ny,j)=rho(:,:,1,j)
        !      enddo
        !fix edges
        do sp=1,no_species
            call    fix_borders(Nx,Ny,Nz,vx(sp,:,:,:),border_matrix)
            call    fix_borders(Nx,Ny,Nz,vy(sp,:,:,:),border_matrix)
            call    fix_borders(Nx,Ny,Nz,vz(sp,:,:,:),border_matrix)
            if (use_moments) then
                call    fix_borders(Nx,Ny,Nz,vx2(sp,:,:,:),border_matrix)
                call    fix_borders(Nx,Ny,Nz,vy2(sp,:,:,:),border_matrix)
                call    fix_borders(Nx,Ny,Nz,vz2(sp,:,:,:),border_matrix)
                call    fix_borders(Nx,Ny,Nz,vx3(sp,:,:,:),border_matrix)
                call    fix_borders(Nx,Ny,Nz,vy3(sp,:,:,:),border_matrix)
                call    fix_borders(Nx,Ny,Nz,vz3(sp,:,:,:),border_matrix)
            end if
            ! norm it against the charge density
            ! we postpone this to the final output of the diagnostics
            if (model%automatic_particle_decomposition.eqv..false..or.model%proc_max.eq.0) then
                do ix1=1,Nx
                    do iy1=1,Ny
                        do iz1=1,Nz
                            if (rho(sp,ix1,iy1,iz1).ne.0.0) then
                                vx(sp,ix1,iy1,iz1) = vx(sp,ix1,iy1,iz1)/abs(rho(sp,ix1,iy1,iz1))*abs(q(sp))
                                vy(sp,ix1,iy1,iz1) = vy(sp,ix1,iy1,iz1)/abs(rho(sp,ix1,iy1,iz1))*abs(q(sp))
                                vz(sp,ix1,iy1,iz1) = vz(sp,ix1,iy1,iz1)/abs(rho(sp,ix1,iy1,iz1))*abs(q(sp))
                                if (use_moments) then
                                    !second order
                                    vx2(sp,ix1,iy1,iz1) = vx2(sp,ix1,iy1,iz1)/abs(rho(sp,ix1,iy1,iz1))*abs(q(sp))
                                    vy2(sp,ix1,iy1,iz1) = vy2(sp,ix1,iy1,iz1)/abs(rho(sp,ix1,iy1,iz1))*abs(q(sp))
                                    vz2(sp,ix1,iy1,iz1) = vz2(sp,ix1,iy1,iz1)/abs(rho(sp,ix1,iy1,iz1))*abs(q(sp))

                                    !third order

                                    vx3(sp,ix1,iy1,iz1) = vx3(sp,ix1,iy1,iz1)/abs(rho(sp,ix1,iy1,iz1))*abs(q(sp))
                                    vy3(sp,ix1,iy1,iz1) = vy3(sp,ix1,iy1,iz1)/abs(rho(sp,ix1,iy1,iz1))*abs(q(sp))
                                    vz3(sp,ix1,iy1,iz1) = vz3(sp,ix1,iy1,iz1)/abs(rho(sp,ix1,iy1,iz1))*abs(q(sp))
                                end if

                            end if

                        end do
                    end do
                end do
            end if
        end do






    end if


end subroutine
subroutine weight_diags(x,y,z,ux,uy,uz,stype,rho,no_tot,A1,A2,A3,A4,A5,A6,A7,A8,Nx,Ny,Nz,no_species,&
        q,ix,iy,iz,objects,model,vx,vy,vz,vx2,vy2,vz2,vx3,vy3,vz3,border_matrix)
    implicit none
    include 'struct.h'
    integer:: no_tot,i,Nx,Ny,Nz,no_species,sp,j,ix1,iy1,iz1
    real, dimension(no_tot):: x,y,z,A1,A2,A3,A4,A5,A6,A7,A8,ux,uy,uz
    integer, dimension(no_tot):: stype,ix,iy,iz
    integer, dimension(Nx,Ny,Nz):: objects
    real, dimension(Nx,Ny,Nz):: border_matrix
    real, dimension(no_species,Nx,Ny,Nz):: rho,vx,vy,vz,vx2,vy2,vz2,vx3,vy3,vz3
    real, dimension(no_species):: q
    type(model_t):: model
    ! construct the charge density
    ! first, zero it out
    vx = 0.0
    vy = 0.0
    vz = 0.0
    vx2 = 0.0
    vy2 = 0.0
    vz2 = 0.0
    vx3 = 0.0
    vy3 = 0.0
    vz3 = 0.0

    do i=1,no_tot
        sp = stype(i)
        if (z(i).lt.model%Lz.and.z(i).gt.0.0) then
            vx(sp,ix(i),iy(i),iz(i)) =vx(sp,ix(i),iy(i),iz(i)) + ux(i)*A1(i)
            vx(sp,ix(i),iy(i) +1,iz(i)) =vx(sp,ix(i),iy(i)+1,iz(i)) + ux(i)*A2(i)
            vx(sp,ix(i)+1,iy(i) +1,iz(i)) =vx(sp,ix(i)+1,iy(i)+1,iz(i)) + ux(i)*A3(i)
            vx(sp,ix(i)+1,iy(i) ,iz(i)) =vx(sp,ix(i)+1,iy(i),iz(i)) + ux(i)*A4(i)
            vx(sp,ix(i),iy(i),iz(i)+1) =vx(sp,ix(i),iy(i),iz(i)+1) + ux(i)*A5(i)
            vx(sp,ix(i),iy(i) +1,iz(i)+1) =vx(sp,ix(i),iy(i)+1,iz(i)+1) + ux(i)*A6(i)
            vx(sp,ix(i)+1,iy(i) +1,iz(i)+1) =vx(sp,ix(i)+1,iy(i)+1,iz(i)+1) + ux(i)*A7(i)
            vx(sp,ix(i)+1,iy(i) ,iz(i)+1) =vx(sp,ix(i)+1,iy(i),iz(i)+1) + ux(i)*A8(i)

            vy(sp,ix(i),iy(i),iz(i)) =vy(sp,ix(i),iy(i),iz(i)) + uy(i)*A1(i)
            vy(sp,ix(i),iy(i) +1,iz(i)) =vy(sp,ix(i),iy(i)+1,iz(i)) + uy(i)*A2(i)
            vy(sp,ix(i)+1,iy(i) +1,iz(i)) =vy(sp,ix(i)+1,iy(i)+1,iz(i)) + uy(i)*A3(i)
            vy(sp,ix(i)+1,iy(i) ,iz(i)) =vy(sp,ix(i)+1,iy(i),iz(i)) + uy(i)*A4(i)
            vy(sp,ix(i),iy(i),iz(i)+1) =vy(sp,ix(i),iy(i),iz(i)+1) + uy(i)*A5(i)
            vy(sp,ix(i),iy(i) +1,iz(i)+1) =vy(sp,ix(i),iy(i)+1,iz(i)+1) + uy(i)*A6(i)
            vy(sp,ix(i)+1,iy(i) +1,iz(i)+1) =vy(sp,ix(i)+1,iy(i)+1,iz(i)+1) + uy(i)*A7(i)
            vy(sp,ix(i)+1,iy(i) ,iz(i)+1) =vy(sp,ix(i)+1,iy(i),iz(i)+1) + uy(i)*A8(i)

            vz(sp,ix(i),iy(i),iz(i)) =vz(sp,ix(i),iy(i),iz(i)) + uz(i)*A1(i)
            vz(sp,ix(i),iy(i) +1,iz(i)) =vz(sp,ix(i),iy(i)+1,iz(i)) + uz(i)*A2(i)
            vz(sp,ix(i)+1,iy(i) +1,iz(i)) =vz(sp,ix(i)+1,iy(i)+1,iz(i)) + uz(i)*A3(i)
            vz(sp,ix(i)+1,iy(i) ,iz(i)) =vz(sp,ix(i)+1,iy(i),iz(i)) + uz(i)*A4(i)
            vz(sp,ix(i),iy(i),iz(i)+1) =vz(sp,ix(i),iy(i),iz(i)+1) + uz(i)*A5(i)
            vz(sp,ix(i),iy(i) +1,iz(i)+1) =vz(sp,ix(i),iy(i)+1,iz(i)+1) + uz(i)*A6(i)
            vz(sp,ix(i)+1,iy(i) +1,iz(i)+1) =vz(sp,ix(i)+1,iy(i)+1,iz(i)+1) + uz(i)*A7(i)
            vz(sp,ix(i)+1,iy(i) ,iz(i)+1) =vz(sp,ix(i)+1,iy(i),iz(i)+1) + uz(i)*A8(i)
            ! second order

            vx2(sp,ix(i),iy(i),iz(i)) =vx2(sp,ix(i),iy(i),iz(i)) + ux(i)*ux(i)*A1(i)
            vx2(sp,ix(i),iy(i) +1,iz(i)) =vx2(sp,ix(i),iy(i)+1,iz(i)) + ux(i)*ux(i)*A2(i)
            vx2(sp,ix(i)+1,iy(i) +1,iz(i)) =vx2(sp,ix(i)+1,iy(i)+1,iz(i)) + ux(i)*ux(i)*A3(i)
            vx2(sp,ix(i)+1,iy(i) ,iz(i)) =vx2(sp,ix(i)+1,iy(i),iz(i)) + ux(i)*ux(i)*A4(i)
            vx2(sp,ix(i),iy(i),iz(i)+1) =vx2(sp,ix(i),iy(i),iz(i)+1) + ux(i)*ux(i)*A5(i)
            vx2(sp,ix(i),iy(i) +1,iz(i)+1) =vx2(sp,ix(i),iy(i)+1,iz(i)+1) + ux(i)*ux(i)*A6(i)
            vx2(sp,ix(i)+1,iy(i) +1,iz(i)+1) =vx2(sp,ix(i)+1,iy(i)+1,iz(i)+1) + ux(i)*ux(i)*A7(i)
            vx2(sp,ix(i)+1,iy(i) ,iz(i)+1) =vx2(sp,ix(i)+1,iy(i),iz(i)+1) + ux(i)*ux(i)*A8(i)

            vy2(sp,ix(i),iy(i),iz(i)) =vy2(sp,ix(i),iy(i),iz(i)) + uy(i)*uy(i)*A1(i)
            vy2(sp,ix(i),iy(i) +1,iz(i)) =vy2(sp,ix(i),iy(i)+1,iz(i)) + uy(i)*uy(i)*A2(i)
            vy2(sp,ix(i)+1,iy(i) +1,iz(i)) =vy2(sp,ix(i)+1,iy(i)+1,iz(i)) + uy(i)*uy(i)*A3(i)
            vy2(sp,ix(i)+1,iy(i) ,iz(i)) =vy2(sp,ix(i)+1,iy(i),iz(i)) + uy(i)*uy(i)*A4(i)
            vy2(sp,ix(i),iy(i),iz(i)+1) =vy2(sp,ix(i),iy(i),iz(i)+1) + uy(i)*uy(i)*A5(i)
            vy2(sp,ix(i),iy(i) +1,iz(i)+1) =vy2(sp,ix(i),iy(i)+1,iz(i)+1) + uy(i)*uy(i)*A6(i)
            vy2(sp,ix(i)+1,iy(i) +1,iz(i)+1) =vy2(sp,ix(i)+1,iy(i)+1,iz(i)+1) + uy(i)*uy(i)*A7(i)
            vy2(sp,ix(i)+1,iy(i) ,iz(i)+1) =vy2(sp,ix(i)+1,iy(i),iz(i)+1) + uy(i)*uy(i)*A8(i)

            vz2(sp,ix(i),iy(i),iz(i)) =vz2(sp,ix(i),iy(i),iz(i)) + uz(i)*uz(i)*A1(i)
            vz2(sp,ix(i),iy(i) +1,iz(i)) =vz2(sp,ix(i),iy(i)+1,iz(i)) + uz(i)*uz(i)*A2(i)
            vz2(sp,ix(i)+1,iy(i) +1,iz(i)) =vz2(sp,ix(i)+1,iy(i)+1,iz(i)) + uz(i)*uz(i)*A3(i)
            vz2(sp,ix(i)+1,iy(i) ,iz(i)) =vz2(sp,ix(i)+1,iy(i),iz(i)) + uz(i)*uz(i)*A4(i)
            vz2(sp,ix(i),iy(i),iz(i)+1) =vz2(sp,ix(i),iy(i),iz(i)+1) + uz(i)*uz(i)*A5(i)
            vz2(sp,ix(i),iy(i) +1,iz(i)+1) =vz2(sp,ix(i),iy(i)+1,iz(i)+1) + uz(i)*uz(i)*A6(i)
            vz2(sp,ix(i)+1,iy(i) +1,iz(i)+1) =vz2(sp,ix(i)+1,iy(i)+1,iz(i)+1) + uz(i)*uz(i)*A7(i)
            vz2(sp,ix(i)+1,iy(i) ,iz(i)+1) =vz2(sp,ix(i)+1,iy(i),iz(i)+1) + uz(i)*uz(i)*A8(i)
            ! third order

            vx3(sp,ix(i),iy(i),iz(i)) =vx3(sp,ix(i),iy(i),iz(i)) + ux(i)*ux(i)*A1(i)
            vx3(sp,ix(i),iy(i) +1,iz(i)) =vx3(sp,ix(i),iy(i)+1,iz(i)) + ux(i)*ux(i)*ux(i)*A2(i)
            vx3(sp,ix(i)+1,iy(i) +1,iz(i)) =vx3(sp,ix(i)+1,iy(i)+1,iz(i)) + ux(i)*ux(i)*ux(i)*A3(i)
            vx3(sp,ix(i)+1,iy(i) ,iz(i)) =vx3(sp,ix(i)+1,iy(i),iz(i)) + ux(i)*ux(i)*ux(i)*A4(i)
            vx3(sp,ix(i),iy(i),iz(i)+1) =vx3(sp,ix(i),iy(i),iz(i)+1) + ux(i)*ux(i)*ux(i)*A5(i)
            vx3(sp,ix(i),iy(i) +1,iz(i)+1) =vx3(sp,ix(i),iy(i)+1,iz(i)+1) + ux(i)*ux(i)*ux(i)*A6(i)
            vx3(sp,ix(i)+1,iy(i) +1,iz(i)+1) =vx3(sp,ix(i)+1,iy(i)+1,iz(i)+1) + ux(i)*ux(i)*ux(i)*A7(i)
            vx3(sp,ix(i)+1,iy(i) ,iz(i)+1) =vx3(sp,ix(i)+1,iy(i),iz(i)+1) + ux(i)*ux(i)*ux(i)*A8(i)

            vy3(sp,ix(i),iy(i),iz(i)) =vy3(sp,ix(i),iy(i),iz(i)) + uy(i)*uy(i)*uy(i)*A1(i)
            vy3(sp,ix(i),iy(i) +1,iz(i)) =vy3(sp,ix(i),iy(i)+1,iz(i)) + uy(i)*uy(i)*uy(i)*A2(i)
            vy3(sp,ix(i)+1,iy(i) +1,iz(i)) =vy3(sp,ix(i)+1,iy(i)+1,iz(i)) + uy(i)*uy(i)*uy(i)*A3(i)
            vy3(sp,ix(i)+1,iy(i) ,iz(i)) =vy3(sp,ix(i)+1,iy(i),iz(i)) + uy(i)*uy(i)*uy(i)*A4(i)
            vy3(sp,ix(i),iy(i),iz(i)+1) =vy3(sp,ix(i),iy(i),iz(i)+1) + uy(i)*uy(i)*uy(i)*A5(i)
            vy3(sp,ix(i),iy(i) +1,iz(i)+1) =vy3(sp,ix(i),iy(i)+1,iz(i)+1) + uy(i)*uy(i)*uy(i)*A6(i)
            vy3(sp,ix(i)+1,iy(i) +1,iz(i)+1) =vy3(sp,ix(i)+1,iy(i)+1,iz(i)+1) + uy(i)*uy(i)*uy(i)*A7(i)
            vy3(sp,ix(i)+1,iy(i) ,iz(i)+1) =vy3(sp,ix(i)+1,iy(i),iz(i)+1) + uy(i)*uy(i)*uy(i)*A8(i)

            vz3(sp,ix(i),iy(i),iz(i)) =vz3(sp,ix(i),iy(i),iz(i)) + uz(i)*uz(i)*uz(i)*A1(i)
            vz3(sp,ix(i),iy(i) +1,iz(i)) =vz3(sp,ix(i),iy(i)+1,iz(i)) + uz(i)*uz(i)*uz(i)*A2(i)
            vz3(sp,ix(i)+1,iy(i) +1,iz(i)) =vz3(sp,ix(i)+1,iy(i)+1,iz(i)) + uz(i)*uz(i)*uz(i)*A3(i)
            vz3(sp,ix(i)+1,iy(i) ,iz(i)) =vz3(sp,ix(i)+1,iy(i),iz(i)) + uz(i)*uz(i)*uz(i)*A4(i)
            vz3(sp,ix(i),iy(i),iz(i)+1) =vz3(sp,ix(i),iy(i),iz(i)+1) + uz(i)*uz(i)*uz(i)*A5(i)
            vz3(sp,ix(i),iy(i) +1,iz(i)+1) =vz3(sp,ix(i),iy(i)+1,iz(i)+1) + uz(i)*uz(i)*uz(i)*A6(i)
            vz3(sp,ix(i)+1,iy(i) +1,iz(i)+1) =vz3(sp,ix(i)+1,iy(i)+1,iz(i)+1) + uz(i)*uz(i)*uz(i)*A7(i)
            vz3(sp,ix(i)+1,iy(i) ,iz(i)+1) =vz3(sp,ix(i)+1,iy(i),iz(i)+1) + uz(i)*uz(i)*uz(i)*A8(i)




        end if
    end do
    ! call fix_periodic_r(slices,proc_no,proc_max,Nx,Ny,Nz,vx,no_species,model)
    ! call fix_periodic_r(slices,proc_no,proc_max,Nx,Ny,Nz,vy,no_species,model)
    ! call fix_periodic_r(slices,proc_no,proc_max,Nx,Ny,Nz,vz,no_species,model)
    !
    ! call fix_periodic_r(slices,proc_no,proc_max,Nx,Ny,Nz,vx2,no_species,model)
    ! call fix_periodic_r(slices,proc_no,proc_max,Nx,Ny,Nz,vy2,no_species,model)
    ! call fix_periodic_r(slices,proc_no,proc_max,Nx,Ny,Nz,vz2,no_species,model)
    !
    !
    ! call fix_periodic_r(slices,proc_no,proc_max,Nx,Ny,Nz,vx3,no_species,model)
    ! call fix_periodic_r(slices,proc_no,proc_max,Nx,Ny,Nz,vy3,no_species,model)
    ! call fix_periodic_r(slices,proc_no,proc_max,Nx,Ny,Nz,vz3,no_species,model)


    !      do j=1,Nz
    !         rho(:,:,1,j)=(rho(:,:,1,j)+rho(:,:,Ny,j))   !PC
    !         rho(:,1,j,:)=(rho(:,1,j,:)+rho(:,Nx,j,:))/2.0   !PC
    !         rho(:,:,j,Nz)=rho(:,:,j,1)
    !         rho(:,:,Ny,j)=rho(:,:,1,j)
    !      enddo
    !fix edges
    do sp=1,no_species
        call    fix_borders(Nx,Ny,Nz,vx(sp,:,:,:),border_matrix)
        call    fix_borders(Nx,Ny,Nz,vy(sp,:,:,:),border_matrix)
        call    fix_borders(Nx,Ny,Nz,vz(sp,:,:,:),border_matrix)
        call    fix_borders(Nx,Ny,Nz,vx2(sp,:,:,:),border_matrix)
        call    fix_borders(Nx,Ny,Nz,vy2(sp,:,:,:),border_matrix)
        call    fix_borders(Nx,Ny,Nz,vz2(sp,:,:,:),border_matrix)
        call    fix_borders(Nx,Ny,Nz,vx3(sp,:,:,:),border_matrix)
        call    fix_borders(Nx,Ny,Nz,vy3(sp,:,:,:),border_matrix)
        call    fix_borders(Nx,Ny,Nz,vz3(sp,:,:,:),border_matrix)

        ! norm it against the charge density
        do ix1=1,Nx
            do iy1=1,Ny
                do iz1=1,Nz
                    if (rho(sp,ix1,iy1,iz1).ne.0.0) then
                        vx(sp,ix1,iy1,iz1) = vx(sp,ix1,iy1,iz1)/abs(rho(sp,ix1,iy1,iz1))*abs(q(sp))
                        vy(sp,ix1,iy1,iz1) = vy(sp,ix1,iy1,iz1)/abs(rho(sp,ix1,iy1,iz1))*abs(q(sp))
                        vz(sp,ix1,iy1,iz1) = vz(sp,ix1,iy1,iz1)/abs(rho(sp,ix1,iy1,iz1))*abs(q(sp))
                        !second order
                        vx2(sp,ix1,iy1,iz1) = vx2(sp,ix1,iy1,iz1)/abs(rho(sp,ix1,iy1,iz1))*abs(q(sp))
                        vy2(sp,ix1,iy1,iz1) = vy2(sp,ix1,iy1,iz1)/abs(rho(sp,ix1,iy1,iz1))*abs(q(sp))
                        vz2(sp,ix1,iy1,iz1) = vz2(sp,ix1,iy1,iz1)/abs(rho(sp,ix1,iy1,iz1))*abs(q(sp))

                        !third order

                        vx3(sp,ix1,iy1,iz1) = vx3(sp,ix1,iy1,iz1)/abs(rho(sp,ix1,iy1,iz1))*abs(q(sp))
                        vy3(sp,ix1,iy1,iz1) = vy3(sp,ix1,iy1,iz1)/abs(rho(sp,ix1,iy1,iz1))*abs(q(sp))
                        vz3(sp,ix1,iy1,iz1) = vz3(sp,ix1,iy1,iz1)/abs(rho(sp,ix1,iy1,iz1))*abs(q(sp))


                    end if

                end do
            end do
        end do

    end do

end subroutine
subroutine fix_borders(Nx,Ny,Nz,m,border_matrix)
    implicit none
    integer:: Nx,Ny,Nz
    real,dimension(Nx,Ny,Nz)::m,border_matrix

    m = m*border_matrix


end subroutine

subroutine make_border_matrix(Nx,Ny,Nz,Nx_total,Ny_total,Nz_total,objects,border_matrix,Nx_start,Ny_start,Nz_start)
    implicit none
    integer::ix,iy,iz,Nx,Ny,Nz,free_cells,s,Nx_total,Ny_total,Nz_total,Nx_start,Ny_start,Nz_start
    real,dimension(Nx,Ny,Nz):: border_matrix
    integer,dimension(Nx_total,Ny_total,Nz_total):: objects
    integer, dimension(Nx_total+2,Ny_total+2,Nz_total):: b_objects
    ! big objects go over the periodic boundaries, which helps a lot
    write(*,*) 'Working with matrix',Nx_total,Ny_total,Nz_total
    write(*,*) 'Working with border_matrix',Nx,Ny,Nz
    write(*,*) 'Working with border_matrix start',Nx_start,Ny_start,Nz_start


    call  prepare_large_grid_i(Nx_total,Ny_total,Nz_total,objects,b_objects)

    border_matrix = 1.0
    do ix=2,Nx_total+1
        do iy=2,Ny_total+1
            do iz=2,Nz_total-1
                s = 0
                free_cells = 8
                ! check if we deal with surface  - it has to be within an object
                if (b_objects(ix,iy,iz).gt.0) then
                    ! check 8 cells around the point to determine which is the surface type
                    ! cell 1
                    s = b_objects(ix-1,iy-1,iz -1)*b_objects(ix-1,iy,iz -1)*b_objects(ix,iy-1,iz -1)*b_objects(ix,iy,iz&
                        -1)*b_objects(ix-1,iy-1,iz)*b_objects(ix-1,iy,iz)*b_objects(ix,iy-1,iz)*b_objects(ix,iy,iz)
                    if (s.gt.0) then
                        free_cells = free_cells - 1
                    end if
                    ! cell 2
                    s = b_objects(ix,iy-1,iz -1)*b_objects(ix,iy,iz -1)*b_objects(ix+1,iy-1,iz -1)*b_objects(ix+1,iy,iz&
                        -1)*b_objects(ix,iy-1,iz)*b_objects(ix,iy,iz)*b_objects(ix+1,iy-1,iz)*b_objects(ix+1,iy,iz)
                    if (s.gt.0) then
                        free_cells = free_cells - 1
                    end if
                    ! cell 3
                    s = b_objects(ix,iy,iz -1)*b_objects(ix,iy+1,iz -1)*b_objects(ix+1,iy,iz -1)*b_objects(ix+1,iy+1,iz&
                        -1)*b_objects(ix,iy,iz)*b_objects(ix,iy+1,iz)*b_objects(ix+1,iy,iz)*b_objects(ix+1,iy+1,iz)
                    if (s.gt.0) then
                        free_cells = free_cells - 1
                    end if
                    ! cell 4
                    s = b_objects(ix-1,iy,iz -1)*b_objects(ix-1,iy+1,iz -1)*b_objects(ix,iy,iz -1)*b_objects(ix,iy+1,iz&
                        -1)*b_objects(ix-1,iy,iz)*b_objects(ix-1,iy+1,iz)*b_objects(ix,iy,iz)*b_objects(ix,iy+1,iz)
                    if (s.gt.0) then
                        free_cells = free_cells - 1
                    end if
                    ! cell 5
                    s = b_objects(ix-1,iy-1,iz )*b_objects(ix-1,iy,iz )*b_objects(ix,iy-1,iz )*b_objects(ix,iy,iz )*&
                        b_objects(ix-1,iy-1,iz+1)*b_objects(ix-1,iy,iz+1)*b_objects(ix,iy-1,iz+1)*b_objects(ix,iy,iz+1)
                    if (s.gt.0) then
                        free_cells = free_cells - 1
                    end if
                    ! cell 6
                    s = b_objects(ix,iy-1,iz)*b_objects(ix,iy,iz )*b_objects(ix+1,iy-1,iz )*&
                        b_objects(ix+1,iy,iz )*b_objects(ix,iy-1,iz+1)*b_objects(ix,iy,iz+1)*&
                        b_objects(ix+1,iy-1,iz+1)*b_objects(ix+1,iy,iz+1)
                    if (s.gt.0) then
                        free_cells = free_cells - 1
                    end if
                    ! cell 7
                    s = b_objects(ix,iy,iz )*b_objects(ix,iy+1,iz )*b_objects(ix+1,iy,iz )*&
                        b_objects(ix+1,iy+1,iz )*b_objects(ix,iy,iz+1)*b_objects(ix,iy+1,iz+1)*&
                        b_objects(ix+1,iy,iz+1)*b_objects(ix+1,iy+1,iz+1)
                    if (s.gt.0) then
                        free_cells = free_cells - 1
                    end if
                    ! cell 8
                    s = b_objects(ix-1,iy,iz )*b_objects(ix-1,iy+1,iz )*b_objects(ix,iy,iz )*&
                        b_objects(ix,iy+1,iz )*b_objects(ix-1,iy,iz+1)*b_objects(ix-1,iy+1,iz+1)*&
                        b_objects(ix,iy,iz+1)*b_objects(ix,iy+1,iz+1)
                    if (s.gt.0) then
                        free_cells = free_cells - 1
                    end if
                    ! now do the weight
                    if (free_cells.gt.0.and.ix.gt.Nx_start.and.ix.le.(Nx_start + Nx )&
                            .and.iy.gt.Ny_start.and.iy.le.(Ny_start + Ny ).and.iz.gt.Nz_start.and.&
                            iz.lt.(Nz_start + Nz )) then
                        border_matrix(ix -Nx_start,iy-Ny_start,iz-Nz_start+1) = 8.0/real(free_cells)
                    end if
                end if
            end do
        end do
    end do
    write(*,*) 'main body done'
    ! we do not allow any electrode to reach the top boundary, that would make little sense
    ! for the bottom boundary, we assuse 4 cells already taken as surface and other 4 are tested
    iz = 1
    do ix=2,Nx_total+1
        do iy=2,Ny_total+1
            s = 0
            free_cells = 4
            ! cell 5
            s = b_objects(ix-1,iy-1,iz )*b_objects(ix-1,iy,iz )*b_objects(ix,iy-1,iz )*&
                b_objects(ix,iy,iz )*b_objects(ix-1,iy-1,iz+1)*b_objects(ix-1,iy,iz+1)*&
                b_objects(ix,iy-1,iz+1)*b_objects(ix,iy,iz+1)
            if (s.gt.0) then
                free_cells = free_cells - 1
            end if
            ! cell 6
            s = b_objects(ix,iy-1,iz)*b_objects(ix,iy,iz )*b_objects(ix+1,iy-1,iz )*&
                b_objects(ix+1,iy,iz )*b_objects(ix,iy-1,iz+1)*b_objects(ix,iy,iz+1)*&
                b_objects(ix+1,iy-1,iz+1)*b_objects(ix+1,iy,iz+1)
            if (s.gt.0) then
                free_cells = free_cells - 1
            end if
            ! cell 7
            s = b_objects(ix,iy,iz )*b_objects(ix,iy+1,iz )*b_objects(ix+1,iy,iz )*&
                b_objects(ix+1,iy+1,iz )*b_objects(ix,iy,iz+1)*b_objects(ix,iy+1,iz+1)*&
                b_objects(ix+1,iy,iz+1)*b_objects(ix+1,iy+1,iz+1)
            if (s.gt.0) then
                free_cells = free_cells - 1
            end if
            ! cell 8
            s = b_objects(ix-1,iy,iz )*b_objects(ix-1,iy+1,iz )*b_objects(ix,iy,iz )*&
                b_objects(ix,iy+1,iz )*b_objects(ix-1,iy,iz+1)*b_objects(ix-1,iy+1,iz+1)*&
                b_objects(ix,iy,iz+1)*b_objects(ix,iy+1,iz+1)
            if (s.gt.0) then
                free_cells = free_cells - 1
            end if
            ! now do the weight
            if (free_cells.gt.0.and.ix.gt.Nx_start.and.ix.le.(Nx_start + Nx ).and.&
                    iy.gt.Ny_start.and.iy.le.(Ny_start + Ny).and.iz.gt.Nz_start.and.iz.lt.(Nz_start + Nz )) then
                border_matrix(ix -Nx_start,iy-Ny_start,iz-Nz_start+1) = 8.0/real(free_cells)
            end if
        end do
    end do

end subroutine














subroutine smooth_edge_charge(Nx,Ny,Nz,edge_charge,edges,object_params,objects,no_objects,n_diel_matrix)
    implicit none
    include "struct.h"

    integer:: Nx,Ny,Nz,ix,iy,iz,N_diel,no_objects
    integer, dimension(Nx,Ny,Nz):: edges,objects,n_diel_matrix
    real, dimension(Nx,Ny,Nz):: edge_charge,new_edge_charge
    real, dimension(Nx+2,Ny+2,Nz):: large_edge_charge
    real*8:: smooth_factor,res_factor
    type(object_t),dimension(no_objects):: object_params
    new_edge_charge = 0.0
    large_edge_charge(2:(Nx+1),2:(Ny+1),:) = edge_charge
    large_edge_charge(1,2:(Ny+1),:) = edge_charge(Nx,:,:)
    large_edge_charge(Nx+2,2:(Ny+1),:) = edge_charge(1,:,:)

    large_edge_charge(2:(Nx+1),1,:) = edge_charge(:,Ny,:)
    large_edge_charge(2:(Nx+1),Ny,:) = edge_charge(:,1,:)

    large_edge_charge(1,1,:) = edge_charge(Nx,Ny,:)
    large_edge_charge(Nx+2,1,:) = edge_charge(1,Ny,:)
    large_edge_charge(1,Ny+2,:) = edge_charge(Nx,1,:)
    large_edge_charge(Nx+2,Ny+2,:) = edge_charge(1,1,:)

    smooth_factor = 0.6
    do ix=1,Nx
        do iy=1,Ny
            do iz=2,Nz-1


                if (edges(ix,iy,iz).gt.0.and.n_diel_matrix(ix,iy,iz).gt.0) then
                    ! write(*,*) 'Object: ',objects(ix,iy,iz),object_params(objects(ix,iy,iz))%param1
                    if (object_params(objects(ix,iy,iz))%param1.eq.1.and.edge_charge(ix,iy,iz).ne.0.0) then
                        res_factor = (1.0 - smooth_factor)/float(n_diel_matrix(ix,iy,iz))
                        new_edge_charge(ix,iy,iz) =new_edge_charge(ix,iy,iz) + smooth_factor*&
                            edge_charge(ix,iy,iz) + res_factor*large_edge_charge(ix,iy+1,iz) &
                            +res_factor*large_edge_charge(ix+1,iy,iz) + res_factor*large_edge_charge(ix,iy,iz+1)+&
                            res_factor*large_edge_charge(ix-1,iy,iz)+res_factor*large_edge_charge(ix,iy-1,iz) + &
                            res_factor*large_edge_charge(ix,iy,iz-1)
                        !       write(*,*) 'Ch shift',edge_charge(ix,iy,iz),new_edge_charge(ix,iy,iz)
                        if (isnan(new_edge_charge(ix,iy,iz))) then
                            write(*,*) 'Error, got a NaN edge charge',ix,iy,iz
                        end if
                    end if
                end if

            end do
        end do
    end do
    edge_charge = new_edge_charge
end subroutine

! this version of smooth charge does not handle periodic boundaries but runs rather faster

subroutine smooth_edge_charge_fast(Nx,Ny,Nz,edge_charge,edges,object_params,objects,no_objects,n_diel_matrix)
    implicit none
    include "struct.h"

    integer:: Nx,Ny,Nz,ix,iy,iz,N_diel,no_objects
    integer, dimension(Nx,Ny,Nz):: edges,objects,n_diel_matrix
    real, dimension(Nx,Ny,Nz):: edge_charge,new_edge_charge
    ! real, dimension(Nx+2,Ny+2,Nz):: large_edge_charge
    real*8:: smooth_factor,res_factor
    type(object_t),dimension(no_objects):: object_params
    new_edge_charge = 0.0
    ! large_edge_charge(2:(Nx+1),2:(Ny+1),:) = edge_charge
    ! large_edge_charge(1,2:(Ny+1),:) = edge_charge(Nx,:,:)
    ! large_edge_charge(Nx+2,2:(Ny+1),:) = edge_charge(1,:,:)
    !
    ! large_edge_charge(2:(Nx+1),1,:) = edge_charge(:,Ny,:)
    ! large_edge_charge(2:(Nx+1),Ny,:) = edge_charge(:,1,:)
    !
    ! large_edge_charge(1,1,:) = edge_charge(Nx,Ny,:)
    ! large_edge_charge(Nx+2,1,:) = edge_charge(1,Ny,:)
    ! large_edge_charge(1,Ny+2,:) = edge_charge(Nx,1,:)
    ! large_edge_charge(Nx+2,Ny+2,:) = edge_charge(1,1,:)

    smooth_factor = 0.5
    do ix=2,Nx-1
        do iy=2,Ny-1
            do iz=2,Nz-1


                if (edges(ix,iy,iz).gt.0.and.n_diel_matrix(ix,iy,iz).gt.0) then
                    ! write(*,*) 'Object: ',objects(ix,iy,iz),object_params(objects(ix,iy,iz))%param1
                    !if (object_params(objects(ix,iy,iz))%param1.eq.1.and.edge_charge(ix,iy,iz).ne.0.0) then
                    if (object_params(objects(ix,iy,iz))%param1.eq.1) then
                        res_factor = (1.0 - smooth_factor)/float(n_diel_matrix(ix,iy,iz))
                        new_edge_charge(ix,iy,iz) =new_edge_charge(ix,iy,iz) + &
                            smooth_factor*edge_charge(ix,iy,iz) + res_factor*edge_charge(ix,iy+1,iz) +&
                            res_factor*edge_charge(ix+1,iy,iz) + res_factor*edge_charge(ix,iy,iz+1)+&
                            res_factor*edge_charge(ix-1,iy,iz)+res_factor*edge_charge(ix,iy-1,iz) + &
                            res_factor*edge_charge(ix,iy,iz-1)
                        !       write(*,*) 'Ch shift',edge_charge(ix,iy,iz),new_edge_charge(ix,iy,iz)
                        ! if (isnan(new_edge_charge(ix,iy,iz))) then
                        ! write(*,*) 'Error, got a NaN edge charge',ix,iy,iz
                        ! end if
                    end if
                end if

            end do
        end do
    end do
    edge_charge = new_edge_charge
end subroutine


subroutine smooth_edge_charge_fast_x(Nx,Ny,Nz,edge_charge,edges,object_params,objects,no_objects,n_diel_matrix)
    implicit none
    include "struct.h"

    integer:: Nx,Ny,Nz,ix,iy,iz,N_diel,no_objects
    integer, dimension(Nx,Ny,Nz):: edges,objects,n_diel_matrix
    real, dimension(Nx,Ny,Nz):: edge_charge,new_edge_charge
    ! real, dimension(Nx+2,Ny+2,Nz):: large_edge_charge
    real*8:: smooth_factor,res_factor
    type(object_t),dimension(no_objects):: object_params
    new_edge_charge = 0.0
    ! large_edge_charge(2:(Nx+1),2:(Ny+1),:) = edge_charge
    ! large_edge_charge(1,2:(Ny+1),:) = edge_charge(Nx,:,:)
    ! large_edge_charge(Nx+2,2:(Ny+1),:) = edge_charge(1,:,:)
    !
    ! large_edge_charge(2:(Nx+1),1,:) = edge_charge(:,Ny,:)
    ! large_edge_charge(2:(Nx+1),Ny,:) = edge_charge(:,1,:)
    !
    ! large_edge_charge(1,1,:) = edge_charge(Nx,Ny,:)
    ! large_edge_charge(Nx+2,1,:) = edge_charge(1,Ny,:)
    ! large_edge_charge(1,Ny+2,:) = edge_charge(Nx,1,:)
    ! large_edge_charge(Nx+2,Ny+2,:) = edge_charge(1,1,:)

    smooth_factor = 0.5
    do ix=2,Nx-1
        do iy=2,Ny-1
            do iz=2,Nz-1


                if (edges(ix,iy,iz).gt.0.and.n_diel_matrix(ix,iy,iz).gt.0) then
                    ! write(*,*) 'Object: ',objects(ix,iy,iz),object_params(objects(ix,iy,iz))%param1
                    !if (object_params(objects(ix,iy,iz))%param1.eq.1.and.edge_charge(ix,iy,iz).ne.0.0) then
                    if (object_params(objects(ix,iy,iz))%param1.eq.1) then
                        !res_factor = (1.0 - smooth_factor)/float(n_diel_matrix(ix,iy,iz))
                        res_factor = (1.0 - smooth_factor)/(sign(1,edges(ix,iy+1,iz)) + sign(1,edges(ix,iy-1,iz)) + sign(1,edges(ix,iy,iz+1)) +sign(1, edges(ix,iy,iz-1)))
                        new_edge_charge(ix,iy,iz) =new_edge_charge(ix,iy,iz) + &
                            smooth_factor*edge_charge(ix,iy,iz) + res_factor*edge_charge(ix,iy+1,iz) +&
                            + res_factor*edge_charge(ix,iy,iz+1)+&
                            +res_factor*edge_charge(ix,iy-1,iz) + &
                            res_factor*edge_charge(ix,iy,iz-1)
                        !       write(*,*) 'Ch shift',edge_charge(ix,iy,iz),new_edge_charge(ix,iy,iz)
                        ! if (isnan(new_edge_charge(ix,iy,iz))) then
                        ! write(*,*) 'Error, got a NaN edge charge',ix,iy,iz
                        ! end if
                    end if
                end if

            end do
        end do
    end do
    edge_charge = new_edge_charge
end subroutine


subroutine prepare_n_diel_matrix(Nx,Ny,Nz,objects,edges,object_params,no_objects,n_diel_matrix)

    implicit none
    include "struct.h"

    integer:: Nx,Ny,Nz,ix,iy,iz,N_diel,no_objects
    integer, dimension(Nx,Ny,Nz):: edges,objects,n_diel_matrix
    integer, dimension(Nx+2,Ny+2,Nz)::large_edges,large_objects
    type(object_t),dimension(no_objects):: object_params

    large_edges(2:(Nx+1),2:(Ny+1),:) = edges
    large_edges(1,2:(Ny+1),:) = edges(Nx,:,:)
    large_edges(Nx+2,2:(Ny+1),:) = edges(1,:,:)

    large_edges(2:(Nx+1),1,:) = edges(:,Ny,:)
    large_edges(2:(Nx+1),Ny+2,:) = edges(:,1,:)

    large_edges(1,1,:) = edges(Nx,Ny,:)
    large_edges(Nx+2,1,:) = edges(1,Ny,:)
    large_edges(1,Ny+2,:) = edges(Nx,1,:)
    large_edges(Nx+2,Ny+2,:) = edges(1,1,:)

    large_objects(2:(Nx+1),2:(Ny+1),:) = objects
    large_objects(1,2:(Ny+1),:) = objects(Nx,:,:)
    large_objects(Nx+2,2:(Ny+1),:) = objects(1,:,:)

    large_objects(2:(Nx+1),1,:) = objects(:,Ny,:)
    large_objects(2:(Nx+1),Ny+2,:) = objects(:,1,:)

    large_objects(1,1,:) = objects(Nx,Ny,:)
    large_objects(Nx+2,1,:) = objects(1,Ny,:)
    large_objects(1,Ny+2,:) = objects(Nx,1,:)
    large_objects(Nx+2,Ny+2,:) = objects(1,1,:)


    n_diel_matrix = 0
    ! do ix=1,no_objects
    ! write(*,*) 'Object',ix,object_params(ix)%param1
    ! end do
    !

    do ix=2,Nx+1
        do iy=2,Ny+1
            do iz=2,Nz-1
                N_diel = 0

                if (large_edges(ix,iy,iz).gt.0) then
                    if (object_params(large_objects(ix,iy,iz))%param1.eq.1) then
                        N_diel = N_diel + 1
                    end if
                end if
                if (large_edges(ix+1,iy,iz).gt.0) then
                    if (object_params(large_objects(ix+1,iy,iz))%param1.eq.1) then

                        N_diel = N_diel + 1
                    end if
                end if

                if (large_edges(ix,iy+1,iz).gt.0) then
                    if (object_params(large_objects(ix,iy+1,iz))%param1.eq.1) then

                        N_diel = N_diel + 1
                    end if
                end if
                if (large_edges(ix-1,iy,iz).gt.0) then
                    if (object_params(large_objects(ix-1,iy,iz))%param1.eq.1) then

                        N_diel = N_diel + 1
                    end if
                end if

                if (large_edges(ix,iy-1,iz).gt.0) then
                    if (object_params(large_objects(ix,iy-1,iz))%param1.eq.1) then

                        N_diel = N_diel + 1
                    end if
                end if
                if (large_edges(ix,iy,iz+1).gt.0) then
                    if (object_params(large_objects(ix,iy,iz+1))%param1.eq.1) then
                        N_diel = N_diel + 1
                    end if
                end if
                if (large_edges(ix,iy,iz-1).gt.0) then
                    if (object_params(large_objects(ix,iy,iz-1))%param1.eq.1) then

                        N_diel = N_diel + 1
                    end if
                end if

                if (large_edges(ix,iy,iz).gt.0.and.N_diel.gt.0) then
                    if (object_params(large_objects(ix,iy,iz))%param1.eq.1) then
                        n_diel_matrix(ix-1,iy-1,iz) = N_diel
                    end if
                end if
                ! if (n_diel_matrix(ix-1,iy-1,iz).ne.0) then
                !  write(*,*) 'N diel',ix,iy,iz,n_diel_matrix(ix,iy,iz)
                ! end if
            end do
        end do
    end do


    !  stop

end subroutine

subroutine prepare_diel_filter(Nx,Ny,Nz,model,diel_filter,debug,edges)
    use mth
    implicit none
    include "struct.h"

    integer:: ix,iy,iz,Nx,Ny,Nz,esum
    integer, dimension(Nx,Ny,Nz):: diel_filter,edges
    type(model_t):: model
    logical:: debug
    ! init filter
    diel_filter = 0

    ! the corners should not be at the periodic boundaries
    ! bottom line is not handled - should not be important
    do ix=2,Nx-1
        do iy=2,Ny-1
            do iz=2,Nz-1
                if (edges(ix,iy,iz).gt.0) then

                    ! z-plane

                    ! how many surface points around?

                    esum = isgn(edges(ix-1,iy,iz)) + isgn(edges(ix+1,iy,iz)) + isgn(edges(ix,iy-1,iz)) + &
                        isgn(edges(ix,iy+1,iz))
                    ! we need just 2 points!
                    if (esum.eq.2) then
                        if (edges(ix,iy+1,iz)*edges(ix -1,iy,iz).ne.0) then
                            diel_filter(ix,iy,iz) = 1
                        elseif (edges(ix,iy-1,iz)*edges(ix -1,iy,iz).ne.0) then
                            diel_filter(ix,iy,iz) = 2
                        elseif (edges(ix,iy-1,iz)*edges(ix +1,iy,iz).ne.0) then
                            diel_filter(ix,iy,iz) = 3
                        elseif (edges(ix,iy+1,iz)*edges(ix +1,iy,iz).ne.0) then
                            diel_filter(ix,iy,iz) = 4
                        end if
                    end if ! esum

                    ! x-plane

                    ! how many surface points around?

                    esum = isgn(edges(ix,iy,iz-1)) + isgn(edges(ix,iy,iz+1)) + isgn(edges(ix,iy-1,iz)) +&
                        isgn(edges(ix,iy+1,iz))
                    ! we need just 2 points!
                    if (esum.eq.2) then
                        if (edges(ix,iy+1,iz)*edges(ix ,iy,iz-1).ne.0) then
                            diel_filter(ix,iy,iz) = 5
                        elseif (edges(ix,iy-1,iz)*edges(ix,iy,iz-1).ne.0) then
                            diel_filter(ix,iy,iz) = 6
                        elseif (edges(ix,iy-1,iz)*edges(ix,iy,iz+1).ne.0) then
                            diel_filter(ix,iy,iz) = 7
                        elseif (edges(ix,iy+1,iz)*edges(ix ,iy,iz+1).ne.0) then
                            diel_filter(ix,iy,iz) = 8
                        end if
                    end if ! esum

                    ! y-plane

                    ! how many surface points around?

                    esum = isgn(edges(ix,iy,iz-1)) + isgn(edges(ix,iy,iz+1)) + isgn(edges(ix-1,iy,iz)) +&
                        isgn(edges(ix+1,iy,iz))
                    ! we need just 2 points!
                    if (esum.eq.2) then
                        if (edges(ix+1,iy,iz)*edges(ix ,iy,iz-1).ne.0) then
                            diel_filter(ix,iy,iz) = 9
                        elseif (edges(ix-1,iy,iz)*edges(ix,iy,iz-1).ne.0) then
                            diel_filter(ix,iy,iz) = 10
                        elseif (edges(ix-1,iy,iz)*edges(ix,iy,iz+1).ne.0) then
                            diel_filter(ix,iy,iz) = 11
                        elseif (edges(ix+1,iy,iz)*edges(ix ,iy,iz+1).ne.0) then
                            diel_filter(ix,iy,iz) = 12
                        end if
                    end if ! esum


                end if !edge point
            end do
        end do
    end do



end subroutine


subroutine filter_edge_charge(Nx,Ny,Nz,diel_filter,edge_charge,model)
    implicit none
    include "struct.h"
    integer:: ix,iy,iz,Nx,Ny,Nz,esum
    integer, dimension(Nx,Ny,Nz):: diel_filter
    real,dimension(Nx,Ny,Nz):: edge_charge,new_edge_charge
    type(model_t):: model
    real:: ch
    ! prepare the new charge matrix
    new_edge_charge = edge_charge

    do ix=2,Nx-1
        do iy=2,Ny-1
            do iz=2,Nz-1
                if (diel_filter(ix,iy,iz).gt.0) then
                    ! according to the type of point make mean out of the surrounding points
                    ! don't touch the surrounding points as we may be risking multiple writes
                    if (diel_filter(ix,iy,iz).eq.1) then
                        ch = (edge_charge(ix,iy,iz) + edge_charge(ix-1,iy,iz)/2.0 + edge_charge(ix,iy+1,iz)/2.0)/2.0
                        new_edge_charge(ix,iy,iz) = ch
                    elseif (diel_filter(ix,iy,iz).eq.2) then
                        ch = (edge_charge(ix,iy,iz) + edge_charge(ix-1,iy,iz)/2.0 + edge_charge(ix,iy-1,iz)/2.0)/2.0
                        new_edge_charge(ix,iy,iz) = ch
                    elseif (diel_filter(ix,iy,iz).eq.3) then
                        ch = (edge_charge(ix,iy,iz) + edge_charge(ix+1,iy,iz)/2.0 + edge_charge(ix,iy-1,iz)/2.0)/2.0
                        new_edge_charge(ix,iy,iz) = ch
                    elseif (diel_filter(ix,iy,iz).eq.4) then
                        ch = (edge_charge(ix,iy,iz) + edge_charge(ix+1,iy,iz)/2.0 + edge_charge(ix,iy+1,iz)/2.0)/2.0
                        new_edge_charge(ix,iy,iz) = ch
                    elseif (diel_filter(ix,iy,iz).eq.5) then
                        ch = (edge_charge(ix,iy,iz) + edge_charge(ix,iy+1,iz)/2.0 + edge_charge(ix,iy,iz-1)/2.0)/2.0
                        new_edge_charge(ix,iy,iz) = ch
                    elseif (diel_filter(ix,iy,iz).eq.6) then
                        ch = (edge_charge(ix,iy,iz) + edge_charge(ix,iy-1,iz)/2.0 + edge_charge(ix,iy,iz-1)/2.0)/2.0
                        new_edge_charge(ix,iy,iz) = ch
                    elseif (diel_filter(ix,iy,iz).eq.7) then
                        ch = (edge_charge(ix,iy,iz) + edge_charge(ix,iy-1,iz)/2.0 + edge_charge(ix,iy,iz+1)/2.0)/2.0
                        new_edge_charge(ix,iy,iz) = ch
                    elseif (diel_filter(ix,iy,iz).eq.8) then
                        ch = (edge_charge(ix,iy,iz) + edge_charge(ix,iy+1,iz)/2.0 + edge_charge(ix,iy,iz+1)/2.0)/2.0
                        new_edge_charge(ix,iy,iz) = ch
                    elseif (diel_filter(ix,iy,iz).eq.9) then
                        ch = (edge_charge(ix,iy,iz) + edge_charge(ix+1,iy,iz)/2.0 + edge_charge(ix,iy,iz-1)/2.0)/2.0
                        new_edge_charge(ix,iy,iz) = ch
                    elseif (diel_filter(ix,iy,iz).eq.10) then
                        ch = (edge_charge(ix,iy,iz) + edge_charge(ix-1,iy,iz)/2.0 + edge_charge(ix,iy,iz-1)/2.0)/2.0
                        new_edge_charge(ix,iy,iz) = ch
                    elseif (diel_filter(ix,iy,iz).eq.11) then
                        ch = (edge_charge(ix,iy,iz) + edge_charge(ix-1,iy,iz)/2.0 + edge_charge(ix,iy,iz+1)/2.0)/2.0
                        new_edge_charge(ix,iy,iz) = ch
                    elseif (diel_filter(ix,iy,iz).eq.12) then
                        ch = (edge_charge(ix,iy,iz) + edge_charge(ix+1,iy,iz)/2.0 + edge_charge(ix,iy,iz+1)/2.0)/2.0
                        new_edge_charge(ix,iy,iz) = ch
                    end if

                end if ! diel filter
            end do
        end do
    end do

    ! copy over the charge density
    edge_charge = new_edge_charge

end subroutine

subroutine construct_bc_matrix(Nx,Ny,Nz,objects,bc_matrix)
    implicit none
    integer:: Nz,Ny,Nx,i,j,k,p1,p2
    integer, dimension(Nx,Ny,Nz):: objects
    real, dimension(Nx,Ny,Nz)::bc_matrix
    bc_matrix= 0
    do i=1,Nx-1
        do j=1,Ny-1
            do k=1,Nz-1

                p1 = objects(i,j,k)*objects(i+1,j,k)*objects(i+1,j+1,k)*objects(i,j+1,k)
                p2 = objects(i,j,k+1)*objects(i+1,j,k+1)*objects(i+1,j+1,k+1)*objects(i,j+1,k+1)

                if (p1.gt.0.and.p2.gt.0) then
                    bc_matrix(i,j,k) = 1
                end if

            end do
        end do
    end do
    ! Periodic BC
    bc_matrix(:,Ny,:) = bc_matrix(:,1,:)
    bc_matrix(Nx,:,:) = bc_matrix(1,:,:)

end subroutine
subroutine construct_bc_matrix_older(Nx,Ny,Nz,Nx_total,Ny_total,Nz_total,objects,bc_matrix,Nx_start,Ny_start,Nz_start)
    implicit none
    integer:: Nz,Ny,Nx,i,j,k,p1,p2,Nx_total,Ny_total,Nz_total,Nx_start,Ny_start,Nz_start
    integer, dimension(Nx,Ny,Nz)::bc_matrix
    integer, dimension(Nx_total,Ny_total,Nz_total):: objects
    integer, dimension(Nx_total+2,Ny_total+2,Nz_total):: large_objects
    write(*,*) 'Constructing large matrix',Nx_total,Ny_total,Nz_total

    call  prepare_large_grid_i(Nx_total,Ny_total,Nz_total,objects,large_objects)

    write(*,*) 'Large matrix constructed'

    bc_matrix= 0
    do i=2,Nx_total+1
        do j=2,Ny_total+1
            do k=2,Nz_total-1

                p1 = large_objects(i,j,k)*large_objects(i+1,j,k)*large_objects(i+1,j+1,k)*large_objects(i,j+1,k)
                p2 = large_objects(i,j,k+1)*large_objects(i+1,j,k+1)*large_objects(i+1,j+1,k+1)*large_objects(i,j+1,k+1)

                if (p1.gt.0.and.p2.gt.0.and.i.gt.Nx_start.and.i.le.(Nx_start + Nx+1).and.j.gt.Ny_start.and.j.le.(Ny_start + Ny+1).and.k.gt.Nz_start.and.k.le.(Nz_start + Nz+1)) then
                    bc_matrix(i -Nx_start,j-Ny_start,k-Nz_start+1) = 1
                end if

            end do
        end do
    end do
    ! bc_matrix(:,Ny,:) = bc_matrix(:,1,:)
    ! bc_matrix(Nx,:,:) = bc_matrix(1,:,:)

end subroutine


real function det3d(m)
    real, dimension(3,3):: m
    real:: d
    d =0.0

    d = m(1,1)*m(2,2)*m(3,3) + m(2,1)*m(3,2)*m(1,3) + m(3,1)*m(1,2)*m(2,3) &
        - m(1,1)*m(3,2)*m(2,3)  - m(2,1)*m(1,2)*m(3,3) - m(3,1)*m(2,2)*m(1,3)


    det3d = d


end function

real function find_intersection_3d(p,u,x1,x2,x3)

    real,dimension(3):: p,u,x1,x2,x3

    real:: a,b,c,d,t,K
    real,dimension(3,3):: m,m2

    d = 1.0

    ! write(*,*) 'x1:',x1
    ! write(*,*) 'x2:',x2
    ! write(*,*) 'x3:',x3

    m(:,1) = x1
    m(:,2) = x2
    m(:,3) = x3
    ! write(*,*) 'm:',m

    K = det3d(m)
    ! write(*,*) 'K:',K
    ! m2 =m
    ! m2(:,1) = 1.0
    ! replace first column by 1

    !a = - d/K*det3d(m2)
    a = - d/K*(x1(2)*(x2(3) - x3(3)) + x2(2)*(x3(3) - x1(3)) + x3(2)*(x1(3) - x2(3)))
    ! write(*,*) 'a:',a

    ! m2 =m
    ! m2(:,2) = 1.0
    ! replace second column by 1
    !  write(*,*) 'm2:',m2

    !b = - d/K*det3d(m2)
    b = - d/K*(  x1(3)*(x2(1) - x3(1)) + x2(3)*(x3(1) - x1(1)) + x3(3)*(x1(1) - x2(1)))

    ! write(*,*) 'b:',b


    ! m2 =m
    ! m2(:,3) = 1.0
    ! replace third column by 1
    ! write(*,*) 'm2:',m2
    !c = - d/K*det3d(m2)
    ! this is not a comment ;)
    c = - d/K*( x1(1)*(x2(2) - x3(2)) + x2(1)*(x3(2) - x1(2)) + x3(1)*(x1(2) - x2(2))            )

    ! write(*,*) 'c:',c

    ! ok now we have the equation ready, let's calculate the t
    if ((a*u(1) + b*u(2) + c*u(3)).eq.0.0) then
        t = 999999.0
    else
        t = -(d + a*p(1) + b*p(2) + c*p(3))/(a*u(1) + b*u(2) + c*u(3))
    end if
    if (t.lt.0) then
        write(*,*) 'x1:',x1
        write(*,*) 'x2:',x2
        write(*,*) 'x3:',x3
        write(*,*) 'p',p
        write(*,*) 'u',u

    end if

    ! debug output
    !write(*,*) 'Point 1',x1
    !write(*,*) 'Point 2',x2
    !write(*,*) 'Point 3',x3
    !write(*,*) 'Plane',a,b,c,d
    !write(*,*) 'point',p
    !write(*,*) 'velocity',u
    !write(*,*) 't',t
    !write(*,*) 'Cross check',p + t*u



    find_intersection_3d = t
end function


subroutine find_bc_check_limits(Nx,Ny,Nz,objects,dx,dy,dz,Lz_low_limit,Lz_high_limit)
    implicit none
    integer:: Nx,Nz,Ny,Nz_low_limit,Nz_high_limit,z,y,found_object
    real:: dx,dz,dy,Lz_low_limit,Lz_high_limit
    integer, dimension(Nx,Ny,Nz):: objects

    Nz_low_limit = 0
    Nz_high_limit = Nz

    found_object = 0
    do z=1,Nz
        if (sum(objects(:,:,z)).eq.0.and.found_object.eq.0) then
            Nz_low_limit = z
        else
            found_object = 1
        end if
    end do

    found_object = 0

    do z=Nz,1,-1
        if (sum(objects(:,:,z)).eq.0.and.found_object.eq.0) then
            Nz_high_limit = z
        else
            found_object = 1
        end if
    end do

    ! safety check
    Nz_low_limit = max(Nz_low_limit - 3,1)
    Nz_high_limit = min(Nz_high_limit + 3,Nz)

    Lz_low_limit = float(Nz_low_limit - 1)*dz
    Lz_high_limit = float(Nz_high_limit -1)*dz

    write(*,*) 'Determined BC limits:',Lz_low_limit,Lz_high_limit

end subroutine



subroutine get_next_cell_border(p,u,dx,dy,dz,t)
    real, dimension(3):: p,u,xa,xb,xc
    real:: dx,dy,dz,t,t1,t2,t3
    integer:: x1,y1,z1

    x1 = int(p(1)/dx) +1
    y1 = int(p(2)/dy) +1
    z1 = int(p(3)/dz)  +1


    if (u(1).gt.0.0) then
        ! x plane positive
        xa = (/x1 +1,y1,z1/)
        xb =  (/x1 +1,y1 +1,z1/)
        xc =  (/x1 +1,y1 +1,z1 + 1/)
    else
        ! x plane negative
        xa = (/x1 ,y1,z1/)
        xb =  (/x1 ,y1 +1,z1/)
        xc =  (/x1 ,y1 +1,z1 + 1/)
    end if
    t1 = find_intersection_3d(p/dx+1.0,u,xa,xb,xc)

    if (t1.lt.0.0) then
        write(*,*) 'Error, t1 less than zero',t1
    end if
    if (u(2).gt.0.0) then
        ! y plane positive
        xa = (/x1 ,y1+1,z1/)
        xb =  (/x1 +1,y1 +1,z1/)
        xc =  (/x1 +1,y1 +1,z1 + 1/)
    else
        ! y plane negative
        xa = (/x1 ,y1,z1/)
        xb =  (/x1+1 ,y1 ,z1/)
        xc =  (/x1+1 ,y1 ,z1 + 1/)
    end if

    t2 = find_intersection_3d(p/dx+1.0,u,xa,xb,xc)

    if (t2.lt.0.0) then
        write(*,*) 'Error, t2 less than zero',t2
    end if

    if (u(3).gt.0.0) then
        ! z plane positive
        xa = (/x1    ,y1,   z1+1/)
        xb =  (/x1 +1,y1 ,  z1+1/)
        xc =  (/x1 +1,y1 +1,z1 + 1/)
    else
        ! y plane negative
        xa = (/x1 ,y1,z1/)
        xb =  (/x1+1 ,y1 ,z1/)
        xc =  (/x1+1 ,y1+1 ,z1/)
    end if

    t3 = find_intersection_3d(p/dx+1.0,u,xa,xb,xc)

    if (t3.lt.0.0) then
        write(*,*) 'Error, t3 less than zero',t3
    end if
    ! write(*,*) 'times:',t1,t2,t3
    t = min(t1,t2,t3)

end subroutine


subroutine fix_periodic_r(slices,proc_no,proc_max,Nx,Ny,Nz,m,no_species,model)
    implicit none
    include 'struct.h'
    integer:: Nx,Ny,Nz,proc_no,proc_max,no_species
    real, dimension(no_species,Nx,Ny,Nz):: m
    type(slice_info), dimension(proc_max+1):: slices
    type(model_t):: model
    if (slices(proc_no+1)%Ly_start.eq.0.0) then
        m(:,1:Nx,1,1:Nz)=2*m(:,1:Nx,1,1:Nz)   !PC
    end if
    if (slices(proc_no+1)%Lx_start.eq.0.0) then
        m(:,1,1:Ny,1:Nz)=2*m(:,1,1:Ny,1:Nz)   !PC
    end if

    if (slices(proc_no+1)%Ly_stop.eq.model%Ly_total) then
        m(:,1:Nx,Ny,1:Nz)=2*m(:,1:Nx,Ny,1:Nz)   !PC
    end if
    if (slices(proc_no+1)%Lx_stop.eq.model%Lx_total) then
        m(:,Nx,1:Ny,1:Nz)=2*m(:,Nx,1:Ny,1:Nz)   !PC
    end if
    if (slices(proc_no+1)%Lz_stop.eq.model%Lz_total) then
        m(:,1:Nx,1:Ny,Nz)=m(:,1:Nx,1:Ny,Nz)*2.0
    end if

end subroutine
