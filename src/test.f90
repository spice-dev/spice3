program testing
    integer:: nx,ny,nz,f
    integer*8:: solver
    real*8, dimension(65*65*65):: bias,Pot,rho
    integer*4, dimension(65*65*65):: bitmap
    integer*8:: solverpointer
    real*4:: t_start,t_stop

    nx = 65
    ny = nx
    nz = nx
    solver = 0
    equipot = 0
    eq_mask = 0.0
    ntot = nx*ny*nz
    bias(1:(nx*ny)) = -3.0
    bitmap(1:(nx*ny)) = 1
    bitmap((ntot -nz):ntot) = 1
    bias((ntot -nz):ntot) = 0.0
    f=0
    solverpointer = 0
    rho = 0.0
    Pot = 0.0
    ! call Solver.SetDimensions(nx, ny, nz);
    write(*,*) 'Calling init',nx,ny,nz
    call InitSolver(nx,ny,nz,solverpointer)
    write(*,*) 'Got pointer',solverpointer
    write(*,*) 'Calling configure',nx,ny,nz

    call ConfigureSolver(solverpointer,bitmap,bias,f,nx,ny,nz)
    write(*,*) 'Calling Solve'
    t_start = secnds(0.0)

    call solvewithsolver(solverpointer,Pot,rho)
    write(*,*) 'Sample Pot points:: [1,1,1]', Pot(1)
    write(*,*) 'Sample Pot points:: [nx,ny,nz]', Pot(ntot)
    write(*,*) 'Sample Pot points:: [nx,ny,nz/2]', Pot(int(ntot/2))
    t_stop = secnds(t_start)
    write(*,*) 'Duration',t_stop
    call freesolver(solverpointer)

end program
