PROGRAM stitcher
    ! simple fortran routine to stitch all decomposed output files into a single one
    ! writtent by Michael Komm
    use getoptions
    implicit none
    ! for command line arguments
    character :: okey
    logical:: verbose, debug,stitch_averaged
    character*512:: ifile, tfile,ofile
    integer:: no_files, Nx_total,Ny_total,Nz_total,Np,history_ntimes,no_species,proc_max,count,nobjects
    stitch_averaged = .false.
    verbose = .false.
    debug = .false.
    write(*,*) 'Parsing command line'
    do
        okey=getopt('i:t:o:n:hvdq')
        if(okey.eq.'>') exit
        if(okey.eq.'!') then
            write(6,*) 'unknown option: ', trim(optarg)
            call display_help()
            stop
        end if
        if(okey.eq.'v') then
            verbose = .true.
            write(6,*) 'Running in verbose mode'
        end if

        if(okey.eq.'d') then
            debug = .true.
            write(6,*) 'Running in debug mode'
        end if
        if(okey.eq.'h') then
            call display_help()
            stop
        end if

        if(okey.eq.'o') then
            ofile=trim(optarg)
            write(6,*) 'Stitching final output file ',trim(optarg)
            stitch_averaged = .true.
        end if
        if(okey.eq.'i') then
            ifile=trim(optarg)
            write(6,*) 'Using input file ',trim(optarg)
        end if
        if(okey.eq.'t') then
            tfile=trim(optarg)
            write(6,*) 'Stitching time diag',trim(optarg)
        end if

    end do


    ! open the first temporal file and exract the global dimensions
    call get_matrix_dimensions(Nx_total,Ny_total,Nz_total,tfile,debug,Np,history_ntimes,no_species,proc_max,count,nobjects)
    write(*,*) 'Total dimensions ',Nx_total,Ny_total,Nz_total,count
    no_files = proc_max +1
    write(*,*) 'Number of slices',no_files
    ! call mainloop
    call mainloop(Nx_total,Ny_total,Nz_total,no_files,verbose, debug,tfile,ofile,Np,history_ntimes,no_species,proc_max,count,nobjects,stitch_averaged)













END PROGRAM

subroutine mainloop(Nx_total,Ny_total,Nz_total,no_files,verbose, debug,tfile,ofile,Np,history_ntimes,no_species,proc_max,count,nobjects,stitch_averaged)
    use MATIO
    implicit none
    integer:: Nx_total,Ny_total,Nz_total,no_files,ierr,version,di,Np,no_species,history_ntimes,proc_max,i,count,nobjects
    real:: dr
    logical:: verbose, debug,stitch_averaged
    character*120:: tfile,ofile,mname
    real*8, dimension(Nx_total,Ny_total,Nz_total):: dumr
    integer, dimension(Nx_total,Ny_total,Nz_total):: dumi
    integer, dimension(no_species,history_ntimes):: snumber
    real, dimension(nobjects,no_species,count):: objects_current
    real, dimension(no_files,10,count):: iter_time
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR
    character*2:: sp_str
    sp_str = '  '


    dumr = 0.0
    dumi = 0
    ! write various constants
    if (debug) then
        write(*,*) 'Working on file',trim(ofile)
    end if
    ! open the MAT file for writting

    IERR = FMAT_CREATE(trim(tfile)//'.mat'//char(0), MAT)

    mname='version'
    call get_integer(mname,di,tfile,debug)
    IERR = FMAT_VARCREATE('version', di, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, di)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('Nx',Nx_total , MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Nx_total)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('Ny',Ny_total , MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Ny_total)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('Nz',Nz_total , MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Nz_total)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('nospecies',no_species , MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, no_species)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('procmax',proc_max , MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, proc_max)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('historyntimes',history_ntimes , MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, history_ntimes)
    IERR = FMAT_VARFREE(MATVAR)

    mname='count'
    call get_integer(mname,di,tfile,debug)
    IERR = FMAT_VARCREATE('count', di, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, di)
    IERR = FMAT_VARFREE(MATVAR)

    write(*,*) 'Restored count',di
    mname='Na'
    call get_integer(mname,di,tfile,debug)
    IERR = FMAT_VARCREATE('Na', di, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, di)
    IERR = FMAT_VARFREE(MATVAR)
    mname='Np'
    call get_integer(mname,di,tfile,debug)
    IERR = FMAT_VARCREATE('Np', di, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, di)
    IERR = FMAT_VARFREE(MATVAR)
    mname='Nc'
    call get_integer(mname,di,tfile,debug)
    IERR = FMAT_VARCREATE('Nc', di, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, di)
    IERR = FMAT_VARFREE(MATVAR)


    IERR = FMAT_CLOSE(MAT)

    ! stitch potential from the time diag files
    mname = 'Pot'
    call stitch_r_matrix(Nx_total,Ny_total,Nz_total,no_files,tfile,mname,dumr,.true.,.true.,debug)
    if (verbose) then
        write(*,*) 'Stitched Potential',sum(dumr)
    end if
    call  write_r_matrix(Nx_total,Ny_total,Nz_total,tfile,dumr,mname,debug,.false.)

    !mname = 'Potvac'
    !call stitch_r_matrix(Nx_total,Ny_total,Nz_total,no_files,tfile,mname,dumr,.true.,.true.,debug)
    !if (verbose) then
    !1  write(*,*) 'Stitched ',mname,sum(dumr)
    !end if

    !call  write_r_matrix(Nx_total,Ny_total,Nz_total,tfile,dumr,mname,debug,.false.)

    mname = 'Potvac'
    call load_r_matrix(Nx_total,Ny_total,Nz_total,tfile,mname,dumr,.true.,debug)
    !call stitch_r_matrix(Nx_total,Ny_total,Nz_total,no_files,tfile,mname,dumr,.true.,.true.,debug)
    if (verbose) then
        write(*,*) 'Loaded ',mname,sum(dumr)
    end if
    call  write_r_matrix(Nx_total,Ny_total,Nz_total,tfile,dumr,mname,debug,.false.)

    mname = 'Ex'
    call stitch_r_matrix(Nx_total,Ny_total,Nz_total,no_files,tfile,mname,dumr,.true.,.true.,debug)
    if (verbose) then
        write(*,*) 'Stitched ',mname,sum(dumr)
    end if
    call  write_r_matrix(Nx_total,Ny_total,Nz_total,tfile,dumr,mname,debug,.false.)

    mname = 'Ey'
    call stitch_r_matrix(Nx_total,Ny_total,Nz_total,no_files,tfile,mname,dumr,.true.,.true.,debug)
    if (verbose) then
        write(*,*) 'Stitched ',mname,sum(dumr)
    end if
    call  write_r_matrix(Nx_total,Ny_total,Nz_total,tfile,dumr,mname,debug,.false.)

    mname = 'Ez'
    call stitch_r_matrix(Nx_total,Ny_total,Nz_total,no_files,tfile,mname,dumr,.true.,.true.,debug)
    if (verbose) then
        write(*,*) 'Stitched ',mname,sum(dumr)
    end if
    call  write_r_matrix(Nx_total,Ny_total,Nz_total,tfile,dumr,mname,debug,.false.)

    mname = 'objects'
    call stitch_i_matrix(Nx_total,Ny_total,Nz_total,no_files,tfile,mname,dumr,.true.,.true.,debug)
    if (verbose) then
        write(*,*) 'Stitched ',mname,sum(dumr)
    end if
    call  write_i_matrix(Nx_total,Ny_total,Nz_total,tfile,dumr,mname,debug,.false.)

    mname = 'edges'
    call stitch_i_matrix(Nx_total,Ny_total,Nz_total,no_files,tfile,mname,dumr,.true.,.true.,debug)
    if (verbose) then
        write(*,*) 'Stitched ',mname,sum(dumr)
    end if
    call  write_i_matrix(Nx_total,Ny_total,Nz_total,tfile,dumr,mname,debug,.false.)

    mname = 'edgecharge'
    call stitch_r_matrix(Nx_total,Ny_total,Nz_total,no_files,tfile,mname,dumr,.false.,.true.,debug)
    if (verbose) then
        write(*,*) 'Stitched ',mname,sum(dumr)
    end if
    call  write_r_matrix(Nx_total,Ny_total,Nz_total,tfile,dumr,mname,debug,.false.)

    mname = 'bcmatrix'
    call stitch_r_matrix(Nx_total,Ny_total,Nz_total,no_files,tfile,mname,dumr,.true.,.true.,debug)
    if (verbose) then
        write(*,*) 'Stitched ',mname,sum(dumr)
    end if
    call  write_r_matrix(Nx_total,Ny_total,Nz_total,tfile,dumr,mname,debug,.false.)



    !mname = 'dielectriccell'
    !call stitch_r_matrix(Nx_total,Ny_total,Nz_total,no_files,tfile,mname,dumr,.true.,.true.,debug)
    !if (verbose) then
    !  write(*,*) 'Stitched ',mname,sum(dumr)
    !end if
    !call  write_r_matrix(Nx_total,Ny_total,Nz_total,tfile,dumr,mname,debug,.false.)


    mname = 'snumber'
    snumber = 0
    call stitch_2D_i_matrix(no_species,history_ntimes,no_files,tfile,mname,snumber,.false.,.true.,debug)
    if (verbose) then
        write(*,*) 'Stitched ',mname,sum(snumber)
    end if
    call  write_2D_i_matrix(no_species,history_ntimes,tfile,snumber,mname,debug,.false.)

    mname = 'objectscurrent'
    write(*,*) 'objectscurrent dim',nobjects,no_species,count
    call stitch_3D_r_matrix(nobjects,no_species,count,no_files,tfile,mname,objects_current,.false.,.true.,debug)

    if (verbose) then
        write(*,*) 'Stitched ',mname,sum(snumber)
    end if
    call  write_r_matrix(nobjects,no_species,count,tfile,objects_current,mname,debug,.false.)

    ! mname = 'itertime'
    !
    ! call stitch_3D_r_matrix(no_files,10,count,no_files,tfile,mname,iter_time,.false.,.true.,debug)
    !
    ! if (verbose) then
    !   write(*,*) 'Stitched ',mname,sum(iter_time)
    ! end if
    ! call  write_r_matrix(no_files,10,count,tfile,iter_time,mname,debug,.false.)


    !
    ! stitch densities
    do i=1,no_species
        write(sp_str(1:2),'(I2.2)') i

        mname = 'rho'//sp_str
        call stitch_r_matrix(Nx_total,Ny_total,Nz_total,no_files,tfile,mname,dumr,.false.,.true.,debug)
        if (verbose) then
            write(*,*) 'Stitched ',mname,sum(dumr)
        end if
        call  write_r_matrix(Nx_total,Ny_total,Nz_total,tfile,dumr,mname,debug,.false.)

        mname = 'vx'//sp_str
        call stitch_r_matrix(Nx_total,Ny_total,Nz_total,no_files,tfile,mname,dumr,.true.,.true.,debug)
        if (verbose) then
            write(*,*) 'Stitched ',mname,sum(dumr)
        end if
        call  write_r_matrix(Nx_total,Ny_total,Nz_total,tfile,dumr,mname,debug,.false.)

        mname = 'vy'//sp_str
        call stitch_r_matrix(Nx_total,Ny_total,Nz_total,no_files,tfile,mname,dumr,.true.,.true.,debug)
        if (verbose) then
            write(*,*) 'Stitched ',mname,sum(dumr)
        end if
        call  write_r_matrix(Nx_total,Ny_total,Nz_total,tfile,dumr,mname,debug,.false.)


        mname = 'vz'//sp_str
        call stitch_r_matrix(Nx_total,Ny_total,Nz_total,no_files,tfile,mname,dumr,.true.,.true.,debug)
        if (verbose) then
            write(*,*) 'Stitched ',mname,sum(dumr)
        end if
        call  write_r_matrix(Nx_total,Ny_total,Nz_total,tfile,dumr,mname,debug,.false.)
    end do

    ! now let's stitch averaged output
    if (stitch_averaged) then
        write(*,*) 'Stitching averaged output from', ofile
        mname = 'Potav'
        call stitch_r_matrix(Nx_total,Ny_total,Nz_total,no_files,ofile,mname,dumr,.true.,.false.,debug)
        if (verbose) then
            write(*,*) 'Stitched averaged Potential',sum(dumr)
        end if
        call  write_r_matrix(Nx_total,Ny_total,Nz_total,ofile,dumr,mname,debug,.false.)


        mname = 'edgechargeav'
        call stitch_r_matrix(Nx_total,Ny_total,Nz_total,no_files,ofile,mname,dumr,.true.,.false.,debug)
        if (verbose) then
            write(*,*) 'Stitched averaged Edge charge',sum(dumr)
        end if
        call  write_r_matrix(Nx_total,Ny_total,Nz_total,ofile,dumr,mname,debug,.false.)

        mname = 'Exav'
        call stitch_r_matrix(Nx_total,Ny_total,Nz_total,no_files,ofile,mname,dumr,.true.,.false.,debug)
        if (verbose) then
            write(*,*) 'Stitched ',mname,sum(dumr)
        end if
        call  write_r_matrix(Nx_total,Ny_total,Nz_total,ofile,dumr,mname,debug,.false.)

        mname = 'Eyav'
        call stitch_r_matrix(Nx_total,Ny_total,Nz_total,no_files,ofile,mname,dumr,.true.,.false.,debug)
        if (verbose) then
            write(*,*) 'Stitched ',mname,sum(dumr)
        end if
        call  write_r_matrix(Nx_total,Ny_total,Nz_total,ofile,dumr,mname,debug,.false.)

        mname = 'Ezav'
        call stitch_r_matrix(Nx_total,Ny_total,Nz_total,no_files,ofile,mname,dumr,.true.,.false.,debug)
        if (verbose) then
            write(*,*) 'Stitched ',mname,sum(dumr)
        end if
        call  write_r_matrix(Nx_total,Ny_total,Nz_total,ofile,dumr,mname,debug,.false.)

        do i=1,no_species
            write(sp_str(1:2),'(I2.2)') i
            mname = 'dens'//sp_str
            call stitch_r_matrix(Nx_total,Ny_total,Nz_total,no_files,ofile,mname,dumr,.false.,.false.,debug)
            if (verbose) then
                write(*,*) 'Stitched ',mname,sum(dumr)
            end if
            call  write_r_matrix(Nx_total,Ny_total,Nz_total,ofile,dumr,mname,debug,.false.)

        end do

        do i=1,no_species
            write(sp_str(1:2),'(I2.2)') i
            mname = 'edgeflux'//sp_str
            call stitch_r_matrix(Nx_total,Ny_total,Nz_total,no_files,ofile,mname,dumr,.false.,.false.,debug)
            if (verbose) then
                write(*,*) 'Stitched ',mname,sum(dumr)
            end if
            call  write_r_matrix(Nx_total,Ny_total,Nz_total,ofile,dumr,mname,debug,.false.)

        end do

        do i=1,no_species
            write(sp_str(1:2),'(I2.2)') i


            mname = 'vxav'//sp_str
            call stitch_r_matrix(Nx_total,Ny_total,Nz_total,no_files,ofile,mname,dumr,.true.,.false.,debug)
            if (verbose) then
                write(*,*) 'Stitched ',mname,sum(dumr)
            end if
            call  write_r_matrix(Nx_total,Ny_total,Nz_total,ofile,dumr,mname,debug,.false.)

            mname = 'vyav'//sp_str
            call stitch_r_matrix(Nx_total,Ny_total,Nz_total,no_files,ofile,mname,dumr,.true.,.false.,debug)
            if (verbose) then
                write(*,*) 'Stitched ',mname,sum(dumr)
            end if
            call  write_r_matrix(Nx_total,Ny_total,Nz_total,ofile,dumr,mname,debug,.false.)

            mname = 'vzav'//sp_str
            call stitch_r_matrix(Nx_total,Ny_total,Nz_total,no_files,ofile,mname,dumr,.true.,.false.,debug)
            if (verbose) then
                write(*,*) 'Stitched ',mname,sum(dumr)
            end if
            call  write_r_matrix(Nx_total,Ny_total,Nz_total,ofile,dumr,mname,debug,.false.)


            !mname = 'edgeflux'//sp_str
            !call stitch_r_matrix(Nx_total,Ny_total,Nz_total,no_files,ofile,mname,dumr,.false.,.false.,debug)
            !if (verbose) then
            !  write(*,*) 'Stitched ',mname,sum(dumr)
            !end if
            !call  write_r_matrix(Nx_total,Ny_total,Nz_total,ofile,dumr,mname,debug,.false.)

            mname = 'edgeenergyflux'//sp_str
            call stitch_r_matrix(Nx_total,Ny_total,Nz_total,no_files,ofile,mname,dumr,.false.,.false.,debug)
            if (verbose) then
                write(*,*) 'Stitched ',mname,sum(dumr)
            end if
            call  write_r_matrix(Nx_total,Ny_total,Nz_total,ofile,dumr,mname,debug,.false.)



        end do


    end if


end subroutine




subroutine display_help()
    write(*,*) 'Postprocess tool for SPICE3'
    write(*,*) '=============================='
    write(*,*) 'usage: ./stitcher [-vhd] [-i INPUT_FILE] [-o OUTPUT_FILE] [-t TDIAG_FILE] [-n no_files]'
    write(*,*) '	-v verbose mode'
    write(*,*) '	-d debug mode'
    write(*,*) '	-h display this help'
    write(*,*) '	-i input file'
    write(*,*) '	-o output file to stitch'
    write(*,*) '	-t time diag file to stitch'
    write(*,*) '	-n number of slices'
end subroutine

subroutine get_matrix_dimensions(Nx,Ny,Nz,mfile,debug,Np, history_ntimes,no_species,proc_max,count,nobjects)
    use MATIO
    implicit none
    integer:: ierr,Nx,Ny,Nz,Np, history_ntimes,no_species,proc_max,count,nobjects
    character*120:: mfile,ifile
    logical:: debug
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR

    ifile =   trim(mfile)//'T00.mat'//char(0)
    if (debug) then
        write(*,*) 'Working on file',ifile
    end if

    ierr = FMat_Open(ifile,MAT_ACC_RDONLY,MAT)
    if (debug) then
        write(*,*) 'Mat file opened with status',ierr
    end if
    ! extract local dimensions
    ierr = FMat_VarReadInfo(MAT,'Nxtotal',MATVAR)
    write(*,*) ierr
    ierr = FMat_VarReadData(MAT,MATVAR,Nx)
    ierr = FMat_VarReadInfo(MAT,'Nytotal',MATVAR)
    write(*,*) ierr
    ierr = FMat_VarReadData(MAT,MATVAR,Ny)
    ierr = FMat_VarReadInfo(MAT,'Nztotal',MATVAR)
    write(*,*) ierr
    ierr = FMat_VarReadData(MAT,MATVAR,Nz)
    ierr = FMat_VarReadInfo(MAT,'nospecies',MATVAR)
    write(*,*) ierr
    ierr = FMat_VarReadData(MAT,MATVAR,no_species)
    ierr = FMat_VarReadInfo(MAT,'procmax',MATVAR)
    write(*,*) ierr
    ierr = FMat_VarReadData(MAT,MATVAR,proc_max)
    ierr = FMat_VarReadInfo(MAT,'Np',MATVAR)
    write(*,*) ierr
    ierr = FMat_VarReadData(MAT,MATVAR,Np)
    ierr = FMat_VarReadInfo(MAT,'historyntimes',MATVAR)
    write(*,*) ierr
    ierr = FMat_VarReadData(MAT,MATVAR,history_ntimes)
    ierr = FMat_VarReadInfo(MAT,'count',MATVAR)
    write(*,*) ierr
    ierr = FMat_VarReadData(MAT,MATVAR,count)
    ierr = FMat_VarReadInfo(MAT,'nobjects',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,nobjects)



    write(*,*) 'Local dimensions', Nx,Ny,Nz
    write(*,*) 'No species',no_species
    write(*,*) 'History ntimes',history_ntimes
    write(*,*) 'Count',count
    write(*,*) 'No objects',nobjects
    write(*,*) 'Proc max',proc_max


    IERR = FMAT_CLOSE(MAT)


end subroutine

subroutine get_integer(mname,di,mfile,debug)
    use MATIO
    implicit none
    integer:: ierr,Nx,Ny,Nz,di
    character*120:: mfile,ifile,mname
    logical:: debug
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR

    ifile =   trim(mfile)//'T00.mat'//char(0)
    ierr = FMat_Open(ifile,MAT_ACC_RDONLY,MAT)
    ierr = FMat_VarReadInfo(MAT,trim(mname)//char(0),MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,di)
    IERR = FMAT_CLOSE(MAT)
end subroutine
subroutine get_real(mname,dr,mfile,debug)
    use MATIO
    implicit none
    integer:: ierr
    real:: dr
    character*120:: mfile,ifile,mname
    logical:: debug
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR

    ifile =   trim(mfile)//'T00.mat'//char(0)
    ierr = FMat_Open(ifile,MAT_ACC_RDONLY,MAT)
    ierr = FMat_VarReadInfo(MAT,trim(mname)//char(0),MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,dr)
    IERR = FMAT_CLOSE(MAT)
end subroutine


! load 3D matrix from slice 0 and copy it to the final output
subroutine load_r_matrix(Nx,Ny,Nz,mfile,mname,omatrix,time_diag,debug)
    use MATIO
    implicit none
    integer:: Nx,Ny,Nz,no_files,i,ierr,Nx_local,Ny_local,Nz_local,Nx_start,Ny_start,Nz_start,Nx_total,Ny_total,Nz_total,Nx_stop,Ny_stop,Nz_stop
    real, dimension(Nx,Ny,Nz):: omatrix,dum
    character*120:: mfile,mname,ifile
    logical:: average,time_diag,debug
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR
    character*2:: sp_str
    sp_str = '  '
    omatrix = 0.0
    ! loop over the output files
    CALL FMAT_LOGINIT('stitch')

    i=1
    dum = 0
    write(sp_str(1:2),'(I2.2)') i -1
    if (time_diag) then
        ifile =   trim(mfile)//'T'//sp_str//'.mat'//char(0)
    else
        ifile =   trim(mfile)//sp_str//'.mat'//char(0)
    end if
    if (debug) then
        write(*,*) 'Working on file',ifile
    end if

    ierr = FMat_Open(ifile,MAT_ACC_RDONLY,MAT)
    ierr = FMat_VarReadInfo(MAT,trim(mname)//char(0),MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,omatrix)
    IERR = FMAT_VARFREE(MATVAR)


end subroutine
! stitch simple 3D matrix, like a potential
subroutine stitch_r_matrix(Nx,Ny,Nz,no_files,mfile,mname,omatrix,average,time_diag,debug)
    use MATIO
    implicit none
    integer:: Nx,Ny,Nz,no_files,i,ierr,Nx_local,Ny_local,Nz_local,Nx_start,Ny_start,Nz_start,Nx_total,Ny_total,Nz_total,Nx_stop,Ny_stop,Nz_stop
    real*8, dimension(Nx,Ny,Nz):: omatrix,dum
    character*120:: mfile,mname,ifile
    logical:: average,time_diag,debug
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR
    character*2:: sp_str
    sp_str = '  '
    omatrix = 0.0
    ! loop over the output files
    CALL FMAT_LOGINIT('stitch')

    do i=1,no_files
        dum = 0
        write(sp_str(1:2),'(I2.2)') i -1
        if (time_diag) then
            ifile =   trim(mfile)//'T'//sp_str//'.mat'//char(0)
        else
            ifile =   trim(mfile)//sp_str//'.mat'//char(0)
        end if
        if (debug) then
            write(*,*) 'Working on file',ifile
        end if

        ierr = FMat_Open(ifile,MAT_ACC_RDONLY,MAT)
        if (debug) then
            write(*,*) i,'Mat file opened with status',ierr
        end if
        ! extract start limits
        ierr = FMat_VarReadInfo(MAT,'Nxstart',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,Nx_start)
        ierr = FMat_VarReadInfo(MAT,'Nystart',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,Ny_start)
        ierr = FMat_VarReadInfo(MAT,'Nzstart',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,Nz_start)
        if (debug) then
            write(*,*) 'Start limits', Nx_start,Ny_start,Nz_start
        end if
        ! extract local dimensions
        ierr = FMat_VarReadInfo(MAT,'Nx',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,Nx_local)
        ierr = FMat_VarReadInfo(MAT,'Ny',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,Ny_local)
        ierr = FMat_VarReadInfo(MAT,'Nz',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,Nz_local)
        if (debug) then
            write(*,*) 'Local dimensions', Nx_local,Ny_local,Nz_local
        end if
        Nx_stop = Nx_start + Nx_local -1
        Ny_stop = Ny_start + Ny_local -1
        Nz_stop = Nz_start + Nz_local -1


        ! extract local dimensions
        ierr = FMat_VarReadInfo(MAT,'Nxtotal',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,Nx_total)
        ierr = FMat_VarReadInfo(MAT,'Nytotal',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,Ny_total)
        ierr = FMat_VarReadInfo(MAT,'Nztotal',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,Nz_total)
        if (debug) then
            write(*,*) 'Total dimensions', Nx_total,Ny_total,Nz_total
        end if

        ! load the matrix
        if (debug) then
            write(*,*) 'Loading matrix ', trim(mname)//char(0)
        end if

        ierr = FMat_VarReadInfo(MAT,trim(mname)//char(0),MATVAR)
        if (debug) then
            write(*,*) 'Loading matrix stauts', ierr
        end if


        if (ierr.eq.0) then
            ierr = FMat_VarReadData(MAT,MATVAR,dum(1:Nx_local,1:Ny_local,1:Nz_local))
            IERR = FMAT_VARFREE(MATVAR)

            ! add it to the total matrix
            if (average) then
                ! replace the values
                omatrix(Nx_start:Nx_stop,Ny_start:Ny_stop,Nz_start:Nz_stop) = dum(1:Nx,1:Ny,1:Nz)

            else
                ! simply add the values
                omatrix(Nx_start:Nx_stop,Ny_start:Ny_stop,Nz_start:Nz_stop) = omatrix(Nx_start:Nx_stop,Ny_start:Ny_stop,Nz_start:Nz_stop) +dum(1:Nx,1:Ny,1:Nz)
            end if

        end if ! no such variable
        ! close the file
    end do
    IERR = FMAT_CLOSE(MAT)
end subroutine

subroutine stitch_2D_i_matrix(Nx,Ny,no_files,mfile,mname,omatrix,average,time_diag,debug)
    use MATIO
    implicit none
    integer:: Nx,Ny,no_files,i,ierr,Nx_local,Ny_local,Nz_local,Nx_start,Ny_start,Nz_start,Nx_total,Ny_total,Nz_total,Nx_stop,Ny_stop,Nz_stop
    integer, dimension(Nx,Ny):: omatrix,dum
    character*120:: mfile,mname,ifile
    logical:: average,time_diag,debug
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR
    character*2:: sp_str
    sp_str = '  '
    omatrix = 0.0
    ! loop over the output files
    CALL FMAT_LOGINIT('stitch')

    do i=1,no_files
        dum = 0
        write(sp_str(1:2),'(I2.2)') i -1
        if (time_diag) then
            ifile =   trim(mfile)//'T'//sp_str//'.mat'//char(0)
        else
            ifile =   trim(mfile)//sp_str//'.mat'//char(0)
        end if
        if (debug) then
            write(*,*) 'Working on file',ifile
        end if

        ierr = FMat_Open(ifile,MAT_ACC_RDONLY,MAT)
        if (debug) then
            write(*,*) i,'Mat file opened with status',ierr
        end if
        ! load the matrix
        if (debug) then
            write(*,*) 'Loading matrix ', trim(mname)//char(0)
        end if

        ierr = FMat_VarReadInfo(MAT,trim(mname)//char(0),MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,dum)
        IERR = FMAT_VARFREE(MATVAR)
        ! add it to the total matrix
        if (average) then
            ! replace the values
            omatrix = omatrix + dum/float(no_files)

        else
            ! simply add the values
            omatrix = omatrix + dum
        end if
        ! close the file
    end do
    IERR = FMAT_CLOSE(MAT)
end subroutine

subroutine stitch_3D_i_matrix(Nx,Ny,Nz,no_files,mfile,mname,omatrix,average,time_diag,debug)
    use MATIO
    implicit none
    integer:: Nx,Ny,Nz,no_files,i,ierr,Nx_local,Ny_local,Nz_local,Nx_start,Ny_start,Nz_start,Nx_total,Ny_total,Nz_total,Nx_stop,Ny_stop,Nz_stop
    integer, dimension(Nx,Ny,Nz):: omatrix,dum
    character*120:: mfile,mname,ifile
    logical:: average,time_diag,debug
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR
    character*2:: sp_str
    sp_str = '  '
    omatrix = 0.0
    ! loop over the output files
    CALL FMAT_LOGINIT('stitch')

    do i=1,no_files
        dum = 0
        write(sp_str(1:2),'(I2.2)') i -1
        if (time_diag) then
            ifile =   trim(mfile)//'T'//sp_str//'.mat'//char(0)
        else
            ifile =   trim(mfile)//sp_str//'.mat'//char(0)
        end if
        if (debug) then
            write(*,*) 'Working on file',ifile
        end if

        ierr = FMat_Open(ifile,MAT_ACC_RDONLY,MAT)
        if (debug) then
            write(*,*) i,'Mat file opened with status',ierr
        end if
        ! load the matrix
        if (debug) then
            write(*,*) 'Loading matrix ', trim(mname)//char(0)
        end if

        ierr = FMat_VarReadInfo(MAT,trim(mname)//char(0),MATVAR)
        write(*,*) 'DIMS',MATVAR%dims(1:MATVAR%rank)
        ierr = FMat_VarReadData(MAT,MATVAR,dum)
        IERR = FMAT_VARFREE(MATVAR)
        ! add it to the total matrix
        if (average) then
            ! replace the values
            omatrix = omatrix + dum/float(no_files)

        else
            ! simply add the values
            omatrix = omatrix + dum
        end if
        ! close the file
    end do
    IERR = FMAT_CLOSE(MAT)
end subroutine
subroutine stitch_3D_r_matrix(Nx,Ny,Nz,no_files,mfile,mname,omatrix,average,time_diag,debug)
    use MATIO
    implicit none
    integer:: Nx,Ny,Nz,no_files,i,ierr,Nx_local,Ny_local,Nz_local,Nx_start,Ny_start,Nz_start,Nx_total,Ny_total,Nz_total,Nx_stop,Ny_stop,Nz_stop
    real, dimension(Nx,Ny,Nz):: omatrix,dum
    character*120:: mfile,mname,ifile
    logical:: average,time_diag,debug
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR
    character*2:: sp_str
    sp_str = '  '
    omatrix = 0.0
    ! loop over the output files
    CALL FMAT_LOGINIT('stitch')

    do i=1,no_files
        dum = 0
        write(sp_str(1:2),'(I2.2)') i -1
        if (time_diag) then
            ifile =   trim(mfile)//'T'//sp_str//'.mat'//char(0)
        else
            ifile =   trim(mfile)//sp_str//'.mat'//char(0)
        end if
        if (debug) then
            write(*,*) 'Working on file',ifile
        end if

        ierr = FMat_Open(ifile,MAT_ACC_RDONLY,MAT)
        if (debug) then
            write(*,*) i,'Mat file opened with status',ierr
        end if
        ! load the matrix
        if (debug) then
            write(*,*) 'Loading matrix ', trim(mname)//char(0)
        end if

        ierr = FMat_VarReadInfo(MAT,trim(mname)//char(0),MATVAR)
        write(*,*) 'DIMS',MATVAR%dims(1:MATVAR%rank)
        ierr = FMat_VarReadData(MAT,MATVAR,dum)
        IERR = FMAT_VARFREE(MATVAR)
        ! add it to the total matrix
        if (average) then
            ! replace the values
            omatrix = omatrix + dum/float(no_files)

        else
            ! simply add the values
            omatrix = omatrix + dum
        end if
        ! close the file
    end do
    IERR = FMAT_CLOSE(MAT)
end subroutine


subroutine stitch_i_matrix(Nx,Ny,Nz,no_files,mfile,mname,omatrix,average,time_diag,debug)
    use MATIO
    implicit none
    integer:: Nx,Ny,Nz,no_files,i,ierr,Nx_local,Ny_local,Nz_local,Nx_start,Ny_start,Nz_start,Nx_total,Ny_total,Nz_total,Nx_stop,Ny_stop,Nz_stop
    integer, dimension(Nx,Ny,Nz):: omatrix,dum
    character*120:: mfile,mname,ifile
    logical:: average,time_diag,debug
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR
    character*2:: sp_str
    sp_str = '  '
    omatrix = 0.0
    ! loop over the output files
    CALL FMAT_LOGINIT('stitch')

    do i=1,no_files
        dum = 0
        write(sp_str(1:2),'(I2.2)') i -1
        if (time_diag) then
            ifile =   trim(mfile)//'T'//sp_str//'.mat'//char(0)
        else
            ifile =   trim(mfile)//sp_str//'.mat'//char(0)
        end if
        if (debug) then
            write(*,*) 'Working on file',ifile
        end if

        ierr = FMat_Open(ifile,MAT_ACC_RDONLY,MAT)
        if (debug) then
            write(*,*) i,'Mat file opened with status',ierr
        end if
        ! extract start limits
        ierr = FMat_VarReadInfo(MAT,'Nxstart',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,Nx_start)
        ierr = FMat_VarReadInfo(MAT,'Nystart',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,Ny_start)
        ierr = FMat_VarReadInfo(MAT,'Nzstart',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,Nz_start)
        if (debug) then
            write(*,*) 'Start limits', Nx_start,Ny_start,Nz_start
        end if
        ! extract local dimensions
        ierr = FMat_VarReadInfo(MAT,'Nx',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,Nx_local)
        ierr = FMat_VarReadInfo(MAT,'Ny',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,Ny_local)
        ierr = FMat_VarReadInfo(MAT,'Nz',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,Nz_local)
        if (debug) then
            write(*,*) 'Local dimensions', Nx_local,Ny_local,Nz_local
        end if
        Nx_stop = Nx_start + Nx_local -1
        Ny_stop = Ny_start + Ny_local -1
        Nz_stop = Nz_start + Nz_local -1


        ! extract local dimensions
        ierr = FMat_VarReadInfo(MAT,'Nxtotal',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,Nx_total)
        ierr = FMat_VarReadInfo(MAT,'Nytotal',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,Ny_total)
        ierr = FMat_VarReadInfo(MAT,'Nztotal',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,Nz_total)
        if (debug) then
            write(*,*) 'Local dimensions', Nx_total,Ny_total,Nz_total
        end if

        ! load the matrix
        if (debug) then
            write(*,*) 'Loading matrix ', trim(mname)//char(0)
        end if

        ierr = FMat_VarReadInfo(MAT,trim(mname)//char(0),MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,dum(1:Nx_local,1:Ny_local,1:Nz_local))
        IERR = FMAT_VARFREE(MATVAR)
        ! add it to the total matrix
        if (average) then
            ! replace the values
            omatrix(Nx_start:Nx_stop,Ny_start:Ny_stop,Nz_start:Nz_stop) = dum(1:Nx,1:Ny,1:Nz)

        else
            ! simply add the values
            omatrix(Nx_start:Nx_stop,Ny_start:Ny_stop,Nz_start:Nz_stop) = omatrix(Nx_start:Nx_stop,Ny_start:Ny_stop,Nz_start:Nz_stop) +dum(1:Nx,1:Ny,1:Nz)
        end if
        ! close the file
    end do
    IERR = FMAT_CLOSE(MAT)
end subroutine




subroutine write_r_matrix(Nx,Ny,Nz,ofile,m,mname,debug,create)
    use MATIO
    implicit none
    integer:: ierr,Nx,Ny,Nz
    character*120:: mname,ofile
    logical:: debug,create
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR
    real, dimension(Nx,Ny,Nz):: m
    if (debug) then
        write(*,*) 'Working on file',trim(ofile)
    end if
    ! open the MAT file for writting
    if (create) then
        IERR = FMAT_CREATE(trim(ofile)//'.mat'//char(0), MAT)
    else
        ierr = FMat_Open(trim(ofile)//'.mat'//char(0),MAT_ACC_RDWR,MAT)
    end if

    IERR = FMAT_VARCREATE(trim(mname)//char(0), m, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, m)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_CLOSE(MAT)
end subroutine


subroutine write_i_matrix(Nx,Ny,Nz,ofile,m,mname,debug,create)
    use MATIO
    implicit none
    integer:: ierr,Nx,Ny,Nz
    character*120:: mname,ofile
    logical:: debug,create
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR
    integer, dimension(Nx,Ny,Nz):: m
    if (debug) then
        write(*,*) 'Working on file',trim(ofile)
    end if
    ! open the MAT file for writting
    if (create) then
        IERR = FMAT_CREATE(trim(ofile)//'.mat'//char(0), MAT)
    else
        ierr = FMat_Open(trim(ofile)//'.mat'//char(0),MAT_ACC_RDWR,MAT)
    end if

    IERR = FMAT_VARCREATE(trim(mname)//char(0), m, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, m)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_CLOSE(MAT)
end subroutine

subroutine write_2D_i_matrix(Nx,Ny,ofile,m,mname,debug,create)
    use MATIO
    implicit none
    integer:: ierr,Nx,Ny
    character*120:: mname,ofile
    logical:: debug,create
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR
    integer, dimension(Nx,Ny):: m
    if (debug) then
        write(*,*) 'Working on file',trim(ofile)
    end if
    ! open the MAT file for writting
    if (create) then
        IERR = FMAT_CREATE(trim(ofile)//'.mat'//char(0), MAT)
    else
        ierr = FMat_Open(trim(ofile)//'.mat'//char(0),MAT_ACC_RDWR,MAT)
    end if

    IERR = FMAT_VARCREATE(trim(mname)//char(0), m, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, m)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_CLOSE(MAT)
end subroutine
subroutine write_2D_r_matrix(Nx,Ny,ofile,m,mname,debug,create)
    use MATIO
    implicit none
    integer:: ierr,Nx,Ny
    character*120:: mname,ofile
    logical:: debug,create
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR
    real, dimension(Nx,Ny):: m
    if (debug) then
        write(*,*) 'Working on file',trim(ofile)
    end if
    ! open the MAT file for writting
    if (create) then
        IERR = FMAT_CREATE(trim(ofile)//'.mat'//char(0), MAT)
    else
        ierr = FMat_Open(trim(ofile)//'.mat'//char(0),MAT_ACC_RDWR,MAT)
    end if

    IERR = FMAT_VARCREATE(trim(mname)//char(0), m, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, m)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_CLOSE(MAT)
end subroutine
