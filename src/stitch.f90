PROGRAM STITCH
    use getoptions
    USE MATIO
    implicit none

    ! SIMPLE PROGRAM FOR STITCHING OF THE MATLAB BINARY FILES
    integer:: Nx_total,Ny_total,Nz_total,Nx,Ny,Nz,i,j,k,ierr,proc_no,version,proc_max
    logical:: verbose
    character(len=120) :: ifile='test.inp'//char(0), ofile='test'//char(0), tfile='t-test'//char(0)
    character :: okey
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR
    character*2:: sp_str
    sp_str = '  '




    write(*,*) 'Parsing command line'
    do
        okey=getopt('i:t:o:vhrcdp:a')
        if(okey.eq.'>') exit
        if(okey.eq.'!') then
            write(6,*) 'unknown option: ', trim(optarg)
            call display_help()
            stop
        end if
        if(okey.eq.'v') then
            verbose = .true.
            write(6,*) 'Running in verbose mode'
        end if
        if(okey.eq.'a') then
            verbose = .true.
            write(6,*) 'Will be stitching averaged diagnostics'
        end if

        if(okey.eq.'h') then
            call display_help()
            stop
        end if

        if(okey.eq.'o') then
            ofile=trim(optarg)
            write(6,*) 'Averaged diagnostics will be stitched from files ',trim(optarg)
        end if
        if(okey.eq.'i') then
            ifile=trim(optarg)
            write(6,*) 'Using input file ',trim(optarg)
        end if
        if(okey.eq.'t') then
            tfile=trim(optarg)
            write(6,*) 'Time diagnostics will be stitched from files ',trim(optarg)
        end if
    end do

    ! open the first time diagnostics and read the dimensions
    proc_no = 0
    write(sp_str(1:2),'(I2.2)') proc_no
    CALL FMAT_LOGINIT('particle')
    ierr = FMat_Open(trim(tfile)//'T'//sp_str//'.mat'//char(0),MAT_ACC_RDONLY,MAT)
    ierr = FMat_VarReadInfo(MAT,'version',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,version)
    write (*,*) 'Loaded version',version
    ! load the dimensions
    ierr = FMat_VarReadInfo(MAT,'Nx_total',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,Nx_total)
    write (*,*) 'Loaded Nx_total',Nx_total
    ierr = FMat_VarReadInfo(MAT,'Ny_total',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,Ny_total)
    write (*,*) 'Loaded Ny_total',Ny_total
    ierr = FMat_VarReadInfo(MAT,'Nz_total',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,Nz_total)
    write (*,*) 'Loaded Nz_total',Nz_total
    ierr = FMat_VarReadInfo(MAT,'proc_max',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,proc_max)
    write (*,*) 'Loaded proc_max',proc_max

    call mainloop(Nx_total,Ny_total,Nz_total,tfile,ofile,proc_max)




END PROGRAM

subroutine mainloop(Nx_total,Ny_total,Nz_total,tfile,ofile,proc_max)
    USE MATIO
    implicit none

    integer:: Nx_total,Ny_total,Nz_total,ierr,proc_max,sp
    real,dimension(Nx_total,Ny_total,Nz_total):: dumr
    integer,dimension(Nx_total,Ny_total,Nz_total):: dumi
    character(len=120):: tfile,ofile
    TYPE(MAT_T)     :: MATOUT
    TYPE(MAT_T),dimension(proc_max+1)     :: MATIN

    TYPE(MATVAR_T)  :: MATVAR
    character(len=120):: sp_str
    sp_str = '  '

    ! open the final t file for writting
    ! OPEN FILE FOR WRITING
    write(*,*) 'Opening final file for writting ',tfile

    IERR = FMAT_CREATE(trim(tfile)//'.mat'//char(0), MATOUT)
    write(*,*) 'Final file opened for writting ',tfile
    ! OPEN PARTIAL FILES FOR READING
    do sp=0, proc_max
        write(sp_str(1:2),'(I2.2)') sp
        ierr = FMat_Open(trim(tfile)//'T'//trim(sp_str)//'.mat'//char(0),MAT_ACC_RDONLY,MATIN(sp+1))
    end do
    write(*,*) 'Partial files opened for reading'

    ! stitch first var - Pot
    dumr = 0.0
    call stitch_real_matrix(Nx_total,Ny_total,Nz_total,dumr,proc_max,MATIN,'Pot')
    IERR = FMAT_VARCREATE('Pot',dumr, MATVAR)
    IERR = FMAT_VARWRITE(MATOUT, MATVAR, dumr)
    IERR = FMAT_VARFREE(MATVAR)


end subroutine


subroutine display_help()
    write(*,*) 'Stitching program'
    write(*,*) '=============================='
    write(*,*) 'usage: ./stitch [-vha] [-i INPUT_FILE] [-o OUTPUT_FILE] [-t TDIAG_FILE] '
    write(*,*) '	-v verbose mode'
    write(*,*) '	-d debug mode'
    write(*,*) '	-h display this help'
    write(*,*) '	-i input file (default sheatRD_tilegap.inp)'
    write(*,*) '	-o output file (default shRDarbitr_tile.mat)'
    write(*,*) '	-t time diag file (default shRDt_tile.mat)'
    write(*,*) '	-a stitch averaged output'
end subroutine

subroutine stitch_real_matrix(Nx_total,Ny_total,Nz_total,dumr,proc_max,MATIN,name)
    USE MATIO
    implicit none

    integer::Nx_total,Ny_total,Nz_total,proc_max,Nx_start,Ny_start,Nz_start,Nx,Ny,Nz,sp,ierr,length
    character(len=120):: name
    TYPE(MAT_T),dimension(proc_max+1)     :: MATIN
    TYPE(MATVAR_T)  :: MATVAR

    real, dimension(Nx_total,Ny_total,Nz_total):: dumr,dumt
    real, dimension(33,33,65):: a
    a = 0
    dumt = 0
    do sp=0,proc_max
        ! read the limits
        ierr = FMat_VarReadInfo(MATIN(sp+1),'Nx_start',MATVAR)
        ierr = FMat_VarReadData(MATIN(sp+1),MATVAR,Nx_start)
        write (*,*) 'Loaded Nx_start',Nx_start
        ierr = FMat_VarReadInfo(MATIN(sp+1),'Ny_start',MATVAR)
        ierr = FMat_VarReadData(MATIN(sp+1),MATVAR,Ny_start)
        write (*,*) 'Loaded Ny_start',Ny_start
        ierr = FMat_VarReadInfo(MATIN(sp+1),'Nz_start',MATVAR)
        ierr = FMat_VarReadData(MATIN(sp+1),MATVAR,Nz_start)
        write (*,*) 'Loaded Nz_start',Nz_start
        ierr = FMat_VarReadInfo(MATIN(sp+1),'Nx',MATVAR)
        ierr = FMat_VarReadData(MATIN(sp+1),MATVAR,Nx)
        write (*,*) 'Loaded Nx',Nx
        ierr = FMat_VarReadInfo(MATIN(sp+1),'Ny',MATVAR)
        ierr = FMat_VarReadData(MATIN(sp+1),MATVAR,Ny)
        write (*,*) 'Loaded Ny',Ny
        ierr = FMat_VarReadInfo(MATIN(sp+1),'Nz',MATVAR)
        ierr = FMat_VarReadData(MATIN(sp+1),MATVAR,Nz)
        write (*,*) 'Loaded Nz',Nz

        write (*,*) 'Loading matrix Pot with dimensions',Nx,Ny,Nz

        ierr = FMat_VarReadInfo(MATIN(sp+1),'Pot',MATVAR)
        length  = PRODUCT(MATVAR%dims(1:MATVAR%rank))
        write(*,*) 'Loaded dimensions',MATVAR%dims(1:MATVAR%rank)
        ierr = FMat_VarReadData(MATIN(sp+1),MATVAR,a)
        write (*,*) 'Loaded matrix',a
        dumr(Nx_start:(Nx_start + Nx-1),Ny_start:(Ny_start + Ny-1),Nz_start:(Nz_start + Nz-1)) = dumr(Nx_start:(Nx_start + Nx-1),Ny_start:(Ny_start + Ny-1),Nz_start:(Nz_start + Nz-1)) + a



    end do


end subroutine
