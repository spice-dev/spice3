subroutine write_t_file(name,snumber,no_species,Nx,Ny,Nz,rho,Pot,objects,history_ntimes,h_pos,count,Np,tpx,tpy,tpz,tpux,tpuy,tpuz,edges,Ex,Ey,Ez,Potvac,vx,vy,vz,objectscurrent,objectscurrenttotal,groupcurrent,nobjects,iter_time,proc_max,no_diag_slots,diag_slots,no_diag,diag_params,int_flux_x,int_flux_y,int_flux_z,Na,Nc,dielectric_cell,edge_charge,diel_filter,bc_matrix,iter_time_no,proc_no,Nx_total,Ny_total,Nz_total,Pot_total,rho_tot_total,model,bstep,v_act,object_params,edge_charge_tot_total,float_constant, i_rel_history,Ts,inj_rem)
    USE MATIO
    implicit none
    include 'struct.h'
    character*120:: name
    integer:: i,no_species,Nx,Ny,Nz,ierr,version,history_ntimes,h_pos,count,sp,Np,nobjects,proc_max,no_diag_slots,nd,slot_counter,no_diag,Na,Nc,iter_time_no,n,proc_no,Nx_total,Ny_total,Nz_total,bstep
    type(diaglist_t), dimension(no_diag):: diag_params
    integer, dimension(no_species,history_ntimes):: snumber
    real,dimension(no_species,Nx,Ny,Nz):: rho,vx,vy,vz
    real, dimension(Nx,Ny,Nz):: Pot,Ex,Ey,Ez,edge_charge,Ts,inj_rem
    logical, dimension(Nx,Ny,Nz):: dielectric_cell
    real, dimension(Nx_total,Ny_total,Nz_total)::Pot_total,rho_tot_total,Potvac,edge_charge_tot_total

    integer, dimension(Nx,Ny,Nz):: objects,edges,diel_filter,bc_matrix
    real, dimension(nobjects,no_species,1:count):: objectscurrent,objectscurrenttotal,groupcurrent
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR
    real,dimension(Np):: tpx,tpy,tpz,tpux,tpuy,tpuz
    real, dimension(1,10,1:iter_time_no):: iter_time
    real, dimension(no_diag_slots,1:count):: diag_slots
    real, dimension(no_species,nobjects,Nx):: int_flux_x
    real, dimension(no_species,nobjects,Ny):: int_flux_y
    real, dimension(no_species,nobjects,Nz):: int_flux_z
    real, dimension(nobjects):: float_constant
    real, dimension(nobjects,50):: i_rel_history

    type(model_t):: model
    type(object_t),dimension(nobjects):: object_params
    real,dimension(nobjects):: object_pot
    character*2:: sp_str
    real:: v_act
    real:: lambda_D,w_i,part_factor,power_factor
    sp_str = '  '

    ! prepare object_param
    do i=1,nobjects
        object_pot(i) = object_params(i)%Pot
    end do

    version = 2
    write(sp_str(1:2),'(I2.2)') proc_no
    CALL FMAT_LOGINIT('particle')

    ! defunct - don't know how to include integers in strings
    write (6,*) 'Writing results to ',trim(name)//'T'//sp_str//'.mat'
    ! OPEN FILE FOR WRITING
    IERR = FMAT_CREATE(trim(name)//'T'//sp_str//'.mat'//char(0), MAT)
    ! WRITE FIRST VARIABLE
    IERR = FMAT_VARCREATE('version', version, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, version)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('bstep', bstep, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, bstep)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('floatconstant', float_constant, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, float_constant)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('irelhistory', i_rel_history, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, i_rel_history)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('objectpot', object_pot, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, object_pot)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('vact', v_act, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, v_act)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('nospecies', no_species, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, no_species)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('procmax', proc_max, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, proc_max)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('nobjects', nobjects, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, nobjects)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('historyntimes', history_ntimes, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, history_ntimes)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('nodiagslots', no_diag_slots, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, no_diag_slots)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('Np', Np, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Np)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('Na', Na, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Na)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('Nc', Nc, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Nc)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('count', count, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, count)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('Nx', Nx, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Nx)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('Ny', Ny, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Ny)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('Nz', Nz, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Nz)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('dx', model%dx, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%dx)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('dy', model%dy, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%dy)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('dz', model%dz, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%dz)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('dt', model%dt, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%dt)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('ksi', model%ksi, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%ksi)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('tau', model%tau, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%tau)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('mu', model%mu, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%mu)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('P0', model%P0, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%P0)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('PL', model%PL, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%PL)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('alphaxz', model%alpha_xz, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%alpha_xz)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('alphayz', model%alpha_yz, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%alpha_yz)
    IERR = FMAT_VARFREE(MATVAR)


    IERR = FMAT_VARCREATE('poisslevels', model%poiss_levels, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%poiss_levels)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('poisssmooths', model%poiss_smooths, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%poiss_smooths)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('poissres', model%poiss_res, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%poiss_res)
    IERR = FMAT_VARFREE(MATVAR)


    ! write MKS info
    IERR = FMAT_VARCREATE('mksB',model%mks_B, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%mks_B)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('mksn0',model%mks_n0, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%mks_n0)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('mksTe',model%mks_Te, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%mks_Te)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('mksmainionm',model%mks_main_ion_m, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%mks_Te)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('mksmainionq',model%mks_main_ion_q, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%mks_main_ion_q)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('mkspar1',model%mks_param1, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%mks_param1)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('mkspar2',model%mks_param2, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%mks_param2)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('mkspar3',model%mks_param3, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%mks_param3)
    IERR = FMAT_VARFREE(MATVAR)
    ! try to calculate some basic parameters so that we don't need to do it every time the output is calculated
    if (model%mks_B.gt.0.0) then
        lambda_D = sqrt(8.85E-12*model%mks_Te/model%mks_n0/1.602E-19)
        w_i = 1.602E-19*model%mks_main_ion_q*model%mks_B/(model%mks_main_ion_m*1.67E-27)
        part_factor = lambda_D*w_i*model%mks_n0*1.602E-19/1E3  ! in kA/m2
        power_factor = model%mks_main_ion_m*1.67E-27*model%mks_n0*lambda_D**3*w_i**3/1E6 ! in MW/m2
        IERR = FMAT_VARCREATE('lambdaD',lambda_D, MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, lambda_D)
        IERR = FMAT_VARFREE(MATVAR)
        IERR = FMAT_VARCREATE('wi',w_i, MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, w_i)
        IERR = FMAT_VARFREE(MATVAR)
        IERR = FMAT_VARCREATE('partfactor',part_factor, MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, part_factor)
        IERR = FMAT_VARFREE(MATVAR)
        IERR = FMAT_VARCREATE('powerfactor',power_factor, MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, power_factor)
        IERR = FMAT_VARFREE(MATVAR)


    end if

    IERR = FMAT_VARCREATE('Lxstart',model%Lx_start, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%Lx_start)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('Lystart',model%Ly_start, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%Lx_start)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('Lystart',model%Lz_start, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%Lx_start)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('Nxstart',model%Nx_start, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%Nx_start)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('Nystart',model%Ny_start, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%Ny_start)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('Nzstart',model%Nz_start, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%Nz_start)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('Nxstop',model%Nx_stop, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%Nx_stop)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('Nystop',model%Ny_stop, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%Ny_stop)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('Nzstop',model%Nz_stop, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%Nz_stop)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('Nxtotal',model%Nx_total, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%Nx_total)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('Nytotal',model%Ny_total, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%Ny_total)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('Nztotal',model%Nz_total, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%Nz_total)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('procmax',model%proc_max, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%proc_max)
    IERR = FMAT_VARFREE(MATVAR)

    !pnumber
    IERR = FMAT_VARCREATE('snumber', snumber, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, snumber)
    IERR = FMAT_VARFREE(MATVAR)
    !Pot
    IERR = FMAT_VARCREATE('Pot', Pot, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Pot)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('Pottotal', Pot_total, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Pot_total)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('edgechargetotal', edge_charge_tot_total, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, edge_charge_tot_total)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('rhotottotal', rho_tot_total, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, rho_tot_total)
    IERR = FMAT_VARFREE(MATVAR)

    !Potvac
    IERR = FMAT_VARCREATE('Potvac', Potvac, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Potvac)
    IERR = FMAT_VARFREE(MATVAR)

    !Ex
    IERR = FMAT_VARCREATE('Ex', Ex, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Ex)
    IERR = FMAT_VARFREE(MATVAR)
    !Ey
    IERR = FMAT_VARCREATE('Ey', Ey, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Ey)
    IERR = FMAT_VARFREE(MATVAR)
    !Ez
    IERR = FMAT_VARCREATE('Ez', Ez, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Ez)
    IERR = FMAT_VARFREE(MATVAR)
    !surface temperature
    IERR = FMAT_VARCREATE('Ts', Ts, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Ts)
    IERR = FMAT_VARFREE(MATVAR)
    !surface temperature
    IERR = FMAT_VARCREATE('injrem', inj_rem, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, inj_rem)
    IERR = FMAT_VARFREE(MATVAR)
    !Objects
    IERR = FMAT_VARCREATE('objects', objects, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, objects)
    IERR = FMAT_VARFREE(MATVAR)
    !edges
    IERR = FMAT_VARCREATE('edges', edges, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, edges)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('dielfilter', diel_filter, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, diel_filter)
    IERR = FMAT_VARFREE(MATVAR)

    !edge charge
    IERR = FMAT_VARCREATE('edgecharge', edge_charge, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, edge_charge)
    IERR = FMAT_VARFREE(MATVAR)
    !edge charge
    IERR = FMAT_VARCREATE('bcmatrix', float(bc_matrix), MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, float(bc_matrix))
    IERR = FMAT_VARFREE(MATVAR)

    !dielectric cells
    IERR = FMAT_VARCREATE('dielectriccell', float(dielectric_cell), MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, float(dielectric_cell))
    IERR = FMAT_VARFREE(MATVAR)

    !objectscurrent
    IERR = FMAT_VARCREATE('objectscurrent', objectscurrent, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, objectscurrent)
    IERR = FMAT_VARFREE(MATVAR)
    !objectscurrent
    IERR = FMAT_VARCREATE('objectscurrenttotal', objectscurrenttotal, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, objectscurrenttotal)
    IERR = FMAT_VARFREE(MATVAR)

    !objectscurrent
    IERR = FMAT_VARCREATE('groupcurrent', groupcurrent, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, groupcurrent)
    IERR = FMAT_VARFREE(MATVAR)

    ! iteration time
    n = min(count,iter_time_no)
    IERR = FMAT_VARCREATE('itertime', iter_time(:,:,1:n), MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, iter_time(:,:,1:n))
    IERR = FMAT_VARFREE(MATVAR)
    ! integral diagnostics
    IERR = FMAT_VARCREATE('intfluxx', int_flux_x, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, int_flux_x)
    IERR = FMAT_VARFREE(MATVAR)
    ! integral diagnostics
    IERR = FMAT_VARCREATE('intfluxy', int_flux_y, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, int_flux_y)
    IERR = FMAT_VARFREE(MATVAR)
    ! integral diagnostics
    IERR = FMAT_VARCREATE('intfluxz', int_flux_z, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, int_flux_z)
    IERR = FMAT_VARFREE(MATVAR)

    !
    ! diagnostics
    write(*,*) 'Writting diagnostics'
    slot_counter = 0
    ! integral diagnostics

    ! only write diags if there are any
    !write(*,*) 'Diag slots: ',no_diag_slots
    if (no_diag_slots.gt.0) then
        ! write the whole diag array for resume
        IERR = FMAT_VARCREATE('diagslots',diag_slots, MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, diag_slots)
        IERR = FMAT_VARFREE(MATVAR)


        do nd =1,no_diag
            ! only save diags which have any data in them
            ! save it under nice names
            if (diag_params(nd)%type.eq.1.or.diag_params(nd)%type.eq.3.or.diag_params(nd)%type.gt.4) then
                ! potential, no change
                if (diag_params(nd)%start_time.le.count) then

                    IERR = FMAT_VARCREATE(trim(diag_params(nd)%name),diag_slots(slot_counter + 1,int(diag_params(nd)%start_time):count), MATVAR)
                    IERR = FMAT_VARWRITE(MAT, MATVAR, diag_slots(slot_counter + 1,int(diag_params(nd)%start_time):count))
                    IERR = FMAT_VARFREE(MATVAR)
                end if
                slot_counter = slot_counter + 1
            else if (diag_params(nd)%type.eq.2.or.diag_params(nd)%type.eq.4) then
                ! E field, suffic _ex etc
                if (diag_params(nd)%start_time.le.count) then
                    !   write(*,*) 'Writting diag ',trim(diag_params(nd)%name)//'x'
                    IERR = FMAT_VARCREATE(trim(diag_params(nd)%name)//'x'//char(0),diag_slots(slot_counter + 1,int(diag_params(nd)%start_time):count), MATVAR)
                    IERR = FMAT_VARWRITE(MAT, MATVAR, diag_slots(slot_counter + 1,int(diag_params(nd)%start_time):count))
                    IERR = FMAT_VARFREE(MATVAR)
                end if
                slot_counter = slot_counter + 1
                if (diag_params(nd)%start_time.le.count) then

                    IERR = FMAT_VARCREATE(trim(diag_params(nd)%name)//'y'//char(0),diag_slots(slot_counter + 1,int(diag_params(nd)%start_time):count), MATVAR)
                    IERR = FMAT_VARWRITE(MAT, MATVAR, diag_slots(slot_counter + 1,int(diag_params(nd)%start_time):count))
                    IERR = FMAT_VARFREE(MATVAR)
                end if
                slot_counter = slot_counter + 1
                if (diag_params(nd)%start_time.le.count) then

                    IERR = FMAT_VARCREATE(trim(diag_params(nd)%name)//'z'//char(0),diag_slots(slot_counter + 1,int(diag_params(nd)%start_time):count), MATVAR)
                    IERR = FMAT_VARWRITE(MAT, MATVAR, diag_slots(slot_counter + 1,int(diag_params(nd)%start_time):count))
                    IERR = FMAT_VARFREE(MATVAR)
                end if
                slot_counter = slot_counter + 1


            end if
        end do
    end if
    ! charge density
    ! we can only write 3D matrixes
    ! so we do it one specie by another
    do sp=1,no_species
        write(sp_str(1:2),'(I2.2)') sp
        IERR = FMAT_VARCREATE('rho'//sp_str, rho(sp,:,:,:), MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, rho(sp,:,:,:))
        IERR = FMAT_VARFREE(MATVAR)
        !vx
        IERR = FMAT_VARCREATE('vx'//sp_str, vx(sp,:,:,:), MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, vx(sp,:,:,:))
        IERR = FMAT_VARFREE(MATVAR)
        !vy
        IERR = FMAT_VARCREATE('vy'//sp_str, vy(sp,:,:,:), MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, vy(sp,:,:,:))
        IERR = FMAT_VARFREE(MATVAR)
        !vz
        IERR = FMAT_VARCREATE('vz'//sp_str, vz(sp,:,:,:), MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, vz(sp,:,:,:))
        IERR = FMAT_VARFREE(MATVAR)


    end do
    ! test particle
    IERR = FMAT_VARCREATE('tpx', tpx(1:count), MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, tpx(1:count))
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('tpy', tpy(1:count), MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, tpy(1:count))
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('tpz', tpz(1:count), MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, tpz(1:count))
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('tpux', tpux(1:count), MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, tpux(1:count))
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('tpuy', tpuy(1:count), MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, tpuy(1:count))
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('tpuz', tpuz(1:count), MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, tpuz(1:count))
    IERR = FMAT_VARFREE(MATVAR)




    IERR = FMAT_CLOSE(MAT)


    write (*,*) 'All done in t file save'



end subroutine

subroutine write_o_file(name,no_species,Nx,Ny,Nz,dens,Potav,vxav,vyav,vzav,Exav,Eyav,Ezav,vx2av,vy2av,vz2av,vx3av,vy3av,vz3av,edge_flux,edge_energy_flux,model,count,proc_no,use_moments,edge_chargeav)
    USE MATIO
    implicit none
    include 'struct.h'
    character*120:: name
    type (model_t):: model
    integer:: no_species,Nx,Ny,Nz,ierr,version,sp,Np,count,proc_no
    real,dimension(no_species,Nx,Ny,Nz):: dens,vxav,vyav,vzav,vx2av,vy2av,vz2av,vx3av,vy3av,vz3av,edge_flux,edge_energy_flux
    real, dimension(Nx,Ny,Nz):: Potav,Exav,Eyav,Ezav,edge_chargeav
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR
    character*2:: sp_str
    logical:: use_moments
    sp_str = '  '
    write(sp_str(1:2),'(I2.2)') proc_no

    version = 2
    CALL FMAT_LOGINIT('averaged')
    ! OPEN FILE FOR WRITING
    IERR = FMAT_CREATE(trim(name)//sp_str//'.mat'//char(0), MAT)
    ! WRITE FIRST VARIABLE
    IERR = FMAT_VARCREATE('version', version, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, version)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('Lxstart',model%Lx_start, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%Lx_start)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('Lystart',model%Ly_start, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%Lx_start)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('Lystart',model%Lz_start, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%Lx_start)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('Nxstart',model%Nx_start, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%Nx_start)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('Nystart',model%Ny_start, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%Ny_start)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('Nzstart',model%Nz_start, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%Nz_start)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('Nxstop',model%Nx_stop, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%Nx_stop)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('Nystop',model%Ny_stop, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%Ny_stop)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('Nzstop',model%Nz_stop, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%Nz_stop)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('Nxtotal',model%Nx_total, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%Nx_total)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('Nytotal',model%Ny_total, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%Ny_total)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('Nztotal',model%Nz_total, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, model%Nz_total)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('Nx', Nx, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Nx)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('Ny', Ny, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Ny)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('Nz', Nz, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Nz)
    IERR = FMAT_VARFREE(MATVAR)


    ! write averaged potential
    !Pot
    IERR = FMAT_VARCREATE('Potav', Potav, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Potav)
    IERR = FMAT_VARFREE(MATVAR)
    !Pot
    IERR = FMAT_VARCREATE('edgechargeav', edge_chargeav, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, edge_chargeav)
    IERR = FMAT_VARFREE(MATVAR)

    !Pot
    IERR = FMAT_VARCREATE('Exav', Exav, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Exav)
    IERR = FMAT_VARFREE(MATVAR)
    !Pot
    IERR = FMAT_VARCREATE('Eyav', Eyav, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Eyav)
    IERR = FMAT_VARFREE(MATVAR)
    !Pot
    IERR = FMAT_VARCREATE('Ezav', Ezav, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Ezav)
    IERR = FMAT_VARFREE(MATVAR)
    if (model%debug) then
        write(*,*) 'Done Electric matrixes'
    end if
    do sp=1,no_species
        if (model%debug) then
            write(*,*) 'Working on specie',sp
        end if

        write(sp_str(1:2),'(I2.2)') sp
        IERR = FMAT_VARCREATE('dens'//sp_str, dens(sp,:,:,:), MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, dens(sp,:,:,:))
        IERR = FMAT_VARFREE(MATVAR)
        !vx
        IERR = FMAT_VARCREATE('vxav'//sp_str, vxav(sp,:,:,:), MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, vxav(sp,:,:,:))
        IERR = FMAT_VARFREE(MATVAR)
        !vy
        IERR = FMAT_VARCREATE('vyav'//sp_str, vyav(sp,:,:,:), MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, vyav(sp,:,:,:))
        IERR = FMAT_VARFREE(MATVAR)
        !vz
        IERR = FMAT_VARCREATE('vzav'//sp_str, vzav(sp,:,:,:), MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, vzav(sp,:,:,:))
        IERR = FMAT_VARFREE(MATVAR)
        if (use_moments) then
            !vx2
            IERR = FMAT_VARCREATE('vx2av'//sp_str, vx2av(sp,:,:,:), MATVAR)
            IERR = FMAT_VARWRITE(MAT, MATVAR, vx2av(sp,:,:,:))
            IERR = FMAT_VARFREE(MATVAR)
            !vy2
            IERR = FMAT_VARCREATE('vy2av'//sp_str, vy2av(sp,:,:,:), MATVAR)
            IERR = FMAT_VARWRITE(MAT, MATVAR, vy2av(sp,:,:,:))
            IERR = FMAT_VARFREE(MATVAR)
            !vz2
            IERR = FMAT_VARCREATE('vz2av'//sp_str, vz2av(sp,:,:,:), MATVAR)
            IERR = FMAT_VARWRITE(MAT, MATVAR, vz2av(sp,:,:,:))
            IERR = FMAT_VARFREE(MATVAR)
            !vx3
            IERR = FMAT_VARCREATE('vx3av'//sp_str, vx3av(sp,:,:,:), MATVAR)
            IERR = FMAT_VARWRITE(MAT, MATVAR, vx3av(sp,:,:,:))
            IERR = FMAT_VARFREE(MATVAR)
            !vy3
            IERR = FMAT_VARCREATE('vy3av'//sp_str, vy3av(sp,:,:,:), MATVAR)
            IERR = FMAT_VARWRITE(MAT, MATVAR, vy3av(sp,:,:,:))
            IERR = FMAT_VARFREE(MATVAR)
            !vz3
            IERR = FMAT_VARCREATE('vz3av'//sp_str, vz3av(sp,:,:,:), MATVAR)
            IERR = FMAT_VARWRITE(MAT, MATVAR, vz3av(sp,:,:,:))
            IERR = FMAT_VARFREE(MATVAR)
        end if
        !edge flux
        IERR = FMAT_VARCREATE('edgeflux'//sp_str, edge_flux(sp,:,:,:)/real(count - model%Na), MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, edge_flux(sp,:,:,:)/real(count - model%Na))
        IERR = FMAT_VARFREE(MATVAR)
        !edge energy flux
        IERR = FMAT_VARCREATE('edgeenergyflux'//sp_str, edge_energy_flux(sp,:,:,:)/real(count - model%Na), MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, edge_energy_flux(sp,:,:,:)/real(count - model%Na))
        IERR = FMAT_VARFREE(MATVAR)


    end do
    IERR = FMAT_CLOSE(MAT)
    if (model%debug) then
        write(*,*) 'All done in write_o_file'
    end if

end subroutine
subroutine save_temp_proc_file(tfile,proc_no,no_tot,x,y,z,ux,uy,uz,stype,count,use_compression,model)
    USE MATIO
    implicit none
    include 'struct.h'

    integer:: no_tot, proc_no,icr,version,IERR,count
    character*120:: tfile
    real*8, dimension(no_tot):: x,y,z,ux,uy,uz
    integer, dimension(no_tot):: stype
    integer*8:: ifile
    character*2:: sp_str
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR
    type(model_t):: model
    integer:: use_compression
    version = 50
    sp_str = '  '
    write(sp_str(1:2),'(I2.2)') proc_no
    CALL FMAT_LOGINIT('particle')

    ! defunct - don't know how to include integers in strings
    !write (6,*) 'Writing results to ',trim(tfile)//sp_str//'.mat'
    ! OPEN FILE FOR WRITING
    IERR = FMAT_CREATE(trim(tfile)//sp_str//'.mat'//char(0), MAT)
    ! WRITE FIRST VARIABLE
    IERR = FMAT_VARCREATE('version', version, MATVAR)
    ierr = FMat_VarWrite(mat,matvar,version,use_compression)

    !    IERR = FMAT_VARWRITE(MAT, MATVAR, version)
    IERR = FMAT_VARFREE(MATVAR)
    !second goes the number of particles

    IERR = FMAT_VARCREATE('ntot', no_tot, MATVAR)
    ierr = FMat_VarWrite(mat,matvar,no_tot,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('count', count, MATVAR)
    ierr = FMat_VarWrite(mat,matvar,count,use_compression)
    IERR = FMAT_VARFREE(MATVAR)


    IERR = FMAT_VARCREATE('nproc', proc_no, MATVAR)
    ierr = FMat_VarWrite(mat,matvar,proc_no,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    !that's it, now the particles
    IERR = FMAT_VARCREATE('x', x + model%Lx_start, MATVAR)
    ierr = FMat_VarWrite(mat,matvar,x+ model%Lx_start,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('y', y+ model%Ly_start, MATVAR)
    ierr = FMat_VarWrite(mat,matvar,y+ model%Ly_start,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('z', z+ model%Lz_start, MATVAR)
    ierr = FMat_VarWrite(mat,matvar,z+ model%Lz_start,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('ux', ux, MATVAR)
    ierr = FMat_VarWrite(mat,matvar,ux,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('uy', uy, MATVAR)
    ierr = FMat_VarWrite(mat,matvar,uy,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('uz', uz, MATVAR)
    ierr = FMat_VarWrite(mat,matvar,uz,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('stype', stype, MATVAR)
    ierr = FMat_VarWrite(mat,matvar,stype,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_CLOSE(MAT)


end subroutine
subroutine restore_particles(tfile,verbose,debug,x,y,z,ux,uy,uz,Nz,Ny,Npts,no_species,stype,no_tot,proc_no,count,model,pnumber,regroup_runs)
    USE MATIO

    implicit none
    include 'struct.h'
    integer:: ierr,length,version,proc_no,Nz,Ny,no_species,count,i,file_count,no_tot_valid,regroup_runs,file_count_max
    integer*8:: Npts
    real, dimension(Npts):: x,y,z,ux,uy,uz
    real, dimension(Npts):: xr,yr,zr,uxr,uyr,uzr
    integer, dimension(Npts):: styper
    integer, dimension(no_species):: pnumber
    integer, dimension(Npts):: stype
    character*120:: tfile,testfile
    logical:: verbose,debug,file_exists
    integer*8:: ifile
    integer:: dumi,no_tot
    real*8:: dumr
    type(model_t):: model
    character*2:: sp_str
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR
    version = 0
    dumi = 0
    sp_str = '  '
    no_tot_valid = 0
    if (regroup_runs.gt.0) then
        file_count_max = regroup_runs
    else
        file_count_max = 99
    end if
    do file_count=0,file_count_max
        write(sp_str(1:2),'(I2.2)') file_count
        testfile = trim(tfile)//sp_str//'.mat'//char(0)
        inquire(file=testfile,exist=file_exists)
        if (file_exists) then

            if (verbose) then
                write(*,*) 'Restoring run from ',testfile
            end if
            CALL FMAT_LOGINIT('particle')
            ierr = FMat_Open(testfile,MAT_ACC_RDONLY,MAT)

            ! extract version
            ierr = FMat_VarReadInfo(MAT,'version',MATVAR)
            ierr = FMat_VarReadData(MAT,MATVAR,version)

            if (verbose) then
                write(*,*) 'Restoring from data file version',version
            end if
            if (version.lt.50) then
                write(*,*) '--------- ERROR ---------'
                write(*,*) 'This MAT file is too old'
                write(*,*) 'Restore function only works with MAT v5 files generated by matIO library'
                stop
            end if
            ! extract no_tot
            ierr = FMat_VarReadInfo(MAT,'ntot',MATVAR)
            ierr = FMat_VarReadData(MAT,MATVAR,no_tot)

            if (verbose) then
                write(*,*) 'Restoring ',no_tot, 'particles'
            end if


            ! extract count
            ierr = FMat_VarReadInfo(MAT,'count',MATVAR)
            ierr = FMat_VarReadData(MAT,MATVAR,count)

            if (verbose) then
                write(*,*) 'Count: ',count
            end if

            ! extract particle vectors

            ! x vector
            ierr = FMat_VarReadInfo(MAT,'x',MATVAR)
            length  = PRODUCT(MATVAR%dims(1:MATVAR%rank))
            if (length.ne.no_tot) then
                write (*,*) 'Error, vector length does not match: x',length, no_tot
            end if
            ierr = FMat_VarReadData(MAT,MATVAR,xr(1:length))
            IERR = FMAT_VARFREE(MATVAR)

            if (verbose) then
                write(*,*) 'Loaded xr vector, check',sum(xr(1:no_tot))
            end if

            ! y vector
            ierr = FMat_VarReadInfo(MAT,'y',MATVAR)
            length  = PRODUCT(MATVAR%dims(1:MATVAR%rank))
            if (length.ne.no_tot) then
                write (*,*) 'Error, vector length does not match: y',length, no_tot
            end if
            ierr = FMat_VarReadData(MAT,MATVAR,yr(1:length))
            IERR = FMAT_VARFREE(MATVAR)

            if (verbose) then
                write(*,*) 'Loaded yr vector, check',sum(yr(1:no_tot))
            end if

            ! z vector
            ierr = FMat_VarReadInfo(MAT,'z',MATVAR)
            length  = PRODUCT(MATVAR%dims(1:MATVAR%rank))
            if (length.ne.no_tot) then
                write (*,*) 'Error, vector length does not match: z',length, no_tot
            end if
            ierr = FMat_VarReadData(MAT,MATVAR,zr(1:length))
            IERR = FMAT_VARFREE(MATVAR)

            if (verbose) then
                write(*,*) 'Loaded zr vector, check',sum(zr(1:no_tot))
            end if

            ! ux vector
            ierr = FMat_VarReadInfo(MAT,'ux',MATVAR)
            length  = PRODUCT(MATVAR%dims(1:MATVAR%rank))
            if (length.ne.no_tot) then
                write (*,*) 'Error, vector length does not match: ux',length, no_tot
            end if
            ierr = FMat_VarReadData(MAT,MATVAR,uxr(1:length))
            IERR = FMAT_VARFREE(MATVAR)

            if (verbose) then
                write(*,*) 'Loaded uxr vector, check',sum(uxr(1:no_tot))
            end if

            ! uy vector
            ierr = FMat_VarReadInfo(MAT,'uy',MATVAR)
            length  = PRODUCT(MATVAR%dims(1:MATVAR%rank))
            if (length.ne.no_tot) then
                write (*,*) 'Error, vector length does not match: uy',length, no_tot
            end if
            ierr = FMat_VarReadData(MAT,MATVAR,uyr(1:length))
            IERR = FMAT_VARFREE(MATVAR)

            if (verbose) then
                write(*,*) 'Loaded uyr vector, check',sum(uyr(1:no_tot))
            end if

            ! uz vector
            ierr = FMat_VarReadInfo(MAT,'uz',MATVAR)
            length  = PRODUCT(MATVAR%dims(1:MATVAR%rank))
            if (length.ne.no_tot) then
                write (*,*) 'Error, vector length does not match: uz',length, no_tot
            end if
            ierr = FMat_VarReadData(MAT,MATVAR,uzr(1:length))
            IERR = FMAT_VARFREE(MATVAR)

            if (verbose) then
                write(*,*) 'Loaded uzr vector, check',sum(uzr(1:no_tot))
            end if
            ! uz vector
            ierr = FMat_VarReadInfo(MAT,'stype',MATVAR)
            length  = PRODUCT(MATVAR%dims(1:MATVAR%rank))
            if (length.ne.no_tot) then
                write (*,*) 'Error, vector length does not match: stype',length, no_tot
            end if
            ierr = FMat_VarReadData(MAT,MATVAR,styper(1:length))
            IERR = FMAT_VARFREE(MATVAR)

            if (verbose) then
                write(*,*) 'Loaded styper vector, check',sum(float(styper(1:no_tot)))
            end if





            IERR = FMAT_CLOSE(MAT)
            ! add the particles to existing vector
            do i=1,no_tot
                if (xr(i).gt.model%Lx_start.and.xr(i).lt.model%Lx_stop.and.yr(i).gt.model%Ly_start.and.yr(i).lt.model%Ly_stop.and.zr(i).gt.model%Lz_start.and.zr(i).lt.model%Lz_stop) then
                    no_tot_valid = no_tot_valid +1
                    x(no_tot_valid) = xr(i) - model%Lx_start
                    y(no_tot_valid) = yr(i) - model%Ly_start
                    z(no_tot_valid) = zr(i) - model%Lz_start
                    ux(no_tot_valid) = uxr(i)
                    uy(no_tot_valid) = uyr(i)
                    uz(no_tot_valid) = uzr(i)
                    stype(no_tot_valid) = styper(i)
                    pnumber(stype(no_tot_valid)) = pnumber(stype(no_tot_valid)) +1
                end if
            end do
        end if ! file exists
    end do! file count
    no_tot = no_tot_valid
end subroutine


subroutine restore_tdata(tfile,objects_current,objects_current_total,group_current,edge_charge,Nx,Ny,Nz,no_objects,no_species,Np,proc_no,verbose,bstep,v_act,object_pot,diag_slots,no_diag_slots,Nx_total,Ny_total,Nz_total,Potvac,float_constant, i_rel_history)
    USE MATIO

    implicit none
    include 'struct.h'
    integer*8:: ifile
    character*120:: tfile,testfile
    integer:: Nx,Ny,Nz,no_objects,no_species,Np,ierr,proc_no,count,bstep,version,no_diag_slots,nds,Nx_total,Ny_total,Nz_total
    real:: v_act
    real, dimension(Nx,Ny,Nz):: edge_charge
    real, dimension(no_objects,no_species,Np):: objects_current,objects_current_total,group_current
    character*2:: sp_str
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR
    logical:: verbose,file_exists
    real,dimension(no_objects)::object_pot
    real,dimension(no_diag_slots,Np):: diag_slots
    real, dimension(Nx_total,Ny_total,Nz_total):: Potvac
    real, dimension(no_objects):: float_constant
    real, dimension(no_objects,50):: i_rel_history

    sp_str = '  '

    write(sp_str(1:2),'(I2.2)') proc_no
    testfile = trim(tfile)//'T'//sp_str//'.mat'//char(0)
    inquire(file=testfile,exist=file_exists)
    if (file_exists) then

        if (verbose) then
            write(*,*) proc_no,'Restoring TDATA from ',testfile
        end if
        CALL FMAT_LOGINIT('tdata')
        ierr = FMat_Open(testfile,MAT_ACC_RDONLY,MAT)

        ! extract version
        ierr = FMat_VarReadInfo(MAT,'version',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,version)

        if (verbose) then
            write(*,*) 'Restoring from data file version',version
        end if
        ! restore count
        ierr = FMat_VarReadInfo(MAT,'count',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,count)
        ! restore count
        ierr = FMat_VarReadInfo(MAT,'bstep',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,bstep)

        ierr = FMat_VarReadInfo(MAT,'vact',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,v_act)
        if (.true.) then
            ierr = FMat_VarReadInfo(MAT,'objectpot',MATVAR)
            ierr = FMat_VarReadData(MAT,MATVAR,object_pot)
        end if
        ierr = FMat_VarReadInfo(MAT,'floatconstant',MATVAR)
        if (ierr.eq.0) then
            ierr = FMat_VarReadData(MAT,MATVAR,float_constant)
        end if
        ierr = FMat_VarReadInfo(MAT,'irelhistory',MATVAR)
        if (ierr.eq.0) then
            ierr = FMat_VarReadData(MAT,MATVAR,i_rel_history)
        end if




        write(*,*) 'Restored count bstep v_act',count,bstep,v_act
        ! restore edge charge
        ierr = FMat_VarReadInfo(MAT,'edgecharge',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,edge_charge)
        IERR = FMAT_VARFREE(MATVAR)
        write(*,*) 'Restored edge charge',sum(abs(edge_charge))
        ! restore potvac
        ierr = FMat_VarReadInfo(MAT,'Potvac',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,potvac)
        IERR = FMAT_VARFREE(MATVAR)
        write(*,*) 'Restored Potvac',sum(abs(potvac))

        ! restore objects_current
        !write(*,*) 'Attempting to restore objects current'

        objects_current = 0.0
        ierr = FMat_VarReadInfo(MAT,'objectscurrent',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,objects_current(:,:,1:count))
        IERR = FMAT_VARFREE(MATVAR)
        write(*,*) 'Restored objects current',sum(abs(objects_current))
        ! restore objects_current_total
        objects_current_total = 0.0
        ierr = FMat_VarReadInfo(MAT,'objectscurrenttotal',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,objects_current_total(:,:,1:count))
        IERR = FMAT_VARFREE(MATVAR)
        write(*,*) 'Restored objects current total',sum(abs(objects_current_total))

        ! restore group_current
        group_current = 0.0
        ierr = FMat_VarReadInfo(MAT,'groupcurrent',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,group_current(:,:,1:count))
        IERR = FMAT_VARFREE(MATVAR)
        write(*,*) 'Restored group current',sum(abs(group_current))

        ! restore restore diag slots
        nds = 0.0
        ierr = FMat_VarReadInfo(MAT,'nodiagslots',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,nds)
        IERR = FMAT_VARFREE(MATVAR)
        write(*,*) 'Restored number of diag slots',nds
        if (nds.eq.no_diag_slots)  then
            if (.true.) then
                diag_slots = 0.0
                ierr = FMat_VarReadInfo(MAT,'diagslots',MATVAR)
                ierr = FMat_VarReadData(MAT,MATVAR,diag_slots(:,1:count))
                IERR = FMAT_VARFREE(MATVAR)
                write(*,*) 'Restored diag slots',sum(abs(diag_slots))
            end if

        else
            write(*,*) 'Number of slots in the file does not match slots in input, diag restore halted',nds,no_diag_slots

        end if


    else

        write(*,*) 'File is not available'
    end if
end subroutine
