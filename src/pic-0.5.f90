PROGRAM SPICE3
    ! 3d3v PIC code based on the SPICE2 code
    ! Written by MK
    ! the code does not use real*8 by default, this has to be specified during the compilation!
    !This code uses following normalization:
    ! t -> t*w_ci
    ! x -> x/debye_length
    ! u -> u/(w_ci*debye_length)
    ! phi -> phi*e/(k*Te)
    ! E -> E/(k*Te/(e*debye_length))
    ! n -> n/n_0

    !Main simulation parameters
    ! tau = Ti/Te
    !  mu = mi/me
    !  ksi = r_l/debye_length
    ! r_l = c_i/w_ciuse getoptions
    use getoptions
    !use IFPORT
    implicit none
    ! MK parallel
    include 'mpif.h'
    include "struct.h"

    ! for command line arguments
    character :: okey
    ! I/O file names
    character(len=512) :: ifile='test.inp'//char(0), ofile='test'//char(0), tfile='t-test'//char(0)
    !additional vars
    logical:: verbose, debug, help,have_gyroelectrons,continue_flag,resume_flag
    real:: ksi,tau,mu,P0,PL,alpha_xz,alpha_yz,Lx,Ly,Lz,dx,dy,dz,Npts_ratio,scenario,ta,tc,tp,t_max
    integer:: psolver,diag_ntimes,dump_period,history_ntimes,Npc,no_diags,step_max
    real:: param1,param2,param3,param4,param5,oparam1,oparam2,oparam3,oparam4,oparam5
    integer:: block,sphere,cylinder,cone,triangle
    real:: xlow,ylow,zlow,xhigh,yhigh,zhigh,pot,negative,xbase,ybase,zbase,length,xa,ya,za,xb,yb,zb,xc,yc,zc
    character(len=512):: name,injection_file
    integer:: no_species,no_objects
    real:: T,m,q,w,injection_rate,xcenter,ycenter,zcenter
    integer:: mpi_rank,motion_method,injection_method,test_particle,p,Nx,Ny,Nz,group,orientation
    logical:: mono
    real:: Umono,radius
    ! parallel stuff
    integer:: ierr,proc_no,par_size,proc_max,no_diag_slots
    ! structures
    type(block_t), dimension(99):: block_params
    type(object_t), dimension(99):: object_params

    type(sphere_t), dimension(99):: sphere_params
    type(cylinder_t), dimension(99):: cylinder_params
    type(cone_t), dimension(99):: cone_params
    type(triangle_t), dimension(99):: triangle_params
    type(diaglist_t), dimension(99):: diag_params

    type(pinfo), dimension(99):: spec_params
    type(model_t):: model
    real:: Bfieldz,Bfieldx,Bfieldy,bx,by,bz,B,pi,cosalpha_xz,cosalpha_yz
    ! general purpose
    integer:: i,j,k,sp
    integer:: N0,Np,Na,Nc,no_spec,type,nbins,spec,no_diag,iter_time_no,Nz_max,regroup_runs
    integer*8:: Npts
    real:: dt,dt1,dt2,Lzi,nr,ttrans,start_time
    logical:: automatic_particle_decomposition,enable_iter_time,use_moments,use_old,zero_E_field,frozen_potvac,advanced_restore,use_surface_heat_tweaks
    ! domain decomposition variables
    real:: Lx_total,Ly_total,Lz_total,Lx_start,Ly_start,Lz_start,Lx_stop,Ly_stop,Lz_stop,Lx_length,Ly_length,Lz_length,Lz_limit,Lz_max,Lz_grid
    integer:: Nx_total,Ny_total,Nz_total,Nx_length,Ny_length,Nz_length,Nx_start,Nx_stop,Ny_start,Ny_stop,Nz_start,Nz_stop,Nz_grid
    type(slice_info), dimension(99):: slices
    integer:: decompose_x,decompose_y,decompose_z
    real*8:: mks_n0,mks_Te,mks_B,mks_main_ion_q,mks_main_ion_m,mks_par1,mks_par2,mks_par3
    real*8:: poiss_res
    integer:: poiss_smooths,poiss_levels

    ! namelist with plasma parameters
    namelist /plasma/ ksi,tau,mu,P0,PL,alpha_xz,alpha_yz
    ! geometry
    namelist /geom/ Lx,Ly,Lz,dx,dy,dz,Npc,Npts_ratio,scenario,decompose_x,decompose_y
    !time duration of the run
    namelist /time/ ta,tc,tp
    !some tunning params
    namelist /control/  psolver,diag_ntimes,dump_period,history_ntimes,zero_E_field,poiss_levels,poiss_smooths,poiss_res
    ! additional general purpose paramters
    namelist /optional/ param1,param2,param3,param4,param5
    ! electrods
    namelist /num_blocks/ block,sphere,cylinder,cone,triangle
    namelist /cuboid/ name,group,xlow,ylow,zlow,xhigh,yhigh,zhigh,pot,negative,param1,param2,param3
    namelist /spheroid/ name,group,xcenter,ycenter,zcenter,radius,pot,negative,xlow,ylow,zlow,xhigh,yhigh,zhigh,param1,param2,param3
    namelist /cylindroid/ name,group,xbase,ybase,zbase,length,radius,pot,negative,orientation,xlow,ylow,zlow,xhigh,yhigh,zhigh,param1,param2,param3
    namelist /conoid/ name,group,xbase,ybase,zbase,length,radius,pot,negative,orientation,xlow,ylow,zlow,xhigh,yhigh,zhigh,param1,param2,param3
    namelist /triangloid/ name,group,xa,ya,za,xb,yb,zb,xc,yc,zc,radius,pot,negative,xlow,ylow,zlow,xhigh,yhigh,zhigh,param1,param2,param3

    namelist /mks/ mks_n0,mks_Te,mks_B,mks_main_ion_q,mks_main_ion_m,mks_par1,mks_par2,mks_par3,use_surface_heat_tweaks

    ! particles
    namelist /num_spec/ no_species
    namelist /specie/ name,T,m,q,w,mpi_rank,motion_method,mono,Umono,injection_file,injection_method,injection_rate
    namelist /num_diag/ no_diag
    namelist /diag/ name,type,xlow,ylow,zlow,xhigh,yhigh,zhigh,start_time,spec,nbins,param1,param2,param3



    ! and that's it for the moment, diag regions will come later
    write(*,*) 'This is the SPICE3 code'
    !MK proper init of logical vars - needed for 64bit
    use_moments = .false.
    use_old = .false.
    verbose = .true.
    debug = .false.
    frozen_potvac = .false.
    have_gyroelectrons = .false.
    automatic_particle_decomposition = .false.
    enable_iter_time = .true.
    continue_flag = .false.
    resume_flag = .false.
    advanced_restore = .false.
    regroup_runs = 0
    ! everything that goes into subroutines must be initialized first
    ierr = 0
    proc_no = 0
    par_size = 0
    Nz_max = 1
    step_max = 0
    ! MK parallelization
    ! disabled for now
    mks_B = 0.0
    mks_n0 = 0.0
    mks_Te = 0.0
    mks_par1 = 0.0
    mks_par2 = 0.0
    mks_par3 = 0.0
    mks_main_ion_m = 0.0
    mks_main_ion_q = 0.0
    ! default values
    poiss_levels = 3
    poiss_smooths = 5
    poiss_res= 1E-13
    use_surface_heat_tweaks = .false.


    call MPI_INIT(ierr)
    write(*,*) 'MPI initalization returned status',ierr
    call MPI_COMM_RANK(MPI_COMM_WORLD,proc_no,ierr)
    call MPI_COMM_SIZE(MPI_COMM_WORLD,par_size,ierr)
    write(*,*) 'Parallelization initialized'
    write(*,*) 'Processors under command:',par_size
    proc_max = par_size -1

    ! default decomposition topology
    ! number of slices in x direction
    decompose_x = 2
    ! number of slices in y direction
    decompose_y = 2

    decompose_z = 1
    t_max = 0
    if (proc_no.ne.0) then
        !put the process to sleep so the info is written in order
        call sleep(2*proc_no + 1)
    end if


    write(*,*) 'This is processor no',proc_no

    !
    ! parsing command line arguments
    write(*,*) 'Parsing command line'
    do
        okey=getopt('i:t:o:g:vkhnrcds:l:p:')
        if(okey.eq.'>') exit
        if(okey.eq.'!') then
            write(6,*) 'unknown option: ', trim(optarg)
            call display_help()
            stop
        end if
        if(okey.eq.'v') then
            verbose = .true.
            write(6,*) 'Running in verbose mode'
        end if
        if(okey.eq.'c') then
            continue_flag = .true.
            write(6,*) 'Running in continue mode'
        end if
        if(okey.eq.'r') then
            resume_flag = .true.
            write(6,*) 'Running in resume mode'
        end if
        if(okey.eq.'n') then
            advanced_restore = .true.
            write(6,*) 'Advanced restore on - particles will be loaded from all processors'
        end if

        if(okey.eq.'k') then
            frozen_potvac = .true.
            write(6,*) 'Running in frozen Potvac mode'
        end if

        if(okey.eq.'d') then
            debug = .true.
            write(6,*) 'Running in debug mode'
        end if
        if(okey.eq.'h') then
            call display_help()
            stop
        end if

        if(okey.eq.'o') then
            ofile=trim(optarg)
            write(6,*) 'Final output will be written to ',trim(optarg)
        end if
        if(okey.eq.'i') then
            ifile=trim(optarg)
            write(6,*) 'Using input file ',trim(optarg)
        end if
        if(okey.eq.'t') then
            tfile=trim(optarg)
            write(6,*) 'Time diag will be written to ',trim(optarg)
        end if
        if(okey.eq.'l') then
            read(optarg,'(i)') p
            t_max = real(p)
            write(6,*) 'Maximum runtime will be limited to (hours)',t_max
            ! convert to seconds
            t_max = t_max *3600;
        end if
        if(okey.eq.'g') then
            read(optarg,'(i)') p
            regroup_runs = p
            write(6,*) 'Regrouping run from no outputs',regroup_runs
            continue_flag = .true.
            ! convert to seconds
        end if
        if(okey.eq.'s') then
            write(6,*) 'Option argument for step limit:',optarg
            read(optarg,'(i)') p
            step_max = p
            write(6,*) 'Maximum runtime will be limited to (steps)',step_max
        endif

    end do
    write(*,*) 'Reading input file'

    open(3,file = trim(ifile)//char(0),status='unknown')
    read(3,nml=plasma)
    read(3,nml=geom)
    do sp=1,10
        ! calculate number of slices in z direction
        if ((decompose_x*decompose_y).gt.par_size) then
            decompose_x = decompose_x/2
        else
            exit
        end if
        if ((decompose_x*decompose_y).gt.par_size) then
            decompose_y = decompose_y/2
        else
            exit
        end if
    end do

    if ((decompose_x*decompose_y).gt.par_size) then
        write(*,*) 'Unable to set the decomposition topology!!!!'
        write(*,*) 'Decomposition topology:',decompose_x,decompose_y,decompose_z
        stop
    end if

    decompose_z = int(par_size/(decompose_x*decompose_y))
    write(*,*) 'Decomposition topology:',decompose_x,decompose_y,decompose_z

    read(3,nml=time)
    read(3,nml=control)
    read(3,nml=optional)
    oparam1 = param1
    oparam2 = param2
    oparam3 = param3
    oparam4 = param4
    oparam5 = param5

    if (oparam3.gt.0) then
        read(3,nml=mks)
    end if


    read(3,nml=num_blocks)
    if (verbose) then
        write(*,*) 'Geometry configuration:'
        write(*,*) '-----------------------'
        write(*,*) 'Blocks:',block
        write(*,*) 'Spheres:',sphere
        write(*,*) 'Cylinders:',cylinder
        write(*,*) 'Cones:',cone
        write(*,*) 'Triangles:',triangle


    end if
    ! load in blocks if there are any
    if (block.gt.0) then
        do i=1,block
            read(3,nml=cuboid)
            block_params(i)%name = name
            block_params(i)%group = group
            block_params(i)%xlow = int(xlow/dx) + 1
            block_params(i)%ylow = int(ylow/dy) + 1
            block_params(i)%zlow = int(zlow/dz) + 1
            block_params(i)%xhigh = int(xhigh/dx) +1
            block_params(i)%yhigh = int(yhigh/dy) +1
            block_params(i)%zhigh = int(zhigh/dz) +1
            block_params(i)%pot = pot
            block_params(i)%negative = negative
            block_params(i)%param1 = param1
            block_params(i)%param2 = param2
            block_params(i)%param3 = param3
            ! general structure
            object_params(i)%name = name
            object_params(i)%group = group
            object_params(i)%pot = pot
            object_params(i)%negative = negative
            object_params(i)%param1 = param1
            object_params(i)%param2 = param2
            object_params(i)%param3 = param3

        end do
    end if
    write(*,*) 'Blocks loaded'
    ! load in blocks if there are any
    if (sphere.gt.0) then
        do i=1,sphere
            read(3,nml=spheroid)
            sphere_params(i)%name = name
            sphere_params(i)%group = group
            sphere_params(i)%xcenter = int(xcenter/dx) + 1
            sphere_params(i)%ycenter = int(ycenter/dy) + 1
            sphere_params(i)%zcenter = int(zcenter/dz) + 1
            sphere_params(i)%radius = radius/sqrt(dx*dx/3.0 + dy*dy/3.0 + dz*dz/3.0)
            sphere_params(i)%pot = pot
            sphere_params(i)%negative = negative
            sphere_params(i)%param1 = param1
            sphere_params(i)%param2 = param2
            sphere_params(i)%param3 = param3
            sphere_params(i)%xlow = int(xlow/dx) + 1
            sphere_params(i)%ylow = int(ylow/dy) + 1
            sphere_params(i)%zlow = int(zlow/dz) + 1
            if (xhigh.eq.0.0) then
                xhigh = Lx
            end if
            if (xhigh.eq.0.0) then
                yhigh = Ly
            end if
            if (zhigh.eq.0) then
                zhigh = Lz
            end if
            sphere_params(i)%xhigh = int(xhigh/dx) +1
            sphere_params(i)%yhigh = int(yhigh/dy) +1
            sphere_params(i)%zhigh = int(zhigh/dz) +1

            ! general structure
            object_params(i+block)%name = name
            object_params(i+block)%group = group
            object_params(i+block)%pot = pot
            object_params(i+block)%negative = negative
            object_params(i+block)%param1 = param1
            object_params(i+block)%param2 = param2
            object_params(i+block)%param3 = param3

        end do
    end if
    write(*,*) 'Spheres loaded'
    if (cylinder.gt.0) then
        do i=1,cylinder
            read(3,nml=cylindroid)
            cylinder_params(i)%name = name
            cylinder_params(i)%group = group
            cylinder_params(i)%x = xbase/dx + 1.0
            cylinder_params(i)%y = ybase/dy + 1.0
            cylinder_params(i)%z = zbase/dz + 1.0
            cylinder_params(i)%length = length/dz
            cylinder_params(i)%xlow = int(xlow/dx) + 1
            cylinder_params(i)%ylow = int(ylow/dy) + 1
            cylinder_params(i)%zlow = int(zlow/dz) + 1
            if (xhigh.eq.0.0) then
                xhigh = Lx
            end if
            if (xhigh.eq.0.0) then
                yhigh = Ly
            end if
            if (zhigh.eq.0) then
                zhigh = Lz
            end if
            cylinder_params(i)%xhigh = int(xhigh/dx) +1
            cylinder_params(i)%yhigh = int(yhigh/dy) +1
            cylinder_params(i)%zhigh = int(zhigh/dz) +1
            cylinder_params(i)%radius = radius/sqrt(dx*dx/2.0 + dy*dy/2.0)
            cylinder_params(i)%pot = pot
            cylinder_params(i)%negative = negative
            cylinder_params(i)%orientation = orientation
            cylinder_params(i)%param1 = param1
            cylinder_params(i)%param2 = param2
            cylinder_params(i)%param3 = param3
            ! general structure
            object_params(i+block+sphere)%name = name
            object_params(i+block+sphere)%group = group
            object_params(i+block+sphere)%pot = pot
            object_params(i+block+sphere)%negative = negative
            object_params(i+block+sphere)%param1 = param1
            object_params(i+block+sphere)%param2 = param2
            object_params(i+block+sphere)%param3 = param3

        end do
    end if

    write(*,*) 'Cylinders loaded'
    if (cone.gt.0) then
        do i=1,cone
            read(3,nml=conoid)
            cone_params(i)%name = name
            cone_params(i)%group = group
            cone_params(i)%x = xbase/dx + 1.0
            cone_params(i)%y = ybase/dy + 1.0
            cone_params(i)%z = zbase/dz + 1.0
            cone_params(i)%length = length/dz
            cone_params(i)%xlow = int(xlow/dx) + 1
            cone_params(i)%ylow = int(ylow/dy) + 1
            cone_params(i)%zlow = int(zlow/dz) + 1
            if (xhigh.eq.0.0) then
                xhigh = Lx
            end if
            if (xhigh.eq.0.0) then
                yhigh = Ly
            end if
            if (zhigh.eq.0) then
                zhigh = Lz
            end if
            cone_params(i)%xhigh = int(xhigh/dx) +1
            cone_params(i)%yhigh = int(yhigh/dy) +1
            cone_params(i)%zhigh = int(zhigh/dz) +1

            cone_params(i)%radius = radius/sqrt(dx*dx/2.0 + dy*dy/2.0)
            cone_params(i)%pot = pot
            cone_params(i)%negative = negative
            cone_params(i)%orientation = orientation

            cone_params(i)%param1 = param1
            cone_params(i)%param2 = param2
            cone_params(i)%param3 = param3
            ! general structure
            object_params(i+block+sphere+cylinder)%name = name
            object_params(i+block+sphere+cylinder)%group = group
            object_params(i+block+sphere+cylinder)%pot = pot
            object_params(i+block+sphere+cylinder)%negative = negative
            object_params(i+block+sphere+cylinder)%param1 = param1
            object_params(i+block+sphere+cylinder)%param2 = param2
            object_params(i+block+sphere+cylinder)%param3 = param3

        end do
    end if
    write(*,*) 'Cones loaded'
    if (triangle.gt.0) then
        do i=1,triangle
            read(3,nml=triangloid)
            triangle_params(i)%name = name
            triangle_params(i)%group = group
            triangle_params(i)%xa = int(xa/dx) + 1.0
            triangle_params(i)%ya = int(ya/dy) + 1.0
            triangle_params(i)%za = int(za/dz) + 1.0
            triangle_params(i)%xb = int(xb/dx) + 1.0
            triangle_params(i)%yb = int(yb/dy) + 1.0
            triangle_params(i)%zb = int(zb/dz) + 1.0
            triangle_params(i)%xc = int(xc/dx) + 1.0
            triangle_params(i)%yc = int(yc/dy) + 1.0
            triangle_params(i)%zc = int(zc/dz) + 1.0

            triangle_params(i)%xlow = int(xlow/dx) + 1
            triangle_params(i)%ylow = int(ylow/dy) + 1
            triangle_params(i)%zlow = int(zlow/dz) + 1
            if (xhigh.eq.0.0) then
                xhigh = Lx
            end if
            if (xhigh.eq.0.0) then
                yhigh = Ly
            end if
            if (zhigh.eq.0.0) then
                zhigh = Lz
            end if
            triangle_params(i)%xhigh = int(xhigh/dx) +1
            triangle_params(i)%yhigh = int(yhigh/dy) +1
            triangle_params(i)%zhigh = int(zhigh/dz) +1
            triangle_params(i)%pot = pot
            triangle_params(i)%negative = negative

            triangle_params(i)%param1 = param1
            triangle_params(i)%param2 = param2
            triangle_params(i)%param3 = param3
            ! general structure
            object_params(i+block+sphere+cylinder+cone)%name = name
            object_params(i+block+sphere+cylinder+cone)%group = group
            object_params(i+block+sphere+cylinder+cone)%pot = pot
            object_params(i+block+sphere+cylinder+cone)%negative = negative
            object_params(i+block+sphere+cylinder+cone)%param1 = param1
            object_params(i+block+sphere+cylinder+cone)%param2 = param2
            object_params(i+block+sphere+cylinder+cone)%param3 = param3

        end do
    end if
    write(*,*) 'Triangles loaded'


    ! load particles
    write(*,*) 'Loading particles'
    read(3,nml=num_spec)
    ! we cannot loop over sp with internal increments!!!
    sp  = 0
    no_spec = no_species

    do i=1,no_spec
        sp = sp +1
        read(3,nml=specie)
        !copy over the species paramters into the data structure
        spec_params(sp)%name = name
        write(*,*) 'Specie: ',name

        spec_params(sp)%T = T
        spec_params(sp)%q = q
        spec_params(sp)%w = w
        spec_params(sp)%m = m
        spec_params(sp)%mpi_rank = mpi_rank
        spec_params(sp)%motion_method = motion_method
        if (motion_method.eq.1) then
            have_gyroelectrons = .true.
            write(*,*) 'Using Gyroelectrons'
        end if
        spec_params(sp)%mono = mono
        spec_params(sp)%Umono = Umono
        spec_params(sp)%injection_file = injection_file
        spec_params(sp)%injection_method = injection_method
        spec_params(sp)%injection_rate = injection_rate
        spec_params(sp)%param1 = param1
        spec_params(sp)%param2 = param2
        spec_params(sp)%param3 = param3

        ! paralelized species
        if (spec_params(sp)%mpi_rank.eq.-1.and.proc_max.gt.0) then
            ! divide the injection
            write(*,*) 'Will use automatic particle decomposition'
            automatic_particle_decomposition = .true.
            ! domain decomposition implies no division of injection
            !  spec_params(sp)%injection_rate = spec_params(sp)%injection_rate/real(proc_max +1)
            spec_params(sp)%mpi_rank = proc_no
            ! create new particles
            !                   no_species = no_species + proc_max
            !                  do k=1,proc_max
            !                        spec_params(k+sp) = spec_params(sp)
            !                         spec_params(k+sp)%mpi_rank = k
            !                  end do
            !             sp = sp + proc_max
        else if (spec_params(sp)%mpi_rank.eq.-1) then
            spec_params(sp)%mpi_rank = 0

        end if
    end do
    ! diagnostics
    read(3,nml=num_diag)
    write(*,*) 'Will load',no_diag,'diagnostics'
    no_diag_slots = 0
    do i=1,no_diag
        read(3,nml=diag)
        write(*,*) 'Loading diag ',name
        write(*,*) 'specie ',spec
        write(*,*) 'type ',type


        diag_params(i)%name = name
        diag_params(i)%type = type
        diag_params(i)%specie = spec
        diag_params(i)%start_time = start_time

        diag_params(i)%x_low = xlow
        diag_params(i)%y_low = ylow
        diag_params(i)%z_low = zlow
        diag_params(i)%x_high = xhigh
        diag_params(i)%y_high = yhigh
        diag_params(i)%z_high = zhigh
        diag_params(i)%nbins = nbins
        diag_params(i)%param1 = param1
        diag_params(i)%param2 = param2
        diag_params(i)%param3 = param3
        if (type.eq.1) then
            no_diag_slots = no_diag_slots + 1
        else if (type.eq.2) then
            no_diag_slots = no_diag_slots + 3
        else if (type.eq.3) then
            no_diag_slots = no_diag_slots + 1
        else if (type.eq.4) then
            no_diag_slots = no_diag_slots + 3
        else
            no_diag_slots = no_diag_slots + 1

        end if






    end do

    ! end of input loading
    write(*,*) 'Input loaded'
    ! no we have to calculate the important parameters for mainloop
    ! fix monoenergetic stuff
    do sp=1,no_species
        write(*,*) 'Specie: ',spec_params(sp)%name
        write(6,*) 'mono=',spec_params(sp)%mono
        write(*,*) 'injection=',spec_params(sp)%injection_rate
        write(*,*) 'MPI rank',spec_params(sp)%mpi_rank

        if (spec_params(sp)%mono) then
            spec_params(sp)%Umono=spec_params(sp)%Umono*ksi*sqrt(spec_params(sp)%T/spec_params(sp)%m)
            write(6,*) 'Umono=',spec_params(sp)%Umono
        endif
    end do
    write(*,*) 'tau=',tau
    write(*,*) 'mu=',mu
    write(*,*) 'ksi=',ksi
    write(*,*) 'P0=',P0
    write(*,*) 'PL=',PL
    !Fields and Constantes
    !write(*,*) 'alpha_xz=',alpha_xz
    write(*,*) 'alpha_yz=',alpha_yz
    pi=acos(-1.0)

    Bfieldz=-1.0                         !fixed from the beginning
    if (abs(alpha_yz).eq.90.0) then
        Bfieldy=0.0
    else
        alpha_yz=alpha_yz*pi/180.0     !transform degres in radians
        Bfieldy=-1.0/tan(alpha_yz)
    endif
    if (abs(alpha_xz).eq.90.0) then
        Bfieldx=0.0
    else
        alpha_xz=alpha_xz*pi/180.0     !transform degres in radians
        Bfieldx=-1.0/tan(alpha_xz)
    endif

    write(*,*) 'Bfieldx=',Bfieldx
    write(*,*) 'Bfieldy=',Bfieldy
    write(*,*) 'Bfieldz=',Bfieldz
    B=sqrt(Bfieldx**2+Bfieldy**2+Bfieldz**2)
    bz=Bfieldz/B   != cosalpha  NEW!!                  !for orientation of B
    by=Bfieldy/B   != costheta
    bx=Bfieldx/B   != cosphi
    !dz is the grid spacing
    write(*,*) 'dz=',dz
    !dy is the grid spacing in y direction
    write(*,*) 'dy=',dy
    !dx is the grid spacing in x direction
    write(*,*) 'dx=',dx
    ! we need this length to properly estimate Npts
    if (alpha_yz.eq.90.0.and.alpha_xz.eq.90.0) then
        Lzi=Lz                                                  !sqrt(2)*ksi=1rLarmor and we take 5 as maximum
    else                                                       !in the Maxwll. distribution. the factor 2 is
        Lzi=Lz+2.0*(3.0*sqrt(2.0)*ksi)*sqrt(bx**2+by**2)   !because we inject particles at 5rL after Lz.
        ! round Lzi to some reasonable number to avoid mistakes
        Lzi = float(int(Lzi))
    endif                                                      !All particles shud be inside
    write(*,*) 'Lzi=',Lzi

    ! here stops the gridding
    Lz_grid = Lz
    Nz_grid=int(Lz_grid/dz+1.E-10)  +1            !+1.d-10 <- Pb avec compilateur Linux/Unix
    Nz_max=int(Lzi/dz+1.E-10)  +1            !+1.d-10 <- Pb avec compilateur Linux/Unix
    Lz_max = Lzi
    ! decomposition
    Lx_total = Lx
    Ly_total = Ly
    Lz_total = Lzi
    !Lz_total = Lz
    !Lz_total = Lzi
    Nx_total=int(Lx_total/dx+1.E-10)+1              !+1.d-10 <- Pb avec compilateur Linux/Unix
    write(*,*) 'Nx_total=',Nx_total
    Lz_limit = Lz
    !Ny is the number of grid points in y direction
    Ny_total=int(Ly_total/dy+1.E-10)+1              !+1.d-10 <- Pb avec compilateur Linux/Unix
    write(*,*) 'Ny_total=',Ny_total
    Nz_total=int(Lz/dz+1.E-10)+1              !+1.d-10 <- Pb avec compilateur Linux/Unix
    write(*,*) 'Nz_total=',Nz_total
    Lx_length = int(Lx_total/float(decompose_x))
    Ly_length = int(Ly_total/float(decompose_y))
    Lz_length = int(Lzi/float(decompose_z) +1.0)
    write(*,*) 'Lengths',Lx_length,Ly_length,Lz_length
    Nx_length=int(Lx_length/dx+1.E-10)              !+1.d-10 <- Pb avec compilateur Linux/Unix
    Ny_length=int(Ly_length/dy+1.E-10)              !+1.d-10 <- Pb avec compilateur Linux/Unix
    Nz_length=int(Lz_length/dz+1.E-10)              !+1.d-10 <- Pb avec compilateur Linux/Unix

    i = 0
    j = 1
    k = 1
    do sp=1,par_size
        i = i+1
        if (i.gt.decompose_x) then
            j = j + 1
            i=1
        end if
        if (j.gt.decompose_y) then
            j =1
            i=1
            k=k+1
        end if
        write(*,*) 'Slice',sp
        write(*,*) 'indexes',i,j,k
        slices(sp)%Nx_start = Nx_length*(i-1)+1
        slices(sp)%Nx_stop = min(Nx_length*i+1,Nx_total)
        slices(sp)%Ny_start = Ny_length*(j-1)+1
        slices(sp)%Ny_stop = min(Ny_length*j+1,Ny_total)
        slices(sp)%Nz_start = Nz_length*(k-1)+1
        slices(sp)%Nz_stop = min(Nz_length*k+1,Nz_grid)
        if (slices(sp)%Nz_start.gt.Nz_total) then
            slices(sp)%Nz_start = Nz_grid
            slices(sp)%Nz_stop = Nz_grid
        end if

        slices(sp)%Lx_start = Lx_length*(i-1)
        slices(sp)%Lx_stop = min(Lx_length*i,Lx_total)
        slices(sp)%Ly_start = Ly_length*(j-1)
        slices(sp)%Ly_stop = min(Ly_length*j,Ly_total)
        slices(sp)%Lz_start = Lz_length*(k-1)
        slices(sp)%Lz_stop = min(Lz_length*k,Lzi)
        slices(sp)%Lz_max = Lz_length*k
        ! this one was missing but also never used
        slices(sp)%Lz_grid = Lz_grid




    end do
    sp = proc_no +1
    Nx_start = slices(sp)%Nx_start
    Ny_start = slices(sp)%Ny_start
    Nz_start = slices(sp)%Nz_start
    Nx_stop = slices(sp)%Nx_stop
    Ny_stop = slices(sp)%Ny_stop
    Nz_stop = slices(sp)%Nz_stop
    Lx_start = slices(sp)%Lx_start
    Ly_start = slices(sp)%Ly_start
    Lz_start = slices(sp)%Lz_start
    Lx_stop = slices(sp)%Lx_stop
    Ly_stop = slices(sp)%Ly_stop
    Lz_stop = slices(sp)%Lz_stop
    Nx = Nx_stop - Nx_start +1
    Ny = Ny_stop - Ny_start +1
    Nz = Nz_stop - Nz_start +1
    Nz_length = Nz_stop - Nz_start
    Lx = Lx_stop - Lx_start
    Ly = Ly_stop - Ly_start
    Lz = Lz_stop - Lz_start
    Lz_length =  Lz


    write(*,*) 'Determined slice bourdaries'
    write(*,*) 'NXR',Nx_start,Nx_stop
    write(*,*) 'NYR',Ny_start,Ny_stop
    write(*,*) 'LZR',Nz_start,Nz_stop
    write(*,*) 'LXR',Lx_start,Lx_stop
    write(*,*) 'LYR',Ly_start,Ly_stop
    write(*,*) 'LZR',Lz_start,Lz_stop



    ! write out the info
    write(*,*) 'Slice limits'
    do sp=1,par_size
        write(*,*) 'Processor',sp
        write(*,*) 'LX',slices(sp)%Lx_start,slices(sp)%Lx_stop
        write(*,*) 'LY',slices(sp)%Ly_start,slices(sp)%Ly_stop
        write(*,*) 'LZ',slices(sp)%Lz_start,slices(sp)%Lz_stop
        write(*,*) 'NX',slices(sp)%Nx_start,slices(sp)%Nx_stop
        write(*,*) 'NY',slices(sp)%Ny_start,slices(sp)%Ny_stop
        write(*,*) 'NZ',slices(sp)%Nz_start,slices(sp)%Nz_stop
    end do


    if (debug) then
        !   write(*,*) 'Initial BOX limits',slices
    end if

    ! Nx_total=int(Lx/dx+1.E-10)+1              !+1.d-10 <- Pb avec compilateur Linux/Unix
    ! Nx = Nx_stop - Nx_start +1
    ! Lx = Lx_stop - Lx_start
    write(*,*) 'Nx=',Nx
    !Ny is the number of grid points in y direction
    ! Ny_total=int(Ly/dy+1.E-10)+1              !+1.d-10 <- Pb avec compilateur Linux/Unix
    !Ly_total = Ly
    ! Ny = Ny_stop - Ny_start +1
    ! Ly = Ly_stop - Ly_start
    write(*,*) 'Ny=',Ny
    ! ! Nz_total=int(Lz/dz+1.E-10)+1              !+1.d-10 <- Pb avec compilateur Linux/Unix
    ! Nz = Nz_stop - Nz_start+1
    ! Lz = Lz_stop - Lz_start
    write(*,*) 'Nz=',Nz


    N0=int(Npc/(dz*dy*dx)+1.E-10)          !+1.d-10 <- Pb avec compilateur Linux
    write(6,*) 'N0=',N0

    !Set time step

    ! reduced  by factor of 2 to suppress instabilit

    dt1=0.25*sqrt((dz**2+ dy**2 + dx**2)/mu/2.0)/ksi
    !   dt2=2.d0*pi/sqrt(mu)/10.d0                 ! dt = 1/10*T for Larmor gyration
    dt2=2.d0*pi/mu/10.d0
    write(*,*) 'dt1=',dt1
    write(*,*) 'dt2=',dt2

    if (have_gyroelectrons)  then
        ! don't use this criterion for gyroelectrons
        dt1 = dt1
        dt2 = 10000.0
    else

    end if
    if (dt1.le.dt2) then
        dt=dt1
    else
        dt=dt2
    endif
    write(*,*) 'dt=',dt
    write(*,*) 'dt limits=',dt1,dt2
    ! we 816
    !need this length to properly estimate Npts
    ! if (alpha_yz.eq.90.0.and.alpha_xz.eq.90.0) then
    !       Lzi=Lz_total                                                  !sqrt(2)*ksi=1rLarmor and we take 5 as maximum
    !    else                                                       !in the Maxwll. distribution. the factor 2 is
    !       Lzi=Lz_total+2.0*(3.0*sqrt(2.0)*ksi)*sqrt(bx**2+by**2)   !because we inject particles at 5rL after Lz.
    !    endif                                                      !All particles shud be inside
    !    write(*,*) 'Lzi=',Lzi
    ! Lz_max= Lzi
    !    !Npts is the total number of particles for n=1 everywhere.
    ! if ((idint(Lzi/dz) + 1).gt.Nz_max) then
    ! Nz_max = idint(Lzi/dz) + 1
    !
    ! end if
    nr = 0.0
    do sp=1,no_species
        if (spec_params(sp)%mpi_rank.eq.proc_no) then
            nr = nr + spec_params(sp)%injection_rate
        end if
    end do
    !fix - in most cases we have ions + electrons
    ! this gives nr = 1.5, while we want 2.0
    nr = nr/1.5*2.0
    !Npts_ratio is a input-specific tuning parameter
    Npts=kidint(Npts_ratio*nr*N0*Lz*Ly*Lx)
    write(*,*) 'Npts=',Npts
    !ttrans is the ion transit time
    ttrans=Lz_total/sqrt(tau)/ksi/abs(bz)
    write(6,*) 'ttrans=',ttrans
    !Np is the number of time steps to be made.
    Np=int(ttrans*tp/dt)+1
    write(6,*) 'Np=',Np
    !Na is the number of time steps before averaging starts.
    Na=int(ttrans*ta/dt)+1
    write(6,*) 'Na=',Na
    !Nc is optional to start some strange stuff in saturated case
    Nc=int(ttrans*tc/dt)+1
    write(6,*) 'Nc=',Nc

    cosalpha_xz=Bfieldx/sqrt(Bfieldx**2+Bfieldz**2)   !NEW!!
    cosalpha_yz=Bfieldy/sqrt(Bfieldy**2+Bfieldz**2)

    write(*,*) 'B-field =',B
    write(*,*) 'bx=',bx
    write(*,*) 'by=',by
    write(*,*) 'bz=',bz
    write(*,*) 'cosalpha_xz=',cosalpha_xz
    write(*,*) 'cosalpha_yz=',cosalpha_yz
    if (history_ntimes.gt.(2*Np)) then
        !too many samples, no combing
        history_ntimes = Np +1
    else if (history_ntimes.eq.0) then
        !forced combing disabled
        history_ntimes = Np +1
    else
        !we need double space for combing to assure history_ntimes samples
        history_ntimes = history_ntimes*2

    end if
    if (enable_iter_time) then
        iter_time_no = Np
    else
        iter_time_no = 10
    end if

    ! now stuff as much as possible into the structure so we can easily pass it to subroutines
    model%ksi = ksi
    model%tau = tau
    model%mu = mu
    model%P0 = P0
    model%PL = PL
    model%alpha_xz = alpha_xz
    model%alpha_yz = alpha_yz
    model%dx = dx
    model%dy = dy
    model%dz = dz
    model%Lx = Lx
    model%Ly = Ly
    model%Lz = Lz
    model%Nx = Nx
    model%Ny = Ny
    model%Nz = Nz
    model%Na = Na
    model%Nc = Nc
    model%Np = Np

    model%Nx_start = Nx_start
    model%Ny_start = Ny_start
    model%Nz_start = Nz_start
    model%Nx_stop = Nx_stop
    model%Ny_stop = Ny_stop
    model%Nz_stop = Nz_stop
    model%Nx_total= Nx_total
    model%Ny_total = Ny_total
    model%Nz_total = Nz_total

    model%Lx_start = Lx_start
    model%Ly_start = Ly_start
    model%Lz_start = Lz_start
    model%Lx_total = Lx_total
    model%Ly_total = Ly_total
    model%Lz_total = Lz_total
    model%Lz_max = Lz_max
    model%Lz_grid = Lz_grid

    model%Lx_stop = Lx_stop
    model%Ly_stop = Ly_stop
    model%Lz_stop = Lz_stop
    model%Na = Na
    model%Np = Np
    model%Nc = Nc
    model%automatic_particle_decomposition  = automatic_particle_decomposition
    model%decompose_x = decompose_x
    model%decompose_y = decompose_y
    model%decompose_z = decompose_z


    model%psolver = psolver
    model%Npc = Npc
    model%continue = continue_flag
    model%resume = resume_flag
    model%advanced_restore = advanced_restore

    !       model%N0 = N0
    model%Lz_limit = Lz_limit
    model%scenario = scenario
    model%param1 = oparam1
    model%param2 = oparam2
    model%param3 = oparam3
    model%param4 = oparam4
    model%param5 = oparam5
    model%dt = dt
    model%Npts = Npts
    model%verbose = verbose
    model%debug = debug
    model%bx = bx
    model%by = by
    model%bz = bz
    model%sqbxby = sqrt(bx*bx + by*by)
    model%tfile = tfile
    model%ofile = ofile
    model%dump_period = dump_period
    model%diag_ntimes = diag_ntimes
    model%proc_no = proc_no
    model%proc_max = proc_max

    model%N0 = N0

    model%poiss_levels = poiss_levels
    model%poiss_smooths = poiss_smooths
    model%poiss_res = poiss_res



    ! load the MKS info

    model%mks_B = mks_B
    model%mks_n0 = mks_n0
    model%mks_Te = mks_Te
    model%mks_param1 = mks_par1
    model%mks_param2 = mks_par2
    model%mks_param3 = mks_par3
    model%mks_main_ion_m = mks_main_ion_m
    model%mks_main_ion_q = mks_main_ion_q
    model%use_surface_heat_tweaks = use_surface_heat_tweaks

    no_objects = block + sphere + cylinder + cone
    !ok, that's it for the init, let's jump into the mainloop
    write(*,*) 'Entering mainloop'
    call mainloop(Npts,model,history_ntimes,Na,Np,Nc,block_params(1:block),sphere_params(1:sphere),cylinder_params(1:cylinder),&
        cone_params(1:cone),triangle_params(1:triangle),spec_params(1:no_species),block,sphere,cylinder,cone,triangle,no_species,Nx,Ny,Nz,proc_max,no_diag,diag_params(1:no_diag),&
        no_diag_slots,no_objects,object_params(1:no_objects),iter_time_no,enable_iter_time,Nz_max,Nx_total,Ny_total,Nz_total,&
        slices(1:(proc_max+1)),Lz_limit,use_moments,use_old,zero_E_field,t_max,step_max,ifile,frozen_potvac,regroup_runs)

    call MPI_FINALIZE(ierr)

contains
    integer function isgn(X)
        integer:: x
        ! integer:: isgn
        if (x.eq.0) then
            isgn =  0
        else if (x.gt.0) then
            isgn =1
        else
            isgn =-1
        end if
    end function

    integer function rsgn(X)
        real:: x
        ! integer:: rsgn
        if (x.eq.0.0) then
            rsgn =  0
        else if (x.gt.0.0) then
            rsgn =1
        else
            rsgn =-1
        end if
    end function


END



subroutine mainloop(Npts,model,history_ntimes,Na,Np,Nc,block_params,sphere_params,cylinder_params,cone_params,triangle_params,spec_params,block,&
        sphere,cylinder,cone,triangle,no_species,Nx,Ny,Nz,proc_max,no_diag,diag_params,no_diag_slots,no_objects,object_params,iter_time_no,&
        enable_iter_time,Nz_max,Nx_total,Ny_total,Nz_total,slices,Lz_limit,use_moments,use_old,zero_E_field,t_max,step_max,ifile,frozen_potvac,regroup_runs)
    implicit none
    include 'struct.h'
    character(len=512) :: ifile, ofile, tfile
    !these vars determine the field sizes
    integer:: Na,Np,Nc,no_species,block,sphere,cylinder,cone,triangle,no_diag,no_diag_slots,ierr,no_objects,iter_time_no,Nz_max,regroup_runs,step_max
    integer*8:: Npts
    ! particle storage
    real, dimension(Npts):: x,y,z,ux,uy,uz,x_old,y_old,z_old
    !integer, dimernsion(Npts):: ix1,iy1,iz1,sort_id
    ! particle type
    integer, dimension(Npts):: stype
    !particle weightsTsviatko
    ! real, dimension(Npts)::A1,A2,A3,A4,A5,A6,A7,A8
    ! related particle arrays
    real, dimension(no_species):: m,q,w,Temp,Lzi,Umono, inj_ratio,utherm
    logical, dimension(no_species):: mono
    integer, dimension(no_species):: pnumber,mpi_rank, injection_method

    ! total number of particles
    integer:: no_tot,nd
    ! plasma parameters
    real:: ksi,tau,mu,P0,PL,alpha_xz,alpha_yz,Lx,Ly,Lz,dx,dy,dz,Npts_ratio,scenario,ta,tc,tp,bx,by,bz,Lz_limit
    integer:: psolver,diag_ntimes,dump_period,history_ntimes,Nx,Ny,Nz,Npc
    real:: param1,param2,param3,param4,param5,oparam1
    ! objects
    type (model_t):: model
    type(object_t), dimension(no_objects):: object_params

    type(block_t), dimension(block):: block_params
    type(sphere_t), dimension(sphere):: sphere_params
    type(cylinder_t), dimension(cylinder):: cylinder_params
    type(cone_t), dimension(cone):: cone_params
    type(triangle_t), dimension(triangle):: triangle_params

    type(diaglist_t), dimension(no_diag):: diag_params

    type(pinfo), dimension(no_species):: spec_params
    ! poisson
    ! time
    real:: dt

    ! general purpose
    real*8:: rndx
    integer:: i,j,k,sp,no_inject
    ! logicals
    logical:: verbose, debug,enable_iter_time
    ! 3D matrixes - we'd better keep a real small number of them
    !  this is used to detect collisions
    ! the potential and electric fields
    real, dimension(Nx,Ny,Nz):: Pot,Potav,Ex,Ey,Ez,rho_tot,Exav,Eyav,Ezav,edge_charge,edge_charge_tot,edge_chargeav,edge_charge_single,inj_rem
    real, dimension(no_species,Nx,Ny,Nz):: rho
    real:: xlow,xhigh,ylow,yhigh,zlow,zhigh,r
    integer:: ix,iy,iz,count
    ! time histories
    ! number of particles
    integer, dimension(no_species,history_ntimes):: snumber,snumber_total
    integer:: h_pos,delta_h
    ! time measurements
    ! these need to have single precision
    real:: t_start, t_fin, t_grid, t_poiss, t_wt, t_lp, t_inj, t_diag,t_bc, t_sum,p_z,t_gd,t_com1,t_com2,clock_start,c_grid,c_poiss,&
        c_wt,c_lp,c_inj,c_diag,c_bc,c_sum,c_rate,c_mpi1,c_mpi2,total_time,c_sort,t_iteration,t_max

    ! test particle
    real,dimension(Np)::tpx,tpy,tpz,tpux,tpuy,tpuz
    ! injection
    real*8, dimension(no_species)::  Gammaz0,umin,umax,N_u_zreal
    integer, dimension(no_species):: N_u_z
    real*8, dimension(no_species,1024) ::  u_z, fu_z
    real*8, dimension(no_species,1024):: X_z,Xu_z
    real*8, dimension(no_species,1024):: ufu_z
    real*8, dimension(no_species):: Iinj,Ninj_fx,Ninj_r
    integer, dimension(no_species):: Ninj
    integer:: npt
    ! leapfrog
    real, dimension(no_species):: Ax_lf,Bx_lf,Cx_lf,Dx_lf,Ex_lf,Fx_lf,Ay_lf,By_lf,Cy_lf,Dy_lf,Ey_lf,Fy_lf
    real, dimension(no_species):: Az_lf,Bz_lf,Cz_lf,Dz_lf,Ez_lf,Fz_lf
    real, dimension(no_species):: Ax_hlf,Bx_hlf,Cx_hlf,Dx_hlf,Ex_hlf,Fx_hlf,Ay_hlf,By_hlf,Cy_hlf,Dy_hlf,Ey_hlf,Fy_hlf
    real, dimension(no_species):: Az_hlf,Bz_hlf,Cz_hlf,Dz_hlf,Ez_hlf,Fz_hlf
    ! averaged diagnostics
    integer:: nav,diag_per,diag_count
    logical:: take_diag
    real, dimension(no_species,Nx,Ny,Nz):: dens,vxav,vyav,vzav,vx2av,vy2av,vz2av,vx,vy,vz,vx2,vy2,vz2,vx3av,vy3av,vz3av,vx3,vy3,vz3
    ! real, dimension(Nx,Ny,Nz):: Potav,Epar,Eperp
    ! edge detection
    integer, dimension(Nx,Ny,Nz):: edges,n_diel_matrix
    ! Poisson solver
    integer*8:: solverpointer
    real*8:: act_r
    ! objects current
    real, dimension(no_objects,no_species,Np):: objects_current,group_current,objects_current_total
    !parallelization
    integer:: proc_no,proc_max,nobjects
    ! time diagnostics,
    type(slice_info), dimension(proc_max+1):: slices

    real, dimension(1,10,iter_time_no):: iter_time
    integer:: sort_period,sort_steps,no_swaps
    real:: sort_fraction
    integer, dimension(Nz_max,Ny):: cell_count

    integer:: max_cell_count,tot_cell_count
    real,dimension(no_diag_slots,Np):: diag_slots
    integer:: diag_slot_counter,node_counter
    real, dimension(no_objects):: float_constant
    real, dimension(no_objects,50):: i_rel_history
    real,dimension(no_species,no_objects,Nx):: int_flux_x
    real,dimension(no_species,no_objects,Ny):: int_flux_y
    real,dimension(no_species,no_objects,Nz):: int_flux_z
    integer:: count_floating_objects
    real:: vstart,vstop,v_act
    integer:: nbsteps,bstep,bint,count_start
    integer:: count_iv_objects
    logical:: have_dielectrics
    logical, dimension(Nx,Ny,Nz):: dielectric_cell
    integer, dimension(Nx,Ny,Nz):: diel_filter
    logical:: zero_E_field,use_moments,use_old
    integer*8:: timestamp,t_intro,t_act
    integer,dimension(Nx,Ny,Nz):: bc_matrix
    real,dimension(Nx,Ny,Nz):: border_matrix

    real:: Lz_low_limit,Lz_high_limit
    logical:: take_weight_diag,ions_are_in_the_box

    ! surface diagnostics
    real, dimension(no_species,Nx,Ny,Nz):: edge_flux,edge_energy_flux
    real:: z_limit
    integer,dimension(Nx,Ny,Nz):: objects
    ! all range matrixes
    real,dimension(Nx_total,Ny_total,Nz_total):: Pot_total,rho_tot_total,edge_charge_tot_total,equipot,Potvac,Ts
    real,dimension(Nx_total,Ny_total,Nz_total):: bc_matrix_total

    integer,dimension(Nx_total*Ny_total*Nz_total):: eq_mask_vector
    real*8,dimension(Nx_total*Ny_total*Nz_total):: bias_vector,rho_vector,Pot_vector
    integer, dimension(Nx_total,Ny_total,Nz_total):: objects_total,poiss_flag
    real:: Lx_total,Ly_total,Lz_total,Lx_start,Ly_start,Lz_start
    integer:: Nx_start,Ny_start,Nz_start,Nx_total,Ny_total,Nz_total
    logical:: save_t_file,frozen_potvac
    character*7:: c_str
    real,dimension(no_objects):: object_pot
    integer:: result1,result2,result3
    real:: edge_charge_decay,init_disp
    real:: therm_Wf,therm_Ei,lambda_D,w_i

    ! SPICE MONITOR
    character*512:: machine,spice_user,perc_str,nproc_str,due_str

    external G05CAF,G05CCF,G05DDF,G05FAF,G05FDF
    c_str = '       '
    machine = '                     '
    spice_user = '                     '
    perc_str = '                     '
    due_str = '                     '
    nproc_str = '                     '


    ! default valuees
    therm_Wf = 4.55 ! eV
    ! default value Hydrogen on tungsten, binding energy is ~1 eV
    !	Ei = 13.6 -  4.55 + 1.0
    therm_Ei = 10.05 ! eV

    ! normalize
    lambda_D = sqrt(8.85E-12*model%mks_Te/model%mks_n0/1.602E-19)
    w_i =  1.602E-19*model%mks_B/model%mks_main_ion_m/1.67E-27

    therm_Wf = therm_Wf*1.602E-19/lambda_D/lambda_D/w_i/w_i/model%mks_main_ion_m/1.67E-27
    therm_Ei= therm_Ei*1.602E-19/lambda_D/lambda_D/w_i/w_i/model%mks_main_ion_m/1.67E-27

    write(*,*) 'Normalized Wf and Ei',therm_Wf,therm_Ei

    take_diag = .false.

    if (model%param5.eq.0.0) then

        edge_charge_decay = 1.0

    else
        edge_charge_decay = model%param5
    end if

    !initial displacement for the thermionic particles
    init_disp = 0.0001

    write(*,*) 'Inside mainloop now'
    Pot_total = 0.0
    rho_tot_total = 0.0
    save_t_file = .false.
    objects_total = 0
    t_intro = 0
    call G05CCF
    call G05FAF(0.d0,1.d0,1,rndx)
    write(*,*) 'First random number', rndx

    call getseconds(t_intro)
    write(*,*) 'Initial timestamp',t_intro

    ! zero E field
    !zero_E_field = .false.
    ions_are_in_the_box = .false.
    ! bias sweeping info
    count_iv_objects = 0
    poiss_flag = 0
    have_dielectrics = .false.
    dielectric_cell = .false.
    vstart = -10.0
    vstop = 10.0
    bstep = 0
    nbsteps = 200
    bint = int(real(Np - Nc)/real(nbsteps))
    if (bint.lt.1) then
        bint = 1
    end if
    v_act = vstart

    edge_flux = 0.0
    edge_energy_flux = 0.0

    ! load stuff from the structure
    ksi = model%ksi
    tau = model%tau
    mu = model%mu
    PL = model%PL
    P0 = model%P0
    alpha_xz =  model%alpha_xz
    alpha_yz =   model%alpha_yz
    dx = model%dx
    dy = model%dy
    dz = model%dz
    Lx = model%Lx
    Ly = model%Ly
    Lz = model%Lz
    Lx_start = model%Lx_start
    Ly_start = model%Ly_start
    Lz_start = model%Lz_start
    Nx_start = model%Nx_start
    Ny_start = model%Ny_start
    Nz_start = model%Nz_start

    !Nx = model%Nx
    !Ny = model%Ny
    !Nz = model%Nz
    psolver = model%psolver
    Npc = model%Npc
    param1 = model%param1
    param2 = model%param2
    param3 = model%param3
    param4 = model%param4
    param5 = model%param5
    dt = model%dt
    verbose = model%verbose
    debug = model%debug
    bx = model%bx
    by = model%by
    bz = model%bz
    scenario = model%scenario
    dump_period = model%dump_period
    diag_per = int((Np - Na)/model%diag_ntimes)
    inj_rem = 0.0
    if (diag_per.lt.1) then
        diag_per = 1
    end if
    proc_no = model%proc_no
    proc_max = model%proc_max
    sort_period = param4
    sort_steps = 0
    sort_fraction = param5
    write(*,*) 'Diag period',diag_per
    write(*,*) 'Sort period',sort_period
    !write(*,*) 'Sort fraction',sort_fraction

    ! init
    ! ierr = 0
    object_pot = 0.0
    n_diel_matrix = 0
    edge_charge = 0.0
    ! edge_charge_single = 0.0
    int_flux_x = 0.0
    int_flux_y = 0.0
    int_flux_z = 0.0
    count_floating_objects= 0
    float_constant = 0.03
    i_rel_history = 0.0
    objects_current = 0.0

    objects_current_total = 0.0
    group_current = 0.0
    eq_mask_vector = 0
    bias_vector = 0.0
    rho_vector = 0.0
    Pot_vector = 0.0
    edges = 0
    diag_count = 0
    nav = 0
    Pot = 0.0
    Potav = 0.0
    edge_chargeav = 0.0
    Exav = 0.0
    Eyav = 0.0
    Ezav = 0.0
    objects = 0
    Ex =0.0
    Ey = 0.0
    Ez = 0.0
    rho = 0.0
    Ts =0.0
    count = 0
    pnumber = 0
    snumber = 0
    snumber_total = 0
    h_pos = 1
    delta_h = 1
    tpx = 0.0
    tpy = 0.0
    tpz = 0.0
    tpux = 0.0
    tpuy = 0.0
    tpuz = 0.0
    GAMMAz0 = 0.0
    u_z = 0.0
    N_u_z = 0.0
    fu_z = 0.0
    X_z = 0.0
    Xu_z = 0.0
    ufu_z = 0.0
    ! averaged stuff
    dens = 0.0
    vxav = 0.0
    vyav = 0.0
    vzav = 0.0
    if (use_moments) then
        vx2av = 0.0
        vy2av = 0.0
        vz2av = 0.0
        vx3av = 0.0
        vy3av = 0.0
        vz3av = 0.0
        vx2 = 0.0
        vy2 = 0.0
        vz2 = 0.0
        vx3 = 0.0
        vy3 = 0.0
        vz3 = 0.0
    end if
    vx = 0.0
    vy = 0.0
    vz = 0.0

    ! zero out leapfrog stuff
    Ax_lf = 0.0
    Bx_lf = 0.0
    Cx_lf = 0.0
    Dx_lf = 0.0
    Ex_lf = 0.0
    Fx_lf = 0.0
    Ay_lf = 0.0
    By_lf = 0.0
    Cy_lf = 0.0
    Dy_lf = 0.0
    Ey_lf = 0.0
    Fy_lf = 0.0
    Az_lf = 0.0
    Bz_lf = 0.0
    Cz_lf = 0.0
    Dz_lf = 0.0
    Ez_lf = 0.0
    Fz_lf = 0.0
    ! half leapfrog for injection
    Ax_hlf = 0.0
    Bx_hlf = 0.0
    Cx_hlf = 0.0
    Dx_hlf = 0.0
    Ex_hlf = 0.0
    Fx_hlf = 0.0
    Ay_hlf = 0.0
    By_hlf = 0.0
    Cy_hlf = 0.0
    Dy_hlf = 0.0
    Ey_hlf = 0.0
    Fy_hlf = 0.0
    Az_hlf = 0.0
    Bz_hlf = 0.0
    Cz_hlf = 0.0
    Dz_hlf = 0.0
    Ez_hlf = 0.0
    Fz_hlf = 0.0
    equipot = 0.0
    solverpointer = 0
    total_time = 0
    iter_time = 0.0
    diag_slots = 0.0
    bc_matrix_total = 0.0

    c_sort = 0
    ! loop over diagnostics and set their start time
    do nd=1,no_diag
        if (diag_params(nd)%start_time.eq.0.0) then
            diag_params(nd)%start_time = 1.0
            ! we start immediately
        else if (diag_params(nd)%start_time.eq.1.0) then
            ! we start at Nc
            diag_params(nd)%start_time = Nc
        else if (diag_params(nd)%start_time.eq.2.0) then
            ! we start at Na
            diag_params(nd)%start_time = Na
        else
            ! we start at given time frame

        end if



    end do
    ! construct the objects matrix
    j = 0
    no_tot = 0
    no_inject = param1
    nobjects = block + sphere + cylinder + cone
    write(*,*) 'Building electrodes'
    ! blocks
    do i=1,block
        write (*,*) 'Building block ',block_params(i)%name
        write (*,*) 'Block potential ',block_params(i)%Pot
        if (block_params(i)%param1.eq.1) then
            have_dielectrics = .true.
            write(*,*) 'This block is dielectric ',i
        end if

        if (block_params(i)%param1.eq.2) then

            count_floating_objects = count_floating_objects +1
        end if
        if (block_params(i)%param1.eq.3) then

            count_iv_objects = count_iv_objects +1
        end if

        j = j +1
        do ix=1,Nx_total
            do iy=1,Ny_total
                do iz=1,Nz_total
                    if ((block_params(i)%xlow.le.ix.and.block_params(i)%xhigh.ge.ix).and.(block_params(i)%ylow.le.iy.and.&
                            block_params(i)%yhigh.ge.iy).and.(block_params(i)%zlow.le.iz.and.block_params(i)%zhigh.ge.iz)) then
                        !     objects(block_params(i)%xlow:block_params(i)%xhigh,block_params(i)%ylow:block_params(i)%yhigh,&
                            !  block_params(i)%zlow:block_params(i)%zhigh) = j
                        ! dielectric objects dont exist for the poisson solver
                        if (block_params(i)%negative.eq.0) then

                            objects_total(ix,iy,iz) = j
                            equipot(ix,iy,iz) = block_params(i)%Pot
                            Ts(ix,iy,iz) = block_params(i)%param2

                            ! 		  poiss_flag(ix,iy,iz) = 0
                            if (block_params(i)%param1.ne.1)  then
                                poiss_flag(ix,iy,iz) = 1
                            else
                                equipot(ix,iy,iz) = 0.0
                                poiss_flag(ix,iy,iz) = 0
                            end if

                        else
                            objects_total(ix,iy,iz) = 0
                            poiss_flag(ix,iy,iz) = 0
                            Ts(ix,iy,iz) = 0
                        end if
                    end if !inside block
                end do
            end do
        end do


    end do
    ! spheres
    ! here we have to loop over the whole space and find points with appropriate distance form given point
    do i=1,sphere
        write (*,*) 'Building sphere ',sphere_params(i)%name
        if (sphere_params(i)%param1.eq.1) then
            have_dielectrics = .true.
            write(*,*) 'This sphere is dielectric ',i
        end if

        if (sphere_params(i)%param1.eq.2) then

            count_floating_objects = count_floating_objects +1
        end if
        if (sphere_params(i)%param1.eq.3) then

            count_iv_objects = count_iv_objects +1
        end if

        j = j+1
        do ix=1,Nx_total
            do iy=1,Ny_total
                do iz=1,Nz_total
                    r = sqrt((real(ix) - sphere_params(i)%xcenter)**2 + (real(iy) - sphere_params(i)%ycenter)**2 +  (real(iz) - &
                        sphere_params(i)%zcenter)**2)
                    if (r < (sphere_params(i)%radius + dx/2.0).and.(ix.ge. sphere_params(i)%xlow).and.(iy.ge. sphere_params(i)%ylow).and.(iz.ge.sphere_params(i)%zlow).and.(ix.le.sphere_params(i)%xhigh).and.(iy.le.sphere_params(i)%yhigh).and.(iz.le.sphere_params(i)%zhigh)  ) then
                        if (sphere_params(i)%negative.eq.0) then
                            objects_total(ix,iy,iz) = j
                            equipot(ix,iy,iz) = sphere_params(i)%Pot
                            Ts(ix,iy,iz) = sphere_params(i)%param2
                            poiss_flag(ix,iy,iz) = 0

                            if (sphere_params(i)%param1.ne.1)  then
                                poiss_flag(ix,iy,iz) = 1
                            else
                                equipot(ix,iy,iz) = 0.0
                                poiss_flag(ix,iy,iz) = 0

                            end if
                        else
                            objects_total(ix,iy,iz) = 0
                            poiss_flag(ix,iy,iz) = 0
                            equipot(ix,iy,iz) = 0.0
                            Ts(ix,iy,iz) = 0.0
                        end if
                    end if
                end do
            end do
        end do
    end do
    ! z-cylinders - cylinders with axis in the z direction
    do i=1,cylinder

        if (cylinder_params(i)%param1.eq.1) then
            have_dielectrics = .true.
            write(*,*) 'This Cylinder is dielectric ',i
        end if

        if (cylinder_params(i)%param1.eq.2) then

            count_floating_objects = count_floating_objects +1
        end if
        if (cylinder_params(i)%param1.eq.3) then

            count_iv_objects = count_iv_objects +1
        end if

        write (*,*) 'Building cylinder ',cylinder_params(i)%name
        write(*,*) 'Base:', cylinder_params(i)%x,cylinder_params(i)%y,cylinder_params(i)%z
        write(*,*) 'Length:', cylinder_params(i)%length
        write(*,*) 'Radius:', cylinder_params(i)%radius
        write(*,*) 'Orientation:', cylinder_params(i)%orientation

        ! we add half of cell to the radius so we get nicer shape
        if (cylinder_params(i)%orientation.eq.3) then
            ! cylinder along z axis
            j = j+1
            do ix=1,Nx_total
                do iy=1,Ny_total
                    do iz=1,Nz_total
                        r = sqrt((real(ix) - cylinder_params(i)%x)**2 + (real(iy) - cylinder_params(i)%y)**2)
                        if (r.le.(cylinder_params(i)%radius + dx/2.0).and.iz.ge.cylinder_params(i)%z.and.iz.le.(cylinder_params(i)%z &
                                + cylinder_params(i)%length).and.(ix.ge.cylinder_params(i)%xlow).and.(iy.ge.cylinder_params(i)%ylow).and.(iz.ge.cylinder_params(i)%zlow).and.(ix.le.cylinder_params(i)%xhigh).and.(iy.le.cylinder_params(i)%yhigh).and.(iz.le.cylinder_params(i)%zhigh)) then
                            if (cylinder_params(i)%negative.eq.0) then
                                objects_total(ix,iy,iz) = j
                                equipot(ix,iy,iz) = cylinder_params(i)%Pot
                                poiss_flag(ix,iy,iz) = j
                                Ts(ix,iy,iz) = cylinder_params(i)%param2

                                if (cylinder_params(i)%param1.ne.1)  then
                                    poiss_flag(ix,iy,iz) = 1
                                else
                                    equipot(ix,iy,iz) = 0.0
                                    poiss_flag(ix,iy,iz) = 0

                                end if

                            else
                                ! negative object
                                equipot(ix,iy,iz) = 0.0
                                objects_total(ix,iy,iz) = 0
                                poiss_flag(ix,iy,iz) = 0
                                Ts(ix,iy,iz) = 0.0

                            end if
                        end if
                    end do
                end do
            end do
        elseif (cylinder_params(i)%orientation.eq.2) then
            ! cylinder along y axis
            j = j+1
            do ix=1,Nx_total
                do iy=1,Ny_total
                    do iz=1,Nz_total
                        r = sqrt((real(ix) - cylinder_params(i)%x)**2 + (real(iz) - cylinder_params(i)%z)**2)
                        if (r.le.(cylinder_params(i)%radius + dx/2.0).and.iy.ge.cylinder_params(i)%y.and.iy.le.(cylinder_params(i)%y &
                                + cylinder_params(i)%length).and.(ix.ge.cylinder_params(i)%xlow).and.(iy.ge.cylinder_params(i)%ylow).and.(iz.ge.cylinder_params(i)%zlow).and.(ix.le.cylinder_params(i)%xhigh).and.(iy.le.cylinder_params(i)%yhigh).and.(iz.le.cylinder_params(i)%zhigh)) then
                            if (cylinder_params(i)%negative.eq.0) then
                                objects_total(ix,iy,iz) = j
                                equipot(ix,iy,iz) = cylinder_params(i)%Pot
                                poiss_flag(ix,iy,iz) = j
                                Ts(ix,iy,iz) = cylinder_params(i)%param2
                                if (cylinder_params(i)%param1.ne.1)  then
                                    poiss_flag(ix,iy,iz) = 1
                                else
                                    equipot(ix,iy,iz) = 0.0
                                    poiss_flag(ix,iy,iz) = 0

                                end if

                            else
                                ! negative object
                                equipot(ix,iy,iz) = 0.0
                                objects_total(ix,iy,iz) = 0
                                poiss_flag(ix,iy,iz) = 0
                                Ts(ix,iy,iz) = 0.0
                            end if
                        end if
                    end do
                end do
            end do



        else
            ! cylinder along x axis
            j = j+1
            do ix=1,Nx_total
                do iy=1,Ny_total
                    do iz=1,Nz_total
                        r = sqrt((real(iz) - cylinder_params(i)%z)**2 + (real(iy) - cylinder_params(i)%y)**2)
                        if (r.le.(cylinder_params(i)%radius + dx/2.0).and.ix.ge.cylinder_params(i)%x.and.ix.le.(cylinder_params(i)%x &
                                + cylinder_params(i)%length).and.(ix.ge.cylinder_params(i)%xlow).and.(iy.ge.cylinder_params(i)%ylow).and.(iz.ge.cylinder_params(i)%zlow).and.(ix.le.cylinder_params(i)%xhigh).and.(iy.le.cylinder_params(i)%yhigh).and.(iz.le.cylinder_params(i)%zhigh)) then
                            if (cylinder_params(i)%negative.eq.0) then
                                objects_total(ix,iy,iz) = j
                                equipot(ix,iy,iz) = cylinder_params(i)%Pot
                                poiss_flag(ix,iy,iz) = j
                                Ts(ix,iy,iz) = cylinder_params(i)%param2
                                if (cylinder_params(i)%param1.ne.1)  then
                                    poiss_flag(ix,iy,iz) = 1
                                else
                                    equipot(ix,iy,iz) = 0.0
                                    poiss_flag(ix,iy,iz) = 0

                                end if

                            else
                                ! negative object
                                equipot(ix,iy,iz) = 0.0
                                objects_total(ix,iy,iz) = 0
                                poiss_flag(ix,iy,iz) = 0
                                Ts(ix,iy,iz) = 0.0

                            end if
                        end if
                    end do
                end do
            end do

        end if

    end do
    ! cones
    do i=1,cone

        write (*,*) 'Building cone ',cone_params(i)%name
        write(*,*) 'Base:', cone_params(i)%x,cone_params(i)%y,cone_params(i)%z
        write(*,*) 'Length:', cone_params(i)%length
        write(*,*) 'Radius:', cone_params(i)%radius
        write(*,*) 'Orientation', cone_params(i)%orientation

        ! we add half of cell to the radius so we get nicer shape
        if (cone_params(i)%param1.eq.1) then
            have_dielectrics = .true.
            write(*,*) 'This Cone is dielectric ',i
        end if

        if (cone_params(i)%param1.eq.2) then

            count_floating_objects = count_floating_objects +1
        end if
        if (cone_params(i)%param1.eq.3) then

            count_iv_objects = count_iv_objects +1
        end if
        if (cone_params(i)%orientation.eq.3) then
            j = j+1
            if (cone_params(i)%length.gt.0) then
                do ix=1,Nx_total
                    do iy=1,Ny_total
                        do iz=max(1,int(cone_params(i)%z)),min(Nz_max,int(cone_params(i)%length + cone_params(i)%z))
                            r = sqrt((real(ix) - cone_params(i)%x)**2 + (real(iy) - cone_params(i)%y)**2)
                            act_r = cone_params(i)%radius + dx/2.0 - (iz - cone_params(i)%z)*(cone_params(i)%radius + dx/2.0)/cone_params(i)%length
                            if (r.le.act_r.and.(ix.ge.cone_params(i)%xlow).and.(iy.ge.cone_params(i)%ylow).and.(iz.ge.cone_params(i)%zlow).and.(ix.le.cone_params(i)%xhigh).and.(iy.le.cone_params(i)%yhigh).and.(iz.le.cone_params(i)%zhigh)) then
                                if (cone_params(i)%negative.eq.0) then
                                    objects_total(ix,iy,iz) = j
                                    equipot(ix,iy,iz) = cone_params(i)%Pot
                                    Ts(ix,iy,iz) = cone_params(i)%param2
                                    if (cone_params(i)%param1.ne.1)  then
                                        poiss_flag(ix,iy,iz) = j
                                    else
                                        equipot(ix,iy,iz) = 0.0
                                        poiss_flag(ix,iy,iz) = 0

                                    end if

                                else
                                    objects_total(ix,iy,iz) = 0
                                    equipot(ix,iy,iz) = 0.0
                                    poiss_flag(ix,iy,iz) = 0
                                    Ts(ix,iy,iz) = 0.0
                                end if
                            end if
                        end do
                    end do
                end do
            else
                do ix=1,Nx_total
                    do iy=1,Ny_total
                        do iz=max(1,int(cone_params(i)%z+ cone_params(i)%length)) , min(Nz_total,int(cone_params(i)%z))
                            r = sqrt((real(ix) - cone_params(i)%x)**2 + (real(iy) - cone_params(i)%y)**2)
                            act_r = cone_params(i)%radius + dx/2.0 - (iz - cone_params(i)%z)*(cone_params(i)%radius + dx/2.0)/cone_params(i)%length
                            if (r.le.act_r.and.(ix.ge.cone_params(i)%xlow).and.(iy.ge.cone_params(i)%ylow).and.(iz.ge.cone_params(i)%zlow).and.(ix.le.cone_params(i)%xhigh).and.(iy.le.cone_params(i)%yhigh).and.(iz.le.cone_params(i)%zhigh)) then
                                if (cone_params(i)%negative.eq.0) then
                                    objects_total(ix,iy,iz) = j
                                    equipot(ix,iy,iz) = cone_params(i)%Pot
                                    Ts(ix,iy,iz) = cone_params(i)%param2
                                else
                                    objects_total(ix,iy,iz) = 0
                                    Ts(ix,iy,iz) = 0.0
                                end if
                            end if
                        end do
                    end do
                end do


            end if

        elseif (cone_params(i)%orientation.eq.2) then
            ! cone in y direction
            j = j+1
            if (cone_params(i)%length.gt.0) then
                do ix=1,Nx_total
                    do iy=max(1,int(cone_params(i)%y)),min(Ny_total,int(cone_params(i)%length + cone_params(i)%y))
                        do iz=1,Nz_total
                            r = sqrt((real(ix) - cone_params(i)%x)**2 + (real(iz) - cone_params(i)%z)**2)
                            act_r = cone_params(i)%radius + dx/2.0 - (iy - cone_params(i)%y)*(cone_params(i)%radius + dx/2.0)/cone_params(i)%length
                            if (r.le.act_r.and.(ix.ge.cone_params(i)%xlow).and.(iy.ge.cone_params(i)%ylow).and.(iz.ge.cone_params(i)%zlow).and.(ix.le.cone_params(i)%xhigh).and.(iy.le.cone_params(i)%yhigh).and.(iz.le.cone_params(i)%zhigh)) then
                                if (cone_params(i)%negative.eq.0) then
                                    objects_total(ix,iy,iz) = j
                                    equipot(ix,iy,iz) = cone_params(i)%Pot
                                    Ts(ix,iy,iz) = cone_params(i)%param2
                                    if (cone_params(i)%param1.ne.1)  then
                                        poiss_flag(ix,iy,iz) = j
                                    else
                                        equipot(ix,iy,iz) = 0.0
                                        poiss_flag(ix,iy,iz) = 0

                                    end if

                                else
                                    objects_total(ix,iy,iz) = 0
                                    equipot(ix,iy,iz) = 0.0
                                    poiss_flag(ix,iy,iz) = 0
                                    Ts(ix,iy,iz) = 0.0

                                end if
                            end if
                        end do
                    end do
                end do
            else
                do ix=1,Nx_total
                    do iy=max(1,int(cone_params(i)%y+ cone_params(i)%length)) , min(Ny_total,int(cone_params(i)%y))
                        do iz=1,Nz_total
                            r = sqrt((real(ix) - cone_params(i)%x)**2 + (real(iz) - cone_params(i)%z)**2)
                            act_r = cone_params(i)%radius + dx/2.0 - (iy - cone_params(i)%y)*(cone_params(i)%radius + dx/2.0)/cone_params(i)%length
                            if (r.le.act_r.and.(ix.ge.cone_params(i)%xlow).and.(iy.ge.cone_params(i)%ylow).and.(iz.ge.cone_params(i)%zlow).and.(ix.le.cone_params(i)%xhigh).and.(iy.le.cone_params(i)%yhigh).and.(iz.le.cone_params(i)%zhigh)) then
                                if (cone_params(i)%negative.eq.0) then
                                    objects_total(ix,iy,iz) = j
                                    equipot(ix,iy,iz) = cone_params(i)%Pot
                                    Ts(ix,iy,iz) = cone_params(i)%param2
                                else
                                    objects_total(ix,iy,iz) = 0
                                    Ts(ix,iy,iz) = 0.0
                                end if
                            end if
                        end do
                    end do
                end do


            end if


        else
            ! cone in x direction
            j = j+1
            if (cone_params(i)%length.gt.0) then
                do ix=max(1,int(cone_params(i)%x)),min(Nx_total,int(cone_params(i)%length + cone_params(i)%x))
                    do iy=1,Ny_total
                        do iz=1,Nz_total
                            r = sqrt((real(iz) - cone_params(i)%z)**2 + (real(iy) - cone_params(i)%y)**2)
                            act_r = cone_params(i)%radius + dx/2.0 - (ix - cone_params(i)%x)*(cone_params(i)%radius + dx/2.0)/cone_params(i)%length
                            if (r.le.act_r.and.(ix.ge.cone_params(i)%xlow).and.(iy.ge.cone_params(i)%ylow).and.(iz.ge.cone_params(i)%zlow).and.(ix.le.cone_params(i)%xhigh).and.(iy.le.cone_params(i)%yhigh).and.(iz.le.cone_params(i)%zhigh)) then
                                if (cone_params(i)%negative.eq.0) then
                                    objects_total(ix,iy,iz) = j
                                    equipot(ix,iy,iz) = cone_params(i)%Pot
                                    Ts(ix,iy,iz) = cone_params(i)%param2
                                    if (cone_params(i)%param1.ne.1)  then
                                        poiss_flag(ix,iy,iz) = j
                                    else
                                        equipot(ix,iy,iz) = 0.0
                                        poiss_flag(ix,iy,iz) = 0

                                    end if

                                else
                                    objects_total(ix,iy,iz) = 0
                                    equipot(ix,iy,iz) = 0.0
                                    poiss_flag(ix,iy,iz) = 0
                                    Ts(ix,iy,iz) = 0.0

                                end if
                            end if
                        end do
                    end do
                end do
            else
                do ix=max(1,int(cone_params(i)%x+ cone_params(i)%length)) ,min(Nx_total,int( cone_params(i)%x))
                    do iy=1,Ny_total
                        do iz=1,Nz_total
                            r = sqrt((real(iz) - cone_params(i)%z)**2 + (real(iy) - cone_params(i)%y)**2)
                            act_r = cone_params(i)%radius + dx/2.0 - (ix - cone_params(i)%x)*(cone_params(i)%radius + dx/2.0)/cone_params(i)%length
                            if (r.le.act_r.and.(ix.ge.cone_params(i)%xlow).and.(iy.ge.cone_params(i)%ylow).and.(iz.ge.cone_params(i)%zlow).and.(ix.le.cone_params(i)%xhigh).and.(iy.le.cone_params(i)%yhigh).and.(iz.le.cone_params(i)%zhigh)) then
                                if (cone_params(i)%negative.eq.0) then
                                    objects_total(ix,iy,iz) = j
                                    equipot(ix,iy,iz) = cone_params(i)%Pot
                                    Ts(ix,iy,iz) = cone_params(i)%param2
                                else
                                    objects_total(ix,iy,iz) = 0
                                    Ts(ix,iy,iz) = 0.0
                                end if
                            end if
                        end do
                    end do
                end do


            end if

        end if ! orientation


    end do

    do i=1,triangle
        write (*,*) 'Building triangloid ',triangle_params(i)%name
        if (triangle_params(i)%param1.eq.1) then
            have_dielectrics = .true.
            write(*,*) 'This triangloid is dielectric ',i
        end if

        if (triangle_params(i)%param1.eq.2) then

            count_floating_objects = count_floating_objects +1
        end if
        if (triangle_params(i)%param1.eq.3) then

            count_iv_objects = count_iv_objects +1
        end if

        if (triangle_params(i)%xa.eq.triangle_params(i)%xb.and.triangle_params(i)%xa.eq.triangle_params(i)%xc) then
            triangle_params(i)%orientation = 1
        elseif ((triangle_params(i)%ya.eq.triangle_params(i)%yb.and.triangle_params(i)%ya.eq.triangle_params(i)%yc)) then
            triangle_params(i)%orientation = 2
        else
            triangle_params(i)%orientation = 3
        end if

        write(*,*) 'Triangle orientation',triangle_params(i)%orientation

        j = j+1
        do ix=1,Nx_total
            do iy=1,Ny_total
                do iz=1,Nz_total

                    result1 = 0
                    result2 = 0
                    result3 = 0

                    if (triangle_params(i)%orientation.eq.1) then
                        call check_halfplane(triangle_params(i)%ya,triangle_params(i)%za,triangle_params(i)%yb,triangle_params(i)%zb,triangle_params(i)%yc,triangle_params(i)%zc,real(iy),real(iz),result1)
                        call check_halfplane(triangle_params(i)%yc,triangle_params(i)%zc,triangle_params(i)%ya,triangle_params(i)%za,triangle_params(i)%yb,triangle_params(i)%zb,real(iy),real(iz),result2)
                        call check_halfplane(triangle_params(i)%yb,triangle_params(i)%zb,triangle_params(i)%yc,triangle_params(i)%zc,triangle_params(i)%ya,triangle_params(i)%za,real(iy),real(iz),result3)
                    elseif (triangle_params(i)%orientation.eq.2) then
                        call check_halfplane(triangle_params(i)%xa,triangle_params(i)%za,triangle_params(i)%xb,triangle_params(i)%zb,triangle_params(i)%xc,triangle_params(i)%zc,real(ix),real(iz),result1)
                        call check_halfplane(triangle_params(i)%xc,triangle_params(i)%zc,triangle_params(i)%xa,triangle_params(i)%za,triangle_params(i)%xb,triangle_params(i)%zb,real(ix),real(iz),result2)
                        call check_halfplane(triangle_params(i)%xb,triangle_params(i)%zb,triangle_params(i)%xc,triangle_params(i)%zc,triangle_params(i)%xa,triangle_params(i)%za,real(ix),real(iz),result3)
                    else
                        call check_halfplane(triangle_params(i)%xa,triangle_params(i)%ya,triangle_params(i)%xb,triangle_params(i)%yb,triangle_params(i)%xc,triangle_params(i)%yc,real(ix),real(iy),result1)
                        call check_halfplane(triangle_params(i)%xc,triangle_params(i)%yc,triangle_params(i)%xa,triangle_params(i)%ya,triangle_params(i)%xb,triangle_params(i)%yb,real(ix),real(iy),result2)
                        call check_halfplane(triangle_params(i)%xb,triangle_params(i)%yb,triangle_params(i)%xc,triangle_params(i)%yc,triangle_params(i)%xa,triangle_params(i)%ya,real(ix),real(iy),result3)
                    end if



                    if ((result1.eq.1).and.(result2.eq.1).and.(result3.eq.1).and.(ix.ge. triangle_params(i)%xlow).and.(iy.ge. triangle_params(i)%ylow).and.(iz.ge.triangle_params(i)%zlow).and.(ix.le.triangle_params(i)%xhigh).and.(iy.le.triangle_params(i)%yhigh).and.(iz.le.triangle_params(i)%zhigh)  ) then
                        if (triangle_params(i)%negative.eq.0) then
                            objects_total(ix,iy,iz) = j
                            equipot(ix,iy,iz) = triangle_params(i)%Pot
                            Ts(ix,iy,iz) = triangle_params(i)%param2

                            poiss_flag(ix,iy,iz) = 0

                            if (triangle_params(i)%param1.ne.1)  then
                                poiss_flag(ix,iy,iz) = 1
                            else
                                equipot(ix,iy,iz) = 0.0
                                poiss_flag(ix,iy,iz) = 0
                                Ts(ix,iy,iz) = 0
                            end if
                        else
                            !write(*,*) 'Clearing neg. point',ix,iy,iz
                            objects_total(ix,iy,iz) = 0
                            poiss_flag(ix,iy,iz) = 0
                            equipot(ix,iy,iz) = 0.0
                            Ts(ix,iy,iz) = 0.0

                        end if
                    end if
                end do
            end do
        end do
    end do

    objects = objects_total(model%Nx_start:model%Nx_stop,model%Ny_start:model%Ny_stop,model%Nz_start:model%Nz_stop)
    ! edge detection
    write(*,*) 'Running edge detection'
    call edge_detection(Nx,Ny,Nz,objects_total,edges,model%Nx_total,model%Ny_total,model%Nz_total,model%Nx_start,model%Ny_start,model%Nz_start)

    call prepare_n_diel_matrix(Nx,Ny,Nz,objects,edges,object_params,no_objects,n_diel_matrix)

    write(*,*) 'Constructing BC matrix'

    !call construct_bc_matrix(Nx,Ny,Nz,model%Nx_total,model%Ny_total,model%Nz_total,objects_total,bc_matrix,model%Nx_start,model%Ny_start,model%Nz_start)
    call construct_bc_matrix(model%Nx_total,model%Ny_total,model%Nz_total,objects_total,bc_matrix_total)
    bc_matrix = int(bc_matrix_total(model%Nx_start:model%Nx_stop,model%Ny_start:model%Ny_stop,model%Nz_start:model%Nz_stop))

    write(*,*) 'Looking for BC limits'
    call find_bc_check_limits(Nx,Ny,Nz,objects,dx,dy,dz,Lz_low_limit,Lz_high_limit)

    write(*,*) 'Constructing border matrix'

    call make_border_matrix(Nx,Ny,Nz,Nx_total,Ny_total,Nz_total,objects_total,border_matrix,model%Nx_start,model%Ny_start,model%Nz_start)

    ! construct the dielectric_cell matrix
    do ix=1,Nx
        do iy=1,Ny
            do iz=1,Nz
                if (edges(ix,iy,iz).gt.0) then
                    ! write(*,*)'Edge cell',ix,iy,iz,objects(ix,iy,iz),object_params(objects(ix,iy,iz))%param1

                    if (object_params(objects(ix,iy,iz))%param1.eq.1) then
                        dielectric_cell(ix,iy,iz) = .true.
                    end if
                end if
            end do
        end do
    end do

    do ix=1,Nx-1
        do iy=1,Ny-1
            do iz=1,Nz-1

                if (edges(ix+1,iy,iz).gt.0) then

                    if (object_params(objects(ix+1,iy,iz))%param1.eq.1) then
                        dielectric_cell(ix,iy,iz) = .true.
                    end if
                end if
                if (edges(ix,iy+1,iz).gt.0) then

                    if (object_params(objects(ix,iy+1,iz))%param1.eq.1) then
                        dielectric_cell(ix,iy,iz) = .true.
                    end if
                end if
                if (edges(ix+1,iy+1,iz).gt.0) then

                    if (object_params(objects(ix+1,iy+1,iz))%param1.eq.1) then
                        dielectric_cell(ix,iy,iz) = .true.
                    end if
                end if
                if (edges(ix,iy,iz+1).gt.0) then

                    if (object_params(objects(ix,iy,iz+1))%param1.eq.1) then
                        dielectric_cell(ix,iy,iz) = .true.
                    end if
                end if
                if (edges(ix+1,iy,iz+1).gt.0) then

                    if (object_params(objects(ix+1,iy,iz+1))%param1.eq.1) then
                        dielectric_cell(ix,iy,iz) = .true.
                    end if
                end if
                if (edges(ix,iy+1,iz+1).gt.0) then

                    if (object_params(objects(ix,iy+1,iz+1))%param1.eq.1) then
                        dielectric_cell(ix,iy,iz) = .true.
                    end if
                end if
                if (edges(ix+1,iy+1,iz+1).gt.0) then

                    if (object_params(objects(ix+1,iy+1,iz+1))%param1.eq.1) then
                        dielectric_cell(ix,iy,iz) = .true.
                    end if
                end if

                if ( dielectric_cell(ix,iy,iz).and.debug) then
                    !      write(*,*) 'Dielectric cell',ix,iy,iz
                end if
            end do
        end do
    end do

    ! copy over the last cells
    dielectric_cell(Nx,1:Ny,1:Nz) = dielectric_cell(Nx-1,1:Ny,1:Nz)
    dielectric_cell(1:Nx,Ny,1:Nz) = dielectric_cell(1:Nx,Ny-1,1:Nz)
    dielectric_cell(Nx,Ny,1:Nz) = dielectric_cell(1,1,1:Nz)

    ! stop
    ! dielectric_cell(Nx,:,:) = dielectric_cell(1,:,:)
    ! dielectric_cell(:,Ny,:) = dielectric_cell(:,1,:)

    ! find z_limit
    z_limit = 1
    do iz=1,Nz
        if (sum(objects(:,:,1)).gt.0) then
            z_limit = iz

        end if

    end do
    z_limit = z_limit + 3
    if (z_limit.gt.Nz-1) then
        z_limit = Nz-1
    end if

    ! POISSON SOLVER UTILITY
    ! init of Poisson solver
    if (proc_no.eq.0) then
        equipot(:,:,1) = model%PL
        equipot(:,:,Nz_total) = model%P0
        POISS_FLAG(:,:,1) = 1
        POISS_FLAG(:,:,Nz_total) = 1
    end if
    ! objects(:,:,1) = 1
    ! objects(:,:,Nz) = 1
    ! only the first processor runs the Solver to save memory

    ! POISSON SOLVER UTILITY
    if (proc_no.eq.0) then
        write(*,*) 'Initialization of Poisson solver'
        call InitSolver(Nx_total,Ny_total,Nz_total,solverpointer)

        write(*,*) 'Tweaking of Poisson solver'
        call TweakSolver(model%poiss_levels,model%poiss_smooths,model%poiss_res,solverpointer)

        write(*,*) 'Configuration of Poisson solver'
        call matrix2vector_i(Nx_total,Ny_total,Nz_total,poiss_flag,eq_mask_vector)
        call matrix2vector_r(Nx_total,Ny_total,Nz_total,equipot,bias_vector)
        ! top and bottom boundaries
        !eq_mask_vector(1:(nx_total*ny_total))  = 1
        !bias_vector(1:(nx_total*ny_total))  = PL
        !eq_mask_vector((nx_total*ny_total*nz_total - nx_total*ny_total +1):(nx_total*ny_total*nz_total))  = 1
        !bias_vector((nx_total*ny_total*nz_total - nx_total*ny_total + 1):(nx_total*ny_total*nz_total))  = P0

        if (verbose) then
            write(*,*) 'VSUM mask',sum(abs(eq_mask_vector))
            write(*,*) 'VSUM bias',sum(abs(bias_vector))

        end if


        call ConfigureSolver(solverpointer,eq_mask_vector,bias_vector,0,Nx_total,Ny_total,Nz_total)

    end if

    ! call prepare_diel_filter(Nx,Ny,Nz,model,diel_filter,debug,edges)
    ! calculate injection boxes for each specie and fill the arrays
    do sp=1,no_species
        m(sp) = spec_params(sp)%m
        Temp(sp) = spec_params(sp)%T
        q(sp) = spec_params(sp)%q
        w(sp) = spec_params(sp)%w
        injection_method(sp) = spec_params(sp)%injection_method
        mono(sp) = spec_params(sp)%mono
        Umono(sp) = spec_params(sp)%Umono
        mpi_rank(sp) = spec_params(sp)%mpi_rank
        inj_ratio(sp) = spec_params(sp)%injection_rate
        if (alpha_xz.eq.90.0.and.alpha_yz.eq.90.0) then
            ! perp injection, no boxes
            Lzi(sp) = (Nz_total-1)*dz
        else
            Lzi(sp)=(Nz_total-1)*dz+2.0*(3.0*sqrt(2.0)*ksi*sqrt(m(sp)))*sqrt(bx**2+by**2)   !because we inject particles at 5rL after Lz.


        end if
    end do
    ! calculate leapfrog coeficients
    do sp=1,no_species
        ! real coeficients
        call leapfrog_init(m(sp),dt,bx,by,bz,q(sp),ksi,Ax_lf(sp),Bx_lf(sp),Cx_lf(sp),Dx_lf(sp),Ex_lf(sp),Fx_lf(sp),Ay_lf(sp),By_lf(sp),&
            Cy_lf(sp),Dy_lf(sp),Ey_lf(sp),Fy_lf(sp),Az_lf(sp),Bz_lf(sp),Cz_lf(sp),Dz_lf(sp),Ez_lf(sp),Fz_lf(sp))
        ! injection coefficients
        if (debug) then
            write(*,*) 'X:',Ax_lf(sp),Bx_lf(sp),Cx_lf(sp)
            write(*,*) 'EX:',Dx_lf(sp),Ex_lf(sp),Fx_lf(sp)

            write(*,*) 'Y:',Ay_lf(sp),By_lf(sp),Cy_lf(sp)
            write(*,*) 'EY:',Dy_lf(sp),Ey_lf(sp),Fy_lf(sp)

            write(*,*) 'Z:',Az_lf(sp),Bz_lf(sp),Cz_lf(sp)
            write(*,*) 'EZ:',Dz_lf(sp),Ez_lf(sp),Fz_lf(sp)
            ! stop
        end if

        call leapfrog_init(m(sp),-dt/2.0,bx,by,bz,q(sp),ksi,Ax_hlf(sp),Bx_hlf(sp),Cx_hlf(sp),Dx_hlf(sp),Ex_hlf(sp),Fx_hlf(sp),&
            Ay_hlf(sp),By_hlf(sp),&
            Cy_hlf(sp),Dy_hlf(sp),Ey_hlf(sp),Fy_hlf(sp),Az_hlf(sp),Bz_hlf(sp),Cz_hlf(sp),Dz_hlf(sp),Ez_hlf(sp),Fz_hlf(sp))
        !writeout

    end do
    !  stop
    ! prepare particle injection
    do sp=1,no_species

        write(*,*) 'Injection of ', trim(spec_params(sp)%name)
        ! 	umin(sp)=-5.d0*sqrt(2*Temp(sp))*ksi/sqrt(m(sp))   !-5.d0*maxi=5.d0*sqrt(2*tau)*ksi
        utherm(sp)=sqrt(Temp(sp))*ksi/sqrt(m(sp))              !ion thermal speed
        write(6,*) 'Utherm=',utherm(sp)
        pnumber(sp)=0                    !no particle in the cells at t=0
        write(6,*) 'N (initial)=',pnumber(sp)
        !====================
        !in the z-direction =
        !====================
        if (mono(sp)) then          !NEW RD
            GAMMAz0(sp)=-Umono(sp)       ! bcz no=1
        else
            open(10,file=trim(spec_params(sp)%injection_file)//char(0),FORM ="formatted")
            read(10,*) N_u_zreal(sp)
            write(6,*) ' N_u_zreal ',N_u_zreal(sp)
            N_u_z(sp)=int(N_u_zreal(sp))
            write(6,*) ' N_u_z ',N_u_z(sp)
            read(10,*) u_z(sp,1:N_u_z(sp))
            u_z(sp,:)=u_z(sp,:)*ksi  !Velocities come from QPIC with diff. norm. !!!
            !!!! HARD ELECTRON HACK !!!
            ! NEEDS PROPER FIX, DEBUG SOLUTION
            if (q(sp).lt.0.0) then
                u_z(sp,:)=u_z(sp,:)*sqrt(mu/200.d0)
                ! hack to restore electrons lost to the wall
                !   inj_ratio(sp) = inj_ratio(sp)*1.05
            end if
            read(10,*) fu_z(sp,1:N_u_z(sp))
            close(10)
            !debug - sflux
            if (debug) then
                write(*,*) 'fu_z',sp,fu_z(sp,1:N_u_z(sp))
                write(*,*) 'u_z',sp,u_z(sp,1:N_u_z(sp))
            end if
            call SFLUX(u_z(sp,1:N_u_z(sp)),N_u_z(sp),fu_z(sp,1:N_u_z(sp)),X_z(sp,1:N_u_z(sp)),Xu_z(sp,1:N_u_z(sp)),&
                ufu_z(sp,1:N_u_z(sp)),GAMMAz0(sp))
            !debug - sflux
            if (debug) then
                !	write(*,*) 'fu_z',sp,fu_z(sp,1:N_u_z(sp))
                ! 	write(*,*) 'X_z',sp,X_z(sp,1:N_u_z(sp))
                ! 	write(*,*) 'Xu_z',sp,Xu_z(sp,1:N_u_z(sp))
                ! 	write(*,*) 'ufu_z',sp,ufu_z(sp,1:N_u_z(sp))
            end if

        endif


    end do
    do sp=1,no_species
        write(*,*) 'Injection ratio for',spec_params(sp)%name, sp
        !valeur du nb de particule a injecter en fontion du flux pour avoir n=1
        Ninj_fx(sp)=inj_ratio(sp)*abs(GAMMAz0(sp)*model%N0*dt*model%Ly_total*model%Lx_total*bz)
        write(6,*) 'Ninj_fx',Ninj_fx(sp),'   GAMMAz0',GAMMAz0(sp)
        !	write(*,*) 'Check:',inj_ratio(sp),abs(GAMMAz0(sp)),N0,dt,Ly,bz
    end do

    ! continue mode - load particles
    ! needs to be fixed for version 0.5
    if (model%continue) then
        write(*,*) 'Restoring particles - continue mode'

        call restore_particles(model%tfile,verbose,debug,x,y,z,ux,uy,uz,Nz,Ny,Npts,no_species,stype,no_tot,proc_no,count,model,pnumber,regroup_runs)
        write(*,*) no_tot,' particles restored'
        count_start  = min(count,Nc)
        save_t_file = .true.
    else if (model%resume) then
        write(*,*) 'Restoring particles - resume mode'

        call restore_particles(model%tfile,verbose,debug,x,y,z,ux,uy,uz,Nz,Ny,Npts,no_species,stype,no_tot,proc_no,count,model,pnumber,regroup_runs)
        write(*,*) no_tot,' particles restored'

        write(*,*) 'Restoring TDATA - resume mode'
        call restore_tdata(model%tfile,objects_current,objects_current_total,group_current,edge_charge,Nx,Ny,Nz,no_objects,no_species,Np,proc_no,verbose,bstep,v_act,object_pot,diag_slots,no_diag_slots,Nx_total,Ny_total,Nz_total,Potvac,float_constant,i_rel_history)
        if (frozen_potvac) then
            Pot_total = Potvac
        end if
        write(*,*) 'proc no after resume',proc_no,count
        save_t_file = .true.
        count_start  = count
        ! restore potentials
        ! POISSON SOLVER UTILITY
        if (proc_no.eq.0) then

            ! Reconstructing the potential matrix. Actual call to the solver utility is below.
            do sp=1,no_objects
                object_params(sp)%Pot = object_pot(sp)
            end do
            ! need to run the whole matrix
            write(*,*) 'Loaded object potentials',object_pot
            do ix=1,Nx_total
                do iy=1,Ny_total
                    do iz=1,Nz_total
                        if (objects_total(ix,iy,iz).gt.0) then
                            if(object_params(objects_total(ix,iy,iz))%param1.eq.2.or.object_params(objects_total(ix,iy,iz))%param1.eq.3) then
                                equipot(ix,iy,iz) = object_params(objects_total(ix,iy,iz))%Pot
                            end if
                        end if

                    end do
                end do
            end do
            !rerun the Poisson configure
            ! POISSON SOLVER UTILITY
            write(*,*) 'Reconfiguring Poisson solver after restart'
            call matrix2vector_r(Nx_total,Ny_total,Nz_total,equipot,bias_vector)
            call ConfigureSolver(solverpointer,eq_mask_vector,bias_vector,1,Nx_total,Ny_total,Nz_total)

        end if ! proc no


    else
        count_start = 1
    end if

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!             MAINLOOP
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    write(*,*) 'Starting time loop',count_start,Np
    do count=count_start,Np
        if (debug) then
            write(*,*) '%%%%%%%%%%%%%%%%%%%%%%%%',count,'%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
        end if

        call gettime(timestamp)
        call getseconds(t_act)
        ! write(*,*) 'Comparing time',timestamp - t_intro,t_max,(t_max - (timestamp - t_intro))/1E6
        if ((real(t_act - t_intro).gt.t_max.and.t_max.gt.0 ) .or. (step_max.gt.0 .and. count .gt. step_max)) then
            ! halt the run
            write(*,*) proc_no,'Maximum run time reached, exiting',t_act - t_intro,t_max,t_act,t_intro
            call sleep(60)
            write(*,*) proc_no,'Dong!'
            call exit(1)
        end if

        clock_start = timestamp/1.0E6
        ! grid particles
        ! make sure we don't pass anything unitialized to the subroutine
        call gettime(timestamp)

        c_grid = timestamp/1.0E6 - clock_start
        if (c_grid.lt.0) then
            c_grid = 0.0
        end if
        c_sum = c_grid
        if (debug) then
            write (*,*) 'After grid'
            !call system("ps axu | grep spice >>mem.log")

        end if
        take_weight_diag = .false.
        if (count==dump_period*int(real(count)/real(dump_period))+1.or.take_diag) then
            take_weight_diag = .true.
        end if
        ! produce charge density
        call weight_particles(x(1:no_tot),y(1:no_tot),z(1:no_tot),ux(1:no_tot),uy(1:no_tot),uz(1:no_tot),stype(1:no_tot),rho,no_tot,Nx,Ny,Nz,&
            no_species,q,objects,model,border_matrix,dx,dy,dz,vx,vy,vz,vx2,vy2,vz2,vx3,vy3,vz3,take_weight_diag,proc_no,proc_max,slices,use_moments)

        call gettime(timestamp)
        c_wt = timestamp/1.E6 - (clock_start + c_sum)
        if (c_wt.lt.0) then
            c_wt = 0.0
        end if

        c_sum = c_sum + c_wt
        if (debug) then
            write (*,*) 'After weight'
        end if


        ! particle sorting
        ! needs to be fixed for version 0.5
        if (sort_period.ne.0) then
            if (count==sort_period*int(real(count)/real(sort_period))+1) then
                no_swaps = 0
                sort_steps = 0
                ! first we build up the sort id - this is rather simple
                !	call  build_sort_id(ix1(1:no_tot),iy1(1:no_tot),iz1(1:no_tot),sort_id(1:no_tot),no_tot,Nx,Ny,Nz)
                ! build sort matrix
                max_cell_count = 0
                tot_cell_count = 0

                call build_count_matrix(Nx,Ny,Nz_max,no_tot,max_cell_count,tot_cell_count,cell_count,model,x(1:no_tot),y(1:no_tot),z(1:no_tot))
                ! sort

                call sort_particles(no_tot,x(1:no_tot),y(1:no_tot),z(1:no_tot),ux(1:no_tot),uy(1:no_tot),uz(1:no_tot),stype(1:no_tot),max_cell_count,Nx,Ny,Nz_max,model)

                call gettime(timestamp)
                c_sort = timestamp/1.E6 - (clock_start + c_sum)
                if (c_sort.lt.0) then
                    c_sort = 0.0
                end if

                c_sum = c_sum + c_sort

                !write(*,*) 'Sorting took',real(c_sort)
                ! write(*,*) 'Number of swaps',no_swaps
                ! write(*,*) 'Unsorted fraction',real(no_swaps)/real(no_tot)
                ! dielectric sorting
                ! if (have_dielectrics) then
                !   write(*,*) 'Smoothing dielectric charge'
                !   call filter_edge_charge(Nx,Ny,Nz,diel_filter,edge_charge,model)
                ! end if


            end if
        end if
        ! run poisson
        ! testing routine
        ! do ix=1,Nx
        ! do iy=1,Ny
        ! do iz=1,Nz
        !       if (objects(ix,iy,iz).eq.0) then
        ! 	      Pot(ix,iy,iz) = iz/Nz*model%P0 + (Nz - iz)/Nz*model%PL
        !
        !       else
        ! 	      Pot(ix,iy,iz) = equipot(ix,iy,iz)
        !
        !       end if
        ! end do
        ! end do
        ! end do
        ! Poisson solver -- transferring data to processor 0
        ! MPI communication
        if (debug) then
            write(*,*) 'Copying edgechage of size',Nx-1
            write(*,*) 'X',Nx_start,Nx_start + Nx-1
            write(*,*) 'Y',Ny_start,Ny_start + Ny-1
            write(*,*) 'Z',Nz_start,Nz_start + Nz-1
        end if
        if (have_dielectrics) then
            edge_charge_tot =  edge_charge
            ! fix borders
            !  edge_charge_tot(:,1,:) = edge_charge_tot(:,1,:)*2
            !	edge_charge_tot(:,Ny,:) = edge_charge_tot(:,Ny,:)*2
            !	edge_charge_tot(1,:,:) = edge_charge_tot(1,:,:)*2
            !edge_charge_tot(Nx,:,:) = edge_charge_tot(Nx,:,:)*2

            edge_charge_tot_total = 0.0
            edge_charge_tot_total(Nx_start:(Nx_start + Nx-1),Ny_start:(Ny_start + Ny-1),Nz_start:(Nz_start + Nz-1)) = edge_charge_tot
        end if
        rho_tot_total = 0.0
        rho_tot = 0.0
        if (debug) then
            write(*,*) 'BOX limits',slices
        end if
        do sp=1,no_species
            if (mpi_rank(sp).eq.proc_no) then

                if (proc_no.eq.0) then
                    rho_tot_total(slices(1)%Nx_start:slices(1)%Nx_stop,slices(1)%Ny_start:slices(1)%Ny_stop,slices(1)%Nz_start:slices(1)%Nz_stop)&
                        = rho_tot_total(slices(1)%Nx_start:slices(1)%Nx_stop,slices(1)%Ny_start:slices(1)%Ny_stop,slices(1)%Nz_start:&
                        slices(1)%Nz_stop) - rho(sp,:,:,:)/real(model%Npc)
                end if
                rho_tot = rho_tot - rho(sp,:,:,:)/real(model%Npc)
                !  	write(*,*) proc_no,'Adding rho',sum(rho(sp,:,:,:)/real(model%N0)),sum(rho_tot)
            end if
        end do
        if (debug) then
            write(*,*) 'Preparing MPI COMM',proc_no
            !call system("ps axu | grep spice >>mem.log")

        end if
        if (count.eq.1) then
            i = 1
        else
            i = count -1
        end if

        if (proc_max.gt.0) then
            if (proc_no.eq.0) then
                objects_current_total(:,:,i) = objects_current(:,:,i)
                call receive_rho(rho_tot_total,no_species,Nx,Ny,Nz,proc_no,mpi_rank,edge_charge_tot_total,proc_max,have_dielectrics,model,Nx_total,&
                    Ny_total,Nz_total,slices,objects_current_total(:,:,i),no_objects)

            else
                call send_rho(rho_tot,no_species,Nx,Ny,Nz,proc_no,mpi_rank,edge_charge_tot,have_dielectrics,objects_current(:,:,i),no_objects)
            end if
            if (debug) then
                write(*,*) 'MPI comm done',proc_no
                !call system("ps axu | grep spice >>mem.log")

            end if
        else
            objects_current_total(:,:,i) = objects_current(:,:,i)
        end if
        call gettime(timestamp)
        c_mpi1 =  timestamp/1.E6 - (clock_start + c_sum)
        if (c_mpi1.lt.0) then
            c_mpi1 = 0.0
        end if

        c_sum = c_sum + c_mpi1


        ! do ix=1,Nx
        !     do iy=1,Ny
        !       do iz=1,Nz
        !
        ! 	  if (objects(ix,iy,iz).eq.0.or.edges(ix,iy,iz).ne.0) then
        ! ! 	      rho_tot(ix,iy,iz) = -real((iz - 65)*(iz-65))/65.0/real(model%N0)
        !                  rho_tot(ix,iy,iz) = 10.0/real(model%N0)
        !           else
        ! 	      rho_tot(ix,iy,iz) = 0.0
        !
        !          end if
        !     end do
        ! end do
        ! end do
        ! first processor runs the solver
        if (have_dielectrics) then
            rho_tot_total= rho_tot_total - edge_charge_tot_total/real(model%Npc)

        end if
        !     write(*,*) proc_no, 'edge charge',sum(abs(edge_charge_tot_total))

        if (debug) then
            write(*,*) 'charge matrix done',proc_no
        end if

        ! POISSON SOLVER UTILITY
        if (proc_no.eq.0) then
            call matrix2vector_r(Nx_total,Ny_total,Nz_total,rho_tot_total*model%dx*model%dy,rho_vector)
            if (debug) then
                write(*,*) 'VSUM rho',sum(abs(rho_vector))
            end if


            !if (ions_are_in_the_box) then
            !else
            !if (sum(rho(1,:,:,:)).gt.1000) then
            !ions_are_in_the_box = .true.
            !write(*,*) 'Ions have arrived into the box, starting poisson solver'
            !end if
            !end if



            if (.not.zero_E_field) then
                ! POISSON SOLVER UTILITY
                ! this is the actual call, other parts are the communication etc.
                call solvewithsolver(solverpointer,Pot_vector,rho_vector,count)
            end if
            if (debug) then
                write(*,*) 'VSUM rho',sum(abs(rho_vector))
                write(*,*) 'VSUM Pot',sum(abs(Pot_vector))

            end if

            call vector2matrix_r(Nx_total,Ny_total,Nz_total,Pot_total,Pot_vector)

            ! dump the potential in two cross-sections
            if (.false.) then
                write(c_str(1:7),'(I7.2)') count
                open(UNIT=12,FILE='data/dump1/p1_'//c_str//'.dat',ACTION='write')
                k = 135
                do i=1,Nx_total
                    do j=1,Nz_total
                        write(12,*) i,k,j,Pot_total(i,k,j)
                    end do
                end do
                close(12)

                open(UNIT=12,FILE='data/dump2/p2_'//c_str//'.dat',ACTION='write')
                k = 125
                do i=1,Nx_total
                    do j=1,Ny_total
                        write(12,*) i,j,k,Pot_total(i,j,k)
                    end do
                end do
                close(12)
            end if

            Pot = Pot_total(slices(proc_no+1)%Nx_start:slices(proc_no+1)%Nx_stop,slices(proc_no+1)%Ny_start:slices(proc_no+1)%Ny_stop,&
                slices(proc_no+1)%Nz_start:slices(proc_no+1)%Nz_stop)

            if (count.eq.1) then

                Potvac = Pot_total
            else
                if (frozen_potvac) then
                    Pot_total = Potvac
                    Pot = Pot_total(slices(proc_no+1)%Nx_start:slices(proc_no+1)%Nx_stop,slices(proc_no+1)%Ny_start:slices(proc_no+1)%Ny_stop,&
                        slices(proc_no+1)%Nz_start:slices(proc_no+1)%Nz_stop)
                end if
            end if
            if (debug) then
                write(*,*) 'VSUM Potm',sum(abs(Pot_total))
            end if
        end if
        !now send the Pot out
        call gettime(timestamp)
        c_poiss = timestamp/1.E6 - (clock_start + c_sum)
        if (c_poiss.lt.0) then
            c_poiss = 0.0
        end if

        c_sum = c_sum + c_poiss

        if (proc_max.gt.0) then
            if (proc_no.eq.0) then
                call  send_pot(Pot_total,Nx_total,Ny_total,Nz_total,proc_max)
            else
                call receive_pot(Pot_total,Nx_total,Ny_total,Nz_total,proc_no)
            end if
        end if

        if (debug) then
            write(*,*) 'Potential slicing',Nx,Ny,Nz
            write(*,*) 'XR',slices(proc_no+1)%Nx_start,slices(proc_no+1)%Nx_stop
            write(*,*) 'YR',slices(proc_no+1)%Ny_start,slices(proc_no+1)%Ny_stop
            write(*,*) 'ZR',slices(proc_no+1)%Nz_start,slices(proc_no+1)%Nz_stop

        end if

        Pot = Pot_total(slices(proc_no+1)%Nx_start:slices(proc_no+1)%Nx_stop,slices(proc_no+1)%Ny_start:slices(proc_no+1)%Ny_stop,&
            slices(proc_no+1)%Nz_start:slices(proc_no+1)%Nz_stop)
        if (debug) then
            write(*,*) 'Potential sliced',sum(Pot)
        end if

        call gettime(timestamp)
        c_mpi2 = timestamp/1.E6 - (clock_start + c_sum)
        if (c_mpi2.lt.0) then
            c_mpi2 = 0.0
        end if

        c_sum = c_sum + c_mpi2

        !do ix=1,Nx
        !  do iy=1,Ny
        !   do iz =1,Nz
        !          Potvac(ix,iy,iz) = -3.0/(1.0 - real(Nz))/(1.0 - real(Nz))*(real(iz) - real(Nz))*(real(iz) - real(Nz))
        !   end do
        !  end do
        !end do
        ! Pot = Potvac





        !  Pot = Potvac
        ! calculate E field
        if (zero_E_field) then
            Ex =0.0
            Ey=0.0
            Ez=0.0

        else
            call calc_E_field_general (Nx,Ny,Nz,Ex,Ey,Ez,Pot_total,dx,dy,dz,edges,objects_total,Nx_total,Ny_total,Nz_total,Nx_start,Ny_start,Nz_start)

            !call calc_E_field_general_unstable (Nx,Nz,Ny,Ex,Ey,Ez,Pot,dx,dy,dz,edges,objects,z_limit)
            ! hack - reduce Ex to supress turbulence
        end if
        if (debug) then
            write (*,*) 'After E field'
        end if
        ! debug - particle no 1
        !j = 1
        !do i=1,no_tot
        !      if (j .eq. 1 .and. q(stype(i)) .lt. 0) then
        !      write(*,*) 'P1H',x(i),y(i),z(i)
        !      write(*,*) 'P1V',ux(i),uy(i),uz(i)
        !      j = 2
        !      end if
        !
        !
        !end do


        ! move particles
        !call proto_move(x(1:no_tot),y(1:no_tot),z(1:no_tot),ux(1:no_tot),uy(1:no_tot),uz(1:no_tot),stype(1:no_tot),no_tot,dt)


        call leapfrog(no_tot,x(1:no_tot),y(1:no_tot),z(1:no_tot),ux(1:no_tot),uy(1:no_tot),uz(1:no_tot),stype(1:no_tot),Ex,Ey,Ez,Ax_lf,Bx_lf,&
            Cx_lf,Dx_lf,Ex_lf,Fx_lf,&
            Ay_lf,By_lf,Cy_lf,Dy_lf,Ey_lf,Fy_lf,Az_lf,Bz_lf,Cz_lf,Dz_lf,Ez_lf,Fz_lf,no_species,Nx,Ny,Nz,dt,model,&
            q,spec_params,m,dx,dy,dz,x_old(1:no_tot),y_old(1:no_tot),z_old(1:no_tot),use_old)
        call gettime(timestamp)
        c_lp =timestamp/1.E6 - (clock_start + c_sum)
        if (c_lp.lt.0) then
            c_lp = 0.0
        end if

        c_sum = c_sum + c_lp

        if (debug) then
            write (*,*) 'After move'
        end if
        ! inject new particles
        ! no_tot = no_tot + no_inject
        ! pnumber(1) = pnumber(1) + no_inject
        ! if (debug) then
        !   write (*,*) 'Injecting:',no_inject
        ! end if

        !call proto_inject(x(1:no_tot),y(1:no_tot),z(1:no_tot),ux(1:no_tot),uy(1:no_tot),uz(1:no_tot),stype(1:no_tot),no_tot,Lx,Ly,Lz,no_inject,ksi)
        do sp=1,no_species
            if (proc_no.eq.mpi_rank(sp)) then
                if (count.eq.1) then
                    Ninj(sp)=int(Ninj_fx(sp))
                    Ninj_r(sp)=Ninj_fx(sp)-int(Ninj_fx(sp))
                else
                    Ninj(sp)=int((Ninj_fx(sp))+Ninj_r(sp))
                    Ninj_r(sp)=(Ninj_fx(sp))+Ninj_r(sp)-Ninj(sp)
                endif
                if (injection_method(sp).ne.5) then
                    npt = no_tot + Ninj(sp)
                end if
                !       ! zero out coefficients so we avoid memory leaks
                ! 	ix1((no_tot+1):npt) = 1
                ! 	iy1((no_tot+1):npt) = 1
                ! 	iz1((no_tot+1):npt) = 1

                !	pnumber(sp) = pnumber(sp) + Ninj(sp)
                if (debug) then
                    write(*,*) 'Species',sp, 'injection',Ninj(sp),no_tot,npt
                end if
                if (injection_method(sp).eq.0) then
                    call inject_top(Ninj(sp),no_tot,npt,x(1:npt),z(1:npt),y(1:npt),ux(1:npt),uy(1:npt),uz(1:npt),mono(sp),utherm(sp),N_u_z(sp),u_z(sp,1:N_u_z(sp)),ufu_z(sp,1:N_u_z(sp)),&
                        Xu_z(sp,1:N_u_z(sp)),Ax_hlf(sp),Bx_hlf(sp),Cx_hlf(sp),Ay_hlf(sp),By_hlf(sp),Cy_hlf(sp),Az_hlf(sp),Bz_hlf(sp),Cz_hlf(sp),Lzi(sp),stype(1:npt),sp,m(sp),q(sp),Umono(sp),Temp(sp),&
                        spec_params,no_species,model,slices,proc_max+1,pnumber,Lz_limit)
                else if (injection_method(sp).eq.1) then
                    call inject_bottom(Ninj(sp),no_tot,npt,x(1:npt),z(1:npt),y(1:npt),ux(1:npt),uy(1:npt),uz(1:npt),mono(sp),utherm(sp),N_u_z(sp),u_z(sp,1:N_u_z(sp)),ufu_z(sp,1:N_u_z(sp)),&
                        Xu_z(sp,1:N_u_z(sp)),Ax_hlf(sp),Bx_hlf(sp),Cx_hlf(sp),Ay_hlf(sp),By_hlf(sp),Cy_hlf(sp),Az_hlf(sp),Bz_hlf(sp),Cz_hlf(sp),Lzi(sp),stype(1:npt),sp,m(sp),q(sp),Umono(sp),Temp(sp),&
                        spec_params,no_species,model,slices,proc_max+1,pnumber,Lz_limit)
                else if (injection_method(sp).eq.5) then
                    ! thermionic emission
                    call inject_therm(Ninj(sp),no_tot,npt,x,z,y,ux,uy,uz,mono(sp),utherm(sp),N_u_z(sp),u_z(sp,1:N_u_z(sp)),ufu_z(sp,1:N_u_z(sp)),&
                        Xu_z(sp,1:N_u_z(sp)),Ax_hlf(sp),Bx_hlf(sp),Cx_hlf(sp),Ay_hlf(sp),By_hlf(sp),Cy_hlf(sp),Az_hlf(sp),Bz_hlf(sp),Cz_hlf(sp),Lzi(sp),stype,sp,m(sp),q(sp),Umono(sp),Temp(sp),&
                        spec_params,no_species,model,slices,proc_max+1,pnumber,Lz_limit,Nx,Ny,Nz,Ts(model%Nx_start:model%Nx_stop,model%Ny_start:model%Ny_stop,model%Nz_start:model%Nz_stop),edges,init_disp,&
                        inj_ratio,edge_flux(sp,:,:,:),edge_energy_flux(sp,:,:,:),objects_current(:,:,count),no_objects,objects,npts,proc_no,inj_rem,therm_Wf,count)
                    ! increase the number of particles
                    npt = no_tot

                end if
            end if
        end do
        if (debug) then
            write (*,*) 'After inject'
        end if
        call gettime(timestamp)
        c_inj = timestamp/1.E6 - (clock_start + c_sum)
        if (c_inj.lt.0) then
            c_inj = 0.0
        end if

        c_sum = c_sum + c_inj
        ! in case the slice occupies whole x or y length, we have to fix the periodic BC here
        if (slices(proc_no+1)%Lx_start.eq.0.0.and.slices(proc_no+1)%Lx_stop.eq.model%Lx_total) then
            ! write(*,*) 'Periodic BC in x direction'
            do i=1,no_tot
                if (x(i).lt.0.0) then
                    x(i) = x(i) + model%Lx_total
                end if
                if (x(i).gt.model%Lx_total) then
                    x(i) = x(i) - model%Lx_total
                end if
            end  do
        end if

        if (slices(proc_no+1)%Ly_start.eq.0.0.and.slices(proc_no+1)%Ly_stop.eq.model%Ly_total) then
            ! write(*,*) 'Periodic BC in x direction'
            do i=1,no_tot
                if (y(i).lt.0.0) then
                    y(i) = y(i) + model%Ly_total
                end if
                if (y(i).gt.model%Ly_total) then
                    y(i) = y(i) - model%Ly_total
                end if
            end  do
        end if


        !call particle_transfer_v1(model,no_tot,x(1:no_tot),y(1:no_tot),z(1:no_tot),ux(1:no_tot),uy(1:no_tot),uz(1:no_tot),stype(1:no_tot),no_species,pnumber,x_old(1:no_tot),y_old(1:no_tot),z_old(1:no_tot),use_old)

        call particle_transfer_v2(model,no_tot,x,y,z,ux,uy,uz,stype,no_species,pnumber,x_old,y_old,z_old,Npts,proc_no,proc_max,debug,slices,use_old)

        ! model for temporal decay of edge charge
        if (have_dielectrics) then
            edge_charge = edge_charge*edge_charge_decay
        end if


        ! collision detections
        call collision_detection(Nx,Ny,Nz,objects,no_tot,x(1:no_tot),y(1:no_tot),z(1:no_tot),ux(1:no_tot),uy(1:no_tot),uz(1:no_tot),stype(1:no_tot),Lzi,no_species,pnumber,model,objects_current(:,:,count),block + sphere + cylinder + cone + triangle,q,m,int_flux_x,int_flux_y,int_flux_z,count,Nc,edge_charge_single,object_params,edges,no_objects,dielectric_cell,bc_matrix_total,Lz_low_limit,Lz_high_limit,spec_params,edge_flux,edge_energy_flux,x_old(1:no_tot),y_old(1:no_tot),z_old(1:no_tot),model%Nx_total,model%Ny_total,model%Nz_total,objects_total,model%npts,slices,model%proc_max,use_old,c_grid,utherm,therm_Wf,therm_Ei)

        if (debug) then
            write (*,*) 'After collision detection'
        end if
        ! smoothing out the edge charge
        if (have_dielectrics) then
            ! smooth the single edge
            call smooth_edge_charge_fast_x(Nx,Ny,Nz,edge_charge_single,edges,object_params,objects,no_objects,n_diel_matrix)
            ! add it to the total edge charge
            edge_charge = edge_charge + edge_charge_single
            ! disabled for now
            !  call smooth_edge_charge_fast(Nx,Ny,Nz,edge_charge,edges,object_params,objects,no_objects,n_diel_matrix)
        end if
        call gettime(timestamp)
        c_bc =timestamp/1.E6 - (clock_start + c_sum)
        if (c_bc.lt.0) then
            c_bc = 0.0
        end if

        c_sum = c_sum + c_bc

        ! diagnostics

        !loop over diagnostics
        ! only 1st processor
        !if (proc_no.eq.0) then
        diag_slot_counter = 0
        do nd=1,no_diag
            if (diag_params(nd)%start_time.le.count) then
                ! this is our diag and the right time
                if (diag_params(nd)%type.eq.1) then
                    ! loop over nodes and store it potential
                    node_counter = 0
                    do ix=1,Nx
                        if (ix.gt.(diag_params(nd)%x_low - Lx_start).and.ix.lt.(diag_params(nd)%x_high - Lx_start)) then
                            do iy=1,Ny
                                if (iy.gt.(diag_params(nd)%y_low - Ly_start).and.iy.lt.(diag_params(nd)%y_high - Ly_start)) then
                                    do iz=1,Nz
                                        if (iz.gt.(diag_params(nd)%z_low - Lz_start).and.iz.lt.(diag_params(nd)%z_high - Lz_start)) then
                                            node_counter = node_counter + 1
                                            diag_slots(diag_slot_counter +1,count) = diag_slots(diag_slot_counter +1,count) + Pot(ix,iy,iz)

                                        end if
                                    end do


                                end if
                            end do

                        end if

                    end do
                    if (node_counter.gt.0) then
                        !write(*,*) proc_no,'diag counter',node_counter,diag_slots(diag_slot_counter +1,count)
                        diag_slots(diag_slot_counter +1,count) = diag_slots(diag_slot_counter +1,count)/real(node_counter)
                        ! if (diag_slots(diag_slot_counter +1,count).gt.100) then
                        ! write(*,*) '####### Strange quench detected at count',count
                        ! write(*,*) 'Qnpot value:',diag_slots(diag_slot_counter +1,count)
                        ! Potvac = Pot
                        !
                        ! end if
                    end if
                    !     diag_slot_counter = diag_slot_counter + 1
                else if (diag_params(nd)%type.eq.2) then
                    ! E f
                    node_counter = 0
                    do ix=1,Nx
                        if (ix.gt.(diag_params(nd)%x_low - Lx_start).and.ix.lt.(diag_params(nd)%x_high - Lx_start)) then
                            do iy=1,Ny
                                if (iy.gt.(diag_params(nd)%y_low - Ly_start).and.iy.lt.(diag_params(nd)%y_high - Ly_start)) then
                                    do iz=1,Nz
                                        if (iz.gt.(diag_params(nd)%z_low - Lz_start).and.iz.lt.(diag_params(nd)%z_high - Lz_start)) then
                                            node_counter = node_counter + 1
                                            diag_slots(diag_slot_counter +1,count) = diag_slots(diag_slot_counter +1,count) + Ex(ix,iy,iz)
                                            diag_slots(diag_slot_counter +2,count) = diag_slots(diag_slot_counter +2,count) + Ey(ix,iy,iz)
                                            diag_slots(diag_slot_counter +3,count) = diag_slots(diag_slot_counter +3,count) + Ez(ix,iy,iz)

                                        end if
                                    end do


                                end if
                            end do

                        end if

                    end do
                    if (node_counter.gt.0) then
                        diag_slots(diag_slot_counter +1,count) = diag_slots(diag_slot_counter +1,count)/real(node_counter)
                        diag_slots(diag_slot_counter +2,count) = diag_slots(diag_slot_counter +2,count)/real(node_counter)
                        diag_slots(diag_slot_counter +3,count) = diag_slots(diag_slot_counter +3,count)/real(node_counter)

                    end if
                    !    diag_slot_counter = diag_slot_counter + 3
                else if (diag_params(nd)%type.eq.3) then
                    ! density
                    node_counter = 0
                    do ix=1,Nx
                        if (ix.gt.(diag_params(nd)%x_low - Lx_start).and.ix.lt.(diag_params(nd)%x_high - Lx_start)) then
                            do iy=1,Ny
                                if (iy.gt.(diag_params(nd)%y_low - Ly_start).and.iy.lt.(diag_params(nd)%y_high - Ly_start)) then
                                    do iz=1,Nz
                                        if (iz.gt.(diag_params(nd)%z_low - Lz_start).and.iz.lt.(diag_params(nd)%z_high - Lz_start)) then
                                            node_counter = node_counter + 1
                                            diag_slots(diag_slot_counter +1,count) = diag_slots(diag_slot_counter +1,count) + rho(diag_params(nd)%specie,ix,iy,iz)

                                        end if
                                    end do


                                end if
                            end do

                        end if

                    end do
                    if (node_counter.gt.0) then
                        diag_slots(diag_slot_counter +1,count) = diag_slots(diag_slot_counter +1,count)/real(node_counter)

                    end if
                    !   diag_slot_counter = diag_slot_counter + 1


                end if
            else if (diag_params(nd)%type.eq.4) then
                ! velocities
                node_counter = 0
                do ix=1,Nx
                    if (ix.gt.(diag_params(nd)%x_low - Lx_start).and.ix.lt.(diag_params(nd)%x_high - Lx_start)) then
                        do iy=1,Ny
                            if (iy.gt.(diag_params(nd)%y_low - Ly_start).and.iy.lt.(diag_params(nd)%y_high - Ly_start)) then
                                do iz=1,Nz
                                    if (iz.gt.(diag_params(nd)%z_low - Lz_start).and.iz.lt.(diag_params(nd)%z_high - Lz_start)) then
                                        node_counter = node_counter + 1
                                        diag_slots(diag_slot_counter +1,count) = diag_slots(diag_slot_counter +1,count) + vx(diag_params(nd)%specie,ix,iy,iz)
                                        diag_slots(diag_slot_counter +2,count) = diag_slots(diag_slot_counter +2,count) + vy(diag_params(nd)%specie,ix,iy,iz)
                                        diag_slots(diag_slot_counter +3,count) = diag_slots(diag_slot_counter +3,count) + vz(diag_params(nd)%specie,ix,iy,iz)

                                    end if
                                end do


                            end if
                        end do

                    end if

                end do
                if (node_counter.gt.0) then
                    diag_slots(diag_slot_counter +1,count) = diag_slots(diag_slot_counter +1,count)/real(node_counter)
                    diag_slots(diag_slot_counter +2,count) = diag_slots(diag_slot_counter +2,count)/real(node_counter)
                    diag_slots(diag_slot_counter +3,count) = diag_slots(diag_slot_counter +3,count)/real(node_counter)

                end if
                !     diag_slot_counter = diag_slot_counter + 3

            end if ! time check
            ! move on the slot
            if (diag_params(nd)%type.eq.1.or.diag_params(nd)%type.eq.3.or.diag_params(nd)%type.gt.4)  then
                diag_slot_counter = diag_slot_counter +1
            else
                diag_slot_counter = diag_slot_counter +3

            end if

        end do ! diag loop
        !end if ! proc no 0

        if (count.eq.delta_h*int(real(count)/real(delta_h))) then
            h_pos = h_pos + 1

            snumber(:,h_pos) = pnumber

        end if

        ! histories are filled, need to comb them down
        if (h_pos.eq.history_ntimes) then
            do sp=1,no_species
                call icomb_history(snumber(sp,:),history_ntimes)
            end do



            delta_h = delta_h*2
            !set the position to the middle - very unwise to use odd number of samples ;)
            h_pos = int(history_ntimes/2)

        end if
        do sp=1,no_species
            !    do k=1,Ny
            !       vwall(sp,count)=vwall(sp,count)+vz(sp,1,k)   ! a verifier
            !    enddo
            !disable
            if ((count >= Na).and.(take_diag)) then
                if (nav.gt.0) then
                    dens(sp,:,:,:)=dens(sp,:,:,:)*real(nav)/(real(nav)+1.0)+rho(sp,:,:,:)/(real(nav)+1.0)/(dx*dy*dz*real(model%N0))
                    vzav(sp,:,:,:)=vzav(sp,:,:,:)*real(nav)/(real(nav)+1.0)+vz(sp,:,:,:)/(real(nav)+1.0)
                    vyav(sp,:,:,:)=vyav(sp,:,:,:)*real(nav)/(real(nav)+1.0)+vy(sp,:,:,:)/(real(nav)+1.0)
                    vxav(sp,:,:,:)=vxav(sp,:,:,:)*real(nav)/(real(nav)+1.0)+vx(sp,:,:,:)/(real(nav)+1.0)
                    if (use_moments) then
                        vz2av(sp,:,:,:)=vz2av(sp,:,:,:)*real(nav)/(real(nav)+1.0)+vz2(sp,:,:,:)/(real(nav)+1.0)
                        vy2av(sp,:,:,:)=vy2av(sp,:,:,:)*real(nav)/(real(nav)+1.0)+vy2(sp,:,:,:)/(real(nav)+1.0)
                        vx2av(sp,:,:,:)=vx2av(sp,:,:,:)*real(nav)/(real(nav)+1.0)+vx2(sp,:,:,:)/(real(nav)+1.0)
                        vz3av(sp,:,:,:)=vz3av(sp,:,:,:)*real(nav)/(real(nav)+1.0)+vz3(sp,:,:,:)/(real(nav)+1.0)
                        vy3av(sp,:,:,:)=vy3av(sp,:,:,:)*real(nav)/(real(nav)+1.0)+vy3(sp,:,:,:)/(real(nav)+1.0)
                        vx3av(sp,:,:,:)=vx3av(sp,:,:,:)*real(nav)/(real(nav)+1.0)+vx3(sp,:,:,:)/(real(nav)+1.0)
                    end if
                else
                    dens(sp,:,:,:)=rho(sp,:,:,:)/(dx*dy*dz*real(model%N0))
                    vzav(sp,:,:,:)=vz(sp,:,:,:)
                    vyav(sp,:,:,:)=vy(sp,:,:,:)
                    vxav(sp,:,:,:)=vx(sp,:,:,:)
                    if (use_moments) then
                        vz2av(sp,:,:,:)=vz2(sp,:,:,:)
                        vy2av(sp,:,:,:)=vy2(sp,:,:,:)
                        vx2av(sp,:,:,:)=vx2(sp,:,:,:)

                        vz3av(sp,:,:,:)=vz3(sp,:,:,:)
                        vy3av(sp,:,:,:)=vy3(sp,:,:,:)
                        vx3av(sp,:,:,:)=vx3(sp,:,:,:)
                    end if
                end if
            endif
        end do
        if ((count >= Na).and.(take_diag)) then
            if (nav.gt.0) then
                !        Epar=Epar*real(nav)/(real(nav)+1.0)+Escz/(real(nav)+1.0)
                !        Eperp=Eperp*real(nav)/(real(nav)+1.0)+Escy/(real(nav)+1.0)
                Potav=Potav*real(nav)/(real(nav)+1.0)+Pot/(real(nav)+1.0)
                edge_chargeav=edge_chargeav*real(nav)/(real(nav)+1.0)+edge_charge/(real(nav)+1.0)
                Exav=Exav*real(nav)/(real(nav)+1.0)+Ex/(real(nav)+1.0)
                Eyav=Eyav*real(nav)/(real(nav)+1.0)+Ey/(real(nav)+1.0)
                Ezav=Ezav*real(nav)/(real(nav)+1.0)+Ez/(real(nav)+1.0)


            else
                !      Epar=Escz
                !       Eperp=Escy
                Potav=Pot
                edge_chargeav= edge_charge
                Exav = Ex
                Eyav = Ey
                Ezav = Ez

            end if

            nav=nav+1
        end if
        if (count.gt. (Na + diag_per*diag_count)) then
            diag_count = diag_count + 1
            take_diag = .true.
        else
            take_diag = .false.
        end if

        call gettime(timestamp)
        c_diag =  timestamp/1.E6 - (clock_start + c_sum)
        if (c_diag.lt.0) then
            c_diag = 0.0
        end if

        c_sum = c_sum + c_diag
        total_time = total_time + c_sum
        ! test particle
        tpx(count) = x(1)
        tpy(count) = y(1)
        tpz(count) = z(1)
        tpux(count) = ux(1)
        tpuy(count) = uy(1)
        tpuz(count) = uz(1)
        ! time diagnostics
        ! first slot - total time
        if (enable_iter_time) then
            iter_time(1,1,count) = real(c_sum)
            ! second slot - gridding
            iter_time(1,2,count) = real(c_grid)
            iter_time(1,3,count) = real(c_wt)
            ! forth slot - Poisson
            iter_time(1,4,count) = real(c_poiss)
            ! fifth slot - LeapFrog
            iter_time(1,5,count) = real(c_lp)
            ! sixth slot - injection
            iter_time(1,6,count) = real(c_inj)
            ! seventh slot - boundary conditions
            iter_time(1,7,count) = real(c_bc) - real(c_grid)
            ! eight slot - diagnostics
            iter_time(1,8,count) = real(c_diag)
            ! ninth slot - MPI 1
            iter_time(1,9,count) = real(c_mpi1)
            ! tenth slot - MPI 2
            iter_time(1,10,count) = real(c_sort)
        else
            iter_time(1,1,1) = real(c_sum)
            ! second slot - gridding
            iter_time(1,2,1) = real(c_grid)
            iter_time(1,3,1) = real(c_wt)
            ! forth slot - Poisson
            iter_time(1,4,1) = real(c_poiss)
            ! fifth slot - LeapFrog
            iter_time(1,5,1) = real(c_lp)
            ! sixth slot - injection
            iter_time(1,6,1) = real(c_inj)
            ! seventh slot - boundary conditions
            iter_time(1,7,1) = real(c_bc)
            ! eight slot - diagnostics
            iter_time(1,8,1) = real(c_diag)
            ! ninth slot - MPI 1
            iter_time(1,9,1) = real(c_mpi1)
            ! tenth slot - MPI 2
            iter_time(1,10,1) = real(c_sort)

        end if


        ! Bias sweeping module

        ! POISSON SOLVER UTILITY
        ! This is a part where the potential of objects change. We have to reconfigure the solver with correct equipot. This may change for a different solver implementation.
        if (proc_no.eq.0.and.count.gt.Nc) then
            if (real(count - Nc).gt.real(bstep)*real(bint).and.count_iv_objects.gt.0) then
                bstep = bstep + 1
                write(*,*) 'Bias sweeping step ',bstep
                v_act  = vstart + (vstop - vstart)/real(nbsteps)*real(bstep)
                write(*,*) 'New bias voltage ',v_act
                call adjust_iv_potentials(count,no_objects,object_params,no_species,Nc,debug,verbose,objects_total,equipot,Nx_total,Ny_total,Nz_total,v_act)

                write(*,*) 'Configuration of Poisson solver'
                call matrix2vector_i(Nx_total,Ny_total,Nz_total,poiss_flag,eq_mask_vector)
                call matrix2vector_r(Nx_total,Ny_total,Nz_total,equipot,bias_vector)
                ! top and bottom boundaries
                eq_mask_vector(1:(nx_total*ny_total))  = 1
                bias_vector(1:(nx_total*ny_total))  = PL
                eq_mask_vector((nx_total*ny_total*nz_total - nx_total*ny_total):(nx_total*ny_total*nz_total))  = 1
                bias_vector((nx_total*ny_total*nz_total - nx_total*ny_total):(nx_total*ny_total*nz_total))  = P0


                call ConfigureSolver(solverpointer,eq_mask_vector,bias_vector,1,Nx_total,Ny_total,Nz_total)
            end if ! do sweep
        end if ! proc no eq 0


        if (save_t_file) then
            write(*,*) 'Requested T file saving'
            call write_t_file(model%tfile,snumber_total,no_species,Nx,Ny,Nz,rho,Pot,objects,history_ntimes,h_pos,count,Np,tpx,tpy,tpz,tpux,tpuy,tpuz,edges,Ex,Ey,Ez,Potvac,vx,vy,vz,objects_current(:,:,1:count),objects_current_total(:,:,1:count),group_current(:,:,1:count),block + sphere + cylinder + cone,iter_time,proc_max,no_diag_slots,diag_slots(1:no_diag_slots,1:count),no_diag,diag_params,int_flux_x,int_flux_y,int_flux_z,Na,Nc,dielectric_cell,edge_charge_tot,diel_filter,bc_matrix,iter_time_no,proc_no,Nx_total,Ny_total,Nz_total,Pot_total,rho_tot_total,model,bstep,v_act,object_params,edge_charge_tot_total,float_constant,i_rel_history,Ts(model%Nx_start:model%Nx_stop,model%Ny_start:model%Ny_stop,model%Nz_start:model%Nz_stop),inj_rem)
            save_t_file = .false.
        end if

        if (count==dump_period*int(real(count)/real(dump_period))+1) then
            snumber_total =snumber
            ! ! diag transfer
            !           objects_current_total = objects_current
            !           snumber_total =snumber
            !
            ! if (proc_max.gt.0) then
            !     if (proc_no.eq.0) then
            !
            ! 	  call receive_diag (mpi_rank,count,snumber_total(:,1:count),debug,no_species,proc_max,Nx,Ny,Nz,rho,objects_current_total(:,:,1:count),nobjects,iter_time,model,iter_time_no)
            !
            !     else
            ! 	  call send_diag (mpi_rank,count,snumber(:,1:count),debug,no_species,proc_max,Nx,Ny,Nz,rho,objects_current(:,:,1:count),nobjects,iter_time,proc_no,model,iter_time_no)
            !
            !     end if
            !
            !
            ! end if

            if (proc_no.eq.0) then
                ! construct group current
                group_current = 0.0
                do sp=1,no_objects
                    group_current(object_params(sp)%group,:,1:count) = group_current(object_params(sp)%group,:,1:count) + objects_current_total(sp,:,1:count)
                end do
                ! write(*,*) 'group current 1',sum(abs(group_current(1,:,1:count)))
                ! write(*,*) 'group current 2',sum(abs(group_current(2,:,1:count)))


                ! POISSON SOLVER UTILITY
                ! Used when the floating potential mode is selected -- equipot changes according to the plasma potential next to it.
                if  (count_floating_objects.gt.0) then
                    ! floating potentials
                    call adjust_group_potentials(count,no_objects,object_params,group_current(:,:,1:count),no_species,Nc,float_constant,i_rel_history,debug,verbose,objects_total,equipot,Nx_total,Ny_total,Nz_total)

                    write(*,*) 'Configuration of Poisson solver'
                    call matrix2vector_i(Nx_total,Ny_total,Nz_total,poiss_flag,eq_mask_vector)
                    call matrix2vector_r(Nx_total,Ny_total,Nz_total,equipot,bias_vector)
                    if (verbose) then
                        write(*,*) 'VSUM mask',sum(abs(eq_mask_vector))
                        write(*,*) 'VSUM bias',sum(abs(bias_vector))
                    end if
                    ! top and bottom boundaries
                    eq_mask_vector(1:(nx_total*ny_total))  = 1
                    bias_vector(1:(nx_total*ny_total))  = PL
                    eq_mask_vector((nx_total*ny_total*nz_total - nx_total*ny_total):(nx_total*ny_total*nz_total))  = 1
                    bias_vector((nx_total*ny_total*nz_total - nx_total*ny_total):(nx_total*ny_total*nz_total))  = P0
                    ! disable for now (memory leak check)
                    ! very special hack
                    ! due to memory leak we have to initialize the solver every time
                    ! this is of course slower but better than nothing
                    !  call FreeSolver(solverpointer)
                    !
                    !  call InitSolver(Nx,Ny,Nz,solverpointer)
                    call ConfigureSolver(solverpointer,eq_mask_vector,bias_vector,1,Nx_total,Ny_total,Nz_total)
                end if

                write (*,*) 'Count:',count
                write (*,*) 'Number of ions',snumber(1,h_pos)
                if (snumber(1,h_pos).lt.-100) then
                    write(*,*) 'Strange error encoutered, lost all particles!'
                    stop
                end if
                write(*,*) 'Progress',int(real(count)/real(Np)*100.0),'% done'
                write(*,*) 'Est. duration',real(total_time)/3600.0/real(count)*real(Np),'h'

                write (*,*) '========================================='
                write(*,*)   'Grid',int(real(c_grid)/real(c_sum)*100),'%, full time',real(c_grid)
                write(*,*)   'Sort',int(real(c_sort)/real(c_sum)*100),'%, full time',real(c_sort)
                write(*,*)   'Weight',int(real(c_wt)/real(c_sum)*100),'%, full time',real(c_wt)
                write(*,*)   'Poiss',int(real(c_poiss)/real(c_sum)*100),'%, full time',real(c_poiss)
                write(*,*)   'LeapFrog',int(real(c_lp)/real(c_sum)*100),'%, full time',real(c_lp)
                write(*,*)   'MPI I',int(real(c_mpi1)/real(c_sum)*100),'%, full time',real(c_mpi1)
                write(*,*)   'MPI II',int(real(c_mpi2)/real(c_sum)*100),'%, full time',real(c_mpi2)

                write(*,*)   'Injection',int(real(c_inj)/real(c_sum)*100),'%, full time',real(c_inj)
                write(*,*)   'BC',int(real(c_bc)/real(c_sum)*100),'%, full time',real(c_bc)
                write(*,*)   'Diag',int(real(c_diag)/real(c_sum)*100),'%, full time',real(c_diag)
                write (*,*) '========================================='
                write(*,*) 'Iteration time',real(c_sum)
                write(*,*) 'total time ',real(total_time)/3600.0,'h'
                if (t_max.gt.0.0) then
                    call getseconds(t_act)
                    write(*,*) 'Time limit (h)',(t_max - real(t_act - t_intro))/3600.0,t_max/3600,t_intro,t_act
                end if
                if (verbose.and.proc_no.eq.0) then
                    write(*,*) 'Writting results to ', model%tfile
                end if

                ! MPI comm of diagnostics

                write(*,*) 'Edge charge before t file',sum(abs(edge_charge_tot_total)),sum(abs(edge_charge))
                ! SPICE MONITOR
                write(perc_str,'(F7.2)') real(count)/real(Np)*100.0
                write(due_str,'(F7.2)') real(total_time)/3600.0/real(count)*real(Np)
                write(nproc_str,'(I4.2)') proc_max+1
                ! read ENV
                call getenv("MACHINE",machine)
                call getenv("SPICE_USER",spice_user)

                ! send the stats
                call system('wget -q -O /dev/null "http://spice.rdx.cz/recv.php?perc=' //trim(perc_str) // '&due=' // trim(due_str) // '&input=' // trim(ifile) // '&machine=' // trim(machine) //'&user=' // trim(spice_user) // '&nproc='// trim(nproc_str) // '&code=spice3-0.5'//'"')

            end if ! proc no eq 0
            ! writeout temporary file
            call write_t_file(model%tfile,snumber_total,no_species,Nx,Ny,Nz,rho,Pot,objects,history_ntimes,h_pos,count,Np,tpx,tpy,tpz,tpux,tpuy,tpuz,edges,Ex,Ey,Ez,Potvac,vx,vy,vz,objects_current(:,:,1:count),objects_current_total(:,:,1:count),group_current(:,:,1:count),block + sphere + cylinder + cone + triangle,iter_time,proc_max,no_diag_slots,diag_slots(1:no_diag_slots,1:count),no_diag,diag_params,int_flux_x,int_flux_y,int_flux_z,Na,Nc,dielectric_cell,edge_charge_tot,diel_filter,bc_matrix,iter_time_no,proc_no,Nx_total,Ny_total,Nz_total,Pot_total,rho_tot_total,model,bstep,v_act,object_params,edge_charge_tot_total,float_constant,i_rel_history,Ts(model%Nx_start:model%Nx_stop,model%Ny_start:model%Ny_stop,model%Nz_start:model%Nz_stop),inj_rem)

            ! stop
            ! if (count.lt.Nc) then
            ! we only save the particles until we get to Nc - this is useful for a simple restart of the simulation
            call save_temp_proc_file(model%tfile,proc_no,no_tot,x(1:no_tot),y(1:no_tot),z(1:no_tot),ux(1:no_tot),uy(1:no_tot),uz(1:no_tot),stype(1:no_tot),count,0,model)
            ! end if

            if (count.gt.Na) then
                ! MPI comm over the averaged file
                ! in case of totally balanced run we don't transfer averaged stuff until the last moment
                if (proc_max.gt.0.and.model%automatic_particle_decomposition.eqv..false.) then
                    write(*,*) 'Averaged diag communication disabled'

                    !     if (proc_no.eq.0) then
                    !  	  call receive_final_diag(no_species,Nx,Ny,Nz,mpi_rank,dens,vxav,vyav,vzav,vx2av,vy2av,vz2av,vx3av,vy3av,vz3av,debug,edge_flux,edge_energy_flux,model,q)
                    !
                    !     else
                    !  	  call send_final_diag(no_species,mpi_rank,Nx,Ny,Nz,dens,vxav,vyav,vzav,vx2av,vy2av,vz2av,vx3av,vy3av,vz3av,debug,proc_no,edge_flux,edge_energy_flux)
                    !
                    !     end if


                end if


                write(*,*) 'Writing averaged file',nav
                call write_o_file(model%ofile,no_species,Nx,Ny,Nz,dens,Potav,vxav,vyav,vzav,Exav,Eyav,Ezav,vx2av,vy2av,vz2av,vx3av,vy3av,vz3av,edge_flux,edge_energy_flux,model,count,proc_no,use_moments,edge_chargeav)

            end if ! count .gt. Na
        end if ! dump

    end do !mainloop
    ! the last transfer for the averaged file
    !if (model%automatic_particle_decomposition.and.proc_max.gt.0) then
    !     if (proc_no.eq.0) then
    !        write(*,*) 'post loop diag fetch'
    !
    !  	  call receive_final_diag(no_species,Nx,Ny,Nz,mpi_rank,dens,vxav,vyav,vzav,vx2av,vy2av,vz2av,vx3av,vy3av,vz3av,debug,edge_flux,edge_energy_flux,model,q)
    !
    !     else
    !  	  call send_final_diag(no_species,mpi_rank,Nx,Ny,Nz,dens,vxav,vyav,vzav,vx2av,vy2av,vz2av,vx3av,vy3av,vz3av,debug,proc_no,edge_flux,edge_energy_flux)
    !
    !     end if

    !elseif (proc_max.eq.0) then

    do sp=1,no_species
        do ix=1,Nx
            do iy=1,Ny
                do iz=1,Nz
                    if (dens(sp,ix,iy,iz).ne.0.0) then
                        vxav(sp,ix,iy,iz) = vxav(sp,ix,iy,iz)/abs(dens(sp,ix,iy,iz))*abs(q(sp))/(model%dx*model%dy*model%dz*real(model%N0))
                        vyav(sp,ix,iy,iz) = vyav(sp,ix,iy,iz)/abs(dens(sp,ix,iy,iz))*abs(q(sp))/(model%dx*model%dy*model%dz*real(model%N0))
                        vzav(sp,ix,iy,iz) = vzav(sp,ix,iy,iz)/abs(dens(sp,ix,iy,iz))*abs(q(sp))/(model%dx*model%dy*model%dz*real(model%N0))
                        !second order
                        if (use_moments) then
                            vx2av(sp,ix,iy,iz) = vx2av(sp,ix,iy,iz)/abs(dens(sp,ix,iy,iz))*abs(q(sp))/(model%dx*model%dy*model%dz*real(model%N0))
                            vy2av(sp,ix,iy,iz) = vy2av(sp,ix,iy,iz)/abs(dens(sp,ix,iy,iz))*abs(q(sp))/(model%dx*model%dy*model%dz*real(model%N0))
                            vz2av(sp,ix,iy,iz) = vz2av(sp,ix,iy,iz)/abs(dens(sp,ix,iy,iz))*abs(q(sp))/(model%dx*model%dy*model%dz*real(model%N0))
                            !third order
                            vx3av(sp,ix,iy,iz) = vx3av(sp,ix,iy,iz)/abs(dens(sp,ix,iy,iz))*abs(q(sp))/(model%dx*model%dy*model%dz*real(model%N0))
                            vy3av(sp,ix,iy,iz) = vy3av(sp,ix,iy,iz)/abs(dens(sp,ix,iy,iz))*abs(q(sp))/(model%dx*model%dy*model%dz*real(model%N0))
                            vz3av(sp,ix,iy,iz) = vz3av(sp,ix,iy,iz)/abs(dens(sp,ix,iy,iz))*abs(q(sp))/(model%dx*model%dy*model%dz*real(model%N0))
                        end if
                    end if
                end do
            end do
        end do
    end do


    !end if
    !if (proc_no.eq.0) then
    write(*,*) 'Writing final averaged file',nav
    call write_o_file(model%ofile,no_species,Nx,Ny,Nz,dens,Potav,vxav,vyav,vzav,Exav,Eyav,Ezav,vx2av,vy2av,vz2av,vx3av,vy3av,vz3av,edge_flux,edge_energy_flux,model,count,proc_no,use_moments,edge_chargeav)

    !    end if ! proc no
    ! give it a final sleep so that all processes can finalize
    write(*,*) proc_no,'Sleeping seconds',floor(float(Nx)/2.0)
    call sleep(floor(float(Nx)/2.0))

    call MPI_FINALIZE(ierr)
end subroutine


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!                                       SUBROUTINES
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine display_help()
    write(*,*) '3D edge plasma simulation code'
    write(*,*) '=============================='
    write(*,*) 'usage: ./pic [-vhd] [-i INPUT_FILE] [-o OUTPUT_FILE] [-t TDIAG_FILE] [-p p_num]'
    write(*,*) '	-v verbose mode'
    write(*,*) '	-d debug mode'
    write(*,*) '	-h display this help'
    write(*,*) '	-i input file (default sheatRD_tilegap.inp)'
    write(*,*) '	-o output file (default shRDarbitr_tile.mat)'
    write(*,*) '	-t time diag file (default shRDt_tile.mat)'
    write(*,*) '	-p track ion number p_num'


end subroutine
! this subroutine discards particles that hit the electrodes or boundaries
! it also moves them over the periodic conditions
subroutine collision_detection(Nx,Ny,Nz,objects,no_tot,x,y,z,ux,uy,uz,stype,Lzi,no_species,pnumber,model,objects_current,nobjects,q,m,int_flux_x,int_flux_y,int_flux_z,c_act,Nc,edge_charge,object_params,edges,no_objects,dielectric_cell,bc_matrix,Lz_low_limit,Lz_high_limit,spec_params,edge_flux,edge_energy_flux,x_old,y_old,z_old,Nx_total,Ny_total,Nz_total,objects_total,npts,slices,proc_max,use_old,c_grid,utherm,therm_Wf,therm_Ei)
    use mth
    implicit none
    include 'struct.h'
    type(pinfo),dimension(no_species):: spec_params
    integer:: Nx,Ny,Nz,no_tot,no_species,nobjects,c_act,Nc,N_edge,no_objects,Nx_total,Ny_total,Nz_total,proc_max,ixo_shifted,iyo_shifted
    integer*8:: Npts
    real:: Lx,Ly,Lz,rx,ry,rz,A1,A2,A3,A4,A5,A6,A7,A8,V,charge_sum,Lz_low_limit,Lz_high_limit,therm_Wf,therm_Ei
    integer:: i,j,k,scenario,ixo,iyo,izo
    real, dimension(no_species):: Lzi,q,m,utherm
    integer, dimension(no_species):: pnumber
    real, dimension(no_tot):: x,y,z,ux,uy,uz,x_old,y_old,z_old
    integer, dimension(no_tot):: stype
    integer, dimension(Nx_total,Ny_total,Nz_total)::objects_total
    real, dimension(Nx_total,Ny_total,Nz_total):: bc_matrix
    real, dimension(Nx,Ny,Nz):: edge_charge
    real, dimension(Nx+1,Ny+1,Nz):: edge_charge_big
    integer, dimension(Nx,Ny,Nz):: objects,edges

    real, dimension(nobjects,no_species):: objects_current
    integer::no_tot_init,sp,ix,iy,iz,did_edge_charge
    integer*8:: s1,s2
    real, dimension(no_species,nobjects,Nx):: int_flux_x
    real, dimension(no_species,nobjects,Ny):: int_flux_y
    real, dimension(no_species,nobjects,Nz):: int_flux_z
    real, dimension(no_species,Nx,Ny,Nz):: edge_flux,edge_energy_flux
    real, dimension(no_species,Nx+1,Ny+1,Nz):: edge_flux_big,edge_energy_flux_big

    logical, dimension(Nx,Ny,Nz):: dielectric_cell
    type(model_t):: model
    type(object_t),dimension(no_objects):: object_params
    real:: tc
    logical:: hit_surface,weird_particle,use_surface_heat_tweaks
    real:: t,E_tot,flux_sum,trajectory
    real,dimension(3):: p,u,f
    integer:: loop_max,loop_count
    logical:: enable_se, enable_see,enable_sie,found_se,found_sie,use_old
    real*8:: se_yield,sie_yield,rndx,q_out
    integer:: se_spec,sie_spec
    integer:: shifted_ix,shifted_iy,shifted_iz
    real:: x_orig,y_orig,z_orig
    ! variables for particle transfer
    real, dimension(int(Npts/10)):: x_s,y_s,z_s,ux_s,uy_s,uz_s,x_r,y_r,z_r,ux_r,uy_r,uz_r
    !SE
    real, dimension(int(Npts/10)):: se_x,se_y,se_z,se_ux,se_uy,se_uz
    integer, dimension(int(Npts/10)):: se_stype
    integer, dimension(int(Npts/10)):: stype_s,stype_r
    integer:: no_s,no_r,no_r_temp,se_no_tot
    type(slice_info), dimension(proc_max+1):: slices
    real:: xold,yold,zold
    integer*8:: timestamp
    real:: c_grid,c_start,c_one,se_T,sie_T

    !   write(*,*) 'Edges 6',model%proc_no,sum(edges(:,:,6))
    !   write(*,*) 'Edges 7',model%proc_no,sum(edges(:,:,7))
    ! if (sum(edges).lt.4000) then
    ! write(*,*) 'Edges problem',sum(edges)
    ! end if
    se_no_tot = 0
    if (Npts.lt.0) then
        write(*,*) 'Error Npts less than zero',c_act,Npts
    end if
    enable_se = .true.
    enable_sie = .true.

    enable_see = .false.
    ! adds Ei to impacting ions, Wf to impacting electrons

    ! E-E secondary emission
    se_yield = 0.4
    found_se = .false.

    if (enable_se) then
        ! find the SE electron species
        do i=1,no_species
            if (spec_params(i)%injection_method.eq.3) then
                se_spec = i
                se_T = spec_params(i)%T
                found_se = .true.
                se_yield  = spec_params(i)%injection_rate
            end if

        end do
        !       write(*,*) c_act,'SE enabled',se_spec,se_yield
        if (found_se) then
        else
            se_yield = 0.0
        end if


    end if

    ! I-E secondary emission
    sie_yield = 0.4
    found_sie = .false.

    if (enable_sie) then
        ! find the SIE electron species
        do i=1,no_species
            if (spec_params(i)%injection_method.eq.4) then
                sie_spec = i
                sie_T = spec_params(i)%T
                found_sie = .true.
                sie_yield  = spec_params(i)%injection_rate
            end if

        end do
        !       write(*,*) c_act,'SE enabled',se_spec,se_yield
        if (found_sie) then
        else
            sie_yield = 0.0
        end if


    end if

    no_tot_init = no_tot
    V = model%dz**3
    !edge_charge_big = 0.0
    if (c_act.gt.model%Na) then
        do sp=1,no_species
            if (spec_params(sp)%mpi_rank.eq.model%proc_no) then
                edge_flux_big(sp,:,:,:) = 0.0
                edge_energy_flux_big(sp,:,:,:) = 0.0
            end if
        end do
    end if
    did_edge_charge = 0
    edge_charge_big = 0.0
    loop_max = 10
    no_s = 0
    c_grid = 0
    c_one = 0
    ! loop over all particles
    do i=no_tot_init,1,-1
        ! forget about gyroelectrons

        !u = (/(x(i) - x_old(i))/model%dt,(y(i) - y_old(i))/model%dt,(z(i) - z_old(i))/model%dt/)
        if (use_old) then
            xold=x_old(i)
            yold=y_old(i)
            zold=z_old(i)
        else
            xold = x(i) - ux(i)*model%dt
            yold = y(i) - uy(i)*model%dt
            zold = z(i) - uz(i)*model%dt
        end if

        u =(/ux(i),uy(i),uz(i)/)
        trajectory = sqrt((x(i) - xold)**2 + (y(i) - yold)**2 + (z(i) - zold)**2)

        ! if (trajectory.gt.10.and.xold.ne.0.0) then
        ! 	write(*,*) 'Warning, super-fast particle?',trajectory
        ! 	write(*,*) 'old pos:',x_old(i),y_old(i),z_old(i)
        ! 	write(*,*) 'new pos:',x(i),y(i),z(i)
        ! 	write(*,*) 'Velocity:',u
        ! 	write(*,*) 'Stype:',stype(i)


        ! end if
        ! first move it over the periodic boundaries
        !     if (x(i).ge.model%Lx) then
        ! if (model%debug) then
        !
        ! !    write(*,*) 'PC:',x(i),modulo(x(i), model%Lx)
        ! end if
        !  	x(i) = modulo(x(i), model%Lx)
        !
        !     end if
        !     if (y(i).ge.model%Ly) then
        ! if (model%debug) then
        !
        ! !    write(*,*) 'PC:',y(i),modulo(y(i), model%Ly)
        ! end if
        ! 	y(i) = modulo(y(i), model%Ly)
        !     end if
        !    if (x(i).lt.0.0) then
        !       ! we need to add small push over the boundary - rounding problems
        !       if (model%debug) then
        ! ! 	    write(*,*) 'PC:',x(i),modulo(x(i),model%Lx)
        !       end if
        !       x(i) =  modulo(x(i),model%Lx)
        !       if (x(i).eq.model%Lx) then
        ! 		! fix the round off error
        ! ! 	  write(*,*) 'Fixing x roundoff',x(i) - 1.0E-5
        !
        ! 		x(i) = x(i) - 1.0E-6
        !       end if
        !     end if
        !     if (y(i).lt.0.0) then
        ! 	if (model%debug) then
        ! ! 	    write(*,*) 'PC:',y(i),modulo(y(i),model%Ly)
        ! 	end if
        ! 	y(i) = modulo(y(i),model%Ly)
        ! 	if (y(i).eq.model%Ly) then
        ! ! 	  write(*,*) 'Fixing y roundoff',y(i) - 1.0E-5
        ! 	  ! fix the round off error
        ! 	  y(i) = y(i) - 1.0E-6
        !       end if
        !
        !     end if

        ! check for collisions with bottom boundary
        ! scenario 1 has fixed boundary at z = 0


        !       if (z(i).lt.0.0.and.model%scenario.eq.1) then
        ! 	  ! replace the particle by the last particle
        ! 	    call discard_particle(x,y,z,ux,uy,uz,stype,i,no_tot,no_tot_init,pnumber,no_species,x_old,y_old,z_old)
        !       else if (z(i).lt.(model%Lz - Lzi(stype(i))).and.model%scenario.eq.2) then
        ! 	  ! replace the particle by the last particle
        ! 	    call discard_particle(x,y,z,ux,uy,uz,stype,i,no_tot,no_tot_init,pnumber,no_species,x_old,y_old,z_old)
        !
        !       else if (z(i).gt.Lzi(stype(i))) then
        !     ! check for upper boundary
        !
        ! 	    call discard_particle(x,y,z,ux,uy,uz,stype,i,no_tot,no_tot_init,pnumber,no_species,x_old,y_old,z_old)
        !       else if (z(i).lt.model%Lz.and.z(i).gt.0.0) then
        ! check for electrodes
        if ((z(i) + model%Lz_start).gt.model%Lz_max) then
            ! 		write(*,*) model%proc_no,'Discarding upper boundary particle',stype(i),z(i)
            call discard_particle(x,y,z,ux,uy,uz,stype,i,no_tot,no_tot_init,pnumber,no_species,x_old,y_old,z_old,use_old)
        else if (z(i).lt.Lz_high_limit.and.z(i).gt.Lz_low_limit) then
            hit_surface = .false.
            ix = int(x(i)/model%dx) + 1
            iy = int(y(i)/model%dy) + 1
            iz = int(z(i)/model%dz) + 1
            !  if (model%debug) then
            !	write(*,*) 'BC',ix,iy,iz
            !     write(*,*) 'BC',x(i),y(i),z(i)
            !  end if
            !     if (ix.ge.model%Nx.or.iy.ge.model%Ny.or.iz.ge.model%Nz.or.ix.lt.1.or.iy.lt.1.or.iz.lt.1) then
            ! 	! error message
            ! 	write(*,*) 'Boundary error'
            ! 	write(*,*) 'specie',stype(i),i
            !
            ! 	write(*,*) 'pos',x(i),y(i),z(i)
            ! 	write(*,*) 'vel',ux(i),uy(i),uz(i)
            ! 	write(*,*) 'I',ix,iy,iz
            !
            !     end if

            ! if (spec_params(stype(i))%motion_method.eq.0) then
            !       ! regular motion
            !  !     x_old = x(i) - ux(i)*model%dt
            !   !    y_old = y(i) - uy(i)*model%dt
            !    !   z_old = z(i) - uz(i)*model%dt
            !
            ! else
            !       ! gyroparticles
            !       if (model%sqbxby.gt.0.0) then
            ! 	    !x_old = x(i) - ( ux(i)*model%bx - uy(i)*model%by/model%sqbxby - uz(i)*model%bx*model%bz/model%sqbxby)*model%dt
            !
            ! 	    !y_old = y(i) - (ux(i)* model%by + uy(i)*model%bx/model%sqbxby - uz(i)*model%by*model%bz/model%sqbxby)*model%dt
            !
            ! 	    !z_old = z(i) - ( ux(i)*model%bz + uz(i)*model%sqbxby)*model%dt
            !       else
            ! 	    !x_old = x(i) - uy(i)*model%dt
            !
            ! 	    !y_old = y(i) - uz(i)*model%dt
            !
            ! 	    !z_old = z(i) -  ux(i)*model%bz*model%dt
            !       end if
            !
            !
            ! end if
            ! shift coordinates
            ! x(i) = x(i) + model%Lx_start
            ! y(i) = y(i) + model%Ly_start
            ! z(i) = z(i) + model%Lz_start
            ! x_old(i) = x_old(i) + model%Lx_start
            ! y_old(i) = y_old(i) + model%Ly_start
            ! z_old(i) = z_old(i) + model%Lz_start

            ! BC
            ! if (x_old(i).gt.model%Lx_total) then
            !       x_old(i) =  x_old(i)- model%Lx_total
            ! else if (x_old(i).lt.0.0) then
            !       x_old(i) =  x_old(i) + model%Lx_total
            !
            ! end if
            !
            ! if (y_old(i).gt.model%Ly_total) then
            !       y_old(i) =  y_old(i) - model%Ly_total
            ! else if (y_old(i).lt.0.0) then
            !       y_old(i) = y_old(i) + model%Ly_total
            ! end if

            ! if (y_old(i).gt.model%Ly) then
            ! write(*,*) 'Something went very wrong with this particle'
            ! write(*,*) 'Pos:', x(i),y(i),z(i)
            ! write(*,*) 'Vel:', ux(i),uy(i),uz(i)
            ! write(*,*) 'delta:', ux(i)*model%dt,uy(i)*model%dt,uz(i)*model%dt
            !
            ! write(*,*) 'Old_Pos:', x_old(i),y_old(i),z_old(i)
            ! write(*,*) 'stype: ', stype(i)
            !
            !
            !
            ! end if

            ixo = int(xold/model%dx) + 1
            iyo = int(yold/model%dy) + 1
            izo = int(zold/model%dz) + 1
            ixo_shifted = ixo
            iyo_shifted = iyo
            if (ixo.lt.1) then
                ixo_shifted= ixo + Nx_total-1
            end if
            if (iyo.lt.1) then
                iyo_shifted= iyo + Ny_total-1
            end if
            if (ixo.gt.Nx_total) then
                ixo_shifted= ixo - (Nx_total-1)
            end if
            if (iyo.gt.Ny_total) then
                iyo_shifted= iyo - (Ny_total-1)
            end if


            if (model%debug) then
                !write(*,*) 'Init checking',ixo,iyo,izo
                if (ixo.eq.0.or.iyo.eq.0.or.izo.eq.0) then
                    write(*,*)'Particle pos',stype(i),x(i),y(i),z(i)
                    write(*,*)'Particle prev pos',xold,yold,zold
                    write(*,*)'Particle vel',ux(i),uy(i),uz(i)
                    write(*,*)'Particle shifted ix',ixo_shifted,iyo_shifted,izo

                end if
            end if

            if ((ixo.eq.ix.and.iyo.eq.iy.and.izo.eq.iz).or.(bc_matrix(ixo_shifted,iyo_shifted,izo).gt.0)) then
                ! we are in the same cell, no problems
                ! or the old pos is inside an object - true for emissive electrons
                weird_particle = .false.
            else

                ! why did we this here?
                p = (/xold + model%Lx_start,yold + model%Ly_start,zold   + model%Lz_start/)
                !p = (/x(i) + model%Lx_start,y(i) + model%Ly_start,z(i)   + model%Lz_start/)
                if (model%debug) then
                    write(*,*) 'Starting position',p(1),p(2),p(3)
                    write(*,*) 'Final position',x(i),y(i),z(i)
                end if
                if (p(1).gt.model%Lx_total) then
                    p(1) =  p(1)- model%Lx_total
                else if (p(1).lt.0.0) then
                    p(1) =  p(1) + model%Lx_total

                end if
                if (p(2).gt.model%Ly_total) then
                    p(2) =  p(2) - model%Ly_total
                else if (p(2).lt.0.0) then
                    p(2) = p(2) + model%Ly_total
                end if
                if (model%debug) then
                    write(*,*) 'Starting position after PBC',p(1),p(2),p(3)

                end if

                if (spec_params(stype(i))%motion_method.eq.0) then

                    u = (/ux(i),uy(i),uz(i)/)
                else
                    ! gyrokinetic particle - we need to transform the velocities back to regular

                    !if (model%sqbxby.gt.0.0) then
                    !u = (/ux(i)*model%bx - uy(i)*model%by/model%sqbxby - uz(i)*model%bx*model%bz/model%sqbxby,ux(i)*model%by + uy(i)*model%bx/model%sqbxby - uz(i)*model%by*model%bz/model%sqbxby,ux(i)*model%bz + uz(i)*model%sqbxby/)
                    !  else
                    !u  = (/uy(i),uz(i),ux(i)*model%bz/)
                    !end if
                    ! simple trick - obtain the velocities from the difference in positions
                    u = (/(x(i) - xold)/model%dt,(y(i) - yold)/model%dt,(z(i) - zold)/model%dt/)
                end if

                if (model%debug) then
                    write(*,*) 'Starting velocity',u
                end if
                weird_particle = .true.
                ! crossing cell boundary, run the costy algorithm


                do loop_count=1,loop_max
                    call get_next_cell_border(p,u,model%dx,model%dy,model%dz,t)

                    ! find new position
                    !if (model%debug) then
                    ! write(*,*) 'Moving particle pos:',p
                    ! write(*,*) 'Moving particle vel:',u
                    ! write(*,*) 'Moving particle t:',t
                    ! write(*,*) 'Moving particle next pos:',p + (t + 1E-8)*u
                    ! write(*,*) 'Moving particle next pos tip:',p + (t*model%dt + 1E-8)*u
                    !end if
                    f = p + (t*model%dx + 1E-8)*u

                    ! BC
                    if (f(1).gt.model%Lx_total) then
                        f(1) =  f(1) - model%Lx_total
                    else if (f(1).lt.0.0) then
                        f(1) =  f(1) + model%Lx_total

                    end if

                    if (f(2).gt.model%Ly_total) then
                        f(2) =  f(2) - model%Ly_total
                    else if (f(2).lt.0.0) then
                        f(2) =  f(2) + model%Ly_total
                    end if

                    ! write(*,*) i,'Tick',f(1),f(2),f(3)
                    ixo = int(f(1)/model%dx) + 1
                    iyo = int(f(2)/model%dy) + 1
                    izo = int(f(3)/model%dz) + 1
                    !if (model%debug) then
                    !    write(*,*) loop_count,'Next pos',f
                    !    write(*,*) 'Checking',ixo,iyo,izo
                    !end if
                    if (bc_matrix(ixo,iyo,izo).gt.0) then
                        ! we have hit the surface
                        ! check if particle has left our box, in that case put it into the buffer for transfer
                        !write(*,*) 'Hit surface'
                        if (f(1).gt.model%Lx_stop.or.f(1).lt.model%Lx_start.or.f(2).gt.model%Ly_stop.or.f(2).lt.model%Ly_start.or.f(3).gt.model%Lz_stop.or.f(3).lt.model%Lz_start) then
                            call gettime(timestamp)
                            c_start = timestamp/1.0E6

                            if (model%debug) then
                                write(*,*) model%proc_no,'Detected particle for transfer',f(1),f(2),f(3)
                            end if
                            if (no_s.lt.int(npts/10)) then
                                no_s = no_s +1
                            else
                                write(*,*) 'Transfer buffer for sending full, discarding'
                            end if

                            x_s(no_s) = f(1)
                            y_s(no_s) = f(2)
                            z_s(no_s) = f(3)
                            ux_s(no_s) = u(1)
                            uy_s(no_s) = u(2)
                            uz_s(no_s) = u(3)
                            stype_s(no_s) = stype(i)
                            ! discard the particle
                            call discard_particle(x,y,z,ux,uy,uz,stype,i,no_tot,no_tot_init,pnumber,no_species,x_old,y_old,z_old,use_old)
                            weird_particle = .false.
                            call gettime(timestamp)
                            c_one = timestamp/1.0E6 - c_start
                            c_grid = c_grid + c_one

                            exit
                        else

                            weird_particle = .false.
                            hit_surface = .true.
                            ! move particle to the crossing point
                            if (model%debug) then
                                x_orig = x(i)
                                y_orig = y(i)
                                z_orig = z(i)
                            end if
                            x(i) = f(1) - model%Lx_start
                            y(i) = f(2) - model%Ly_start
                            z(i) = f(3) - model%Lz_start
                            ix = int(x(i)/model%dx) + 1
                            iy = int(y(i)/model%dy) + 1
                            iz = int(z(i)/model%dz) + 1

                            !    write(*,*) i,'Particle hit surface',x(i),y(i),z(i)

                            exit
                        end if ! inside box
                    end if
                    if (model%debug) then
                        ! write(*,*) 'Comparing cells'
                        ! write(*,*) 'Actual:',ixo,iyo,izo
                        ! write(*,*) 'Final:',ix + model%Nx_start -1,iy + model%Ny_start -1,iz + model%Nz_start -1
                        ! write(*,*) 'Shift', model%Nx_start,model%Ny_start,model%Nz_start



                    end if
                    if (ixo.eq.(ix + model%Nx_start -1).and.iyo.eq.(iy + model%Ny_start -1).and.izo.eq.(iz + model%Nz_start -1)) then
                        ! we have reached the final cell without hitting the surface, it's OK
                        !    write(*,*) i,'Clean particle'

                        weird_particle = .false.
                        exit
                    end if

                    p  = f
                end do ! leap through cells
                ! shift back
                ! x(i) = x(i) - model%Lx_start
                ! y(i) = y(i) - model%Ly_start
                ! z(i) = z(i) - model%Lz_start
                ! ix = int(x(i)/model%dx) + 1
                ! iy = int(y(i)/model%dy) + 1
                ! iz = int(z(i)/model%dz) + 1

                ! x_old(i) = x_old(i) - model%Lx_start
                ! y_old(i) = y_old(i) - model%Ly_start
                ! z_old(i) = z_old(i) - model%Lz_start

                if (weird_particle) then
                    write(*,*) i,'Detected weird particle!!'
                    write(*,*) i,'Old:',xold,yold,zold

                    write(*,*) i,'Pos:',x(i),y(i),z(i)

                    write(*,*) i,'Vel:',u

                    if (spec_params(stype(i))%motion_method.eq.1) then

                        if (model%sqbxby.gt.0.0) then
                            write(*,*) 'Cartesian vel',ux(i)*model%bx - uy(i)*model%by/model%sqbxby - uz(i)*model%bx*model%bz/model%sqbxby,ux(i)*model%by + uy(i)*model%bx/model%sqbxby - uz(i)*model%by*model%bz/model%sqbxby,ux(i)*model%bz + uz(i)*model%sqbxby

                        else
                            write(*,*) 'Cartesian vel',uy(i),uz(i),ux(i)*model%bz

                        end if


                    end if
                    write(*,*) i,'Discarding'

                    call discard_particle(x,y,z,ux,uy,uz,stype,i,no_tot,no_tot_init,pnumber,no_species,x_old,y_old,z_old,use_old)
                else

                end if

            end if
            !
            if (hit_surface) then
                ! add it to the objects_current
                if (model%debug) then
                    write(*,*) 'Surface hit',ix,iy,iz
                    write(*,*) 'Pos',x(i),y(i),z(i)
                    write(*,*) 'Old pos',xold,yold,zold
                    write(*,*) 'Vel',ux(i),uy(i),uz(i),model%dt
                    write(*,*) 'Orig',x_orig,y_orig,z_orig


                end if
                shifted_ix = ix + model%Nx_start -1
                shifted_iy = iy + model%Ny_start -1
                shifted_iz = iz + model%Nz_start -1
                ! surface diagnostics

                call add_particle_to_surf_diag(x(i),y(i),z(i),ux(i),uy(i),uz(i),stype(i),ix,iy,iz,objects_current,edge_flux_big,edge_energy_flux_big, model,Nx,Ny,Nz,no_species,dielectric_cell,edge_charge_big,edges,q,nobjects,objects_total,Nx_total,Ny_total,Nz_total,shifted_ix,shifted_iy,shifted_iz,m,c_act,Nc,int_flux_x,int_flux_y,int_flux_z,did_edge_charge,.false.,therm_Wf,therm_Ei)

                ! secondary emission
                ! e-e
                ! allow cascades
                ! only on dielectrics
                ! split if to save time

                ! E-E secondary emission
                if (se_yield.gt.0.0.and.c_act.gt.Nc) then
                    if (q(stype(i)).lt.0.0.and.dielectric_cell(ix,iy,iz)) then
                        !if (q(stype(i)).gt.0.0) then
                        ! call rand
                        call G05FAF(0.d0,1.d0,1,rndx)
                        if (rndx.le.se_yield) then
                            ! let's generate a new particle
                            ! put it into the last position of the impacting particle
                            se_no_tot = se_no_tot + 1
                            se_x(se_no_tot) = xold
                            se_y(se_no_tot) = yold
                            se_z(se_no_tot) = zold
                            ! generate velocities with inversed signs of the impacting velocity
                            ! will not probably work with gyroparticles
                            ! temperature of the secondary particles controlled by T parameter in the input
                            call G05FDF(0.d0,1.d0,1,rndx)
                            se_ux(se_no_tot) = - abs(rndx)*utherm(stype(i))*ux(i)/abs(ux(i))*se_T
                            call G05FDF(0.d0,1.d0,1,rndx)
                            se_uy(se_no_tot) = - abs(rndx)*utherm(stype(i))*uy(i)/abs(uy(i))*se_T
                            call G05FDF(0.d0,1.d0,1,rndx)
                            se_uz(se_no_tot) = - abs(rndx)*utherm(stype(i))*uz(i)/abs(uz(i))*se_T
                            se_stype(se_no_tot) = se_spec
                            if (model%debug) then
                                write(*,*) 'Injecting SE particle'
                                write(*,*) 'Old particle',stype(i),xold,yold,zold
                                write(*,*) 'New particle',se_ux(se_no_tot),se_uy(se_no_tot),se_uz(se_no_tot)
                            end if

                            ! add the particle to the surf dieg with oposite charge
                            call add_particle_to_surf_diag(x(i),y(i),z(i),se_ux(se_no_tot),se_uy(se_no_tot),se_uz(se_no_tot),se_spec,ix,iy,iz,objects_current,edge_flux_big,edge_energy_flux_big, model,Nx,Ny,Nz,no_species,dielectric_cell,edge_charge_big,edges,q,nobjects,objects_total,Nx_total,Ny_total,Nz_total,shifted_ix,shifted_iy,shifted_iz,m,c_act,Nc,int_flux_x,int_flux_y,int_flux_z,did_edge_charge,.true.,therm_Wf,therm_Ei)


                        end if
                    end if
                end if

                ! I-E secondary emission
                if (sie_yield.gt.0.0.and.c_act.gt.Nc) then
                    if (q(stype(i)).gt.0.0.and.dielectric_cell(ix,iy,iz)) then
                        !if (q(stype(i)).gt.0.0) then
                        ! call rand
                        call G05FAF(0.d0,1.d0,1,rndx)
                        if (rndx.le.sie_yield) then
                            ! let's generate a new particle
                            ! put it into the last position of the impacting particle
                            se_no_tot = se_no_tot + 1
                            se_x(se_no_tot) = xold
                            se_y(se_no_tot) = yold
                            se_z(se_no_tot) = zold
                            ! generate velocities with inversed signs of the impacting velocity
                            ! will not probably work with gyroparticles
                            ! temperature of the secondary particles controlled by T parameter in the input
                            call G05FDF(0.d0,1.d0,1,rndx)
                            se_ux(se_no_tot) = - abs(rndx)*utherm(stype(i))*ux(i)/abs(ux(i))*sie_T
                            call G05FDF(0.d0,1.d0,1,rndx)
                            se_uy(se_no_tot) = - abs(rndx)*utherm(stype(i))*uy(i)/abs(uy(i))*sie_T
                            call G05FDF(0.d0,1.d0,1,rndx)
                            se_uz(se_no_tot) = - abs(rndx)*utherm(stype(i))*uz(i)/abs(uz(i))*sie_T
                            se_stype(se_no_tot) = sie_spec
                            if (model%debug) then
                                write(*,*) 'Injecting SE particle'
                                write(*,*) 'Old particle',stype(i),xold,yold,zold
                                write(*,*) 'New particle',se_ux(se_no_tot),se_uy(se_no_tot),se_uz(se_no_tot)
                            end if

                            ! add the particle to the surf dieg with oposite charge
                            call add_particle_to_surf_diag(x(i),y(i),z(i),se_ux(se_no_tot),se_uy(se_no_tot),se_uz(se_no_tot),sie_spec,ix,iy,iz,objects_current,edge_flux_big,edge_energy_flux_big, model,Nx,Ny,Nz,no_species,dielectric_cell,edge_charge_big,edges,q,nobjects,objects_total,Nx_total,Ny_total,Nz_total,shifted_ix,shifted_iy,shifted_iz,m,c_act,Nc,int_flux_x,int_flux_y,int_flux_z,did_edge_charge,.true.,therm_Wf,therm_Ei)


                        end if
                    end if
                end if


                if (model%debug) then
                    write(*,*) 'Done surf diag'
                end if
                call discard_particle(x,y,z,ux,uy,uz,stype,i,no_tot,no_tot_init,pnumber,no_species,x_old,y_old,z_old,use_old)
            end if

            !     end if
        end if
    end do ! particle loop

    ! copy the SE particles into buffer
    if (se_no_tot.gt.0) then

        do i=1,se_no_tot
            no_tot = no_tot+1
            x(no_tot) = se_x(i)
            y(no_tot) = se_y(i)
            z(no_tot) = se_z(i)
            ux(no_tot) = se_ux(i)
            uy(no_tot) = se_uy(i)
            uz(no_tot) = se_uz(i)
            stype(no_tot) = se_stype(i)
            pnumber(se_stype(i)) = pnumber(se_stype(i)) +1
        end do
        if (model%debug) then
            write(*,*)c_act,'Generated SE electrons',se_no_tot
        end if
    end if

    call gettime(timestamp)
    c_start = timestamp/1.0E6

    ! now comes the particle transfer
    no_r = 0
    do sp=0,model%proc_max
        if (sp.eq.model%proc_no) then
            call	send_boundary_particles(no_s,x_s,y_s,z_s,ux_s,uy_s,uz_s,stype_s,model%proc_no,model%proc_max,model,slices,npts)
        else
            call	receive_boundary_particles(no_r,x_r,y_r,z_r,ux_r,uy_r,uz_r,stype_r,model%proc_no,model%proc_max,sp,model,npts,slices)

        end if
    end do ! processor loop
    if (model%debug) then
        write(*,*) model%proc_no,'End of parallel comm'
    end if

    ! select particles for our slice
    no_r_temp = no_r
    do i=no_r,1,-1
        if (x_r(i).gt.model%Lx_start.and.y_r(i).gt.model%Ly_start.and.z_r(i).gt.model%Lz_start.and.x_r(i).lt.model%Lx_stop.and.y_r(i).lt.model%Ly_stop.and.z_r(i).lt.model%Lz_stop) then
            ! ok this is our particle
            ! shift it to our coordinates
            x_r(i) = x_r(i) - model%Lx_start
            y_r(i) = y_r(i) - model%Ly_start
            z_r(i) = z_r(i) - model%Lz_start

        else
            ! this is not our particle, discard it
            x_r(i) = x_r(no_r_temp)
            y_r(i) = y_r(no_r_temp)
            z_r(i) = z_r(no_r_temp)
            ux_r(i) = ux_r(no_r_temp)
            uy_r(i) = uy_r(no_r_temp)
            uz_r(i) = uz_r(no_r_temp)
            stype_r(i) = stype_r(no_r_temp)
            no_r_temp = no_r_temp -1

        end if
    end do
    no_r = no_r_temp



    ! run through the received particles and add them to surface diagnostics
    if (model%debug) then
        write(*,*) model%proc_no,'Received boundary particles',no_r
    end if
    do i=1,no_r
        ix = int(x_r(i)/model%dx) + 1
        iy = int(y_r(i)/model%dy) + 1
        iz = int(z_r(i)/model%dz) + 1
        shifted_ix = ix + model%Nx_start -1
        shifted_iy = iy + model%Ny_start -1
        shifted_iz = iz + model%Nz_start -1
        ! disable for now
        call add_particle_to_surf_diag(x_r(i),y_r(i),z_r(i),ux_r(i),uy_r(i),uz_r(i),stype_r(i),ix,iy,iz,objects_current,edge_flux_big,edge_energy_flux_big, model,Nx,Ny,Nz,no_species,dielectric_cell,edge_charge_big,edges,q,nobjects,objects_total,Nx_total,Ny_total,Nz_total,shifted_ix,shifted_iy,shifted_iz,m,c_act,Nc,int_flux_x,int_flux_y,int_flux_z,did_edge_charge,.false.,therm_Wf,therm_Ei)
    end do ! boundary particles
    call gettime(timestamp)
    ! c_grid = timestamp/1.0E6 - c_start


    if (did_edge_charge.gt.0) then
        ! edge_charge_big(1,1:Ny,:) = edge_charge_big(1,1:Ny,:) +  edge_charge_big(Nx,1:Ny,:)
        ! edge_charge_big(Nx,1:Ny,:) = edge_charge_big(1,1:Ny,:)
        ! edge_charge_big(1:Nx,1,:) = edge_charge_big(1:Nx,Ny,:) +  edge_charge_big(1:Nx,1,:)
        ! edge_charge_big(1:Nx,Ny,:) = edge_charge_big(1:Nx,1,:)
        !edge_charge = edge_charge + edge_charge_big(1:Nx,1:Ny,:)
        edge_charge =  edge_charge_big(1:Nx,1:Ny,:)

        ! fix the borders

    end if

    if (c_act.gt.model%Na) then
        ! write(*,*) 'Merging edgeflux',c_act,model%proc_no,sum(abs(edge_flux))
        do sp=1,no_species
            if (spec_params(sp)%mpi_rank.eq.model%proc_no) then

                edge_flux_big(sp,1,1:Ny,:) = edge_flux_big(sp,1,1:Ny,:) +  edge_flux_big(sp,Nx,1:Ny,:)
                edge_flux_big(sp,Nx,1:Ny,:) = edge_flux_big(sp,1,1:Ny,:)
                edge_flux_big(sp,1:Nx,1,:) = edge_flux_big(sp,1:Nx,Ny,:) +  edge_flux_big(sp,1:Nx,1,:)
                edge_flux_big(sp,1:Nx,Ny,:) = edge_flux_big(sp,1:Nx,1,:)
                edge_flux(sp,:,:,:) = edge_flux(sp,:,:,:) + edge_flux_big(sp,1:Nx,1:Ny,:)

                edge_energy_flux_big(sp,1,1:Ny,:) = edge_energy_flux_big(sp,1,1:Ny,:) +  edge_energy_flux_big(sp,1,1:Ny,:)
                edge_energy_flux_big(sp,Nx,1:Ny,:) = edge_energy_flux_big(sp,1,1:Ny,:)
                edge_energy_flux_big(sp,1:Nx,1,:) = edge_energy_flux_big(sp,1:Nx,Ny,:) +  edge_energy_flux_big(sp,1:Nx,1,:)
                edge_energy_flux_big(sp,1:Nx,Ny,:) = edge_energy_flux_big(sp,1:Nx,1,:)
                edge_energy_flux(sp,:,:,:) = edge_energy_flux(sp,:,:,:) + edge_energy_flux_big(sp,1:Nx,1:Ny,:)
            end if

        end do

    end if

    ! check
    if (model%debug) then
        write(*,*) 'Discarded:',no_tot_init - no_tot
        write(*,*) 'Remaining:',no_tot
        write(*,*) 'Specie 1:',pnumber(1)
    end if
end subroutine

subroutine add_particle_to_surf_diag(x,y,z,ux,uy,uz,stype,ix,iy,iz,objects_current,edge_flux_big,edge_energy_flux_big, model,Nx,Ny,Nz,no_species,dielectric_cell,edge_charge_big,edges,q,nobjects,objects_total,Nx_total,Ny_total,Nz_total,shifted_ix,shifted_iy,shifted_iz,m,c_act,Nc,int_flux_x,int_flux_y,int_flux_z,did_edge_charge,emissive,therm_Wf,therm_Ei)
    use mth
    implicit none
    include 'struct.h'
    real:: x,y,z,ux,uy,uz,A1,A2,A3,A4,A5,A6,A7,A8,rx,ry,rz,V,tc,charge_sum,flux_sum, E_tot,q_out,therm_Wf,therm_Ei,mass
    integer:: stype,ix,iy,iz,Nx,Ny,Nz,nobjects,Nx_total,Ny_total,Nz_total,shifted_ix,shifted_iy,shifted_iz,c_act,Nc,Na,no_species
    type(model_t):: model
    real, dimension(no_species,Nx+1,Ny+1,Nz):: edge_flux_big,edge_energy_flux_big
    logical, dimension(Nx,Ny,Nz):: dielectric_cell
    real, dimension(Nx+1,Ny+1,Nz):: edge_charge_big
    integer, dimension(Nx,Ny,Nz):: objects,edges
    real, dimension(no_species)::q,m
    real, dimension(nobjects,no_species):: objects_current
    integer, dimension(Nx_total,Ny_total,Nz_total):: objects_total
    real, dimension(no_species,nobjects,Nx):: int_flux_x
    real, dimension(no_species,nobjects,Ny):: int_flux_y
    real, dimension(no_species,nobjects,Nz):: int_flux_z
    integer:: did_edge_charge
    logical:: emissive

    mass = 3670.0
    if (model%mks_main_ion_m.gt.0) then
        mass = 1860.0*model%mks_main_ion_m
    end if

    V = model%dx*model%dy*model%dz
    if (model%debug) then
        write(*,*) 'Inside surf diag'
        write(*,*) 'pos',x,y,z
        write(*,*) 'vel',ux,uy,uz
        write(*,*) 'Ipos',ix,iy,iz
        write(*,*) 'Spos',shifted_ix,shifted_iy,shifted_iz

        if (objects_total(shifted_ix,shifted_iy,shifted_iz).lt.1) then

            write(*,*) 'Particle error'
            stop

        end if
    end if
    ! write(*,*) 'Objects ID',objects_total(shifted_ix,shifted_iy,shifted_iz),ix,iy,iz
    if (q(stype).gt.0) then
        objects_current(objects_total(shifted_ix,shifted_iy,shifted_iz),stype) = objects_current(objects_total(shifted_ix,shifted_iy,shifted_iz),stype) + q(stype)
    else
        ! this is fix for mu=200 electrons
        if (emissive) then
            objects_current(objects_total(shifted_ix,shifted_iy,shifted_iz),stype) = objects_current(objects_total(shifted_ix,shifted_iy,shifted_iz),stype) - q(stype)*sqrt(m(stype)*mass)
        else
            objects_current(objects_total(shifted_ix,shifted_iy,shifted_iz),stype) = objects_current(objects_total(shifted_ix,shifted_iy,shifted_iz),stype) + q(stype)*sqrt(m(stype)*mass)

        end if
    end if
    if (model%debug) then
        write(*,*) 'Done objects current'
    end if
    if (c_act .gt. Nc) then
        ! add it to the integral characteristics
        int_flux_x(stype,objects_total(shifted_ix,shifted_iy,shifted_iz),ix) =     int_flux_x(stype,objects_total(shifted_ix,shifted_iy,shifted_iz),ix) + 1.0
        int_flux_y(stype,objects_total(shifted_ix,shifted_iy,shifted_iz),iy) =     int_flux_y(stype,objects_total(shifted_ix,shifted_iy,shifted_iz),iy) + 1.0
        int_flux_z(stype,objects_total(shifted_ix,shifted_iy,shifted_iz),iz) =     int_flux_z(stype,objects_total(shifted_ix,shifted_iy,shifted_iz),iz) + 1.0
        ! edge charge
        ! see how many points around the particle lie on a surface
        ! calculate the weights
        rx = x/model%dx - float(ix-1)
        ry = y/model%dy - float(iy-1)
        rz = z/model%dz - float(iz-1)

        A1 = (1.0 - rx)*(1.0 - ry)*(1.0 - rz)
        A2 = (1.0 - rx)*ry*(1.0 - rz)
        A3 = rx*ry*(1.0 - rz)
        A4 = rx*(1.0 - ry)*(1.0 - rz)
        A5 = (1.0 - rx)*(1.0 - ry)*rz
        A6 = (1.0 - rx)*ry*rz
        A7 = rx*ry*rz
        A8 = rx*(1.0 - ry)*rz
        charge_sum = 1.0E-12 + A1*isgn(edges(ix,iy,iz)) + A2*isgn(edges(ix,iy+1,iz)) + A3*isgn(edges(ix+1,iy+1,iz)) + A4*isgn(edges(ix+1,iy,iz)) + A5*isgn(edges(ix,iy,iz+1)) + A6*isgn(edges(ix,iy+1,iz+1)) + A7*isgn(edges(ix+1,iy+1,iz+1)) + A8*isgn(edges(ix+1,iy,iz+1))

        if (model%debug) then
            tc = A1 + A2 + A3 + A4 + A5 + A6 + A7 + A8
            if (abs(tc - 1.0).gt.0.001) then
                write(*,*) 'weights error, A sum',tc
                write(*,*) 'Particle',stype,q(stype)
                write(*,*) 'Pos',x,y,z
                write(*,*) 'A: ',A1, A2,A3,A4,A5,A6,A7,A8
            end if
            if (charge_sum.gt.1.0.or.charge_sum.lt.0.0) then
                write(*,*) 'BC sum error',charge_sum
                write(*,*) 'Particke',stype,q(stype)
                write(*,*) 'Pos',x,y,z
                write(*,*) 'A: ',A1, A2,A3,A4,A5,A6,A7,A8
                write(*,*) 'isgn: ',isgn(edges(ix,iy,iz)),isgn(edges(ix,iy+1,iz)),isgn(edges(ix+1,iy,iz)),isgn(edges(ix+1,iy+1,iz)),isgn(edges(ix,iy,iz+1)),isgn(edges(ix,iy+1,iz+1)),isgn(edges(ix+1,iy,iz+1)),isgn(edges(ix+1,iy+1,iz+1))


            end if
        end if ! debug

        if (c_act.gt.model%Na) then
            ! add the particle to the edge flux diagnostics
            flux_sum = charge_sum/(model%dz/model%dt/real(model%Npc))
            !  write(*,*) 'particle to edgeflux',flux_sum,stype,iz
            ! if (flux_sum.lt.1E10) then
            !   write(*,*) 'Pos:',x,y,z
            !   write(*,*) 'iPos:',ix,iy,iz
            !
            !   write(*,*) 'V:',ux,uy,uz
            !                 write(*,*) 'A: ',A1, A2,A3,A4,A5,A6,A7,A8
            !                   write(*,*) 'isgn: ',isgn(edges(ix,iy,iz)),isgn(edges(ix,iy+1,iz)),isgn(edges(ix+1,iy,iz)),isgn(edges(ix+1,iy+1,iz)),isgn(edges(ix,iy,iz+1)),isgn(edges(ix,iy+1,iz+1)),isgn(edges(ix+1,iy,iz+1)),isgn(edges(ix+1,iy+1,iz+1))
            !   write(*,*) 'Edges 6',model%proc_no,sum(edges(:,:,6))
            !   write(*,*) 'Edges 7',model%proc_no,sum(edges(:,:,7))
            !
            ! end if

            edge_flux_big(stype,ix,iy,iz)   = edge_flux_big(stype,ix,iy,iz) + A1*isgn(edges(ix,iy,iz))/flux_sum
            edge_flux_big(stype,ix,iy+1,iz) = edge_flux_big(stype,ix,iy+1,iz) + A2*isgn(edges(ix,iy+1,iz))/flux_sum
            edge_flux_big(stype,ix+1,iy+1,iz) = edge_flux_big(stype,ix+1,iy+1,iz) + A3*isgn(edges(ix+1,iy+1,iz))/flux_sum
            edge_flux_big(stype,ix+1,iy,iz) = edge_flux_big(stype,ix+1,iy,iz) + A4*isgn(edges(ix+1,iy,iz))/flux_sum

            edge_flux_big(stype,ix,iy,iz+1) = edge_flux_big(stype,ix,iy,iz+1) + A5*isgn(edges(ix,iy,iz+1))/flux_sum
            edge_flux_big(stype,ix,iy+1,iz+1) = edge_flux_big(stype,ix,iy+1,iz+1) + A6*isgn(edges(ix,iy+1,iz+1))/flux_sum
            edge_flux_big(stype,ix+1,iy+1,iz+1) = edge_flux_big(stype,ix+1,iy+1,iz+1) + A7*isgn(edges(ix+1,iy+1,iz+1))/flux_sum
            edge_flux_big(stype,ix+1,iy,iz+1) = edge_flux_big(stype,ix+1,iy,iz+1) + A8*isgn(edges(ix+1,iy,iz+1))/flux_sum

            if(model%use_surface_heat_tweaks.and.(.not.emissive)) then
                if(q(stype).gt.0.0) then
                    ! ions add Ei
                    E_tot = (ux*ux +uy*uy + uz*uz + 2*therm_Ei/m(stype))*model%dz/model%dt/real(model%Npc)*m(stype)*0.5
                else
                    ! electrons add Wf
                    E_tot = (ux*ux +uy*uy + uz*uz + 2*therm_Wf/m(stype))*model%dz/model%dt/real(model%Npc)*m(stype)*0.5
                end if

            else
                E_tot = (ux*ux +uy*uy + uz*uz)*model%dz/model%dt/real(model%Npc)*m(stype)*0.5
            end if


            edge_energy_flux_big(stype,ix,iy,iz)   = edge_energy_flux_big(stype,ix,iy,iz) + A1*isgn(edges(ix,iy,iz))/charge_sum*E_tot
            edge_energy_flux_big(stype,ix,iy+1,iz) = edge_energy_flux_big(stype,ix,iy+1,iz) + A2*isgn(edges(ix,iy+1,iz))/charge_sum*E_tot
            edge_energy_flux_big(stype,ix+1,iy,iz) = edge_energy_flux_big(stype,ix+1,iy,iz) + A4*isgn(edges(ix+1,iy,iz))/charge_sum*E_tot
            edge_energy_flux_big(stype,ix+1,iy+1,iz) = edge_energy_flux_big(stype,ix+1,iy+1,iz) + A3*isgn(edges(ix+1,iy+1,iz))/charge_sum*E_tot
            edge_energy_flux_big(stype,ix,iy,iz+1) = edge_energy_flux_big(stype,ix,iy,iz+1) + A5*isgn(edges(ix,iy,iz+1))/charge_sum*E_tot
            edge_energy_flux_big(stype,ix,iy+1,iz+1) = edge_energy_flux_big(stype,ix,iy+1,iz+1) + A6*isgn(edges(ix,iy+1,iz+1))/charge_sum*E_tot
            edge_energy_flux_big(stype,ix+1,iy,iz+1) = edge_energy_flux_big(stype,ix+1,iy,iz+1) + A8*isgn(edges(ix+1,iy,iz+1))/charge_sum*E_tot
            edge_energy_flux_big(stype,ix+1,iy+1,iz+1) = edge_energy_flux_big(stype,ix+1,iy+1,iz+1) + A7*isgn(edges(ix+1,iy+1,iz+1))/charge_sum*E_tot
        end if

        ! now add charge to the appropriate points
        if (dielectric_cell(ix,iy,iz)) then
            ! we had some secondary emission, disabled for now
            q_out = q(stype)
            ! electrons
            if (q_out.lt.0.0) then
                if (emissive) then
                    charge_sum = -charge_sum
                end if
                ! disable for now
                !q_out = 0.0

                edge_charge_big(ix,iy,iz)   = edge_charge_big(ix,iy,iz) + q_out*A1*isgn(edges(ix,iy,iz))/charge_sum*sqrt(m(stype)*mass)
                ! if (dielectric_cell(ix,iy+1,iz)) then
                edge_charge_big(ix,iy+1,iz) = edge_charge_big(ix,iy+1,iz) + q_out*A2*isgn(edges(ix,iy+1,iz))/charge_sum*sqrt(m(stype)*mass)
                !end if
                !if (dielectric_cell(ix+1,iy,iz)) then
                edge_charge_big(ix+1,iy,iz) = edge_charge_big(ix+1,iy,iz) + q_out*A4*isgn(edges(ix+1,iy,iz))/charge_sum*sqrt(m(stype)*mass)
                !end if
                !if (dielectric_cell(ix+1,iy+1,iz)) then
                edge_charge_big(ix+1,iy+1,iz) = edge_charge_big(ix+1,iy+1,iz) + q_out*A3*isgn(edges(ix+1,iy+1,iz))/charge_sum*sqrt(m(stype)*mass)
                !end if
                !if (dielectric_cell(ix,iy,iz+1)) then
                edge_charge_big(ix,iy,iz+1) = edge_charge_big(ix,iy,iz+1) + q_out*A5*isgn(edges(ix,iy,iz+1))/charge_sum*sqrt(m(stype)*mass)
                !end if
                !if (dielectric_cell(ix,iy+1,iz+1)) then
                edge_charge_big(ix,iy+1,iz+1) = edge_charge_big(ix,iy+1,iz+1) + q_out*A6*isgn(edges(ix,iy+1,iz+1))/charge_sum*sqrt(m(stype)*mass)
                !end if
                !if (dielectric_cell(ix+1,iy,iz+1)) then
                edge_charge_big(ix+1,iy,iz+1) = edge_charge_big(ix+1,iy,iz+1) + q_out*A8*isgn(edges(ix+1,iy,iz+1))/charge_sum*sqrt(m(stype)*mass)
                !end if
                !if (dielectric_cell(ix+1,iy+1,iz+1)) then
                edge_charge_big(ix+1,iy+1,iz+1) = edge_charge_big(ix+1,iy+1,iz+1) + q_out*A7*isgn(edges(ix+1,iy+1,iz+1))/charge_sum*sqrt(m(stype)*mass)
                !end if
            else

                !ions
                !	q_out = q(stype)

                edge_charge_big(ix,iy,iz)   = edge_charge_big(ix,iy,iz) + q_out*A1*isgn(edges(ix,iy,iz))/charge_sum
                !if (dielectric_cell(ix,iy+1,iz)) then
                edge_charge_big(ix,iy+1,iz) = edge_charge_big(ix,iy+1,iz) + q_out*A2*isgn(edges(ix,iy+1,iz))/charge_sum
                !end if
                !if (dielectric_cell(ix+1,iy,iz)) then
                edge_charge_big(ix+1,iy,iz) = edge_charge_big(ix+1,iy,iz) + q_out*A4*isgn(edges(ix+1,iy,iz))/charge_sum
                !end if
                !if (dielectric_cell(ix+1,iy+1,iz)) then
                edge_charge_big(ix+1,iy+1,iz) = edge_charge_big(ix+1,iy+1,iz) + q_out*A3*isgn(edges(ix+1,iy+1,iz))/charge_sum
                !end if
                !if (dielectric_cell(ix,iy,iz+1)) then
                edge_charge_big(ix,iy,iz+1) = edge_charge_big(ix,iy,iz+1) + q_out*A5*isgn(edges(ix,iy,iz+1))/charge_sum
                !end if
                !if (dielectric_cell(ix+1,iy,iz+1)) then
                edge_charge_big(ix,iy+1,iz+1) = edge_charge_big(ix,iy+1,iz+1) + q_out*A6*isgn(edges(ix,iy+1,iz+1))/charge_sum
                !end if
                !if (dielectric_cell(ix+1,iy,iz+1)) then
                edge_charge_big(ix+1,iy,iz+1) = edge_charge_big(ix+1,iy,iz+1) + q_out*A8*isgn(edges(ix+1,iy,iz+1))/charge_sum
                !end if
                !if (dielectric_cell(ix+1,iy+1,iz+1)) then
                edge_charge_big(ix+1,iy+1,iz+1) = edge_charge_big(ix+1,iy+1,iz+1) + q_out*A7*isgn(edges(ix+1,iy+1,iz+1))/charge_sum
                !end if

            end if
            ! 		end if
            did_edge_charge = 1
        else
            ! this one was not caught in dielectrics
            !write(*,*) 'Particle not in dielectrics'
            !write(*,*) ix,iy,iz,q_out
            !write(*,*) dielectric_cell(ix,iy,iz),edges(ix,iy,iz)

        end if ! ions/electrons
    end if ! c_act gt Na


end subroutine

subroutine  discard_particle(x,y,z,ux,uy,uz,stype,i,no_tot,no_tot_init,pnumber,no_species,x_old,y_old,z_old,use_old)
    implicit none
    integer:: i,no_tot,no_tot_init,j,k,no_species
    integer, dimension(no_species):: pnumber
    real, dimension(no_tot_init):: x,y,z,ux,uy,uz,x_old,y_old,z_old
    integer, dimension(no_tot_init):: stype
    logical:: use_old
    k = stype(i)
    ! if (k.gt.4) then
    !       write(*,*) 'Discarding',x(i),y(i),z(i),stype(i)
    !
    ! end if

    if (i.lt.no_tot) then
        x(i) = x(no_tot)
        y(i) = y(no_tot)
        z(i) = z(no_tot)
        if (use_old) then
            x_old(i) = x_old(no_tot)
            y_old(i) = y_old(no_tot)
            z_old(i) = z_old(no_tot)
        end if
        ux(i) = ux(no_tot)
        uy(i) = uy(no_tot)
        uz(i) = uz(no_tot)
        stype(i) = stype(no_tot)
    end if
    ! reduce number of particles
    no_tot = no_tot - 1
    ! reduce particle statistics
    pnumber(k) = pnumber(k) -1
end subroutine


subroutine rcomb_history(a,history_ntimes)
    !for combing of real*8 arrays
    integer:: k,history_ntimes
    real, dimension(history_ntimes):: a
    !get the averages
    do k=1,history_ntimes-1,2
        !average
        a(k) = (a(k) + a(k+1))/2.0
    end do
    !half the array
    do k=1,int(history_ntimes/2)
        !average
        a(k) = a(2*k)
    end do
    !zero the rest
    do k=int(history_ntimes/2),history_ntimes
        !average
        a(k) = 0.0
    end do


end subroutine


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


subroutine icomb_history(a,history_ntimes)
    !for combing of integer arrays
    integer:: k,history_ntimes
    integer, dimension(history_ntimes):: a
    !get the averages
    do k=1,history_ntimes-1,2
        !average
        a(k) = (a(k) + a(k+1))/2
    end do
    !half the array
    do k=1,int(history_ntimes/2)
        !shift array
        a(k) = a(2*k)
    end do
    !zero the rest
    do k=int(history_ntimes/2),history_ntimes
        a(k) = 0
    end do


end subroutine

subroutine prepare_large_grid_r(Nx,Ny,Nz,a,b)
    implicit none
    integer:: Nx,Ny,Nz
    real, dimension(Nx,Ny,Nz)::a
    real, dimension(Nx+2,Ny+2,Nz)::b
    b(2:(Nx+1),2:(Ny+1),:) = a
    b(1,2:(Ny+1),:) = a(Nx,:,:)
    b(Nx+2,2:(Ny+1),:) = a(1,:,:)
    b(2:(Nx+1),1,:) = a(:,Ny,:)
    b(2:(Nx+1),Ny+2,:) = a(:,1,:)
    b(1,1,:) = a(Nx,Ny,:)
    b(Nx+2,1,:) = a(Nx,Ny,:)
    b(1,Ny+2,:) = a(Nx,Ny,:)
    b(Nx+2,Ny+2,:) = a(Nx,Ny,:)
end subroutine

subroutine prepare_large_grid_i(Nx,Ny,Nz,a,b)
    implicit none
    integer:: Nx,Ny,Nz
    integer, dimension(Nx,Ny,Nz)::a
    integer, dimension(Nx+2,Ny+2,Nz)::b
    b(2:(Nx+1),2:(Ny+1),:) = a
    b(1,2:(Ny+1),:) = a(Nx,:,:)
    b(Nx+2,2:(Ny+1),:) = a(1,:,:)
    b(2:(Nx+1),1,:) = a(:,Ny,:)
    b(2:(Nx+1),Ny+2,:) = a(:,1,:)
    b(1,1,:) = a(Nx,Ny,:)
    b(Nx+2,1,:) = a(Nx,Ny,:)
    b(1,Ny+2,:) = a(Nx,Ny,:)
    b(Nx+2,Ny+2,:) = a(Nx,Ny,:)
end subroutine

subroutine calc_E_field_general (Nx,Ny,Nz,Ex,Ey,Ez,Pot,dx,dy,dz,edges,objects,Nx_total,Ny_total,Nz_total,Nx_start,Ny_start,Nz_start)
    implicit none
    !calculates E field at the grid points
    !2-points difference form (inside the region)
    integer:: ix,iy,iz,Nx,Ny,Nz,Nx_total,Ny_total,Nz_total,Nx_start,Ny_start,Nz_start
    real, dimension(Nx,Ny,Nz):: Ex,Ey,Ez
    real, dimension(Nx_total,Ny_total,Nz_total):: Pot
    integer, dimension(Nx_total,Ny_total,Nz_total):: objects

    real, dimension(Nx_total+2,Ny_total+2,Nz_total):: bPot
    integer, dimension(Nx_total+2,Ny_total+2,Nz_total):: b_objects

    integer, dimension(Nx,Ny,Nz):: edges
    real:: dx,dz,dy
    bPot = 0.0
    b_objects = 0.0
    call prepare_large_grid_r(Nx_total,Ny_total,Nz_total,Pot,bPot)
    call prepare_large_grid_i(Nx_total,Ny_total,Nz_total,objects,b_objects)



    do ix=2,Nx_total+1
        do iy=2,Ny_total+1
            do iz=1,Nz_total
                if ((ix.gt.Nx_start.and.ix.le.(Nx_start + Nx )).and.(iy.gt.Ny_start.and.iy.le.(Ny_start + Ny)).and.(iz.gt.Nz_start.and.iz.le.(Nz_start + Nz -1))) then
                    if (iz.eq.1) then
                        Ez(ix-Nx_start,iy-Ny_start,1)=(bPot(ix,iy,1)-bPot(ix,iy,2))/dz
                        ! 	  Ex(ix-1,iy-1,iz) = (bPot(ix-1,iy,iz)-Pot(ix+1,iy,iz))/dx/2.0
                        ! 	  Ey(ix-1,iy-1,iz) = (bPot(ix,iy-1,iz)-Pot(ix,iy+1,iz))/dy/2.0
                        if (edges(ix-Nx_start,iy-Ny_start,iz).eq.0) then
                            Ex(ix-Nx_start,iy-Ny_start,1) = (bPot(ix-1,iy,iz)-bPot(ix+1,iy,iz))/dx/2.0
                            Ey(ix-Nx_start,iy-Ny_start,1) = (bPot(ix,iy-1,iz)-bPot(ix,iy+1,iz))/dy/2.0
                        else
                            ! x-direction
                            if (b_objects(ix-1,iy,iz).gt.0.and.b_objects(ix+1,iy,iz).eq.0) then
                                ! surface in x direction positive
                                Ex(ix-Nx_start,iy-Ny_start,iz - Nz_start+1)=(bPot(ix,iy,iz)-bPot(ix+1,iy,iz))/dx
                            else if (b_objects(ix+1,iy,iz).gt.0.and.b_objects(ix-1,iy,iz).eq.0) then
                                ! surface in x direction negative
                                Ex(ix-Nx_start,iy-Ny_start,iz - Nz_start+1)=(bPot(ix-1,iy,iz)-bPot(ix,iy,iz))/dx
                            else
                                !Ex(ix-1,iy-1,iz) = (bPot(ix-1,iy,iz)-bPot(ix+1,iy,iz))/dx/2.0
                                Ex(ix-Nx_start,iy-Ny_start,iz - Nz_start+1) = (bPot(ix,iy,iz)-bPot(ix+1,iy,iz))/dx
                            end if
                            ! y-direction
                            if (b_objects(ix,iy-1,iz).gt.0.and.b_objects(ix,iy+1,iz).eq.0) then
                                ! surface in y direction positive
                                Ey(ix-Nx_start,iy-Ny_start,iz - Nz_start+1)=(bPot(ix,iy,iz)-bPot(ix,iy+1,iz))/dy
                            else if (b_objects(ix,iy+1,iz).gt.0.and.b_objects(ix,iy-1,iz).eq.0) then
                                ! surface in y direction negative
                                Ey(ix-Nx_start,iy-Ny_start,iz - Nz_start+1)=(bPot(ix,iy-1,iz)-bPot(ix,iy,iz))/dy
                            else
                                ! 					Ey(ix-1,iy-1,iz) = (bPot(ix,iy-1,iz)-bPot(ix,iy+1,iz))/dy/2.0
                                Ey(ix-Nx_start,iy-Ny_start,iz - Nz_start+1) = (bPot(ix,iy,iz)-bPot(ix,iy+1,iz))/dy

                            end if

                        end if

                    elseif (iz.eq.Nz_total) then
                        Ez(ix-Nx_start,iy-Ny_start,iz - Nz_start+1)=(bPot(ix,iy,Nz-1)-bPot(ix,iy,Nz))/dz
                        Ex(ix-Nx_start,iy-Ny_start,iz - Nz_start+1) = (bPot(ix-1,iy,iz)-bPot(ix+1,iy,iz))/dx/2.0
                        Ey(ix-Nx_start,iy-Ny_start,iz - Nz_start+1) = (bPot(ix,iy-1,iz)-bPot(ix,iy+1,iz))/dy/2.0
                    else
                        ! middle of the space
                        if (edges(ix-Nx_start,iy-Ny_start,iz - Nz_start+1).eq.0) then
                            Ex(ix-Nx_start,iy-Ny_start,iz - Nz_start+1) = (bPot(ix-1,iy,iz)-bPot(ix+1,iy,iz))/dx/2.0
                            Ey(ix-Nx_start,iy-Ny_start,iz - Nz_start+1) = (bPot(ix,iy-1,iz)-bPot(ix,iy+1,iz))/dy/2.0
                            Ez(ix-Nx_start,iy-Ny_start,iz - Nz_start+1) = (bPot(ix,iy,iz-1)-bPot(ix,iy,iz+1))/dz/2.0
                        else
                            if (b_objects(ix,iy,iz-1).gt.0.and.b_objects(ix,iy,iz+1).eq.0) then
                                ! surface in z direction positive
                                ! test of new formula
                                Ez(ix-Nx_start,iy-Ny_start,iz - Nz_start+1)=(bPot(ix,iy,iz)-bPot(ix,iy,iz+1))/dz
                                !   Ez(ix-1,iy-1,iz)=(-bPot(ix,iy,iz+2) + 4.0*bPot(ix,iy,iz+1) - 3.0*bPot(ix,iy,iz))/dz/2.0
                            else if (b_objects(ix,iy,iz+1).gt.0.and.b_objects(ix,iy,iz-1).eq.0) then
                                ! surface in z direction negative
                                Ez(ix-Nx_start,iy-Ny_start,iz - Nz_start+1)=(bPot(ix,iy,iz-1)-bPot(ix,iy,iz))/dz
                            else
                                Ez(ix-Nx_start,iy-Ny_start,iz - Nz_start+1) = (bPot(ix,iy,iz)-bPot(ix,iy,iz+1))/dz

                                ! 				Ez(ix-1,iy-1,iz) = (bPot(ix,iy,iz-1)-bPot(ix,iy,iz+1))/dz/2.0
                            end if
                            ! x-direction
                            if (b_objects(ix-1,iy,iz).gt.0.and.b_objects(ix+1,iy,iz).eq.0) then
                                ! surface in x direction positive
                                ! test of new formula
                                Ex(ix-Nx_start,iy-Ny_start,iz - Nz_start+1)=(bPot(ix,iy,iz)-bPot(ix+1,iy,iz))/dx
                                !	Ex(ix-1,iy-1,iz)=(-bPot(ix+2,iy,iz)+4.0*bPot(ix+1,iy,iz) - 3.0*bPot(ix,iy,iz))/dx/2.0

                            else if (b_objects(ix+1,iy,iz).gt.0.and.b_objects(ix-1,iy,iz).eq.0) then
                                ! surface in x direction negative

                                ! test of new formula
                                Ex(ix-Nx_start,iy-Ny_start,iz - Nz_start+1)=(bPot(ix-1,iy,iz)-bPot(ix,iy,iz))/dx
                                !	    	 Ex(ix-1,iy-1,iz)=-(-bPot(ix-2,iy,iz)+4.0*bPot(ix-1,iy,iz) - 3.0*bPot(ix,iy,iz))/dx/2.0

                            else
                                Ex(ix-Nx_start,iy-Ny_start,iz - Nz_start+1) = (bPot(ix,iy,iz)-bPot(ix+1,iy,iz))/dx

                                ! 		Ex(ix-1,iy-1,iz) = (bPot(ix-1,iy,iz)-bPot(ix+1,iy,iz))/dx/2.0
                            end if
                            ! y-direction
                            if (b_objects(ix,iy-1,iz).gt.0.and.b_objects(ix,iy+1,iz).eq.0) then
                                ! surface in y direction positive
                                ! test of new formula
                                Ey(ix-Nx_start,iy-Ny_start,iz - Nz_start+1)=(bPot(ix,iy,iz)-bPot(ix,iy+1,iz))/dy
                                !	    	 Ey(ix-1,iy-1,iz)=(-bPot(ix,iy+2,iz)+4.0*bPot(ix,iy+1,iz) - 3.0*bPot(ix,iy,iz))/dy/2.0

                            else if (b_objects(ix,iy+1,iz).gt.0.and.b_objects(ix,iy-1,iz).eq.0) then
                                ! surface in y direction negative
                                Ey(ix-Nx_start,iy-Ny_start,iz - Nz_start+1)=(bPot(ix,iy-1,iz)-bPot(ix,iy,iz))/dy
                                !	    	 Ey(ix-1,iy-1,iz)=-(-bPot(ix,iy-2,iz)+4.0*bPot(ix,iy-1,iz) - 3.0*bPot(ix,iy,iz))/dy/2.0

                            else
                                Ey(ix-Nx_start,iy-Ny_start,iz - Nz_start+1) = (bPot(ix,iy,iz)-bPot(ix,iy+1,iz))/dy

                                ! 		Ey(ix-1,iy-1,iz) = (bPot(ix,iy-1,iz)-bPot(ix,iy+1,iz))/dy/2.0
                            end if

                        end if
                    endif
                end if
            enddo
        enddo
    end do

end subroutine

subroutine calc_E_field_general_unstable (Nx,Nz,Ny,Ex,Ey,Ez,Pot,dx,dy,dz,edges,objects,z_limit)
    implicit none
    !calculates E field at the grid points
    !2-points difference form (inside the region)
    integer:: ix,iy,iz,Nx,Ny,Nz
    real*8:: z_limit
    real, dimension(Nx,Ny,Nz):: Ex,Ey,Ez,Pot
    real, dimension(Nx+2,Ny+2,Nz):: bPot
    real, dimension(Nx+2,Ny+2,Nz):: b_objects

    integer, dimension(Nx,Ny,Nz):: edges,objects
    real:: dx,dz,dy
    bPot(2:(Nx+1),2:(Ny+1),:) = Pot
    ! x periodic
    bPot(1,2:(Ny+1),:) = Pot(Nx,:,:)
    bPot(Nx+2,2:(Ny+1),:) = Pot(1,:,:)
    ! y periodic
    bPot(2:(Nx+1),1,:) = Pot(:,Ny,:)
    bPot(2:(Nx+1),Ny+2,:) = Pot(:,1,:)
    ! corner
    bPot(1,1,:) = Pot(Nx,Ny,:)
    bPot(Nx+2,1,:) = Pot(Nx,Ny,:)
    bPot(1,Ny+2,:) = Pot(Nx,Ny,:)
    bPot(Nx+2,Ny+2,:) = Pot(Nx,Ny,:)

    b_objects(2:(Nx+1),2:(Ny+1),:) = objects
    ! x periodic
    b_objects(1,2:(Ny+1),:) = objects(Nx,:,:)
    b_objects(Nx+2,2:(Ny+1),:) = objects(1,:,:)
    ! y periodic
    b_objects(2:(Nx+1),1,:) = objects(:,Ny,:)
    b_objects(2:(Nx+1),Ny+2,:) = objects(:,1,:)
    ! corner
    b_objects(1,1,:) = objects(Nx,Ny,:)
    b_objects(Nx+2,1,:) = objects(Nx,Ny,:)
    b_objects(1,Ny+2,:) = objects(Nx,Ny,:)
    b_objects(Nx+2,Ny+2,:) = objects(Nx,Ny,:)


    do ix=2,Nx+1
        do iy=2,Ny+1
            do iz=1,Nz
                if (iz.eq.1) then
                    Ez(ix-1,iy-1,1)=(bPot(ix,iy,1)-bPot(ix,iy,2))/dz
                    ! 	  Ex(ix-1,iy-1,iz) = (bPot(ix-1,iy,iz)-Pot(ix+1,iy,iz))/dx/2.0
                    ! 	  Ey(ix-1,iy-1,iz) = (bPot(ix,iy-1,iz)-Pot(ix,iy+1,iz))/dy/2.0
                    if (edges(ix-1,iy-1,iz).eq.0) then
                        if (iz.gt.z_limit) then
                            Ex(ix-1,iy-1,iz) = (bPot(ix-1,iy,iz)-bPot(ix,iy,iz))/dx
                            Ey(ix-1,iy-1,iz) = (bPot(ix,iy-1,iz)-bPot(ix,iy,iz))/dy

                        else
                            Ex(ix-1,iy-1,iz) = (bPot(ix-1,iy,iz)-bPot(ix+1,iy,iz))/dx/2.0
                            Ey(ix-1,iy-1,iz) = (bPot(ix,iy-1,iz)-bPot(ix,iy+1,iz))/dy/2.0

                        end if
                    else
                        ! x-direction
                        if (b_objects(ix-1,iy,iz).gt.0.and.b_objects(ix+1,iy,iz).eq.0) then
                            ! surface in x direction positive
                            Ex(ix-1,iy-1,iz)=(bPot(ix,iy,iz)-bPot(ix+1,iy,iz))/dx
                        else if (b_objects(ix+1,iy,iz).gt.0.and.b_objects(ix-1,iy,iz).eq.0) then
                            ! surface in x direction negative
                            Ex(ix-1,iy-1,iz)=(bPot(ix-1,iy,iz)-bPot(ix,iy,iz))/dx
                        else
                            !Ex(ix-1,iy-1,iz) = (bPot(ix-1,iy,iz)-bPot(ix+1,iy,iz))/dx/2.0
                            Ex(ix-1,iy-1,iz) = (bPot(ix,iy,iz)-bPot(ix+1,iy,iz))/dx
                        end if
                        ! y-direction
                        if (b_objects(ix,iy-1,iz).gt.0.and.b_objects(ix,iy+1,iz).eq.0) then
                            ! surface in y direction positive
                            Ey(ix-1,iy-1,iz)=(bPot(ix,iy,iz)-bPot(ix,iy+1,iz))/dy
                        else if (b_objects(ix,iy+1,iz).gt.0.and.b_objects(ix,iy-1,iz).eq.0) then
                            ! surface in y direction negative
                            Ey(ix-1,iy-1,iz)=(bPot(ix,iy-1,iz)-bPot(ix,iy,iz))/dy
                        else
                            ! 					Ey(ix-1,iy-1,iz) = (bPot(ix,iy-1,iz)-bPot(ix,iy+1,iz))/dy/2.0
                            Ey(ix-1,iy-1,iz) = (bPot(ix,iy,iz)-bPot(ix,iy+1,iz))/dy

                        end if

                    end if

                elseif (iz.eq.Nz) then
                    Ez(ix-1,iy-1,Nz)=(bPot(ix,iy,Nz-1)-bPot(ix,iy,Nz))/dz
                    Ex(ix-1,iy-1,iz) = (bPot(ix-1,iy,iz)-bPot(ix+1,iy,iz))/dx/2.0
                    Ey(ix-1,iy-1,iz) = (bPot(ix,iy-1,iz)-bPot(ix,iy+1,iz))/dy/2.0
                else
                    ! middle of the space
                    if (edges(ix-1,iy-1,iz).eq.0) then

                        ! have nice averaging in the quasineutral space
                        if (iz.gt.z_limit) then
                            Ex(ix-1,iy-1,iz) = (bPot(ix-1,iy,iz)-bPot(ix+1,iy,iz))/dx/2.0
                            Ey(ix-1,iy-1,iz) = (bPot(ix,iy-1,iz)-bPot(ix,iy+1,iz))/dy/2.0
                            Ez(ix-1,iy-1,iz) = (bPot(ix,iy,iz-1)-bPot(ix,iy,iz+1))/dz/2.0
                        else
                            ! have precise E field along the electrodes
                            Ex(ix-1,iy-1,iz) = (bPot(ix,iy,iz)-bPot(ix+1,iy,iz))/dx
                            Ey(ix-1,iy-1,iz) = (bPot(ix,iy,iz)-bPot(ix,iy+1,iz))/dy
                            Ez(ix-1,iy-1,iz) = (bPot(ix,iy,iz)-bPot(ix,iy,iz+1))/dz

                        end if
                    else
                        if (b_objects(ix,iy,iz-1).gt.0.and.b_objects(ix,iy,iz+1).eq.0) then
                            ! surface in z direction positive
                            ! test of new formula
                            Ez(ix-1,iy-1,iz)=(bPot(ix,iy,iz)-bPot(ix,iy,iz+1))/dz
                            !   Ez(ix-1,iy-1,iz)=(-bPot(ix,iy,iz+2) + 4.0*bPot(ix,iy,iz+1) - 3.0*bPot(ix,iy,iz))/dz/2.0
                        else if (b_objects(ix,iy,iz+1).gt.0.and.b_objects(ix,iy,iz-1).eq.0) then
                            ! surface in z direction negative
                            Ez(ix-1,iy-1,iz)=(bPot(ix,iy,iz-1)-bPot(ix,iy,iz))/dz
                        else
                            Ez(ix-1,iy-1,iz) = (bPot(ix,iy,iz)-bPot(ix,iy,iz+1))/dz

                            ! 				Ez(ix-1,iy-1,iz) = (bPot(ix,iy,iz-1)-bPot(ix,iy,iz+1))/dz/2.0
                        end if
                        ! x-direction
                        if (b_objects(ix-1,iy,iz).gt.0.and.b_objects(ix+1,iy,iz).eq.0) then
                            ! surface in x direction positive
                            ! test of new formula
                            Ex(ix-1,iy-1,iz)=(bPot(ix,iy,iz)-bPot(ix+1,iy,iz))/dx
                            !	Ex(ix-1,iy-1,iz)=(-bPot(ix+2,iy,iz)+4.0*bPot(ix+1,iy,iz) - 3.0*bPot(ix,iy,iz))/dx/2.0

                        else if (b_objects(ix+1,iy,iz).gt.0.and.b_objects(ix-1,iy,iz).eq.0) then
                            ! surface in x direction negative

                            ! test of new formula
                            Ex(ix-1,iy-1,iz)=(bPot(ix-1,iy,iz)-bPot(ix,iy,iz))/dx
                            !	    	 Ex(ix-1,iy-1,iz)=-(-bPot(ix-2,iy,iz)+4.0*bPot(ix-1,iy,iz) - 3.0*bPot(ix,iy,iz))/dx/2.0

                        else
                            Ex(ix-1,iy-1,iz) = (bPot(ix,iy,iz)-bPot(ix+1,iy,iz))/dx

                            ! 		Ex(ix-1,iy-1,iz) = (bPot(ix-1,iy,iz)-bPot(ix+1,iy,iz))/dx/2.0
                        end if
                        ! y-direction
                        if (b_objects(ix,iy-1,iz).gt.0.and.b_objects(ix,iy+1,iz).eq.0) then
                            ! surface in y direction positive
                            ! test of new formula
                            Ey(ix-1,iy-1,iz)=(bPot(ix,iy,iz)-bPot(ix,iy+1,iz))/dy
                            !	    	 Ey(ix-1,iy-1,iz)=(-bPot(ix,iy+2,iz)+4.0*bPot(ix,iy+1,iz) - 3.0*bPot(ix,iy,iz))/dy/2.0

                        else if (b_objects(ix,iy+1,iz).gt.0.and.b_objects(ix,iy-1,iz).eq.0) then
                            ! surface in y direction negative
                            Ey(ix-1,iy-1,iz)=(bPot(ix,iy-1,iz)-bPot(ix,iy,iz))/dy
                            !	    	 Ey(ix-1,iy-1,iz)=-(-bPot(ix,iy-2,iz)+4.0*bPot(ix,iy-1,iz) - 3.0*bPot(ix,iy,iz))/dy/2.0

                        else
                            Ey(ix-1,iy-1,iz) = (bPot(ix,iy,iz)-bPot(ix,iy+1,iz))/dy

                            ! 		Ey(ix-1,iy-1,iz) = (bPot(ix,iy-1,iz)-bPot(ix,iy+1,iz))/dy/2.0
                        end if

                    end if
                endif
            enddo
        enddo
    end do
    !----------------------------------------------------------------------------------------

end subroutine

subroutine edge_detection(Nx,Ny,Nz,objects,edges,Nx_total,Ny_total,Nz_total,Nx_start,Ny_start,Nz_start)
    implicit none
    integer:: ix,iy,iz,Nx,Ny,Nz,s,free_cells,s1,s2,Nx_total,Ny_total,Nz_total,Nx_start,Ny_start,Nz_start
    integer, dimension(Nx,Ny,Nz):: edges
    integer, dimension(Nx_total,Ny_total,Nz_total):: objects

    integer, dimension(Nx_total+2,Ny_total+2,Nz_total):: b_objects
    b_objects = 0
    b_objects(2:(Nx_total+1),2:(Ny_total+1),:) = objects
    ! x periodic
    b_objects(1,2:(Ny_total+1),:) = objects(Nx_total,:,:)
    b_objects(Nx_total+2,2:(Ny_total+1),:) = objects(1,:,:)
    ! y periodic
    b_objects(2:(Nx_total+1),1,:) = objects(:,Ny_total,:)
    b_objects(2:(Nx_total+1),Ny_total+2,:) = objects(:,1,:)
    ! corner
    b_objects(1,1,:) = objects(Nx_total,Ny_total,:)
    b_objects(Nx_total+2,1,:) = objects(Nx_total,Ny_total,:)
    b_objects(1,Ny_total+2,:) = objects(Nx_total,Ny_total,:)
    b_objects(Nx_total+2,Ny_total+2,:) = objects(Nx_total,Ny_total,:)
    ! write(*,*) 'edge detection',sum(objects),sum(b_objects)
    ! write(*,*) 'objects 6 7',sum(objects(:,:,6)),sum(objects(:,:,7))
    ! write(*,*) 'Start',Nx_start,Ny_start,Nz_start
    ! write(*,*) 'DIM',Nx,Ny,Nz

    edges = 0
    do ix=2,Nx_total+1
        do iy=2,Ny_total+1
            do iz=2,Nz_total-1
                s = 0
                free_cells = 8
                if (ix.gt.Nx_start.and.ix.le.(Nx_start + Nx).and.iy.gt.Ny_start.and.iy.le.(Ny_start + Ny).and.iz.gt.Nz_start.and.iz.lt.(Nz_start + Nz)) then
                    ! check if we deal with surface  - it has to be within an object
                    if (b_objects(ix,iy,iz).gt.0) then
                        ! check 8 cells around the point to determine which is the surface type
                        ! cell 1
                        s1 = b_objects(ix-1,iy-1,iz -1)*b_objects(ix-1,iy,iz -1)*b_objects(ix,iy-1,iz -1)*b_objects(ix,iy,iz -1)
                        s2 = b_objects(ix-1,iy-1,iz)*b_objects(ix-1,iy,iz)*b_objects(ix,iy-1,iz)*b_objects(ix,iy,iz)
                        if (s1.ne.0.and.s2.ne.0) then
                            free_cells = free_cells - 1
                        end if
                        ! cell 2
                        s1 = b_objects(ix,iy-1,iz -1)*b_objects(ix,iy,iz -1)*b_objects(ix+1,iy-1,iz -1)*b_objects(ix+1,iy,iz -1)
                        s2 = b_objects(ix,iy-1,iz)*b_objects(ix,iy,iz)*b_objects(ix+1,iy-1,iz)*b_objects(ix+1,iy,iz)
                        if (s1.ne.0.and.s2.ne.0) then
                            free_cells = free_cells - 1
                        end if
                        ! cell 3
                        s1 = b_objects(ix,iy,iz -1)*b_objects(ix,iy+1,iz -1)*b_objects(ix+1,iy,iz -1)*b_objects(ix+1,iy+1,iz -1)
                        s2 =  b_objects(ix,iy,iz)*b_objects(ix,iy+1,iz)*b_objects(ix+1,iy,iz)*b_objects(ix+1,iy+1,iz)
                        if (s1.ne.0.and.s2.ne.0) then
                            free_cells = free_cells - 1
                        end if
                        ! cell 4
                        s1 = b_objects(ix-1,iy,iz -1)*b_objects(ix-1,iy+1,iz -1)*b_objects(ix,iy,iz -1)*b_objects(ix,iy+1,iz -1)
                        s2 = b_objects(ix-1,iy,iz)*b_objects(ix-1,iy+1,iz)*b_objects(ix,iy,iz)*b_objects(ix,iy+1,iz)
                        if (s1.ne.0.and.s2.ne.0) then
                            free_cells = free_cells - 1
                        end if
                        ! cell 5
                        s1 = b_objects(ix-1,iy-1,iz )*b_objects(ix-1,iy,iz )*b_objects(ix,iy-1,iz )*b_objects(ix,iy,iz )
                        s2 = b_objects(ix-1,iy-1,iz+1)*b_objects(ix-1,iy,iz+1)*b_objects(ix,iy-1,iz+1)*b_objects(ix,iy,iz+1)
                        if (s1.ne.0.and.s2.ne.0) then
                            free_cells = free_cells - 1
                        end if
                        ! cell 6
                        s1 = b_objects(ix,iy-1,iz)*b_objects(ix,iy,iz )*b_objects(ix+1,iy-1,iz )*b_objects(ix+1,iy,iz )
                        s2 = b_objects(ix,iy-1,iz+1)*b_objects(ix,iy,iz+1)*b_objects(ix+1,iy-1,iz+1)*b_objects(ix+1,iy,iz+1)
                        if (s1.ne.0.and.s2.ne.0) then
                            free_cells = free_cells - 1
                        end if
                        ! cell 7
                        s1 = b_objects(ix,iy,iz )*b_objects(ix,iy+1,iz )*b_objects(ix+1,iy,iz )*b_objects(ix+1,iy+1,iz )
                        s2 = b_objects(ix,iy,iz+1)*b_objects(ix,iy+1,iz+1)*b_objects(ix+1,iy,iz+1)*b_objects(ix+1,iy+1,iz+1)
                        if (s1.ne.0.and.s2.ne.0) then
                            free_cells = free_cells - 1
                        end if
                        ! cell 8
                        s1 = b_objects(ix-1,iy,iz )*b_objects(ix-1,iy+1,iz )*b_objects(ix,iy,iz )*b_objects(ix,iy+1,iz )
                        s2 = b_objects(ix-1,iy,iz+1)*b_objects(ix-1,iy+1,iz+1)*b_objects(ix,iy,iz+1)*b_objects(ix,iy+1,iz+1)
                        if (s1.ne.0.and.s2.ne.0) then
                            free_cells = free_cells - 1
                        end if
                        ! if (iz.eq.7) then
                        !   write(*,*) 'D',ix,iy,free_cells
                        !   write(*,*) 'P',ix -1 - Nx_start +1,iy -1 - Ny_start +1,free_cells
                        !
                        ! end if
                        ! now do the weight
                        !                       if (free_cells.gt.0) then
                        ! if (free_cells.eq.6) then
                        !     write(*,*) 'P',ix,iy,iz
                        ! end if
                        edges(ix-1 - Nx_start +1,iy-1- Ny_start +1,iz- Nz_start +1) =  free_cells
                        ! 		      end if
                    end if
                end if
            end do
        end do
    end do
    ! stop
    ! we do not allow any electrode to reach the top boundary, that would make little sense
    ! for the bottom boundary, we assuse 4 cells already taken as surface and other 4 are tested
    if (Nz_start.eq.1) then
        iz = 1
        do ix=2,Nx_total+1
            do iy=2,Ny_total+1
                s = 0
                free_cells = 4
                if (ix.gt.Nx_start.and.ix.le.(Nx_start + Nx).and.iy.gt.Ny_start.and.iy.le.(Ny_start + Ny).and.iz.gt.Nz_start.and.iz.lt.(Nz_start + Nz)) then

                    if (b_objects(ix,iy,iz).gt.0) then
                        ! cell 5
                        s = b_objects(ix-1,iy-1,iz )*b_objects(ix-1,iy,iz )*b_objects(ix,iy-1,iz )*b_objects(ix,iy,iz )*b_objects(ix-1,iy-1,iz+1)*b_objects(ix-1,iy,iz+1)*b_objects(ix,iy-1,iz+1)*b_objects(ix,iy,iz+1)
                        if (s.gt.0) then
                            free_cells = free_cells - 1
                        end if
                        ! cell 6
                        s = b_objects(ix,iy-1,iz)*b_objects(ix,iy,iz )*b_objects(ix+1,iy-1,iz )*b_objects(ix+1,iy,iz )*b_objects(ix,iy-1,iz+1)*b_objects(ix,iy,iz+1)*b_objects(ix+1,iy-1,iz+1)*b_objects(ix+1,iy,iz+1)
                        if (s.gt.0) then
                            free_cells = free_cells - 1
                        end if
                        ! cell 7
                        s = b_objects(ix,iy,iz )*b_objects(ix,iy+1,iz )*b_objects(ix+1,iy,iz )*b_objects(ix+1,iy+1,iz )*b_objects(ix,iy,iz+1)*b_objects(ix,iy+1,iz+1)*b_objects(ix+1,iy,iz+1)*b_objects(ix+1,iy+1,iz+1)
                        if (s.gt.0) then
                            free_cells = free_cells - 1
                        end if
                        ! cell 8
                        s = b_objects(ix-1,iy,iz )*b_objects(ix-1,iy+1,iz )*b_objects(ix,iy,iz )*b_objects(ix,iy+1,iz )*b_objects(ix-1,iy,iz+1)*b_objects(ix-1,iy+1,iz+1)*b_objects(ix,iy,iz+1)*b_objects(ix,iy+1,iz+1)
                        if (s.gt.0) then
                            free_cells = free_cells - 1
                        end if
                        ! now do the weight
                        !                       if (free_cells.gt.0) then
                        edges(ix-Nx_start,iy-Ny_start,1) =  free_cells
                        ! 		      end if
                    end if
                end if
            end do
        end do
    end if
    ! write(*,*) 'Edge detectio 6 7',sum(edges(:,:,6)),sum(edges(:,:,7))
end subroutine

! matrix <->  vector conversions for Poisson solver

subroutine matrix2vector_r(nx,ny,nz,m,v)
    integer:: nx,ny,nz,ix,iy,iz
    real, dimension(nx,ny,nz)::m
    real*8, dimension(nx*ny*nz):: v

    do ix=1,nx
        do iy=1,ny
            do iz=1,nz
                v(ix + (iy-1)*nx + (iz-1)*ny*nx) = m(ix,iy,iz)
            end do
        end do
    end do

end subroutine
subroutine matrix2vector_i(nx,ny,nz,m,v)
    integer:: nx,ny,nz,ix,iy,iz
    integer, dimension(nx,ny,nz)::m
    integer, dimension(nx*ny*nz):: v

    do ix=1,nx
        do iy=1,ny
            do iz=1,nz
                if (m(ix,iy,iz).gt.0) then
                    v(ix + (iy-1)*nx + (iz-1)*ny*nx) = 1
                else
                    v(ix + (iy-1)*nx + (iz-1)*ny*nx) = 0
                end if
            end do
        end do
    end do

end subroutine

subroutine vector2matrix_r(nx,ny,nz,m,v)
    integer:: nx,ny,nz,ix,iy,iz
    real, dimension(nx,ny,nz)::m
    real*8, dimension(nx*ny*nz):: v
    do ix=1,nx
        do iy=1,ny
            do iz=1,nz
                m(ix,iy,iz) = v(ix + (iy-1)*nx + (iz-1)*ny*nx)
            end do
        end do
    end do

end subroutine

! precision-independent math functions
!    function iint(r) result(i)
!   !converts real to integer
!         implicit none
!         real:: r
!         integer:: i
!       write(*,*) 'Got kind ',kind(r)
! 	if (kind(r).eq.8) then
! 	  ! double precision
! 	    i = idint(r)
! 	else
! 	    ! single precision
! 	    i = int(r)
! 	end if
!    end function iint
subroutine build_sort_id(ix1,iy1,iz1,sort_id,no_tot,Nx,Ny,Nz)
    integer:: no_tot,Nx,Ny,Nz,i
    integer,dimension(no_tot):: ix1,iy1,iz1,sort_id

    do i=1,no_tot
        sort_id(i) = ix1(i) + Nx*(iy1(i)-1)
    end do
end subroutine

subroutine bubble_sort(x,y,z,ux,uy,uz,ix1,iy1,iz1,stype,sort_id,no_tot,no_swaps)
    integer:: no_tot,no_swaps,i,t_ix1,t_iy1,t_iz1,t_stype,t_sort_id
    real:: t_x,t_y,t_z,t_ux,t_uy,t_uz
    integer,dimension(no_tot):: ix1,iy1,iz1,stype,sort_id
    real,dimension(no_tot):: x,y,z,ux,uy,uz
    no_swaps = 0
    ! run descending
    do i=1,no_tot-1
        if (sort_id(i).gt.sort_id(i+1)) then
            ! swap the 2 particles
            t_x = x(i)
            t_y = y(i)
            t_z = z(i)
            t_ux = ux(i)
            t_uy = uy(i)
            t_uz = uz(i)
            t_ix1 = ix1(i)
            t_iy1 = iy1(i)
            t_iz1 = iz1(i)
            t_stype = stype(i)
            t_sort_id = sort_id(i)
            ! one particle
            x(i) = x(i+1)
            y(i) = y(i+1)
            z(i) = z(i+1)
            ux(i) = ux(i+1)
            uy(i) = uy(i+1)
            uz(i) = uz(i+1)
            ix1(i) = ix1(i+1)
            iy1(i) = iy1(i+1)
            iz1(i) = iz1(i+1)
            stype(i) = stype(i+1)
            sort_id(i) = sort_id(i+1)
            ! other particle
            x(i+1) = t_x
            y(i+1) = t_y
            z(i+1) = t_z
            ux(i+1) = t_ux
            uy(i+1) = t_uy
            uz(i+1) = t_uz
            ix1(i+1) = t_ix1
            iy1(i+1) = t_iy1
            iz1(i+1) = t_iz1
            stype(i+1) = t_stype
            sort_id(i+1) = t_sort_id
            no_swaps  = no_swaps+1
        end if

    end do
    ! run ascending
    ! do i=no_tot,2,-1
    !       if (sort_id(i).lt.sort_id(i-1)) then
    ! 	  ! swap the 2 particles
    ! 	  t_x = x(i)
    ! 	  t_y = y(i)
    ! 	  t_z = z(i)
    ! 	  t_ux = ux(i)
    ! 	  t_uy = uy(i)
    ! 	  t_uz = uz(i)
    ! 	  t_ix1 = ix1(i)
    ! 	  t_iy1 = iy1(i)
    ! 	  t_iz1 = iz1(i)
    ! 	  t_stype = stype(i)
    ! 	  t_sort_id = sort_id(i)
    ! ! one particle
    ! 	x(i) = x(i-1)
    ! 	y(i) = y(i-1)
    ! 	z(i) = z(i-1)
    ! 	ux(i) = ux(i-1)
    ! 	uy(i) = uy(i-1)
    ! 	uz(i) = uz(i-1)
    ! 	ix1(i) = ix1(i-1)
    ! 	iy1(i) = iy1(i-1)
    ! 	iz1(i) = iz1(i-1)
    ! 	stype(i) = stype(i-1)
    ! 	sort_id(i) = sort_id(i-1)
    ! ! other particle
    ! 	x(i-1) = t_x
    ! 	y(i-1) = t_y
    ! 	z(i-1) = t_z
    ! 	ux(i-1) = t_ux
    ! 	uy(i-1) = t_uy
    ! 	uz(i-1) = t_uz
    ! 	ix1(i-1) = t_ix1
    ! 	iy1(i-1) = t_iy1
    ! 	iz1(i-1) = t_iz1
    ! 	stype(i-1) = t_stype
    ! 	sort_id(i-1) = t_sort_id
    ! no_swaps  = no_swaps+1
    !       end if
    !
    ! end do
    ! debug printout - first 100 particles
    do i=1,max(100,no_tot)
        write(*,*) 'SID',sort_id(i)
    end do

end subroutine

subroutine build_count_matrix(Nx,Ny,Nz,anp,max_cell_count,tot_cell_count,cell_count,model,x,y,z)
    implicit none
    include 'struct.h'
    type(model_t):: model
    integer:: anp,tot_cell_count,max_cell_count,Nx,Ny,Nz,ix,iy
    integer, dimension(Nz,Ny):: cell_count
    real, dimension(anp):: x,y,z
    integer:: i,j
    cell_count = 0
    do i=1,anp
        iy = idint(y(i)/model%dy) + 1
        ! lets try z instead of x
        ix = idint(z(i)/model%dz) + 1
        cell_count(ix,iy) = cell_count(ix,iy) + 1
        tot_cell_count = tot_cell_count +1
        if (cell_count(ix,iy).gt.max_cell_count) then
            max_cell_count = cell_count(ix,iy)
        end if
    end do


end subroutine



subroutine sort_particles(anp,x,y,z,ux,uy,uz,stype,max_cell_count,Nx,Ny,Nz,model)
    implicit none
    include 'struct.h'
    type(model_t):: model
    integer:: anp,max_cell_count,Ny,sp,Nx,no_diag_reg,Nz
    real, dimension(anp):: x,y,z,ux,uy,uz,tmp
    integer, dimension(anp):: key
    integer, dimension(Nz,Ny,max_cell_count):: sort_matrix
    integer, dimension(Nz,Ny):: cell_tmp
    integer, dimension(anp):: stype
    !integer, dimension(no_diag_reg):: particle_no
    integer:: i,j,k, k_max,a,pos,ix,iy,vac_pos
    cell_tmp = 0
    sort_matrix = 0
    key = 0
    ! build sort matrix
    !do sp=1,no_species
    do i=1,anp
        !	if (stype(i).eq.sp) then
        iy = idint(y(i)/model%dy) + 1
        ! lets try z instead of x
        ix = idint(z(i)/model%dz) + 1
        if (ix.gt.Nz.or.ix.lt.1) then
            write(*,*) 'Incorrect IX index',ix,z(i)
        end if
        if (iy.gt.Ny.or.iy.lt.1) then
            write(*,*) 'Incorrect IY index',iy,y(i)
        end if

        k = cell_tmp(ix,iy) +1
        if (k.gt.max_cell_count) then
            write(*,*) 'K exceeds sort matrix',k,max_cell_count,ix,iy

        end if
        cell_tmp(ix,iy)  = k
        sort_matrix(ix,iy,k) = i
        !	end if
    end do
    !end do
    ! sort the particles according to the sort matrix
    ! build the key
    if (.true.) then
        pos = 1
        do i=1,Nz
            do j=1,Ny
                do a=1,cell_tmp(i,j)
                    key(pos) = sort_matrix(i,j,a)
                    pos = pos +1
                end do
            end do
        end do
        ! copy over vectors
        do i=1,anp
            tmp(i) = x(key(i))
        end do
        x = tmp

        do i=1,anp
            tmp(i) = z(key(i))
        end do
        z = tmp

        do i=1,anp
            tmp(i) = y(key(i))
        end do
        y = tmp

        do i=1,anp
            tmp(i) = ux(key(i))
        end do
        ux = tmp

        do i=1,anp
            tmp(i) = uy(key(i))
        end do
        uy = tmp

        do i=1,anp
            tmp(i) = uz(key(i))
        end do
        uz = tmp
        do i=1,anp
            tmp(i) = stype(key(i))
        end do
        stype = tmp


    end if

    ! switch test particles
    ! do i=1,no_diag_reg
    ! 	if (particle_no(i).gt.0) then
    ! 		particle_no(i) = key(particle_no(i))
    ! 	end if
    ! end do

end subroutine
! blocks
subroutine adjust_group_potentials(count,no_objects,object_params,group_current,no_species,Nc,float_constant,i_rel_history,debug,verbose,objects,equipot,Nx,Ny,Nz)
    implicit none
    include 'struct.h'
    integer:: i,count,sp,no_objects,no_species,Nc,sp_start,Nx,Ny,Nz,ix,iy,iz,blocks,gp
    real:: i_rel_max,i_rel_treshold,i_rel
    type(object_t), dimension(no_objects):: object_params
    real, dimension(no_objects,no_species,1:count):: group_current
    real, dimension(no_objects):: float_constant
    real, dimension(no_objects,50):: i_rel_history
    integer, dimension(Nx,Ny,Nz):: objects
    real, dimension(Nx,Ny,Nz):: equipot
    real, dimension(no_species):: i_temp
    real:: max_i_temp

    logical:: debug,verbose

    !!!!!!! FLOATING POTENTIALS - THERE THEY GO
    ! make revision of currents flowing to them
    ! and adjust the potential if needed
    ! we have no unified  interface for objects so we go through all shapes
    i = 0
    i_rel_max = 0.0
    ! better for the calculation if we only adapt to some accuracy, inversion of the matrix takes a lot of time
    i_rel_treshold = 0.01
    verbose = .true.
    if (count.gt.101) then
        do gp=1,no_objects
            write(*,*) 'Adjusting group',gp
            if (sum(abs(group_current(gp,:,(count-101):count))).gt.0.0) then
                max_i_temp = 0.0
                do i=1,no_species
                    i_temp(i)  =  sum(abs(group_current(gp,i,(count-101):count)))
                    if (i_temp(i).gt.max_i_temp) then
                        max_i_temp = i_temp(i)
                    end if
                end do

                i_rel = sum(group_current(gp,:,(count-101):count))/max_i_temp
                !                 write(*,*) 'Calc i_rel',i_rel
                !                 write(*,*) 'float',i_rel,i_rel_treshold,count,Nc

            else
                i_rel = 0.0
            end if
            if (i_rel_max.lt.abs(i_rel)) then
                i_rel_max = abs(i_rel)
            end if

            if (abs(i_rel).gt.i_rel_treshold.and.count.gt.Nc) then
                ! loop over objects and set new potential if necessary
                do sp=1,no_objects
                    !              write(*,*) 'O:',object_params(sp)%param1,object_params(sp)%group

                    if (object_params(sp)%param1.eq.2.and.object_params(sp)%group.eq.gp) then
                        object_params(sp)%Pot = object_params(sp)%Pot + float_constant(gp)*i_rel
                        if (verbose) then
                            write(*,*) 'Changing Pot to ',object_params(sp)%Pot
                        end if

                    end if
                end do
                ! add to history
                call add_to_i_rel_history(i_rel,i_rel_history(gp,:))
                ! addapt the constant
                call adapt_float_constant(float_constant(gp),i_rel_history(gp,:))
                ! run through the area and fix the values
                do ix=1,Nx
                    do iy=1,Ny
                        do iz=1,Nz
                            if (objects(ix,iy,iz).gt.0) then
                                if(object_params(objects(ix,iy,iz))%param1.eq.2) then
                                    equipot(ix,iy,iz) = object_params(objects(ix,iy,iz))%Pot
                                end if
                            end if

                        end do
                    end do
                end do
            end if
        end do
    end if

end subroutine

subroutine adjust_iv_potentials(count,no_objects,object_params,no_species,Nc,debug,verbose,objects,equipot,Nx,Ny,Nz,v_act)
    implicit none
    include 'struct.h'
    integer:: i,count,sp,no_objects,no_species,Nc,sp_start,Nx,Ny,Nz,ix,iy,iz,blocks,gp
    real:: v_act
    type(object_t), dimension(no_objects):: object_params
    integer, dimension(Nx,Ny,Nz):: objects
    real, dimension(Nx,Ny,Nz):: equipot

    logical:: debug,verbose

    ! sweep potentials adjustment

    i = 0
    do sp=1,no_objects
        !              write(*,*) 'O:',object_params(sp)%param1,object_params(sp)%group

        if (object_params(sp)%param1.eq.3) then
            object_params(sp)%Pot = v_act
        end if
    end do
    ! add to history
    write(*,*) 'Potentials updated, recreating equipot'
    do ix=1,Nx
        do iy=1,Ny
            do iz=1,Nz
                if (objects(ix,iy,iz).gt.0) then
                    if(object_params(objects(ix,iy,iz))%param1.eq.3) then
                        equipot(ix,iy,iz) = object_params(objects(ix,iy,iz))%Pot
                    end if
                end if

            end do
        end do
    end do


end subroutine




subroutine add_to_i_rel_history(i_rel,history)
    implicit none
    real*8:: i_rel
    real*8, dimension(50):: history
    integer::i
    do i=2,50
        history(i -1) = history(i)
    end do
    history(50) = i_rel
end subroutine

subroutine adapt_float_constant(float_constant,history)
    implicit none
    real*8:: float_constant,prod
    real*8, dimension(10):: history
    integer:: i,test
    ! check if there are any zero members
    prod  =1.0
    do i=1,10
        prod = prod*history(i)
    end do

    if (prod.eq.0.0) then
        ! too few samples, no change now
    else
        ! check for monotone series
        test = 0
        do i=1,9
            if (history(i)*history(i+1) .lt. 0.0) then
                test = 1
            end if
        end do
        if (test.eq.0) then
            ! double the constant
            ! float_constant = float_constant*2.0
            write(*,*) 'Changing float_constant to', float_constant
        end if

        ! check for socilating series
        test = 0
        do i=1,9
            if (history(i)*history(i+1) .gt. 0.0) then
                test = 1
            end if
        end do
        if (test.eq.0) then
            ! half the constant
            float_constant = float_constant/2.0
            write(*,*) 'Changing float_constant to', float_constant

        end if





    end if




end subroutine
subroutine check_halfplane(ya,za,yb,zb,yc,zc,y1,z1,result1)
    implicit none
    real*8:: ya,za,yb,zb,yc,zc,yt,ytc,a,b,bc
    real:: y1,z1
    integer:: result1
    logical:: debug


    debug = .false.
    if (debug) then
        write(*,*) 'A: ', za,ya
        write(*,*) 'B: ', zb,yb
        write(*,*) 'C: ', zc,yc
        write(*,*) 'X: ', z1,y1





    end if


    if (zb.ne.za) then
        !regular line
        a = (yb - ya)/(zb - za)
        b = - (yb - ya)/(zb - za)*za + ya
        bc = -a*zc + yc
        yt = a*z1  + b
        ytc = a*z1 + bc
        if (debug) then
            write(*,*) 'a: ',a
            write(*,*) 'b: ',b
            write(*,*) 'bc: ',bc

        end if

        if (abs(yt - y1) .le. abs(yt - ytc).and.((yt.ge.y1.and.yt.ge.ytc).or.(yt.le.y1.and.yt.le.ytc))) then
            result1 = 1
        else
            result1 = 0
        end if
    else
        ! straight line
        if (sign(1.0,real(zc - za)) .eq. sign(1.0,z1 - real(za)).and.abs(zc - za) .ge. abs(z1 - za)) then
            result1 = 1
        else
            result1 = 0
        end if



    end if




end subroutine

