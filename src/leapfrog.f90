subroutine proto_move(x,y,z,ux,uy,uz,stype,no_tot,dt)
    integer:: no_tot
    real, dimension(no_tot):: x,y,z,ux,uy,uz
    integer, dimension(no_tot):: stype
    real:: dt
    ! move all the particles at once
    x = x + ux*dt
    y = y + uy*dt
    z = z + uz*dt




end subroutine

subroutine leapfrog_init(m,dt,bx,by,bz,q,ksi,Ax_lf,Bx_lf,Cx_lf,Dx_lf,Ex_lf,Fx_lf,Ay_lf,By_lf,&
        Cy_lf,Dy_lf,Ey_lf,Fy_lf,Az_lf,Bz_lf,Cz_lf,Dz_lf,Ez_lf,Fz_lf)
    implicit none
    real:: m,dt,bx,by,bz,q,ksi
    real:: Ax_lf,Bx_lf,Cx_lf,Dx_lf,Ex_lf,Fx_lf,Ay_lf,By_lf,Cy_lf,Dy_lf,Ey_lf,Fy_lf
    real:: Az_lf,Bz_lf,Cz_lf,Dz_lf,Ez_lf,Fz_lf
    real:: kx,ky,kz,k,dtu,mu2
    write(*,*) 'Received'
    write(*,*) 'B',bx,by,bz
    write(*,*) 'm',m
    write(*,*) 'q',q
    write(*,*) 'dt',dt


    k = m*( 4.0*m**2 + q**2*dt**2*(bx**2 + by**2 + bz**2))

    !------------LF----------------------------------
    Ax_lf=1.0/k*(4.0*m**3+ m*q**2*dt**2*(bx**2 - by**2 - bz**2))
    Bx_lf= 1.0/k*(2.0*q**2*dt**2*m*bx*by + 4.0*q*dt*bz*m**2)

    Cx_lf= 1.0/k*(2.0*q**2*dt**2*m*bz*bx - 4.0*q*dt*by*m**2)

    Dx_lf= 1.0/k*(q**3*dt**3*bx**2+ 4.0*q*dt*m**2)*ksi**2
    Ex_lf= 1.0/k*(q**3*dt**3*bx*by + 2.0*q**2*dt**2*m*bz)*ksi**2
    Fx_lf= 1.0/k*(q**3*dt**3*bx*bz - 2.0*q**2*dt**2*m*by)*ksi**2
    !        write(*,*) 'Fx',Fx_lf

    Ay_lf=1.0/k*(2.0*m*q**2*dt**2*bx*by - 4.0*m**2*q*dt*bz)
    By_lf=1.0/k*(4.0*m**3 + m*q**2*dt**2*(by**2 - bx**2 - bz**2))
    Cy_lf=1.0/k*(2.0*m*q**2*dt**2*bz*by + 4.0*m**2*q*dt*bx)
    Dy_lf=1.0/k*(q**3*dt**3*bx*by - 2.0*m*q**2*dt**2*bz)*ksi**2
    Ey_lf=1.0/k*(q**3*dt**3*by**2 + 4.0*m**2*q*dt)*ksi**2
    Fy_lf=1.0/k*(q**3*dt**3*bz*by + 2.0*m*q**2*dt**2*bx)*ksi**2

    Az_lf = 1.0/k*(2.0*q**2*dt**2*m*bx*bz + 4.0*q*dt*m**2*by)
    Bz_lf = 1.0/k*(2.0*q**2*dt**2*m*by*bz - 4.0*q*dt*m**2*bx)
    Cz_lf = 1.0/k*(4.0*m**3 + m*q**2*dt**2*(bz**2 - bx**2 - by**2))
    Dz_lf = 1.0/k*(q**3*dt**3*bx*bz + 2.0*q**2*dt**2*m*by)*ksi**2
    Ez_lf = 1.0/k*(q**3*dt**3*by*bz - 2.0*q**2*dt**2*m*bx)*ksi**2
    Fz_lf = 1.0/k*(q**3*dt**3*bz**2 + 4.0*q*dt*m**2)*ksi**2
    ! we start with old leapfrog - we want to see it the solver is right
    ! 	mu2 = 1/(m*m)x
    ! 	dtu = dt
    ! 	Ax_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2)*(8.d0+2.d0*mu2*(bx**2-by**2-bz**2)*dtu**2)
    ! 	Bx_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2)*(q/m*8.d0*bz*dtu + 4.d0*bx*by*dtu*dtu)
    ! 	Cx_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2)*(-q/m*8.d0*by*dtu + 4.d0*bx*bz*dtu*dtu)
    ! 	Dx_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2)*2.d0*ksi**2*mu2*dtu**2*(2.d0*bz + q/m*bx*by*dtu)
    ! 	Ex_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2)*2.d0*ksi**2*mu2*dtu**2*(2.d0*by - q/m*bx*bz*dtu)
    !
    ! 	Ay_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2)*(4.d0*bx*by*mu2*dtu*dtu - q/m*8.d0*bz*dtu)
    ! 	By_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2)*(8.d0+2.d0*mu2*(-bx**2+by**2-bz**2)*dtu**2)
    ! 	Cy_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2)*(q/m*8.d0*bx*dtu + 4.d0*by*bz*dtu*dtu*mu2)
    ! 	Dy_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2)*2.d0/m*ksi**2*dtu*(4.d0 + by**2*dtu**2*mu2)
    ! 	Ey_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2)*2.d0*ksi**2*mu2*dtu**2*(2.d0*bx + q/m*by*bz*dtu)
    ! 	Az_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2)*(8.d0*q/m*by*dtu + 4.d0*bx*bz*dtu*dtu*mu2)
    ! 	Bz_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2)*(4.d0*by*bz*dtu*dtu*mu2 - q/m*8.d0*bx*dtu)
    ! 	Cz_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2)*(8.d0+2.d0*mu2*(-bx**2-by**2+bz**2)*dtu**2)
    ! 	Dz_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2)*2.d0*ksi**2*mu2*dtu**2*(q/m*by*bz*dtu - 2.d0*bx)
    ! 	Ez_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2)*2.d0*ksi**2/m*dtu*(4.d0 + bz**2*dtu**2*mu2)
    !
end subroutine

subroutine leapfrog(no_tot,x,y,z,ux,uy,uz,stype,Ex,Ey,Ez,Ax_lf,Bx_lf,Cx_lf,Dx_lf,Ex_lf,Fx_lf,Ay_lf,By_lf,&
        Cy_lf,Dy_lf,Ey_lf,Fy_lf,Az_lf,Bz_lf,Cz_lf,Dz_lf,Ez_lf,Fz_lf,no_species,Nx,Ny,Nz,dt,model,q,spec_params,m,dx,dy,dz,x_old,y_old,z_old,use_old)
    include 'struct.h'
    type(model_t):: model
    type(pinfo), dimension(no_species):: spec_params
    integer:: no_tot,sp,no_species,i,j,k,ix,iy,iz,Nx,Ny,Nz
    real,dimension(no_tot):: x,y,z,ux,uy,uz,x_old,y_old,z_old
    real:: A1,A2,A3,A4,A5,A6,A7,A8,rx,ry,rz
    integer, dimension(no_tot):: stype
    real, dimension(Nx,Ny,Nz):: Ex,Ey,Ez
    real, dimension(no_species):: Ax_lf,Bx_lf,Cx_lf,Dx_lf,Ex_lf,Fx_lf,Ay_lf,By_lf,Cy_lf,Dy_lf,Ey_lf,Fy_lf
    real,dimension(no_species):: Az_lf,Bz_lf,Cz_lf,Dz_lf,Ez_lf,Fz_lf,q,m
    real:: ux_temp,uy_temp,uz_temp,Exa,Eya,Eza,dt,ux_old,uy_old,uz_old,Epar,vx,vy,vz,vpar,vperp1,vperp2,sqbxby,dx,dy,dz
    logical:: use_old
    sqbxby = sqrt(model%bx*model%bx + model%by*model%by)
    do i=1,no_tot
        sp = stype(i)
        if (use_old) then
            x_old(i) = x(i)
            y_old(i) = y(i)
            z_old(i) = z(i)
        end if
        if (spec_params(stype(i))%motion_method.eq.1) then
            ! gyrokinetic motion

            if (z(i).lt.max(0.0,(model%Lz_grid - model%Lz_start)).and.z(i).gt.0.0) then

                ix = idint(x(i)/model%dx) +1
                iy = idint(y(i)/model%dy) +1
                iz = idint(z(i)/model%dz) +1

                rx = x(i)/dx - ix +1
                ry = y(i)/dy - iy+1
                rz = z(i)/dz - iz+1
                A1 = (1.0 - rx)*(1.0 - ry)*(1.0 - rz)
                A2 = (1.0 - rx)*ry*(1.0 - rz)
                A3 = rx*ry*(1.0 - rz)
                A4 = rx*(1.0 - ry)*(1.0 - rz)
                A5 = (1.0 - rx)*(1.0 - ry)*rz
                A6 = (1.0 - rx)*ry*rz
                A7 = rx*ry*rz
                A8 = rx*(1.0 - ry)*rz


                Exa = Ex(ix,iy,iz)*A1 +Ex(ix,iy+1,iz)*A2 +  Ex(ix+1,iy+1,iz)*A3 + Ex(ix+1,iy,iz)*A4 &
                    +Ex(ix,iy,iz+1)*A5 +Ex(ix,iy+1,iz+1)*A6 +  Ex(ix+1,iy+1,iz+1)*A7 + Ex(ix+1,iy,iz+1)*A8
                Eya = Ey(ix,iy,iz)*A1 +Ey(ix,iy+1,iz)*A2 +  Ey(ix+1,iy+1,iz)*A3 + Ey(ix+1,iy,iz)*A4 &
                    +Ey(ix,iy,iz+1)*A5 +Ey(ix,iy+1,iz+1)*A6 +  Ey(ix+1,iy+1,iz+1)*A7 + Ey(ix+1,iy,iz+1)*A8
                Eza = Ez(ix,iy,iz)*A1 +Ez(ix,iy+1,iz)*A2 +  Ez(ix+1,iy+1,iz)*A3 + Ez(ix+1,iy,iz)*A4 &
                    +Ez(ix,iy,iz+1)*A5 +Ez(ix,iy+1,iz+1)*A6 +  Ez(ix+1,iy+1,iz+1)*A7 + Ez(ix+1,iy,iz+1)*A8
            else
                Exa = 0.0
                Eya = 0.0
                Eza = 0.0


            end if
            ! first get the Epar component
            Epar = model%bx*Exa + model%by*Eya + model%bz*Eza

            ! now the ExB drift
            vx = (Eya*model%bz - Eza*model%by)*model%ksi*model%ksi
            vy  = (Eza*model%bx - Exa*model%bz)*model%ksi*model%ksi
            vz  = (Exa*model%by - Eya*model%bx)*model%ksi*model%ksi
            ! check that the parallel component of the drift is close to zero
            vpar = vx*model%bx +  vy*model%by +  vz*model%bz
            ! write(*,*) 'Vpar check',vpar,ux(i)
            ! construct the drift velocities
            if (sqbxby.gt.0.0) then
                vperp1 = -vx*model%by/sqbxby + vy*model%bx/sqbxby
                vperp2 = -vx*model%bx*model%bz/sqbxby - vy*model%by*model%bz/sqbxby +vz*sqbxby
            else
                vperp1 = vx
                vperp2 = vy
            end if
            ! get the new vpar
            ux(i) = model%dt*q(stype(i))/m(stype(i))*model%ksi*model%ksi*Epar + ux(i)
            uy(i) = vperp1
            uz(i) = vperp2

            ! now move the particle, this is the tricky bit
            if (sqbxby.gt.0.0) then
                x(i) = x(i) +  (ux(i)*model%bx - uy(i)*model%by/sqbxby - uz(i)*model%bx*model%bz/sqbxby)*model%dt

                y(i) = y(i) + (ux(i)* model%by + uy(i)*model%bx/sqbxby - uz(i)*model%by*model%bz/sqbxby)*model%dt

                z(i) = z(i) + (ux(i)*model%bz + uz(i)*sqbxby)*model%dt



            else
                x(i) = x(i)    + uy(i)*model%dt

                y(i) = y(i) + uz(i)*model%dt

                z(i) = z(i) + ux(i)*model%bz*model%dt

                ! if (x(i).lt.0.0.or.y(i).lt.0.0.or.z(i).lt.0.0) then
                !       write(*,*) 'leapfrog position error'
                !       write(*,*) 'Pos:',x(i),y(i),z(i)
                !       write(*,*) 'Vel:',ux(i),uy(i),uz(i)
                !       write(*,*) 'index:',i,stype(i)
                !
                !
                ! end if

            end if

        else
            ! regular motion
            if (z(i).lt.max(0.0,(model%Lz_grid - model%Lz_start)).and.z(i).gt.0.0) then

                ix = idint(x(i)/model%dx) +1
                iy = idint(y(i)/model%dy) +1
                iz = idint(z(i)/model%dz) +1
                rx = x(i)/dx - ix +1
                ry = y(i)/dy - iy+1
                rz = z(i)/dz - iz+1
                A1 = (1.0 - rx)*(1.0 - ry)*(1.0 - rz)
                A2 = (1.0 - rx)*ry*(1.0 - rz)
                A3 = rx*ry*(1.0 - rz)
                A4 = rx*(1.0 - ry)*(1.0 - rz)
                A5 = (1.0 - rx)*(1.0 - ry)*rz
                A6 = (1.0 - rx)*ry*rz
                A7 = rx*ry*rz
                A8 = rx*(1.0 - ry)*rz

                Exa = Ex(ix,iy,iz)*A1 +Ex(ix,iy+1,iz)*A2 +  Ex(ix+1,iy+1,iz)*A3 + Ex(ix+1,iy,iz)*A4 &
                    +Ex(ix,iy,iz+1)*A5 +Ex(ix,iy+1,iz+1)*A6 +  Ex(ix+1,iy+1,iz+1)*A7 + Ex(ix+1,iy,iz+1)*A8
                Eya = Ey(ix,iy,iz)*A1 +Ey(ix,iy+1,iz)*A2 +  Ey(ix+1,iy+1,iz)*A3 + Ey(ix+1,iy,iz)*A4 &
                    +Ey(ix,iy,iz+1)*A5 +Ey(ix,iy+1,iz+1)*A6 +  Ey(ix+1,iy+1,iz+1)*A7 + Ey(ix+1,iy,iz+1)*A8
                Eza = Ez(ix,iy,iz)*A1 +Ez(ix,iy+1,iz)*A2 +  Ez(ix+1,iy+1,iz)*A3 + Ez(ix+1,iy,iz)*A4 &
                    +Ez(ix,iy,iz+1)*A5 +Ez(ix,iy+1,iz+1)*A6 +  Ez(ix+1,iy+1,iz+1)*A7 + Ez(ix+1,iy,iz+1)*A8

                ux_temp  = ux(i)*Ax_lf(sp) + uy(i)*Bx_lf(sp) + uz(i)*Cx_lf(sp) + Exa*Dx_lf(sp) + Eya*Ex_lf(sp) + Eza*Fx_lf(sp)
                uy_temp  = ux(i)*Ay_lf(sp) + uy(i)*By_lf(sp) + uz(i)*Cy_lf(sp) + Exa*Dy_lf(sp) + Eya*Ey_lf(sp) + Eza*Fy_lf(sp)
                uz_temp  = ux(i)*Az_lf(sp) + uy(i)*Bz_lf(sp) + uz(i)*Cz_lf(sp) + Exa*Dz_lf(sp) + Eya*Ez_lf(sp) + Eza*Fz_lf(sp)
                ! 	  ux_temp  = ux(i)*Ax_lf(sp) + uy(i)*Bx_lf(sp) + uz(i)*Cx_lf(sp)  + Eya*Dx_lf(sp) - Eza*Ex_lf(sp)
                ! 	  uy_temp  = ux(i)*Ay_lf(sp) + uy(i)*By_lf(sp) + uz(i)*Cy_lf(sp)  +q(sp)*Eya*Dy_lf(sp) + Eza*Ey_lf(sp)
                ! 	  uz_temp  = ux(i)*Az_lf(sp) + uy(i)*Bz_lf(sp) + uz(i)*Cz_lf(sp)  + Eya*Dz_lf(sp) + q(sp)*Eza*Ez_lf(sp)

            else
                !write(*,*) 'Box movement',stype(i),x(i),y(i),z(i)
                ux_temp  = ux(i)*Ax_lf(sp) + uy(i)*Bx_lf(sp) + uz(i)*Cx_lf(sp)
                uy_temp  = ux(i)*Ay_lf(sp) + uy(i)*By_lf(sp) + uz(i)*Cy_lf(sp)
                uz_temp  = ux(i)*Az_lf(sp) + uy(i)*Bz_lf(sp) + uz(i)*Cz_lf(sp)


            end if
            ! 	ux_old = ux(i)
            ! 	uy_old = uy(i)
            ! 	uz_old = uz(i)

            ux(i)=ux_temp
            uy(i)=uy_temp
            uz(i)=uz_temp
            z(i) = z(i) + dt*uz(i)
            y(i) = y(i) + dt*uy(i)
            x(i) = x(i) + dt*ux(i)
            ! if (isnan(ux(i))) then
            ! write(*,*) 'leapfrog error!'
            ! 	write(*,*) 'pos',x(i),y(i),z(i)
            ! 	write(*,*) 'vel',ux(i),uy(i),uz(i)
            ! 	write(*,*) 'old',ux_old,uy_old,uz_old
            ! 	write(*,*) 'LP',Ax_lf(sp),Bx_lf(sp),Cx_lf(sp)
            ! 	write(*,*) 'LP2',Dx_lf(sp),Ex_lf(sp),Fx_lf(sp)
            !
            ! 	write(*,*) 'E',Exa,Eya,Eza
            !
            ! stop
            ! end if
        end if ! gyrokinetics
    end do
    ! if (no_tot.gt.1) then
    ! write(*,*) 'Pos:',x(no_tot),y(no_tot),z(no_tot)
    ! write(*,*) 'Vel:',ux(no_tot),uy(no_tot),uz(no_tot)
    !
    !
    ! end if

end subroutine
