! subroutines concerning communication among processors

! sending charge density to the first processor
subroutine send_rho(rho_tot,no_species,Nx,Ny,Nz,proc_no,mpi_rank,edge_charge,have_dielectrics,objects_current,no_objects)
    implicit none
    include 'mpif.h'

    integer:: no_species,Nx,Ny,Nz,proc_no,i,sp,ierr,no_objects
    integer, dimension(no_species):: mpi_rank
    real, dimension(Nx,Ny,Nz):: rho_tot
    real, dimension(Nx,Ny,Nz):: edge_charge
    real, dimension(no_objects,no_species,1):: objects_current
    logical:: debug,have_dielectrics
    integer:: par_status(MPI_STATUS_SIZE)

    debug = .false.
    ierr = 0
    par_status = 0

    !sender
    i = 18
    if (debug) then
        write(*,*) proc_no,'Sending rho ',proc_no,sum(abs(rho_tot))
        write(*,*) 'Sending array of size',proc_no,Nx,Ny,Nz
    end if

    call MPI_SEND(rho_tot,Nx*Ny*Nz,MPI_DOUBLE_PRECISION,0,i*proc_no,MPI_COMM_WORLD,ierr)

    if (have_dielectrics) then
        i = 19
        call MPI_SEND(edge_charge,Nx*Ny*Nz,MPI_DOUBLE_PRECISION,0,i*proc_no,MPI_COMM_WORLD,ierr)
        if (debug) then
            write(*,*) 'Sending edge_charge',proc_no,sum(abs(edge_charge))
        end if
    end if
    ! send objects_current
    i = 20
    call MPI_SEND(objects_current,no_objects*no_species,MPI_DOUBLE_PRECISION,0,i*proc_no,MPI_COMM_WORLD,ierr)
    if (debug) then
        write(*,*) 'Sending objectscurrent',proc_no,sum(abs(objects_current))
    end if


end subroutine



! receiving charge density - first processor
subroutine receive_rho(rho_tot,no_species,Nx,Ny,Nz,proc_no,mpi_rank,edge_charge,proc_max,have_dielectrics,model,Nx_total,Ny_total,Nz_total,slices,objects_current,no_objects)
    implicit none
    include 'mpif.h'
    include 'struct.h'

    integer:: no_species,Nx,Ny,Nz,proc_no,i,sp,ierr,proc_max,p,Nx_total,Ny_total,Nz_total,Nx_length,Ny_length,Nz_length,no_objects
    integer, dimension(no_species):: mpi_rank
    real, dimension(Nx_total,Ny_total,Nz_total):: rho_tot,edge_charge
    real, dimension(Nx,Ny,Nz)::blank_edge_charge
    real,dimension(Nx_total,Ny_total,Nz_total):: dumr
    logical:: debug,have_dielectrics
    integer:: par_status(MPI_STATUS_SIZE)
    type(model_t):: model
    type(slice_info), dimension(proc_max+1):: slices
    real, dimension(no_objects,no_species,1) :: objects_current, dumo
    debug = .false.
    ierr = 0
    par_status = 0
    blank_edge_charge = 0.0
    dumr = 0.0
    do p=1,proc_max
        !sender
        i = 18
        Nx_length = slices(p+1)%Nx_stop -slices(p+1)%Nx_start
        Ny_length = slices(p+1)%Ny_stop -slices(p+1)%Ny_start
        Nz_length = slices(p+1)%Nz_stop -slices(p+1)%Nz_start

        if (debug) then
            write(*,*) 'Expecting array of size',Nx_length+1,Ny_length+1,Nz_length+1
        end if
        call MPI_RECV(dumr(1:(Nx_length+1),1:(Ny_length+1),1:(Nz_length+1)),(Nx_length+1)*(Ny_length+1)*(Nz_length+1),MPI_DOUBLE_PRECISION,p,i*p,MPI_COMM_WORLD,par_status,ierr)
        rho_tot(slices(p+1)%Nx_start:slices(p+1)%Nx_stop,slices(p+1)%Ny_start:slices(p+1)%Ny_stop,slices(p+1)%Nz_start:slices(p+1)%Nz_stop) = rho_tot(slices(p+1)%Nx_start:slices(p+1)%Nx_stop,slices(p+1)%Ny_start:slices(p+1)%Ny_stop,slices(p+1)%Nz_start:slices(p+1)%Nz_stop) + dumr(1:(Nx_length+1),1:(Ny_length+1),1:(Nz_length+1))
        if (debug) then
            write(*,*) 'Received rho',p,sum(abs(dumr(1:Nx_length,1:Ny_length,1:Nz_length)))
        end if
        if  (have_dielectrics) then
            ! write(*,*) 'Waiting for the edge charge from proc',proc_no
            !sender
            i = 19
            call MPI_RECV(dumr(1:(Nx_length+1),1:(Ny_length+1),1:(Nz_length+1)),(Nx_length+1)*(Ny_length+1)*(Nz_length+1),MPI_DOUBLE_PRECISION,p,i*p,MPI_COMM_WORLD,par_status,ierr)
            if (debug) then
                write(*,*) 'Received edge_charge',p,sum(abs(dumr(1:(Nx_length+1),1:(Ny_length+1),1:(Nz_length+1))))
            end if
            edge_charge(slices(p+1)%Nx_start:slices(p+1)%Nx_stop,slices(p+1)%Ny_start:slices(p+1)%Ny_stop,slices(p+1)%Nz_start:slices(p+1)%Nz_stop) = edge_charge(slices(p+1)%Nx_start:slices(p+1)%Nx_stop,slices(p+1)%Ny_start:slices(p+1)%Ny_stop,slices(p+1)%Nz_start:slices(p+1)%Nz_stop) + dumr(1:(Nx_length+1),1:(Ny_length+1),1:(Nz_length+1))
        end if
        i = 20
        call MPI_RECV(dumo,no_species*no_objects,MPI_DOUBLE_PRECISION,p,i*p,MPI_COMM_WORLD,par_status,ierr)
        objects_current = objects_current + dumo
        if (debug) then
            write(*,*) 'Received objects_current',p,sum(abs(dumo))
        end if

    end do
end subroutine

! First processor sends out new Potential
subroutine send_pot(Pot,Nx,Ny,Nz,proc_max)
    implicit none
    include 'mpif.h'

    integer:: Nx,Ny,Nz,proc_max,proc_no,ierr,i
    real, dimension(Nx,Ny,Nz):: Pot
    logical:: debug
    integer:: par_status(MPI_STATUS_SIZE)

    debug = .false.
    ierr = 0
    par_status = 0

    do proc_no=1,proc_max
        i = 20
        call MPI_SEND(Pot,Nx*Ny*Nz,MPI_DOUBLE_PRECISION,proc_no,i*proc_no,MPI_COMM_WORLD,ierr)
        if (debug) then
            write(*,*) 'Sending Pot',proc_no,sum(abs(Pot))
        end if
    end do
end subroutine

! Receiving the new potential
subroutine receive_pot(Pot,Nx,Ny,Nz,proc_no)
    implicit none
    include 'mpif.h'

    integer:: Nx,Ny,Nz,proc_no,ierr,i
    real, dimension(Nx,Ny,Nz):: Pot
    logical:: debug
    integer:: par_status(MPI_STATUS_SIZE)
    debug = .false.
    ierr = 0
    par_status = 0
    if (debug) then
        write(*,*) 'Expecting Pot',Nx,Ny,Nz
    end if
    i = 20
    call MPI_RECV(Pot,Nx*Ny*Nz,MPI_DOUBLE_PRECISION,0,i*proc_no,MPI_COMM_WORLD,par_status,ierr)
    if (debug) then
        write(*,*) 'Received Pot',proc_no,sum(abs(Pot))
    end if
end subroutine

! receiving averaged diagnostics by first processor
subroutine receive_final_diag(no_species,Nx,Ny,Nz,mpi_rank,dens,vxav,vyav,vzav,vx2av,vy2av,vz2av,vx3av,vy3av,vz3av,debug,edge_flux,edge_energy_flux,model,q)
    implicit none
    include 'mpif.h'
    include 'struct.h'
    integer:: i,no_species,Nx,Ny,Nz,sp,ierr,p,proc_max,ix1,iy1,iz1
    real, dimension(no_species, Nx,Ny,Nz):: dens,vxav,vyav,vzav,vx2av,vy2av,vz2av,vx3av,vy3av,vz3av,edge_flux,edge_energy_flux
    real, dimension(Nx,Ny,Nz):: dumg
    integer, dimension(no_species):: mpi_rank
    real,dimension(no_species)::q
    logical:: debug,last_transfer
    type(model_t):: model
    integer par_status(MPI_STATUS_SIZE)
    ierr= 0
    par_status = 0
    proc_max = model%proc_max
    dumg = 0.0
    ! debug = .true.
    ! loop over species
    do sp=1,no_species
        i = 141

        if (mpi_rank(sp).ne.0) then
            call MPI_RECV(dens(sp,:,:,:),Nx*Ny*Nz,MPI_DOUBLE_PRECISION,mpi_rank(sp),i*sp,MPI_COMM_WORLD,par_status,ierr)


        elseif (model%automatic_particle_decomposition) then

            do p=1,proc_max
                call MPI_RECV(dumg,Nx*Ny*Nz,MPI_DOUBLE_PRECISION,p,i*sp,MPI_COMM_WORLD,par_status,ierr)
                dens(sp,:,:,:) = dens(sp,:,:,:) + dumg
                if (debug) then
                    write(*,*) 'Received dens',sp,sum(abs(dumg))
                end if
            end do
        end if
        i = i+ 1

        if (mpi_rank(sp).ne.0) then
            call MPI_RECV(vzav(sp,:,:,:),Nx*Ny*Nz,MPI_DOUBLE_PRECISION,mpi_rank(sp),i*sp,MPI_COMM_WORLD,par_status,ierr)


        elseif(model%automatic_particle_decomposition) then

            do p=1,proc_max
                call MPI_RECV(dumg,Nx*Ny*Nz,MPI_DOUBLE_PRECISION,p,i*sp,MPI_COMM_WORLD,par_status,ierr)
                vzav(sp,:,:,:) = vzav(sp,:,:,:) + dumg
                if (debug) then
                    write(*,*) 'Received vzav',sp,sum(abs(dumg))
                end if
            end do
        end if

        i = i+ 1
        if (mpi_rank(sp).ne.0) then
            call MPI_RECV(vyav(sp,:,:,:),Nx*Ny*Nz,MPI_DOUBLE_PRECISION,mpi_rank(sp),i*sp,MPI_COMM_WORLD,par_status,ierr)
        elseif(model%automatic_particle_decomposition) then

            do p=1,proc_max
                call MPI_RECV(dumg,Nx*Ny*Nz,MPI_DOUBLE_PRECISION,p,i*sp,MPI_COMM_WORLD,par_status,ierr)
                vyav(sp,:,:,:) = vyav(sp,:,:,:) + dumg
                if (debug) then
                    write(*,*) 'Received vyav',sp,sum(abs(dumg))
                end if
            end do
        end if

        i = i+ 1
        if (mpi_rank(sp).ne.0) then
            call MPI_RECV(vxav(sp,:,:,:),Nx*Ny*Nz,MPI_DOUBLE_PRECISION,mpi_rank(sp),i*sp,MPI_COMM_WORLD,par_status,ierr)
        elseif(model%automatic_particle_decomposition) then

            do p=1,proc_max
                call MPI_RECV(dumg,Nx*Ny*Nz,MPI_DOUBLE_PRECISION,p,i*sp,MPI_COMM_WORLD,par_status,ierr)
                vxav(sp,:,:,:) = vxav(sp,:,:,:) + dumg
                if (debug) then
                    write(*,*) 'Received vxav',sp,sum(abs(dumg))
                end if
            end do
        end if

        i = i+ 1

        if (mpi_rank(sp).ne.0) then
            call MPI_RECV(vz2av(sp,:,:,:),Nx*Ny*Nz,MPI_DOUBLE_PRECISION,mpi_rank(sp),i*sp,MPI_COMM_WORLD,par_status,ierr)


        elseif(model%automatic_particle_decomposition) then

            do p=1,proc_max
                call MPI_RECV(dumg,Nx*Ny*Nz,MPI_DOUBLE_PRECISION,p,i*sp,MPI_COMM_WORLD,par_status,ierr)
                vz2av(sp,:,:,:) = vz2av(sp,:,:,:) + dumg
                if (debug) then
                    write(*,*) 'Received vz2av',sp,sum(abs(dumg))
                end if
            end do
        end if

        i = i+ 1
        if (mpi_rank(sp).ne.0) then
            call MPI_RECV(vy2av(sp,:,:,:),Nx*Ny*Nz,MPI_DOUBLE_PRECISION,mpi_rank(sp),i*sp,MPI_COMM_WORLD,par_status,ierr)
        elseif(model%automatic_particle_decomposition) then

            do p=1,proc_max
                call MPI_RECV(dumg,Nx*Ny*Nz,MPI_DOUBLE_PRECISION,p,i*sp,MPI_COMM_WORLD,par_status,ierr)
                vy2av(sp,:,:,:) = vy2av(sp,:,:,:) + dumg
                if (debug) then
                    write(*,*) 'Received vy2av',sp,sum(abs(dumg))
                end if
            end do
        end if

        i = i+ 1
        if (mpi_rank(sp).ne.0) then
            call MPI_RECV(vx2av(sp,:,:,:),Nx*Ny*Nz,MPI_DOUBLE_PRECISION,mpi_rank(sp),i*sp,MPI_COMM_WORLD,par_status,ierr)
        elseif(model%automatic_particle_decomposition) then

            do p=1,proc_max
                call MPI_RECV(dumg,Nx*Ny*Nz,MPI_DOUBLE_PRECISION,p,i*sp,MPI_COMM_WORLD,par_status,ierr)
                vx2av(sp,:,:,:) = vx2av(sp,:,:,:) + dumg
                if (debug) then
                    write(*,*) 'Received vx2av',sp,sum(abs(dumg))
                end if
            end do
        end if

        i = i+ 1

        if (mpi_rank(sp).ne.0) then
            call MPI_RECV(vz3av(sp,:,:,:),Nx*Ny*Nz,MPI_DOUBLE_PRECISION,mpi_rank(sp),i*sp,MPI_COMM_WORLD,par_status,ierr)


        elseif(model%automatic_particle_decomposition) then

            do p=1,proc_max
                call MPI_RECV(dumg,Nx*Ny*Nz,MPI_DOUBLE_PRECISION,p,i*sp,MPI_COMM_WORLD,par_status,ierr)
                vz3av(sp,:,:,:) = vz3av(sp,:,:,:) + dumg
                if (debug) then
                    write(*,*) 'Received vz3av',sp,sum(abs(dumg))
                end if
            end do
        end if

        i = i+ 1
        if (mpi_rank(sp).ne.0) then
            call MPI_RECV(vy3av(sp,:,:,:),Nx*Ny*Nz,MPI_DOUBLE_PRECISION,mpi_rank(sp),i*sp,MPI_COMM_WORLD,par_status,ierr)
        elseif(model%automatic_particle_decomposition) then

            do p=1,proc_max
                call MPI_RECV(dumg,Nx*Ny*Nz,MPI_DOUBLE_PRECISION,p,i*sp,MPI_COMM_WORLD,par_status,ierr)
                vy3av(sp,:,:,:) = vy3av(sp,:,:,:) + dumg
                if (debug) then
                    write(*,*) 'Received vy3av',sp,sum(abs(dumg))
                end if
            end do
        end if

        i = i+ 1
        if (mpi_rank(sp).ne.0) then
            call MPI_RECV(vx3av(sp,:,:,:),Nx*Ny*Nz,MPI_DOUBLE_PRECISION,mpi_rank(sp),i*sp,MPI_COMM_WORLD,par_status,ierr)
        elseif(model%automatic_particle_decomposition) then

            do p=1,proc_max
                call MPI_RECV(dumg,Nx*Ny*Nz,MPI_DOUBLE_PRECISION,p,i*sp,MPI_COMM_WORLD,par_status,ierr)
                vx3av(sp,:,:,:) = vx3av(sp,:,:,:) + dumg
                if (debug) then
                    write(*,*) 'Received vx3av',sp,sum(abs(dumg))
                end if
            end do
        end if




        i = i+ 1
        if (mpi_rank(sp).ne.0) then
            call MPI_RECV(edge_flux(sp,:,:,:),Nx*Ny*Nz,MPI_DOUBLE_PRECISION,p,i*sp,MPI_COMM_WORLD,par_status,ierr)
        elseif(model%automatic_particle_decomposition) then

            do p=1,proc_max
                call MPI_RECV(dumg,Nx*Ny*Nz,MPI_DOUBLE_PRECISION,p,i*sp,MPI_COMM_WORLD,par_status,ierr)
                edge_flux(sp,:,:,:) = edge_flux(sp,:,:,:) + dumg
                if (debug) then
                    write(*,*) 'Received edge_flux',sp,sum(abs(dumg))
                end if
            end do
        end if
        i = i+ 1
        if (mpi_rank(sp).ne.0) then
            call MPI_RECV(edge_energy_flux(sp,:,:,:),Nx*Ny*Nz,MPI_DOUBLE_PRECISION,mpi_rank(sp),i*sp,MPI_COMM_WORLD,par_status,ierr)
        elseif(model%automatic_particle_decomposition) then

            do p=1,proc_max
                call MPI_RECV(dumg,Nx*Ny*Nz,MPI_DOUBLE_PRECISION,p,i*sp,MPI_COMM_WORLD,par_status,ierr)
                edge_energy_flux(sp,:,:,:) = edge_energy_flux(sp,:,:,:) + dumg
                if (debug) then
                    write(*,*) 'Received edge_energy_flux',sp,sum(abs(dumg))
                end if
            end do
        end if


    end do

    ! now we need to fix the velocities
    if (model%automatic_particle_decomposition) then
        write(*,*) 'Velocity factor:',(model%dx*model%dy*model%dz*real(model%N0))
        do sp=1,no_species
            do ix1=1,Nx
                do iy1=1,Ny
                    do iz1=1,Nz
                        if (dens(sp,ix1,iy1,iz1).ne.0.0) then
                            vxav(sp,ix1,iy1,iz1) = vxav(sp,ix1,iy1,iz1)/abs(dens(sp,ix1,iy1,iz1))*abs(q(sp))/(model%dx*model%dy*model%dz*real(model%N0))
                            vyav(sp,ix1,iy1,iz1) = vyav(sp,ix1,iy1,iz1)/abs(dens(sp,ix1,iy1,iz1))*abs(q(sp))/(model%dx*model%dy*model%dz*real(model%N0))
                            vzav(sp,ix1,iy1,iz1) = vzav(sp,ix1,iy1,iz1)/abs(dens(sp,ix1,iy1,iz1))*abs(q(sp))/(model%dx*model%dy*model%dz*real(model%N0))
                            !second order
                            vx2av(sp,ix1,iy1,iz1) = vx2av(sp,ix1,iy1,iz1)/abs(dens(sp,ix1,iy1,iz1))*abs(q(sp))/(model%dx*model%dy*model%dz*real(model%N0))
                            vy2av(sp,ix1,iy1,iz1) = vy2av(sp,ix1,iy1,iz1)/abs(dens(sp,ix1,iy1,iz1))*abs(q(sp))/(model%dx*model%dy*model%dz*real(model%N0))
                            vz2av(sp,ix1,iy1,iz1) = vz2av(sp,ix1,iy1,iz1)/abs(dens(sp,ix1,iy1,iz1))*abs(q(sp))/(model%dx*model%dy*model%dz*real(model%N0))

                            !third order

                            vx3av(sp,ix1,iy1,iz1) = vx3av(sp,ix1,iy1,iz1)/abs(dens(sp,ix1,iy1,iz1))*abs(q(sp))/(model%dx*model%dy*model%dz*real(model%N0))
                            vy3av(sp,ix1,iy1,iz1) = vy3av(sp,ix1,iy1,iz1)/abs(dens(sp,ix1,iy1,iz1))*abs(q(sp))/(model%dx*model%dy*model%dz*real(model%N0))
                            vz3av(sp,ix1,iy1,iz1) = vz3av(sp,ix1,iy1,iz1)/abs(dens(sp,ix1,iy1,iz1))*abs(q(sp))/(model%dx*model%dy*model%dz*real(model%N0))


                        end if

                    end do
                end do
            end do
        end do



        !       vzav = vzav/real(model%proc_max +1)
        !       vyav = vyav/real(model%proc_max +1)
        !       vxav = vxav/real(model%proc_max +1)
        !
        !       vz2av = vz2av/real(model%proc_max +1)
        !       vy2av = vy2av/real(model%proc_max +1)
        !       vx2av = vx2av/real(model%proc_max +1)
        !
        !       vz3av = vz3av/real(model%proc_max +1)
        !       vy3av = vy3av/real(model%proc_max +1)
        !       vx3av = vx3av/real(model%proc_max +1)


    end if

end subroutine
! sending the final diag to the first processor
subroutine send_final_diag(no_species,mpi_rank,Nx,Ny,Nz,dens,vxav,vyav,vzav,vx2av,vy2av,vz2av,vx3av,vy3av,vz3av,debug,proc_no,edge_flux,edge_energy_flux)
    implicit none
    include 'mpif.h'
    integer:: ierr,sp,no_species,Nx,Ny,Nz,i,proc_no
    integer:: par_status(MPI_STATUS_SIZE)
    integer, dimension(no_species):: mpi_rank
    real, dimension(no_species,Nx,Ny,Nz):: dens,vxav,vyav,vzav,vx2av,vy2av,vz2av,vx3av,vy3av,vz3av,edge_flux,edge_energy_flux
    logical:: debug
    par_status = 0
    ierr = 0





    do sp=1,no_species
        if (mpi_rank(sp).eq.proc_no) then
            i = 141

            if (debug) then
                write(*,*) 'Sending dens',sum(abs(dens(sp,:,:,:)))
            end if
            call MPI_SEND(dens(sp,:,:,:),Nx*Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp,MPI_COMM_WORLD,ierr)

            i = i +1
            if (debug) then
                write(*,*) 'Sending vzav',sum(abs(vzav(sp,:,:,:)))
            end if

            call MPI_SEND(vzav(sp,:,:,:),Nx*Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp,MPI_COMM_WORLD,ierr)

            if (debug) then
                write(*,*) 'Sending vyav',sum(abs(vyav(sp,:,:,:)))
            end if
            i = i +1
            call MPI_SEND(vyav(sp,:,:,:),Nx*Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp,MPI_COMM_WORLD,ierr)

            if (debug) then
                write(*,*) 'Sending vxav',sum(abs(vxav(sp,:,:,:)))
            end if
            i = i +1
            call MPI_SEND(vxav(sp,:,:,:),Nx*Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp,MPI_COMM_WORLD,ierr)

            if (debug) then
                write(*,*) 'Sending vz2av',sum(abs(vz2av(sp,:,:,:)))
            end if
            i = i +1
            call MPI_SEND(vz2av(sp,:,:,:),Nx*Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp,MPI_COMM_WORLD,ierr)

            if (debug) then
                write(*,*) 'Sending vy2av',sum(abs(vy2av(sp,:,:,:)))
            end if
            i = i +1
            call MPI_SEND(vy2av(sp,:,:,:),Nx*Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp,MPI_COMM_WORLD,ierr)

            if (debug) then
                write(*,*) 'Sending vx2av',sum(abs(vx2av(sp,:,:,:)))
            end if
            i = i +1
            call MPI_SEND(vx2av(sp,:,:,:),Nx*Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp,MPI_COMM_WORLD,ierr)

            if (debug) then
                write(*,*) 'Sending vz3av',sum(abs(vz3av(sp,:,:,:)))
            end if
            i = i +1
            call MPI_SEND(vz3av(sp,:,:,:),Nx*Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp,MPI_COMM_WORLD,ierr)

            if (debug) then
                write(*,*) 'Sending vy3av',sum(abs(vy3av(sp,:,:,:)))
            end if
            i = i +1
            call MPI_SEND(vy3av(sp,:,:,:),Nx*Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp,MPI_COMM_WORLD,ierr)

            if (debug) then
                write(*,*) 'Sending vx3av',sum(abs(vx3av(sp,:,:,:)))
            end if
            i = i +1
            call MPI_SEND(vx3av(sp,:,:,:),Nx*Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp,MPI_COMM_WORLD,ierr)

            if (debug) then
                write(*,*) 'Sending edge_flux',sum(abs(edge_flux(sp,:,:,:)))
            end if
            i = i +1
            call MPI_SEND(edge_flux(sp,:,:,:),Nx*Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp,MPI_COMM_WORLD,ierr)


            if (debug) then
                write(*,*) 'Sending edge_energy_flux',sum(abs(edge_energy_flux(sp,:,:,:)))
            end if
            i = i +1
            call MPI_SEND(edge_energy_flux(sp,:,:,:),Nx*Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp,MPI_COMM_WORLD,ierr)


        end if
    end do
end subroutine

subroutine receive_diag (mpi_rank,count,snumber,debug,no_species,proc_max,Nx,Ny,Nz,rho,objects_current,nobjects,iter_time,model,iter_time_no)
    implicit none
    include 'mpif.h'
    include 'struct.h'
    integer:: ierr,i,np,no_species,sp,proc_max,Nx,Ny,Nz,nobjects,count,p,iter_time_no,n
    logical:: debug
    integer par_status(MPI_STATUS_SIZE)
    integer, dimension(no_species):: mpi_rank
    real, dimension(no_species,Nx,Ny,Nz):: rho
    real,dimension(Nx,Ny,Nz):: dumr
    real, dimension(nobjects,no_species,1:count):: objects_current
    real, dimension(nobjects,1:count):: dumc
    real, dimension(proc_max+1,10,iter_time_no):: iter_time
    integer, dimension(no_species,1:count):: snumber
    integer, dimension(1:count):: dumn
    type(model_t):: model
    ! receives data from all other procs so it can be stored in a temporary file
    !receiver
    ! Processor - related diag
    !for simplicity we store actual number of particles running at each processor
    if (debug) then
        write(*,*) 'Receive diag - proc_max',proc_max
    end if
    dumr = 0.0
    dumc = 0.0
    dumn = 0.0
    i = 43
    n = min(count,iter_time_no)
    do np=1,proc_max
        call MPI_RECV(iter_time(np +1,:,1:n),n*10,MPI_DOUBLE_PRECISION,np,i*np,MPI_COMM_WORLD,par_status,ierr)
        if (debug) then
            write(*,*) 'MPI Received iteration_time_hist',sum(iter_time(np +1,:,1:n)),np
        end if
    end do
    i  = 44
    do sp=1,no_species
        if (mpi_rank(sp).ne.0) then
            call MPI_RECV(snumber(sp,:),count,MPI_INTEGER,mpi_rank(sp),i*sp,MPI_COMM_WORLD,par_status,ierr)

        elseif(model%automatic_particle_decomposition) then
            do p=1,proc_max
                call MPI_RECV(dumn,count,MPI_INTEGER,p,i*sp,MPI_COMM_WORLD,par_status,ierr)
                snumber(sp,:) = snumber(sp,:) + dumn
                if (debug) then
                    write(*,*) 'MPI Received snumber',sum(dumn)
                end if
            end do
        end if
    end do

    i  = 45
    do sp=1,no_species
        if (mpi_rank(sp).ne.0) then
            call MPI_RECV(rho(sp,:,:,:),Ny*Nz*Nx,MPI_DOUBLE_PRECISION,mpi_rank(sp),i*sp,MPI_COMM_WORLD,par_status,ierr)



        elseif(model%automatic_particle_decomposition) then
            do p=1,proc_max
                call MPI_RECV(dumr,Ny*Nz*Nx,MPI_DOUBLE_PRECISION,p,i*sp,MPI_COMM_WORLD,par_status,ierr)
                rho(sp,:,:,:) = rho(sp,:,:,:) + dumr
                if (debug) then
                    write(*,*) 'MPI Received rho',sum(abs(dumr))
                end if
            end do
        end if
    end do


    i  = 46
    do sp=1,no_species
        if (mpi_rank(sp).ne.0) then
            call MPI_RECV(objects_current(:,sp,:),nobjects*count,MPI_DOUBLE_PRECISION,mpi_rank(sp),i*sp*mpi_rank(sp),MPI_COMM_WORLD,par_status,ierr)

        elseif(model%automatic_particle_decomposition) then
            do p=1,proc_max
                call MPI_RECV(dumc,nobjects*count,MPI_DOUBLE_PRECISION,p,i*sp*p,MPI_COMM_WORLD,par_status,ierr)


                objects_current(:,sp,:) = objects_current(:,sp,:) + dumc

                if (debug) then
                    write(*,*) 'MPI Received objects current',sum(abs(dumc))
                end if
            end do
        end if
    end do

end subroutine

subroutine send_diag (mpi_rank,count,snumber,debug,no_species,proc_max,Nx,Ny,Nz,rho,objects_current,nobjects,iter_time,proc_no,model,iter_time_no)
    implicit none
    include 'mpif.h'
    include 'struct.h'
    integer:: ierr,i,np,no_species,sp,proc_max,Nx,Ny,Nz,nobjects,count,proc_no,iter_time_no,n
    logical:: debug
    integer par_status(MPI_STATUS_SIZE)
    real, dimension(no_species,Nx,Ny,Nz):: rho
    real, dimension(nobjects,no_species,1:count):: objects_current
    real, dimension(proc_max+1,10,1:count):: iter_time
    integer, dimension(no_species,1:count):: snumber
    integer, dimension(no_species):: mpi_rank
    type(model_t):: model
    i = 43
    if (debug) then
        write(*,*) 'MPI:: Sending iteration_time_hist',sum(iter_time(proc_no+1,:,1:n)),proc_no
    end if
    n = min(count,iter_time_no)
    call MPI_SEND(iter_time(proc_no+1,:,1:n),n*10,MPI_DOUBLE_PRECISION,0,i*proc_no,MPI_COMM_WORLD,ierr)
    i = 44
    do sp=1,no_species
        if (mpi_rank(sp).eq.proc_no) then
            if (debug) then
                write(*,*) 'MPI:: Sending snumber',sum(snumber(sp,:))
            end if
            call MPI_SEND(snumber(sp,:),count,MPI_INTEGER,0,i*sp,MPI_COMM_WORLD,ierr)
        end if
    end do

    i = 45
    do sp=1,no_species
        if (mpi_rank(sp).eq.proc_no) then
            if (debug) then
                write(*,*) 'MPI:: Sending rho',sum(rho(sp,:,:,:))
            end if
            call MPI_SEND(rho(sp,:,:,:),Nx*Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp,MPI_COMM_WORLD,ierr)
        end if
    end do
    i = 46
    do sp=1,no_species
        if (mpi_rank(sp).eq.proc_no) then
            call MPI_SEND(objects_current(:,sp,:),nobjects*count,MPI_DOUBLE_PRECISION,0,i*sp*proc_no,MPI_COMM_WORLD,ierr)
        end if
    end do



end subroutine

subroutine particle_transfer_v1(model,no_tot,x,y,z,ux,uy,uz,stype,no_species,pnumber,x_old,y_old,z_old,use_old)
    implicit none
    include 'struct.h'

    integer:: i,no_tot,no_part,no_species
    integer,dimension(no_species):: pnumber
    real, dimension(no_tot):: x,y,z,ux,uy,uz,x_old,y_old,z_old
    integer, dimension(no_tot):: stype
    type(model_t):: model
    logical:: use_old
    no_part = no_tot
    ! proto version  -discard all particles out of the boundaries
    do i=no_tot,1,-1
        if (x(i).lt.0.0.or.y(i).lt.0.0.or.z(i).lt.0.0.or.x(i).gt.model%Lx.or.y(i).gt.model%Ly.or.z(i).gt.model%Lz) then
            ! 	  write(*,*) 'Discarding particle',x(i),y(i),z(i)
            ! 	  write(*,*) 'Box',model%Lx,model%Ly,model%Lz
            !
            call discard_particle(x,y,z,ux,uy,uz,stype,i,no_part,no_tot,pnumber,no_species,x_old,y_old,z_old,use_old)
        end if
    end do

end subroutine
! broadcast model
subroutine particle_transfer_v2(model,no_tot,x,y,z,ux,uy,uz,stype,no_species,pnumber,x_old,y_old,z_old,npts,proc_no,proc_max,debug,slices,use_old)
    implicit none
    include 'struct.h'
    include 'mpif.h'
    integer:: i,no_tot,no_part,no_species,no_m,r_no,proc_no,proc_max,sp,p_count,ierr
    integer*8:: Npts
    integer,dimension(no_species):: pnumber
    real, dimension(npts):: x,y,z,ux,uy,uz,x_old,y_old,z_old
    integer, dimension(npts):: stype
    real, dimension(int(npts/10)):: mx,my,mz,mux,muy,muz,mx_old,my_old,mz_old,rx,ry,rz,rux,ruy,ruz,rx_old,ry_old,rz_old
    integer, dimension(int(npts/10)):: mstype,rstype
    logical:: debug,use_old
    type(model_t):: model
    type(slice_info), dimension(proc_max+1):: slices
    no_part = no_tot
    no_m = 0
    ! proto version  -discard all particles out of the boundaries
    ! write(*,*) proc_no,'Box boundaries'
    ! write(*,*) proc_no,'X limits',slices(proc_no+1)%Lx_start,slices(proc_no+1)%Lx_stop
    ! write(*,*) proc_no,'Y limits',slices(proc_no+1)%Ly_start,slices(proc_no+1)%Ly_stop
    ! write(*,*) proc_no,'Z limits',slices(proc_no+1)%Lz_start,slices(proc_no+1)%Lz_stop
    ! write(*,*) proc_no,'Model',model%Lx,model%Ly,model%Lz
    !
    ! stop


    !write(*,*) model%proc_no,'Transfer z limits',slices(model%proc_no +1)%Lz_stop
    do i=no_tot,1,-1


        ! this will have to be fixed in future!
        if (x(i).lt.0.0.or.y(i).lt.0.0.or.z(i).lt.0.0.or.x(i).gt.model%Lx.or.y(i).gt.model%Ly.or.z(i).gt.slices(model%proc_no +1)%Lz_stop) then
            !if (model%proc_no.gt.0) then

            ! 	  write(*,*) model%proc_no,'Discarding particle',x(i),y(i),z(i)
            !end if
            ! 	  write(*,*) 'Box',model%Lx,model%Ly,model%Lz
            !
            ! add the particle to buffer
            no_m = no_m+1
            if (no_m.gt.int(npts/10)) then
                write(*,*) proc_no,'Too many transfering particles, discarding',no_m
            end if
            ! shift coordinates
            mx(no_m) = x(i) + slices(proc_no+1)%Lx_start
            my(no_m) = y(i) + slices(proc_no+1)%Ly_start
            mz(no_m) = z(i) + slices(proc_no+1)%Lz_start
            mux(no_m) = ux(i)
            muy(no_m) = uy(i)
            muz(no_m) = uz(i)

            mstype(no_m) = stype(i)
            if (use_old) then
                mx_old(no_m) = x_old(i) + slices(proc_no+1)%Lx_start
                my_old(no_m) = y_old(i) + slices(proc_no+1)%Ly_start
                mz_old(no_m) = z_old(i) + slices(proc_no+1)%Lz_start
            end if
            ! periodic boundary conditions
            if (mx(no_m).lt.0.0) then
                mx(no_m) = mx(no_m) + model%Lx_total
            end if
            if (my(no_m).lt.0.0) then
                my(no_m) = my(no_m) + model%Ly_total
            end if
            if (mx(no_m).gt.model%Lx_total) then
                mx(no_m) = mx(no_m) - model%Lx_total
            end if
            if (my(no_m).gt.model%Ly_total) then
                my(no_m) = my(no_m) - model%Ly_total
            end if

            call discard_particle(x(1:no_tot),y(1:no_tot),z(1:no_tot),ux(1:no_tot),uy(1:no_tot),uz(1:no_tot),stype(1:no_tot),i,no_part,no_tot,pnumber,no_species,x_old(1:no_tot),y_old(1:no_tot),z_old(1:no_tot),use_old)
        end if
    end do
    no_tot = no_part
    if (debug) then
        do i=no_tot,1,-1
            if (x(i).lt.0.0.or.y(i).lt.0.0.or.z(i).lt.0.0.or.x(i).gt.model%Lx.or.y(i).gt.model%Ly.or.z(i).gt.model%Lz_max) then
                write(*,*) 'Error - wrong particle remained in set'
                write(*,*) 'Pos',x(i),y(i),z(i)
                write(*,*) 'X limits: 0,',model%Lx
                write(*,*) 'Y limits: 0,',model%Ly
                write(*,*) 'Z limits: 0,',model%Lz
                write(*,*) 'X check',slices(proc_no+1)%Lx_start,slices(proc_no+1)%Lx_stop
                write(*,*) 'Y check',slices(proc_no+1)%Ly_start,slices(proc_no+1)%Ly_stop
                write(*,*) 'Z check',slices(proc_no+1)%Ly_start,slices(proc_no+1)%Lz_stop
                stop
            end if
        end do


    end if
    ! broadcast the particles
    if (debug) then
        write(*,*) proc_no,'Collected particles for transfer',no_m
    end if

    if(debug) then
        write(*,*) 'R'
    end if

    ! loop over processors
    do sp=0,proc_max
        if (sp.eq.proc_no) then
            ! send
            call MPI_BCAST(no_m,1,MPI_INTEGER,proc_no,MPI_COMM_WORLD,ierr)
            if (debug) then
                write(*,*) proc_no,'Sending shift i, proc:',proc_no,no_m
            end if
            if (no_m.gt.0) then
                call MPI_BCAST(mx(1:no_m),no_m,MPI_DOUBLE_PRECISION,proc_no,MPI_COMM_WORLD,ierr)
                if (debug) then
                    write(*,*) proc_no,'Sending shift x, proc:',proc_no
                end if

                call MPI_BCAST(my(1:no_m),no_m,MPI_DOUBLE_PRECISION,proc_no,MPI_COMM_WORLD,ierr)
                if (debug) then
                    write(*,*) proc_no,'Sending shift y, proc:',proc_no
                end if
                call MPI_BCAST(mz(1:no_m),no_m,MPI_DOUBLE_PRECISION,proc_no,MPI_COMM_WORLD,ierr)
                if (debug) then
                    write(*,*) proc_no,'Sending shift z, proc:',proc_no
                end if
                call MPI_BCAST(mux(1:no_m),no_m,MPI_DOUBLE_PRECISION,proc_no,MPI_COMM_WORLD,ierr)
                if (debug) then
                    write(*,*) proc_no,'Sending shift ux, proc:',proc_no
                end if
                call MPI_BCAST(muy(1:no_m),no_m,MPI_DOUBLE_PRECISION,proc_no,MPI_COMM_WORLD,ierr)
                if (debug) then
                    write(*,*) proc_no,'Sending shift uy, proc:',proc_no
                end if
                call MPI_BCAST(muz(1:no_m),no_m,MPI_DOUBLE_PRECISION,proc_no,MPI_COMM_WORLD,ierr)
                if (debug) then
                    write(*,*) proc_no,'Sending shift uz, proc:',proc_no
                end if
                call MPI_BCAST(mstype(1:no_m),no_m,MPI_INTEGER,proc_no,MPI_COMM_WORLD,ierr)
                if (debug) then
                    write(*,*) proc_no,'Sending shift stype, proc:',proc_no
                end if
                if (use_old) then
                    call MPI_BCAST(mx_old(1:no_m),no_m,MPI_DOUBLE_PRECISION,proc_no,MPI_COMM_WORLD,ierr)
                    if (debug) then
                        write(*,*) proc_no,'Sending shift old_x, proc:',proc_no
                    end if

                    call MPI_BCAST(my_old(1:no_m),no_m,MPI_DOUBLE_PRECISION,proc_no,MPI_COMM_WORLD,ierr)
                    if (debug) then
                        write(*,*) proc_no,'Sending shift old_y, proc:',proc_no
                    end if
                    call MPI_BCAST(mz_old(1:no_m),no_m,MPI_DOUBLE_PRECISION,proc_no,MPI_COMM_WORLD,ierr)
                    if (debug) then
                        write(*,*) proc_no,'Sending shift old_z, proc:',proc_no
                    end if
                end if
            end if ! no_m .gt.0
        else
            ! receive
            call MPI_BCAST(r_no,1,MPI_INTEGER,sp,MPI_COMM_WORLD,ierr)

            if (debug) then
                write(*,*) proc_no,'Received inc i, proc:',r_no
            end if

            if (r_no.gt.0) then
                call MPI_BCAST(rx(1:r_no),r_no,MPI_DOUBLE_PRECISION,sp,MPI_COMM_WORLD,ierr)
                if (debug) then
                    write(*,*) proc_no,'Received  x:',sum(rx(1:r_no))
                end if

                call MPI_BCAST(ry(1:r_no),r_no,MPI_DOUBLE_PRECISION,sp,MPI_COMM_WORLD,ierr)
                if (debug) then
                    write(*,*) proc_no,'Received  y:',sum(ry(1:r_no))
                end if

                call MPI_BCAST(rz(1:r_no),r_no,MPI_DOUBLE_PRECISION,sp,MPI_COMM_WORLD,ierr)
                if (debug) then
                    write(*,*) proc_no,'Received  z:'
                end if
                call MPI_BCAST(rux(1:r_no),r_no,MPI_DOUBLE_PRECISION,sp,MPI_COMM_WORLD,ierr)
                if (debug) then
                    write(*,*) proc_no,'Received  ux:'
                end if
                call MPI_BCAST(ruy(1:r_no),r_no,MPI_DOUBLE_PRECISION,sp,MPI_COMM_WORLD,ierr)
                if (debug) then
                    write(*,*) proc_no,'Received  uy:'
                end if
                call MPI_BCAST(ruz(1:r_no),r_no,MPI_DOUBLE_PRECISION,sp,MPI_COMM_WORLD,ierr)
                if (debug) then
                    write(*,*) proc_no,'Received  uz:'
                end if
                call MPI_BCAST(rstype(1:r_no),r_no,MPI_INTEGER,sp,MPI_COMM_WORLD,ierr)
                if (debug) then
                    write(*,*) proc_no,'Received  stype:'
                end if
                if (use_old) then
                    call MPI_BCAST(rx_old(1:r_no),r_no,MPI_DOUBLE_PRECISION,sp,MPI_COMM_WORLD,ierr)
                    if (debug) then
                        write(*,*) proc_no,'Received  old_x:',sum(rx(1:r_no))
                    end if

                    call MPI_BCAST(ry_old(1:r_no),r_no,MPI_DOUBLE_PRECISION,sp,MPI_COMM_WORLD,ierr)
                    if (debug) then
                        write(*,*) proc_no,'Received  old_y:',sum(ry(1:r_no))
                    end if

                    call MPI_BCAST(rz_old(1:r_no),r_no,MPI_DOUBLE_PRECISION,sp,MPI_COMM_WORLD,ierr)
                    if (debug) then
                        write(*,*) proc_no,'Received  old_z:'
                    end if
                end if
                ! run through the incoming particles and select those which belong to our slice
                p_count = 0
                do i=1,r_no
                    ! this will have to be fixed properly
                    if (rx(i).gt.slices(proc_no+1)%Lx_start.and.ry(i).gt.slices(proc_no+1)%Ly_start.and.rz(i).gt.slices(proc_no+1)%Lz_start&
                            .and.rx(i).lt.slices(proc_no+1)%Lx_stop.and.ry(i).lt.slices(proc_no+1)%Ly_stop.and.rz(i).lt.model%Lz_max) then
                        ! add this particle to our stack
                        no_tot = no_tot + 1
                        x(no_tot) = rx(i) - slices(proc_no+1)%Lx_start
                        z(no_tot) = rz(i) - slices(proc_no+1)%Lz_start
                        y(no_tot) = ry(i) - slices(proc_no+1)%Ly_start
                        if (use_old) then
                            x_old(no_tot) = rx_old(i) - slices(proc_no+1)%Lx_start
                            z_old(no_tot) = rz_old(i) - slices(proc_no+1)%Lz_start
                            y_old(no_tot) = ry_old(i) - slices(proc_no+1)%Ly_start
                        end if
                        ux(no_tot) = rux(i)
                        uy(no_tot) = ruy(i)
                        uz(no_tot) = ruz(i)
                        stype(no_tot) = rstype(i)
                        pnumber(stype(no_tot)) = pnumber(stype(no_tot)) +1
                        p_count = p_count + 1
                    end if
                end do
                if (debug) then
                    write(*,*) proc_no,'Received particles from proc',p_count,sp
                end if
            end if ! r_no .gt. 0



        end if ! send/receive
    end do

    if (debug) then
        do i=no_tot,1,-1
            if (x(i).lt.0.0.or.y(i).lt.0.0.or.z(i).lt.0.0.or.x(i).gt.model%Lx.or.y(i).gt.model%Ly.or.z(i).gt.model%Lz_max) then
                write(*,*) 'Error - wrong particle recived',i
                write(*,*) 'Pos',x(i),y(i),z(i)
                write(*,*) 'X limits: 0,',model%Lx
                write(*,*) 'Y limits: 0,',model%Ly
                write(*,*) 'Z limits: 0,',model%Lz
                write(*,*) 'X check',slices(proc_no+1)%Lx_start,slices(proc_no+1)%Lx_stop
                write(*,*) 'Y check',slices(proc_no+1)%Ly_start,slices(proc_no+1)%Ly_stop
                write(*,*) 'Z check',slices(proc_no+1)%Ly_start,slices(proc_no+1)%Lz_stop
                stop
            end if
        end do


    end if

end subroutine

subroutine send_boundary_particles(no_s,x_s,y_s,z_s,ux_s,uy_s,uz_s,stype_s,proc_no,proc_max,model,slices,npts)
    implicit none
    include 'struct.h'
    include 'mpif.h'

    integer:: no_s,proc_no,proc_max,ierr
    integer*8:: Npts
    real, dimension(int(npts/10)):: x_s,y_s,z_s,ux_s,uy_s,uz_s
    integer, dimension(int(npts/10)):: stype_s
    type(model_t):: model
    logical:: debug
    type(slice_info), dimension(proc_max+1):: slices

    debug = model%debug
    call MPI_BCAST(no_s,1,MPI_INTEGER,proc_no,MPI_COMM_WORLD,ierr)
    if (debug) then
        write(*,*) proc_no,'Sending shift i, proc:',proc_no,no_s
    end if
    if (no_s.gt.0) then
        call MPI_BCAST(x_s(1:no_s),no_s,MPI_DOUBLE_PRECISION,proc_no,MPI_COMM_WORLD,ierr)
        if (debug) then
            write(*,*) proc_no,'Sending shift x, proc:',proc_no
        end if

        call MPI_BCAST(y_s(1:no_s),no_s,MPI_DOUBLE_PRECISION,proc_no,MPI_COMM_WORLD,ierr)
        if (debug) then
            write(*,*) proc_no,'Sending shift y, proc:',proc_no
        end if
        call MPI_BCAST(z_s(1:no_s),no_s,MPI_DOUBLE_PRECISION,proc_no,MPI_COMM_WORLD,ierr)
        if (debug) then
            write(*,*) proc_no,'Sending shift z, proc:',proc_no
        end if
        call MPI_BCAST(ux_s(1:no_s),no_s,MPI_DOUBLE_PRECISION,proc_no,MPI_COMM_WORLD,ierr)
        if (debug) then
            write(*,*) proc_no,'Sending shift ux, proc:',proc_no
        end if
        call MPI_BCAST(uy_s(1:no_s),no_s,MPI_DOUBLE_PRECISION,proc_no,MPI_COMM_WORLD,ierr)
        if (debug) then
            write(*,*) proc_no,'Sending shift uy, proc:',proc_no
        end if
        call MPI_BCAST(uz_s(1:no_s),no_s,MPI_DOUBLE_PRECISION,proc_no,MPI_COMM_WORLD,ierr)
        if (debug) then
            write(*,*) proc_no,'Sending shift uz, proc:',proc_no
        end if
        call MPI_BCAST(stype_s(1:no_s),no_s,MPI_INTEGER,proc_no,MPI_COMM_WORLD,ierr)
        if (debug) then
            write(*,*) proc_no,'Sending shift stype, proc:',proc_no
        end if
    end if ! any particles to send

end subroutine

subroutine 	receive_boundary_particles(no_r,x_r,y_r,z_r,ux_r,uy_r,uz_r,stype_r,proc_no,proc_max,sp,model,npts,slices)

    implicit none
    include 'struct.h'
    include 'mpif.h'

    integer:: no_r,no_ra,proc_no,proc_max,ierr,sp,i,no_r_temp
    integer*8:: Npts
    real, dimension(int(npts/10)):: x_r,y_r,z_r,ux_r,uy_r,uz_r
    integer, dimension(int(npts/10)):: stype_r
    type(model_t):: model
    logical:: debug
    type(slice_info), dimension(proc_max+1):: slices
    debug = model%debug
    ! check incoming particles
    call MPI_BCAST(no_ra,1,MPI_INTEGER,sp,MPI_COMM_WORLD,ierr)

    if (debug) then
        write(*,*) proc_no,'Received inc i, proc:',no_ra
    end if

    if (no_ra.gt.0) then
        call MPI_BCAST(x_r((no_r+1):(no_r + no_ra)),no_ra,MPI_DOUBLE_PRECISION,sp,MPI_COMM_WORLD,ierr)
        if (debug) then
            write(*,*) proc_no,'Received  x:',sum(x_r((no_r+1):(no_r + no_ra)))
        end if

        call MPI_BCAST(y_r((no_r+1):(no_r + no_ra)),no_ra,MPI_DOUBLE_PRECISION,sp,MPI_COMM_WORLD,ierr)
        if (debug) then
            write(*,*) proc_no,'Received  y:',sum(y_r((no_r+1):(no_r + no_ra)))
        end if

        call MPI_BCAST(z_r((no_r+1):(no_r + no_ra)),no_ra,MPI_DOUBLE_PRECISION,sp,MPI_COMM_WORLD,ierr)
        if (debug) then
            write(*,*) proc_no,'Received  z:',sum(z_r((no_r+1):(no_r + no_ra)))
        end if
        call MPI_BCAST(ux_r((no_r+1):(no_r + no_ra)),no_ra,MPI_DOUBLE_PRECISION,sp,MPI_COMM_WORLD,ierr)
        if (debug) then
            write(*,*) proc_no,'Received  ux:',sum(ux_r((no_r+1):(no_r + no_ra)))
        end if
        call MPI_BCAST(uy_r((no_r+1):(no_r + no_ra)),no_ra,MPI_DOUBLE_PRECISION,sp,MPI_COMM_WORLD,ierr)
        if (debug) then
            write(*,*) proc_no,'Received  uy:',sum(uy_r((no_r+1):(no_r + no_ra)))
        end if
        call MPI_BCAST(uz_r((no_r+1):(no_r + no_ra)),no_ra,MPI_DOUBLE_PRECISION,sp,MPI_COMM_WORLD,ierr)
        if (debug) then
            write(*,*) proc_no,'Received  uz:',sum(uz_r((no_r+1):(no_r + no_ra)))
        end if
        call MPI_BCAST(stype_r((no_r+1):(no_r + no_ra)),no_ra,MPI_INTEGER,sp,MPI_COMM_WORLD,ierr)
        if (debug) then
            write(*,*) proc_no,'Received  stype:',sum(stype_r((no_r+1):(no_r + no_ra)))
        end if

        ! run through the incoming particles and select those which belong to our slice
        if (debug) then
            write(*,*) proc_no,'Received particles from proc',no_ra
        end if
        no_r = no_r + no_ra
    end if ! r_no .gt. 0


end subroutine

