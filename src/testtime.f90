PROGRAM testtime
    integer*8:: t_start,t_stop,s_start,s_stop
    real:: delta
    t_start = 0
    t_stop = 0
    call gettime(t_start)
    call getseconds(s_start)

    call sleep(5)
    call getseconds(s_stop)
    call gettime(t_stop)
    delta = real(t_stop - t_start)/1.0E6 + real(s_stop - s_start)
    write(*,*)'Delta',delta,t_start,t_stop
    write(*,*) 'Start time',s_start
    write(*,*) 'Stop time',s_stop
END PROGRAM
