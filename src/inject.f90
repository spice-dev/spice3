subroutine proto_inject(x,y,z,ux,uy,uz,stype,no_tot,Lx,Ly,Lz,no_inject,ksi)
    implicit none
    integer:: no_tot,no_inject,i
    real, dimension(no_tot):: x,y,z,ux,uy,uz
    integer,dimension(no_tot):: stype
    real:: Lx,Ly,Lz,ksi
    real*8:: rndx
    rndx = 0.0
    do i=no_tot - no_inject +1,no_tot
        ! x position
        call G05FAF(0.d0,1.d0,1,rndx)
        x(i) = rndx*Lx
        ! y position
        call G05FAF(0.d0,1.d0,1,rndx)
        y(i) = rndx*Ly
        ! z position
        call G05FAF(0.d0,1.d0,1,rndx)
        z(i) = rndx*Lz
        !only 1 type
        stype(i) = 1
        ! velocities
        call G05FAF(0.d0,1.d0,1,rndx)
        ux(i) = (rndx - 0.5)*ksi
        call G05FAF(0.d0,1.d0,1,rndx)
        uy(i) = (rndx - 0.5)*ksi
        call G05FAF(0.d0,1.d0,1,rndx)
        uz(i) = (rndx - 0.5)*ksi


    end do
    ! that's it
end subroutine
! subroutines concerning particle injection
subroutine SFLUX(u,N_u,fu,X,Xu,ufu,GAMMAi0)
    implicit none
    real*8 u,fu,X,Xu,ufu,GAMMAi0
    integer N_u,j,i

    dimension X(N_u),Xu(N_u),ufu(N_u),u(N_u),fu(N_u)

    !Etape 1: calcul du tableau des X (correspondant aux u)
    !   X=0.d0*u
    do j=2,N_u
        X(j)=X(j-1)+(u(j)-u(j-1))*(fu(j)+fu(j-1))/2.d0
    enddo
    !write(*,*) 'X lining done:', sum(X)

    !Etape 2: normalisation de fu
    fu=fu/X(N_u)
    !write(*,*) 'fu norm done:', sum(fu)

    !   write(6,*) ' fu ',fu
    !   write(6,*) ' X(N_u)_old ',X(N_u)
    X=X/X(N_u)
    write(*,*) ' X(N_u)_norm ',X(N_u)

    !Etape 3: definition de ufu + son integrale
    !  Xu=0.d0*u
    !  ufu=0.d0
    do i=1,N_u
        ufu(i)=u(i)*fu(i)
    enddo
    !write(*,*) 'ufu lining done:', sum(ufu)

    do j=2,N_u
        Xu(j)=Xu(j-1)+(u(j)-u(j-1))*(ufu(j)+ufu(j-1))/2.d0
    enddo
    !write(*,*) 'Xu lining done:', sum(Xu)


    !ionic flux at t=0
    GAMMAi0=Xu(N_u)

    !Etape 4: normalisation de ufu
    ufu=ufu/Xu(N_u)
    !   write(6,*) ' u*fu',ufu
    !   write(6,*) ' Xu(N_u)_old',Xu(N_u)
    Xu=Xu/Xu(N_u)
    !   write(6,*) ' Xu(N_u)_norm ',Xu(N_u)

    return
END

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

subroutine VELOCITY(u,ufu,N_u,Xu,uzii)
    !          	call VELOCITY(u_z,ufu_z,N_u_z,Xu_z,uparl) !arbitrary normalized velocity
    implicit none
    real*8 u,ufu,Xu,uzii,randx
    integer N_u,j,jdwn_temp,jup_temp,it,jtemp,k

    dimension u(N_u),ufu(N_u),Xu(N_u)

    !velocity debug
    !Etape 1: Calcul des indices k tq: Xu(k)<randx<Xu(k+1)
    call G05FAF(0.d0,1.d0,1,randx)
    jdwn_temp=1
    jup_temp=N_u
    jtemp=idint((jup_temp-jdwn_temp)*1.d0/2.d0+0.5)
    k=0

    12 continue

    if (randx.gt.Xu(jtemp)) then
        jdwn_temp=jtemp

        if (randx.eq.Xu(jtemp+1)) then
            k=jtemp+1
        elseif (randx.lt.Xu(jtemp+1)) then
            k=jtemp
        else
            jtemp=jtemp+idint((jup_temp-jdwn_temp)*1.d0/2.d0+0.5)
            goto 12
        endif

    elseif (randx.lt.Xu(jtemp)) then
        jup_temp=jtemp

        if (randx.ge.Xu(jtemp-1)) then
            k=jtemp-1
        else
            jtemp=jtemp-idint((jup_temp-jdwn_temp)*1.d0/2.d0+0.5)
            goto 12
        endif
    endif

    !Etape 2: Calcul de la uzi en fonction du randx
    uzii=u(k)+(-ufu(k)+sqrt(ufu(k)*ufu(k)+2.d0*(ufu(k+1)-ufu(k))/(u(k+1)-u(k))*(randx-Xu(k))))/(ufu(k+1)-ufu(k))*(u(k+1)-u(k))
    ! write(*,*) 'Velocity: uzii',uzii
    ! return
END

! thermionic injection
subroutine inject_therm(Ninj,no_part,no_part_fin,x,z,y,ux,uy,uz,mono,utherm,N_u_z,u_z,ufu_z,&
        Xu_z,Ax_hlf,Bx_hlf,Cx_hlf,Ay_hlf,By_hlf,Cy_hlf,Az_hlf,Bz_hlf,Cz_hlf,Lzi,stype,sp,m,q,Umono,T,&
        spec_params,no_species,model,slices,no_slices,pnumber,Lz_limit,Nx,Ny,Nz,Ts,edges,init_disp,&
        inj_ratio,edge_flux,edge_energy_flux,objects_current,no_objects,objects,npts,proc_no,inj_rem,therm_Wf_norm,c_act)
    !use IFPORT
    implicit none
    !INJECT new ions from plasma side (to replace the outflux)
    !=========================================================
    include "struct.h"
    integer:: i,Ninj,no_part,no_part_fin,sp,no_species,no_supra_loops,k,no_slices,Nx,Ny,Nz,j,N0,do_loop,no_objects,npts,proc_no,c_act
    type(pinfo), dimension(no_species):: spec_params
    integer, dimension(no_species) :: pnumber
    real, dimension(no_species) :: inj_ratio
    real:: Ninj_r,Ninj_fx,Umono,Lz_limit
    real:: bx,by,bz,first_push
    real, dimension(npts):: x,z,y,ux,uy,uz
    real, dimension(Nx,Ny,Nz):: edge_flux,edge_energy_flux,edge_flux_temp,edge_energy_flux_temp
    integer, dimension(Nx,Ny,Nz):: objects
    integer, dimension(npts):: stype
    logical:: mono
    real:: Lx,Lz,Ly,dt,utherm,sqbx2by2,midLz,m,q,T,Lzi,ddx,ddy,ddz
    integer:: N_u_z
    real*8, dimension(N_u_z):: u_z,ufu_z,Xu_z
    real:: Ax_hlf,Bx_hlf,Cx_hlf,Ay_hlf,By_hlf,Cy_hlf,Az_hlf,Bz_hlf,Cz_hlf
    real*8:: upar1,uperp1,uperp2,ux_temp,uy_temp,uz_temp
    real*8:: rndx, rndxn
    real:: Lx_start,Ly_start,Lz_start,Lx_total,Ly_total,Lz_total,init_disp,mean_Ts,thermionic_A,thermionic_Wf,lambda_D,act_inj,Ts_norm,w_i,therm_Wf_norm,E_tot
    type(model_t):: model
    type(slice_info),dimension(no_slices):: slices
    real:: uz_sum
    integer:: uz_count,discarded_count,orientation
    real, dimension(Nx,Ny,Nz):: Ts,inj_rem
    integer, dimension(Nx,Ny,Nz):: edges

    real, dimension(no_objects,no_species):: objects_current

    ! write(*,*) 'Injecting', Ninj, 'particles'
    !Generate a velocity distribution for the ions
    rndxn = 0.0
    rndx = 0.0
    edge_flux_temp = 0.0
    edge_energy_flux_temp =  0.0
    bx = model%bx
    by = model%by
    bz = model%bz
    Lx = model%Lx
    Ly = model%Ly
    Lz = model%Lz
    Lx_start = model%Lx_start
    Ly_start = model%Ly_start
    Lz_start = model%Lz_start
    Lx_total = model%Lx_total
    Ly_total = model%Ly_total
    Lz_total = model%Lz_total
    uz_sum  = 0.0
    uz_count = 0
    discarded_count =0
    dt = model%dt
    sqbx2by2=sqrt(bx**2+by**2)
    ! default thermionic constant
    thermionic_A = 60.0E4
    thermionic_Wf = 4.55
    lambda_D = sqrt(8.85E-12*model%mks_Te/model%mks_n0/1.602E-19)
    w_i =  1.602E-19*model%mks_B/model%mks_main_ion_m/1.67E-27
    ! loop over cells to determine if we are at hot surface

    ! first loop checks for surfaces in the x-y plane
    do i=1,(Nx-1)
        do j=1,(Ny-1)
            do k=2,(Nz-1)
                if (Ts(i,j,k).gt.0.0.and.edges(i,j,k).gt.0.0) then
                    ! this is a promissing cell,let's determine the orientation
                    ! this loops deals with surfaces oriented in z direction
                    if (edges(i,j,k).gt.0.0.and.edges(i+1,j,k).gt.0.0.and.edges(i,j+1,k).gt.0.0.and.edges(i+1,j+1,k).gt.0.0) then
                        ! nextdetermine the orientation
                        if (k.eq.1) then
                            orientation  = 1
                        elseif (objects(i,j,k-1)*objects(i+1,j,k-1)*objects(i,j+1,k-1)*objects(i+1,j+1,k-1).gt.0.0) then
                            orientation  = 1
                        else
                            orientation = 2
                        end if

                        ! now get the mean Ts
                        mean_Ts = (Ts(i,j,k) + Ts(i+1,j,k) + Ts(i,j+1,k) + Ts(i+1,j+1,k))/4.0
                        Ts_norm = mean_Ts/11600/model%mks_Te
                        ! get the number of particles to be injected

                        ! following the richardson's formula j = A*T^2*exp(-Wf/kT)
                        ! A = 60E4 A/m2/K2 effective Richardson constant
                        ! Wf = 4.55 eV based on experiment for tungsten
                        !      write(*,*) 'Thermionic factor A*T^2*exp(-Wf/kT)',thermionic_A*mean_Ts**2*exp(-thermionic_Wf/(mean_Ts/11600))
                        act_inj=thermionic_A/1.602E-19*mean_Ts**2*exp(-thermionic_Wf/(mean_Ts/11600))*inj_ratio(sp)*dt*model%N0*model%dx*model%dy/(model%mks_n0*lambda_D*w_i) ! + inj_rem(i,j,k)
                        !write(*,*) 'Therm inj',act_inj,thermionic_A*mean_Ts**2*exp(-thermionic_Wf/(mean_Ts/11600))
                        !write(*,*) 'Injecting at',i,j,k,edges(i,j,k)
                        ! loop many times to inject given number of electrons
                        do_loop = int(act_inj) + 1
                        do while (do_loop.gt.0)
                            ! generate random number to check if we do inject this particle
                            call G05FAF(0.d0,1.d0,1,rndx)
                            if (rndx.lt.act_inj) then
                                ! let's inject new particle
                                no_part=no_part+1
                                pnumber(sp) = pnumber(sp) + 1
                                stype(no_part) = sp
                                objects_current(objects(i,j,k),sp) = objects_current(objects(i,j,k),sp) - q
                                ! positions
                                call G05FAF(0.d0,1.d0,1,rndx)
                                if (orientation.eq.1) then
                                    z(no_part) =  (float(k)-1.0)*model%dz + init_disp*(1.0 + rndx)
                                else
                                    z(no_part) =  (float(k)-1.0)*model%dz - init_disp*(1.0 + rndx)
                                end if
                                call G05FAF(0.d0,1.d0,1,ddx)
                                x(no_part) = (float(i)-1.0)*model%dx + ddx*model%dx
                                call G05FAF(0.d0,1.d0,1,ddy)
                                y(no_part) = (float(j) - 1.0)*model%dy + ddy*model%dy
                                ! velocities - temperature depends on the local surface temperature
                                call G05FDF(0.d0,1.d0,1,rndxn)
                                ux(no_part)=rndxn*utherm*sqrt(Ts_norm)
                                call G05FDF(0.d0,1.d0,1,rndxn)
                                uy(no_part)=rndxn*utherm*sqrt(Ts_norm)
                                call VELOCITY(u_z,ufu_z,N_u_z,Xu_z,upar1) !arbitrary normalized velocity function
                                if (orientation.eq.1) then
                                    uz(no_part)=-upar1*sqrt(Ts_norm)
                                else
                                    uz(no_part)=upar1*sqrt(Ts_norm)
                                end if
                                ! diagnostics
                                if (c_act.gt.model%Na) then
                                    edge_flux_temp(i,j,k) = edge_flux_temp(i,j,k) - (1.0-ddx)*(1.0-ddy)*(model%dz/model%dt/real(model%Npc))
                                    edge_flux_temp(i+1,j,k) = edge_flux_temp(i+1,j,k) - (ddx)*(1.0-ddy)*(model%dz/model%dt/real(model%Npc))
                                    edge_flux_temp(i,j+1,k) = edge_flux_temp(i,j+1,k) - (1.0-ddx)*(ddy)*(model%dz/model%dt/real(model%Npc))
                                    edge_flux_temp(i+1,j+1,k) = edge_flux_temp(i+1,j+1,k) - ddx*ddy*(model%dz/model%dt/real(model%Npc))

                                    ! add the work function - this energy is lost from the surface due to replacement eletron
                                    if (model%use_surface_heat_tweaks) then
                                        E_tot = (ux(no_part)*ux(no_part) +uy(no_part)*uy(no_part) + uz(no_part)*uz(no_part) + 2*therm_Wf_norm/m)*model%dz/model%dt/real(model%Npc)*m*0.5
                                    else
                                        E_tot = (ux(no_part)*ux(no_part) +uy(no_part)*uy(no_part) + uz(no_part)*uz(no_part))*model%dz/model%dt/real(model%Npc)*m*0.5

                                    end if
                                    edge_energy_flux_temp(i,j,k) = edge_energy_flux_temp(i,j,k) - E_tot *(1.0-ddx)*(1.0-ddy)
                                    edge_energy_flux_temp(i+1,j,k) = edge_energy_flux_temp(i+1,j,k) - E_tot*(ddx)*(1.0-ddy)
                                    edge_energy_flux_temp(i,j+1,k) = edge_energy_flux_temp(i,j+1,k) - E_tot*(1.0-ddx)*(ddy)
                                    edge_energy_flux_temp(i+1,j+1,k) = edge_energy_flux_temp(i+1,j+1,k) - E_tot*ddx*ddy
                                end if
                                act_inj = act_inj - 1.0
                            end if ! rndx

                            do_loop = do_loop - 1
                        end do ! particle loop

                    end if ! check if there's a flat surface
                end if ! check for Ts > 0
            end do
        end do
    end do

    ! second loop checks for surfaces in the x-z plane
    do i=1,(Nx-1)
        do j=1,(Ny-1)
            do k=2,(Nz-1)
                if (Ts(i,j,k).gt.0.0.and.edges(i,j,k).gt.0.0) then
                    ! this is a promissing cell,let's determine the orientation
                    ! this loops deals with surfaces oriented in y direction
                    if (edges(i,j,k).gt.0.0.and.edges(i+1,j,k).gt.0.0.and.edges(i,j,k+1).gt.0.0.and.edges(i+1,j,k+1).gt.0.0) then
                        ! next determine the orientation
                        if (j.eq.1) then
                            orientation  = 1
                        elseif (objects(i,j-1,k)*objects(i+1,j-1,k)*objects(i,j-1,k+1)*objects(i+1,j-1,k+1).gt.0.0) then
                            orientation  = 1
                        else
                            orientation = 2
                        end if

                        ! now get the mean Ts
                        mean_Ts = (Ts(i,j,k) + Ts(i+1,j,k) + Ts(i,j,k+1) + Ts(i+1,j,k+1))/4.0
                        Ts_norm = mean_Ts/11600/model%mks_Te
                        ! get the number of particles to be injected

                        ! following the richardson's formula j = A*T^2*exp(-Wf/kT)
                        ! A = 60E4 A/m2/K2 effective Richardson constant
                        ! Wf = 4.55 eV based on experiment for tungsten
                        !      write(*,*) 'Thermionic factor A*T^2*exp(-Wf/kT)',thermionic_A*mean_Ts**2*exp(-thermionic_Wf/(mean_Ts/11600))
                        act_inj=thermionic_A/1.602E-19*mean_Ts**2*exp(-thermionic_Wf/(mean_Ts/11600))*inj_ratio(sp)*dt*model%N0*model%dx*model%dy/(model%mks_n0*lambda_D*w_i) ! + inj_rem(i,j,k)
                        !write(*,*) 'Therm inj',act_inj,thermionic_A*mean_Ts**2*exp(-thermionic_Wf/(mean_Ts/11600))
                        !write(*,*) 'Injecting at',i,j,k,edges(i,j,k)
                        ! loop many times to inject given number of electrons
                        do_loop = int(act_inj) + 1
                        do while (do_loop.gt.0)
                            ! generate random number to check if we do inject this particle
                            call G05FAF(0.d0,1.d0,1,rndx)
                            if (rndx.lt.act_inj) then
                                ! let's inject new particle
                                no_part=no_part+1
                                pnumber(sp) = pnumber(sp) + 1
                                stype(no_part) = sp
                                objects_current(objects(i,j,k),sp) = objects_current(objects(i,j,k),sp) - q
                                ! positions
                                call G05FAF(0.d0,1.d0,1,rndx)
                                if (orientation.eq.1) then
                                    y(no_part) =  (float(j)-1.0)*model%dy + init_disp*(1.0 + rndx)
                                else
                                    y(no_part) =  (float(j)-1.0)*model%dy - init_disp*(1.0 + rndx)
                                end if
                                call G05FAF(0.d0,1.d0,1,ddx)
                                x(no_part) = (float(i)-1.0)*model%dx + ddx*model%dx
                                call G05FAF(0.d0,1.d0,1,ddy)
                                z(no_part) = (float(k) - 1.0)*model%dz + ddy*model%dz
                                ! velocities - temperature depends on the local surface temperature
                                call G05FDF(0.d0,1.d0,1,rndxn)
                                ux(no_part)=rndxn*utherm*sqrt(Ts_norm)
                                call G05FDF(0.d0,1.d0,1,rndxn)
                                uz(no_part)=rndxn*utherm*sqrt(Ts_norm)
                                call VELOCITY(u_z,ufu_z,N_u_z,Xu_z,upar1) !arbitrary normalized velocity function
                                if (orientation.eq.1) then
                                    uy(no_part)=-upar1*sqrt(Ts_norm)
                                else
                                    uy(no_part)=upar1*sqrt(Ts_norm)
                                end if
                                ! diagnostics
                                if (c_act.gt.model%Na) then
                                    edge_flux_temp(i,j,k) = edge_flux_temp(i,j,k) - (1.0-ddx)*(1.0-ddy)*(model%dz/model%dt/real(model%Npc))
                                    edge_flux_temp(i+1,j,k) = edge_flux_temp(i+1,j,k) - (ddx)*(1.0-ddy)*(model%dz/model%dt/real(model%Npc))
                                    edge_flux_temp(i,j,k+1) = edge_flux_temp(i,j,k+1) - (1.0-ddx)*(ddy)*(model%dz/model%dt/real(model%Npc))
                                    edge_flux_temp(i+1,j,k+1) = edge_flux_temp(i+1,j,k+1) - ddx*ddy*(model%dz/model%dt/real(model%Npc))

                                    ! add the work function - this energy is lost from the surface due to replacement eletron
                                    if (model%use_surface_heat_tweaks) then
                                        E_tot = (ux(no_part)*ux(no_part) +uy(no_part)*uy(no_part) + uz(no_part)*uz(no_part) + 2*therm_Wf_norm/m)*model%dz/model%dt/real(model%Npc)*m*0.5
                                    else
                                        E_tot = (ux(no_part)*ux(no_part) +uy(no_part)*uy(no_part) + uz(no_part)*uz(no_part))*model%dz/model%dt/real(model%Npc)*m*0.5

                                    end if
                                    edge_energy_flux_temp(i,j,k) = edge_energy_flux_temp(i,j,k) - E_tot *(1.0-ddx)*(1.0-ddy)
                                    edge_energy_flux_temp(i+1,j,k) = edge_energy_flux_temp(i+1,j,k) - E_tot*(ddx)*(1.0-ddy)
                                    edge_energy_flux_temp(i,j,k+1) = edge_energy_flux_temp(i,j,k+1) - E_tot*(1.0-ddx)*(ddy)
                                    edge_energy_flux_temp(i+1,j,k+1) = edge_energy_flux_temp(i+1,j,k+1) - E_tot*ddx*ddy
                                end if
                                act_inj = act_inj - 1.0
                            end if ! rndx

                            do_loop = do_loop - 1
                        end do ! particle loop

                    end if ! check if there's a flat surface
                end if ! check for Ts > 0
            end do
        end do
    end do

    ! third loop checks for surfaces in the y-z plane
    do i=1,(Nx-1)
        do j=1,(Ny-1)
            do k=2,(Nz-1)
                if (Ts(i,j,k).gt.0.0.and.edges(i,j,k).gt.0.0) then
                    ! this is a promissing cell,let's determine the orientation
                    ! this loops deals with surfaces oriented in x direction
                    if (edges(i,j,k).gt.0.0.and.edges(i,j+1,k).gt.0.0.and.edges(i,j,k+1).gt.0.0.and.edges(i,j+1,k+1).gt.0.0) then
                        ! next determine the orientation
                        if (i.eq.1) then
                            orientation  = 1
                        elseif (objects(i-1,j,k)*objects(i-1,j+1,k)*objects(i-1,j,k+1)*objects(i-1,j+1,k+1).gt.0.0) then
                            orientation  = 1
                        else
                            orientation = 2
                        end if

                        ! now get the mean Ts
                        mean_Ts = (Ts(i,j,k) + Ts(i,j+1,k) + Ts(i,j,k+1) + Ts(i,j+1,k+1))/4.0
                        Ts_norm = mean_Ts/11600/model%mks_Te
                        ! get the number of particles to be injected

                        ! following the richardson's formula j = A*T^2*exp(-Wf/kT)
                        ! A = 60E4 A/m2/K2 effective Richardson constant
                        ! Wf = 4.55 eV based on experiment for tungsten
                        !      write(*,*) 'Thermionic factor A*T^2*exp(-Wf/kT)',thermionic_A*mean_Ts**2*exp(-thermionic_Wf/(mean_Ts/11600))
                        act_inj=thermionic_A/1.602E-19*mean_Ts**2*exp(-thermionic_Wf/(mean_Ts/11600))*inj_ratio(sp)*dt*model%N0*model%dx*model%dy/(model%mks_n0*lambda_D*w_i) ! + inj_rem(i,j,k)
                        !write(*,*) 'Therm inj',act_inj,thermionic_A*mean_Ts**2*exp(-thermionic_Wf/(mean_Ts/11600))
                        !write(*,*) 'Injecting at',i,j,k,edges(i,j,k)
                        ! loop many times to inject given number of electrons
                        do_loop = int(act_inj) + 1
                        do while (do_loop.gt.0)
                            ! generate random number to check if we do inject this particle
                            call G05FAF(0.d0,1.d0,1,rndx)
                            if (rndx.lt.act_inj) then
                                ! let's inject new particle
                                no_part=no_part+1
                                pnumber(sp) = pnumber(sp) + 1
                                stype(no_part) = sp
                                objects_current(objects(i,j,k),sp) = objects_current(objects(i,j,k),sp) - q
                                ! positions
                                call G05FAF(0.d0,1.d0,1,rndx)
                                if (orientation.eq.1) then
                                    x(no_part) =  (float(i)-1.0)*model%dx + init_disp*(1.0 + rndx)
                                else
                                    x(no_part) =  (float(i)-1.0)*model%dx - init_disp*(1.0 + rndx)
                                end if
                                call G05FAF(0.d0,1.d0,1,ddx)
                                y(no_part) = (float(j)-1.0)*model%dy + ddx*model%dy
                                call G05FAF(0.d0,1.d0,1,ddy)
                                z(no_part) = (float(k) - 1.0)*model%dz + ddy*model%dz
                                ! velocities - temperature depends on the local surface temperature
                                call G05FDF(0.d0,1.d0,1,rndxn)
                                uy(no_part)=rndxn*utherm*sqrt(Ts_norm)
                                call G05FDF(0.d0,1.d0,1,rndxn)
                                uz(no_part)=rndxn*utherm*sqrt(Ts_norm)
                                call VELOCITY(u_z,ufu_z,N_u_z,Xu_z,upar1) !arbitrary normalized velocity function
                                if (orientation.eq.1) then
                                    ux(no_part)=-upar1*sqrt(Ts_norm)
                                else
                                    ux(no_part)=upar1*sqrt(Ts_norm)
                                end if
                                ! diagnostics
                                if (c_act.gt.model%Na) then
                                    edge_flux_temp(i,j,k) = edge_flux_temp(i,j,k) - (1.0-ddx)*(1.0-ddy)*(model%dz/model%dt/real(model%Npc))
                                    edge_flux_temp(i,j+1,k) = edge_flux_temp(i,j+1,k) - (ddx)*(1.0-ddy)*(model%dz/model%dt/real(model%Npc))
                                    edge_flux_temp(i,j,k+1) = edge_flux_temp(i,j,k+1) - (1.0-ddx)*(ddy)*(model%dz/model%dt/real(model%Npc))
                                    edge_flux_temp(i,j+1,k+1) = edge_flux_temp(i,j+1,k+1) - ddx*ddy*(model%dz/model%dt/real(model%Npc))

                                    ! add the work function - this energy is lost from the surface due to replacement eletron
                                    if (model%use_surface_heat_tweaks) then
                                        E_tot = (ux(no_part)*ux(no_part) +uy(no_part)*uy(no_part) + uz(no_part)*uz(no_part) + 2*therm_Wf_norm/m)*model%dz/model%dt/real(model%Npc)*m*0.5
                                    else
                                        E_tot = (ux(no_part)*ux(no_part) +uy(no_part)*uy(no_part) + uz(no_part)*uz(no_part))*model%dz/model%dt/real(model%Npc)*m*0.5

                                    end if
                                    edge_energy_flux_temp(i,j,k) = edge_energy_flux_temp(i,j,k) - E_tot *(1.0-ddx)*(1.0-ddy)
                                    edge_energy_flux_temp(i,j+1,k) = edge_energy_flux_temp(i,j+1,k) - E_tot*(ddx)*(1.0-ddy)
                                    edge_energy_flux_temp(i,j,k+1) = edge_energy_flux_temp(i,j,k+1) - E_tot*(1.0-ddx)*(ddy)
                                    edge_energy_flux_temp(i,j+1,k+1) = edge_energy_flux_temp(i,j+1,k+1) - E_tot*ddx*ddy
                                end if
                                act_inj = act_inj - 1.0
                            end if ! rndx

                            do_loop = do_loop - 1
                        end do ! particle loop

                    end if ! check if there's a flat surface
                end if ! check for Ts > 0
            end do
        end do
    end do






    ! fix the borders
    edge_flux_temp(1,1:Ny,:) = edge_flux_temp(1,1:Ny,:) +  edge_flux_temp(Nx,1:Ny,:)
    edge_flux_temp(Nx,1:Ny,:) = edge_flux_temp(1,1:Ny,:)
    edge_flux_temp(1:Nx,1,:) = edge_flux_temp(1:Nx,Ny,:) +  edge_flux_temp(1:Nx,1,:)
    edge_flux_temp(1:Nx,Ny,:) = edge_flux_temp(1:Nx,1,:)
    edge_flux = edge_flux + edge_flux_temp

    edge_energy_flux_temp(1,1:Ny,:) = edge_energy_flux_temp(1,1:Ny,:) +  edge_energy_flux_temp(Nx,1:Ny,:)
    edge_energy_flux_temp(Nx,1:Ny,:) = edge_energy_flux_temp(1,1:Ny,:)
    edge_energy_flux_temp(1:Nx,1,:) = edge_energy_flux_temp(1:Nx,Ny,:) +  edge_energy_flux_temp(1:Nx,1,:)
    edge_energy_flux_temp(1:Nx,Ny,:) = edge_energy_flux_temp(1:Nx,1,:)
    edge_energy_flux = edge_energy_flux + edge_energy_flux_temp


end subroutine



subroutine inject_top(Ninj,no_part,no_part_fin,x,z,y,ux,uy,uz,mono,utherm,N_u_z,u_z,ufu_z,&
        Xu_z,Ax_hlf,Bx_hlf,Cx_hlf,Ay_hlf,By_hlf,Cy_hlf,Az_hlf,Bz_hlf,Cz_hlf,Lzi,stype,sp,m,q,Umono,T,&
        spec_params,no_species,model,slices,no_slices,pnumber,Lz_limit)
    !use IFPORT
    implicit none
    !INJECT new ions from plasma side (to replace the outflux)
    !=========================================================
    include "struct.h"
    integer:: i,Ninj,no_part,no_part_fin,sp,no_species,no_supra_loops,k,no_slices
    type(pinfo), dimension(no_species):: spec_params
    integer, dimension(no_species) :: pnumber
    real:: Ninj_r,Ninj_fx,Umono,Lz_limit
    real:: bx,by,bz,first_push
    real, dimension(no_part_fin):: x,z,y,ux,uy,uz,old_x,old_y,old_z
    integer, dimension(no_part_fin):: stype
    logical:: mono
    real:: Lx,Lz,Ly,dt,utherm,sqbx2by2,midLz,m,q,T,Lzi
    integer:: N_u_z
    real*8, dimension(N_u_z):: u_z,ufu_z,Xu_z
    real:: Ax_hlf,Bx_hlf,Cx_hlf,Ay_hlf,By_hlf,Cy_hlf,Az_hlf,Bz_hlf,Cz_hlf
    real*8:: upar1,uperp1,uperp2,ux_temp,uy_temp,uz_temp
    real*8:: rndx, rndxn
    real:: Lx_start,Ly_start,Lz_start,Lx_total,Ly_total,Lz_total
    type(model_t):: model
    type(slice_info),dimension(no_slices):: slices
    real:: uz_sum
    integer:: uz_count,discarded_count
    ! write(*,*) 'Injecting', Ninj, 'particles'
    !Generate a velocity distribution for the ions
    rndxn = 0.0
    rndx = 0.0
    bx = model%bx
    by = model%by
    bz = model%bz
    Lx = model%Lx
    Ly = model%Ly
    Lz = model%Lz
    Lx_start = model%Lx_start
    Ly_start = model%Ly_start
    Lz_start = model%Lz_start
    Lx_total = model%Lx_total
    Ly_total = model%Ly_total
    Lz_total = model%Lz_total
    uz_sum  = 0.0
    uz_count = 0
    discarded_count =0
    dt = model%dt
    sqbx2by2=sqrt(bx**2+by**2)
    midLz = (model%Lz_grid + model%Lz_max)/2.0
    ! write(*,*) 'Injection midplane ',midLz,Lz_limit,model%Lz_max
    ! write(*,*) model%proc_no,'Ninj',Ninj
    do i=1,Ninj
        no_part=no_part+1
        !MK 14/08/06
        !now is the time to initalize particle
        z(no_part)=0.0       !toroidal position (the initial position is defined in the injection module)
        y(no_part)=0.0       !radial position (the initial position is defined in the injection module)
        x(no_part)=0.0       !radial position (the initial position is defined in the injection module)

        uz(no_part)=0.0      !parallel speed
        uy(no_part)=0.0      !radial speed
        ux(no_part)=0.0      !poloidal speed
        stype(no_part) = sp
        !=====================================OLD METHOD============================================
        if (bx.eq.0.0.AND.by.eq.0.0) then          ! Bz = z-axis !!! SO WE USE OLD METHOD!
            !in z-direction
            !--------------
            if (mono) then          !NEW RD
                upar1=Umono
                upar1 = -upar1
            else
                call VELOCITY(u_z,ufu_z,N_u_z,Xu_z,upar1) !arbitrary normalized velocity function
            endif

            if (spec_params(stype(no_part))%motion_method.eq.1) then
                ! gyrokinetics!!!
                ux(no_part) = -upar1
                uy(no_part)  = 0.0
                uz(no_part) = 0.0
                !         write(*,*) 'Old method gyro',sp,ux(no_part)
                call G05FAF(0.d0,1.d0,1,rndx)
                z(no_part)=model%Lz_grid - abs(ux(no_part))*dt*rndx               !uniform distribution in space
                !         write(*,*) 'Old method gyro',sp,ux(no_part),z(no_part)

            else
                ! regular motion

                !! TEMPERATURE HACK - should have been here before???
                uz(no_part) =upar1
                call G05FAF(0.d0,1.d0,1,rndx)
                z(no_part)=midLz+uz(no_part)*dt*rndx               !uniform distribution in space
                !in y-direction
                !--------------
                call G05FDF(0.d0,1.d0,1,rndxn)
                !     write(*,*) 'Deb',rndxn,utherm
                uy(no_part)=rndxn*utherm
                call G05FDF(0.d0,1.d0,1,rndxn)
                ux(no_part)=rndxn*utherm


            end if ! gyrokinetics

            call G05FAF(0.d0,1.d0,1,rndx)
            y(no_part)=rndx*Ly_total                           !uniform distribution in space
            !in x-direction
            call G05FAF(0.d0,1.d0,1,rndx)
            x(no_part)=rndx*Lx_total                           !uniform distribution in space
        else
            !=======================================NEW METHOD====================================
            if (mono) then
                upar1=Umono
            else
                call VELOCITY(u_z,ufu_z,N_u_z,Xu_z,upar1) !arbitrary normalized velocity function
            endif
            upar1 = -upar1
            ! 	write(*,*) 'Upar1',sp,upar1

            if (spec_params(stype(no_part))%motion_method.eq.1) then
                !           upar1 = -5.0
                !           uperp1 = 0.0
                !           uperp2 = 0.0

                ux(no_part) = upar1
                uy(no_part) = 0.0
                uz(no_part) = 0.0

                call G05FAF(0.d0,1.d0,1,rndx)
                z(no_part) =midLz+upar1*dt*rndx*bz

                call G05FAF(0.d0,1.d0,1,rndx)
                y(no_part) = Ly_total*rndx

                call G05FAF(0.d0,1.d0,1,rndx)
                x(no_part) = Lx_total*rndx

            else
                call G05FDF(0.d0,1.d0,1,rndxn)
                uperp1=rndxn*utherm                 !Maxwellian distribution
                call G05FDF(0.d0,1.d0,1,rndxn)
                uperp2=rndxn*utherm                 !Maxwellian distribution

                uz(no_part)=upar1*bz+uperp2*sqbx2by2

                call G05FAF(0.d0,1.d0,1,rndx)
                z(no_part)=midLz+upar1*dt*rndx*bz+q*uperp1*m*sqbx2by2   !+uperp1i is ok for the ions!!
                uy(no_part)=upar1*by+uperp1*bx/sqbx2by2-uperp2*by*bz/sqbx2by2

                call G05FAF(0.d0,1.d0,1,rndx)
                y(no_part)=rndx*Ly_total
                call G05FAF(0.d0,1.d0,1,rndx)
                x(no_part)=rndx*Lx_total

                ux(no_part)=upar1*bx-uperp1*by/sqbx2by2-uperp2*bx*bz/sqbx2by2

            end if ! gyrokinetics

            !==================================================================================
        endif   !if (bx.eq.0.d0.AND.by.eq.0.d0) then


        if (spec_params(stype(no_part))%motion_method.eq.1) then
            ! no windback for gyroparticles - moving along straight line in the injection box!


        else
            !1/2 wind back for next L.F
            !--------------------------
            ux_temp=ux(no_part)*Ax_hlf + uy(no_part)*Bx_hlf + uz(no_part)*Cx_hlf

            uy_temp=ux(no_part)*Ay_hlf + uy(no_part)*By_hlf + uz(no_part)*Cy_hlf

            uz_temp=ux(no_part)*Az_hlf + uy(no_part)*Bz_hlf + uz(no_part)*Cz_hlf

            ux(no_part)=ux_temp
            uy(no_part)=uy_temp
            uz(no_part)=uz_temp
        end if
        ! if (sp.eq.2) then
        ! 	write(*,*) "Dux",sp,ux(no_part)
        ! 	write(*,*) "Duy",sp,uy(no_part)
        ! 	write(*,*) "Duz",sp,uz(no_part)
        !  	write(*,*) "Pos",y(no_part), z(no_part)

        ! end if
        ! if (model%debug) then
        !     write(*,*) 'Pos:',x(no_part),y(no_part),z(no_part)
        !     write(*,*) 'Vel:',ux(no_part),uy(no_part),uz(no_part)
        !
        ! end if
        ! if (isnan(ux(no_part))) then
        ! write(*,*) 'Injection error!'
        ! 	write(*,*) 'pos',x(no_part),y(no_part),z(no_part)
        ! 	write(*,*) 'vel',ux(no_part),uy(no_part),uz(no_part)
        !
        ! stop
        ! end if
        ! domain decomposition check
        !write(*,*) 'Decomp',Lz_start,model%Lz_max
        ! this will have to be fixed in future
        if (x(no_part).gt.Lx_start.and.x(no_part).lt.(Lx_start + Lx).and.y(no_part).gt.Ly_start.and.y(no_part).lt.(Ly_start + Ly).and.z(no_part).gt.Lz_start.and.z(no_part).lt.slices(model%proc_no+1)%Lz_stop) then
            !if (model%debug) then
            !    write(*,*) 'Pos',sp,x(no_part),y(no_part),z(no_part)
            !end if
            ! ok we have this particle
            x(no_part) = x(no_part) - Lx_start
            y(no_part) = y(no_part) - Ly_start
            z(no_part) = z(no_part) - Lz_start
            pnumber(sp) = pnumber(sp) + 1

            !       if (stype(no_part).eq.1) then
            ! 	uz_sum = uz_sum + z(no_part)
            ! 	uz_count= uz_count +1
            !       end if
        else

            ! if (z(no_part).gt.(Lz_start + Lz)) then
            !       write(*,*) 'Discarding particle',x(no_part),y(no_part),z(no_part)
            !       write(*,*) 'X range',Lx_start,Lx_start + Lx
            !       write(*,*) 'Y range',Ly_start,Ly_start + Ly
            !       write(*,*) 'Z range',Lz_start,Lz_start + Lz
            ! ! stop
            ! end if
            ! out of sight
            no_part = no_part -1
            discarded_count  = discarded_count +1
        end if

    enddo
    ! write(*,*) model%proc_no,'Inject discarded',discarded_count
    ! write(*,*) model%proc_no,'Inject XR',Lx_start,Lx_start + Lx
    ! write(*,*) model%proc_no,'Inject YR',Lx_start,Ly_start + Ly
    ! write(*,*) model%proc_no,'Inject ZR',Lz_start,slices(model%proc_no+1)%Lz_stop
    ! write(*,*) model%proc_no,'Inject TOTAL',Lx_total,Ly_total,midLz


    !write(*,*) model%proc_no,'Inject zlimits',Lz_start,slices(model%proc_no+1)%Lz_stop
    ! if (stype(no_part).eq.1) then
    ! write(*,*) model%proc_no,'Average uz',uz_sum/float(uz_count),uz_count
    ! write(*,*) model%proc_no, 'X',Lx_start,Lx_start+Lx
    ! write(*,*) model%proc_no, 'Y',Ly_start,Ly_start+Ly
    ! write(*,*) model%proc_no, 'Z',Lz_start,Lz_start+Lz
    ! end if
    ! debug stat
    ! write(*,*) 'Inject stats ux:', sum(ux(no_part - Ninj:no_part))/real(Ninj)
    ! write(*,*) 'Inject stats uy:', sum(uy(no_part - Ninj:no_part))/real(Ninj)
    ! write(*,*) 'Inject stats uz:', sum(uz(no_part - Ninj:no_part))/real(Ninj)
    ! write(*,*) 'Total particle count after inject',no_part
end subroutine

! injection of particles from the bottom boundary for the 2 sources scenarios

subroutine inject_bottom(Ninj,no_part,no_part_fin,x,z,y,ux,uy,uz,mono,utherm,N_u_z,u_z,ufu_z,&
        Xu_z,Ax_hlf,Bx_hlf,Cx_hlf,Ay_hlf,By_hlf,Cy_hlf,Az_hlf,Bz_hlf,Cz_hlf,Lzi,stype,sp,m,q,Umono,T,&
        spec_params,no_species,model,slices,no_slices,pnumber,Lz_limit)
    !use IFPORT
    implicit none
    !INJECT new ions from plasma side (to replace the outflux)
    !=========================================================
    include "struct.h"
    integer:: i,Ninj,no_part,no_part_fin,sp,no_species,no_supra_loops,k,no_slices
    type(pinfo), dimension(no_species):: spec_params
    integer, dimension(no_species) :: pnumber
    real:: Ninj_r,Ninj_fx,Umono,Lz_limit
    real:: bx,by,bz,first_push
    real, dimension(no_part_fin):: x,z,y,ux,uy,uz,old_x,old_y,old_z
    integer, dimension(no_part_fin):: stype
    logical:: mono
    real:: Lx,Lz,Ly,dt,utherm,sqbx2by2,midLz,m,q,T,Lzi
    integer:: N_u_z
    real*8, dimension(N_u_z):: u_z,ufu_z,Xu_z
    real:: Ax_hlf,Bx_hlf,Cx_hlf,Ay_hlf,By_hlf,Cy_hlf,Az_hlf,Bz_hlf,Cz_hlf
    real*8:: upar1,uperp1,uperp2,ux_temp,uy_temp,uz_temp
    real*8:: rndx, rndxn
    real:: Lx_start,Ly_start,Lz_start,Lx_total,Ly_total,Lz_total
    type(model_t):: model
    type(slice_info),dimension(no_slices):: slices
    real:: uz_sum
    integer:: uz_count,discarded_count
    ! write(*,*) 'Injecting', Ninj, 'particles'
    !Generate a velocity distribution for the ions
    rndxn = 0.0
    rndx = 0.0
    bx = model%bx
    by = model%by
    bz = model%bz
    Lx = model%Lx
    Ly = model%Ly
    Lz = model%Lz
    Lx_start = model%Lx_start
    Ly_start = model%Ly_start
    Lz_start = model%Lz_start
    Lx_total = model%Lx_total
    Ly_total = model%Ly_total
    Lz_total = model%Lz_total
    uz_sum  = 0.0
    uz_count = 0
    discarded_count =0
    dt = model%dt
    sqbx2by2=sqrt(bx**2+by**2)
    midLz = (model%Lz_grid + model%Lz_max)/2.0
    ! write(*,*) 'Injection midplane ',midLz,Lz_limit,model%Lz_max
    ! write(*,*) model%proc_no,'Ninj',Ninj
    do i=1,Ninj
        no_part=no_part+1
        !MK 14/08/06
        !now is the time to initalize particle
        z(no_part)=0.0       !toroidal position (the initial position is defined in the injection module)
        y(no_part)=0.0       !radial position (the initial position is defined in the injection module)
        x(no_part)=0.0       !radial position (the initial position is defined in the injection module)

        uz(no_part)=0.0      !parallel speed
        uy(no_part)=0.0      !radial speed
        ux(no_part)=0.0      !poloidal speed
        stype(no_part) = sp
        !=====================================OLD METHOD============================================
        if (bx.eq.0.0.AND.by.eq.0.0) then          ! Bz = z-axis !!! SO WE USE OLD METHOD!
            !in z-direction
            !--------------
            if (mono) then          !NEW RD
                upar1=Umono
                upar1 = -upar1
            else
                call VELOCITY(u_z,ufu_z,N_u_z,Xu_z,upar1) !arbitrary normalized velocity function
            endif

            if (spec_params(stype(no_part))%motion_method.eq.1) then
                ! gyrokinetics!!!
                ux(no_part) = upar1
                uy(no_part)  = 0.0
                uz(no_part) = 0.0
                !         write(*,*) 'Old method gyro',sp,ux(no_part)
                call G05FAF(0.d0,1.d0,1,rndx)
                z(no_part)=abs(ux(no_part))*dt*rndx               !uniform distribution in space
                !         write(*,*) 'Old method gyro',sp,ux(no_part),z(no_part)

            else
                ! regular motion

                !! TEMPERATURE HACK - should have been here before???
                uz(no_part) =-upar1
                call G05FAF(0.d0,1.d0,1,rndx)
                z(no_part)=uz(no_part)*dt*rndx               !uniform distribution in space
                !in y-direction
                !--------------
                call G05FDF(0.d0,1.d0,1,rndxn)
                !     write(*,*) 'Deb',rndxn,utherm
                uy(no_part)=rndxn*utherm
                call G05FDF(0.d0,1.d0,1,rndxn)
                ux(no_part)=rndxn*utherm


            end if ! gyrokinetics

            call G05FAF(0.d0,1.d0,1,rndx)
            y(no_part)=rndx*Ly_total                           !uniform distribution in space
            !in x-direction
            call G05FAF(0.d0,1.d0,1,rndx)
            x(no_part)=rndx*Lx_total                           !uniform distribution in space
        else
            !=======================================NEW METHOD====================================
            if (mono) then
                upar1=Umono
            else
                call VELOCITY(u_z,ufu_z,N_u_z,Xu_z,upar1) !arbitrary normalized velocity function
            endif
            !    upar1 = -upar1
            ! 	write(*,*) 'Upar1',sp,upar1

            if (spec_params(stype(no_part))%motion_method.eq.1) then
                !           upar1 = -5.0
                !           uperp1 = 0.0
                !           uperp2 = 0.0

                ux(no_part) = upar1
                uy(no_part) = 0.0
                uz(no_part) = 0.0

                call G05FAF(0.d0,1.d0,1,rndx)
                z(no_part) =Lz_total -midLz+upar1*dt*rndx*bz

                call G05FAF(0.d0,1.d0,1,rndx)
                y(no_part) = Ly_total*rndx

                call G05FAF(0.d0,1.d0,1,rndx)
                x(no_part) = Lx_total*rndx

            else
                call G05FDF(0.d0,1.d0,1,rndxn)
                uperp1=rndxn*utherm                 !Maxwellian distribution
                call G05FDF(0.d0,1.d0,1,rndxn)
                uperp2=rndxn*utherm                 !Maxwellian distribution

                uz(no_part)=upar1*bz+uperp2*sqbx2by2

                call G05FAF(0.d0,1.d0,1,rndx)
                z(no_part)=Lz_total - midLz+upar1*dt*rndx*bz+q*uperp1*m*sqbx2by2   !+uperp1i is ok for the ions!!
                uy(no_part)=upar1*by+uperp1*bx/sqbx2by2-uperp2*by*bz/sqbx2by2

                call G05FAF(0.d0,1.d0,1,rndx)
                y(no_part)=rndx*Ly_total
                call G05FAF(0.d0,1.d0,1,rndx)
                x(no_part)=rndx*Lx_total

                ux(no_part)=upar1*bx-uperp1*by/sqbx2by2-uperp2*bx*bz/sqbx2by2

            end if ! gyrokinetics

            !==================================================================================
        endif   !if (bx.eq.0.d0.AND.by.eq.0.d0) then


        if (spec_params(stype(no_part))%motion_method.eq.1) then
            ! no windback for gyroparticles - moving along straight line in the injection box!


        else
            !1/2 wind back for next L.F
            !--------------------------
            ux_temp=ux(no_part)*Ax_hlf + uy(no_part)*Bx_hlf + uz(no_part)*Cx_hlf

            uy_temp=ux(no_part)*Ay_hlf + uy(no_part)*By_hlf + uz(no_part)*Cy_hlf

            uz_temp=ux(no_part)*Az_hlf + uy(no_part)*Bz_hlf + uz(no_part)*Cz_hlf

            ux(no_part)=ux_temp
            uy(no_part)=uy_temp
            uz(no_part)=uz_temp
        end if
        ! if (sp.eq.2) then
        ! 	write(*,*) "Dux",sp,ux(no_part)
        ! 	write(*,*) "Duy",sp,uy(no_part)
        ! 	write(*,*) "Duz",sp,uz(no_part)
        !  	write(*,*) "Pos",y(no_part), z(no_part)

        ! end if
        ! if (model%debug) then
        !     write(*,*) 'Pos:',x(no_part),y(no_part),z(no_part)
        !     write(*,*) 'Vel:',ux(no_part),uy(no_part),uz(no_part)
        !
        ! end if
        ! if (isnan(ux(no_part))) then
        ! write(*,*) 'Injection error!'
        ! 	write(*,*) 'pos',x(no_part),y(no_part),z(no_part)
        ! 	write(*,*) 'vel',ux(no_part),uy(no_part),uz(no_part)
        !
        ! stop
        ! end if
        ! domain decomposition check
        !write(*,*) 'Decomp',Lz_start,model%Lz_max
        ! this will have to be fixed in future
        if (x(no_part).gt.Lx_start.and.x(no_part).lt.(Lx_start + Lx).and.y(no_part).gt.Ly_start.and.y(no_part).lt.(Ly_start + Ly).and.z(no_part).gt.Lz_start.and.z(no_part).lt.slices(model%proc_no+1)%Lz_stop) then
            !write(*,*) 'Pos',sp,x(no_part),y(no_part),z(no_part)

            ! ok we have this particle
            x(no_part) = x(no_part) - Lx_start
            y(no_part) = y(no_part) - Ly_start
            z(no_part) = z(no_part) - Lz_start
            pnumber(sp) = pnumber(sp) + 1

            !       if (stype(no_part).eq.1) then
            ! 	uz_sum = uz_sum + z(no_part)
            ! 	uz_count= uz_count +1
            !       end if
        else

            ! if (z(no_part).gt.(Lz_start + Lz)) then
            !       write(*,*) 'Discarding particle',x(no_part),y(no_part),z(no_part)
            !       write(*,*) 'X range',Lx_start,Lx_start + Lx
            !       write(*,*) 'Y range',Ly_start,Ly_start + Ly
            !       write(*,*) 'Z range',Lz_start,Lz_start + Lz
            ! ! stop
            ! end if
            ! out of sight
            no_part = no_part -1
            discarded_count  = discarded_count +1
        end if

    enddo
    ! write(*,*) model%proc_no,'Inject discarded',discarded_count
    ! write(*,*) model%proc_no,'Inject XR',Lx_start,Lx_start + Lx
    ! write(*,*) model%proc_no,'Inject YR',Lx_start,Ly_start + Ly
    ! write(*,*) model%proc_no,'Inject ZR',Lz_start,slices(model%proc_no+1)%Lz_stop
    ! write(*,*) model%proc_no,'Inject TOTAL',Lx_total,Ly_total,midLz


    !write(*,*) model%proc_no,'Inject zlimits',Lz_start,slices(model%proc_no+1)%Lz_stop
    ! if (stype(no_part).eq.1) then
    ! write(*,*) model%proc_no,'Average uz',uz_sum/float(uz_count),uz_count
    ! write(*,*) model%proc_no, 'X',Lx_start,Lx_start+Lx
    ! write(*,*) model%proc_no, 'Y',Ly_start,Ly_start+Ly
    ! write(*,*) model%proc_no, 'Z',Lz_start,Lz_start+Lz
    ! end if
    ! debug stat
    ! write(*,*) 'Inject stats ux:', sum(ux(no_part - Ninj:no_part))/real(Ninj)
    ! write(*,*) 'Inject stats uy:', sum(uy(no_part - Ninj:no_part))/real(Ninj)
    ! write(*,*) 'Inject stats uz:', sum(uz(no_part - Ninj:no_part))/real(Ninj)
    ! write(*,*) 'Total particle count after inject',no_part
end subroutine

! injection of particles from the bottom boundary for the 2 sources scenarios





