#!/bin/bash
# compile script for SPICE3
# to determine double precision add
# -r8 on the ifort

source ./config/config.sh

#set -o verbose

OUT_DIR=$SPICE3_INSTALL_DIR/bin
SRC_DIR=$SPICE3_INSTALL_DIR/src
LIB_DIR=$SPICE3_INSTALL_DIR/lib
mkdir -p $OUT_DIR


FC_FLAGS="-O3 -axAVX -qoverride-limits -r8 -qopenmp"
OUT_FILE="spice3-0.5.bin"
STITCHER_OUT_FILE="stitcher.bin"

S3_MOD_FILES="mth.f90 getoptions.f90"
S3_SRC_FILES="pic-0.5.f90 mth.o getoptions.o grid.f90 inject.f90 leapfrog.f90 matfile.f90 mpicom.f90 gan.f90"
S3_STITCHER_SRC_FILES="stitcher.f90 mth.o getoptions.o"

S3_LIB_CLOCK=$LIB_DIR/clock/clock/lib/libclock.a
S3_LIB_INTEL="-lintlc -lifcore -lifport"
S3_LIB_MATIO="$LIB_DIR/matio/matio-1.3.4/src/.libs/libmatio.a -I$LIB_DIR/matio/matio-1.3.4/src -lz"
S3_LIB_UMFPACK="$LIB_DIR/UF/UMFPACK/Lib/libumfpack.a $LIB_DIR/UF/AMD/Lib/libamd.a $SPICE3_BLAS $SPICE3_LAPACK"
S3_LIB_SOLVER="$LIB_DIR/solver/wrapper/wrapper.o $LIB_DIR/solver/lib/libpoissolver.a $S3_LIB_UMFPACK -lstdc++"

cd $SRC_DIR

for MOD_FILE in $S3_MOD_FILES; do
    CMD="$SPICE3_MPIF $FC_FLAGS -c $MOD_FILE"
    echo $CMD
    eval $CMD
done

CMD="$SPICE3_MPIF $FC_FLAGS -o $OUT_DIR/$OUT_FILE $S3_SRC_FILES $S3_LIB_CLOCK $S3_LIB_INTEL $S3_LIB_MATIO $S3_LIB_SOLVER"
echo $CMD
eval $CMD

CMD="$SPICE3_MPIF -g -C -r8 -qoverride-limits -o $OUT_DIR/$STITCHER_OUT_FILE $S3_STITCHER_SRC_FILES $S3_LIB_CLOCK $S3_LIB_MATIO"
echo $CMD
eval $CMD
